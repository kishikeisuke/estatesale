<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

$wpthk_defaults = array();

/*---------------------------------------------------------------------------
 * サムネイル追加
 * メディア画面でサイズ選択できるようにしたバージョン
 *---------------------------------------------------------------------------*/
add_action( 'after_setup_theme', function() {
	global $my_custom_image_sizes;

	$my_custom_image_sizes = array(
		'thumb75' => array(
			'name'       => __( 'Micro thumbnail', 'wpthk' ),
			'width'      => 75,
			'height'     => 75,
			'crop'       => true,
			'selectable' => false
		),
		'thumb100' => array(
			'name'       => __( 'Small thumbnail', 'wpthk' ),
			'width'      => 100,
			'height'     => 100,
			'crop'       => true,
			'selectable' => false
		),
		'thumb320' => array(
			'name'       => __( 'Horizontal thumbnail', 'wpthk' ),	// 選択肢のラベル名
			'width'      => 320,         	// 最大画像幅
			'height'     => 180,         	// 最大画像高さ
			'crop'       => true,        	// 切り抜きを行うかどうか
			'selectable' => true         	// 選択肢に含めるかどうか
		),
		'thumb530' => array(
			'name'       => __( 'Small size', 'wpthk' ) . '(' . __( 'Side bar with 336 px', 'wpthk' ) . ')',
			'width'      => 530,
			'height'     => 530,
			'crop'       => false,
			'selectable' => true
		),
		'thumb565' => array(
			'name'       => __( 'Small size', 'wpthk' ) . '(' . __( 'Side bar with 300 px', 'wpthk' ) . ')',
			'width'      => 565,
			'height'     => 565,
			'crop'       => false,
			'selectable' => true
		),
		'thumb710' => array(
			'name'       => __( 'Large size', 'wpthk' ) . '(' . __( 'Side bar with 336 px', 'wpthk' ) . ')',
			'width'      => 710,
			'height'     => 710,
			'crop'       => false,
			'selectable' => true
		),
		'thumb725' => array(
			'name'       => __( 'Large size', 'wpthk' ) . '(' . __( 'Side bar with 300 px', 'wpthk' ) . ')',
			'width'      => 725,
			'height'     => 725,
			'crop'       => false,
			'selectable' => true
		),
	);

	$w = (int)get_theme_mod( 'thumbnail_width', 150 );
	$h = (int)get_theme_mod( 'thumbnail_height', 150 );
	if( $w >= 0 && $h >= 0 ) {
		if( $w !== 150 || $h !== 150 ) {
			$my_custom_image_sizes += array(
				'thumb' . $w . 'x' . $h => array(
					'name'       => __( 'User thumbnail', 'wpthk' ),
					'width'      => $w,
					'height'     => $h,
					'crop'       => true,
					'selectable' => true
				),
			);

			$ws = (int)ceil( $w * 0.666 );
			$hs = (int)ceil( $h * 0.666 );

			if( $ws >= 0 && $hs >= 0 ) {
				$my_custom_image_sizes += array(
					'thumb' . $ws . 'x' . $hs => array(
						'name'       => __( 'User thumbnail small', 'wpthk' ),
						'width'      => $ws,
						'height'     => $hs,
						'crop'       => true,
						'selectable' => false
					),
				);
			}
		}
	}

	foreach( $my_custom_image_sizes as $slug => $size ) {
		add_image_size( $slug, $size['width'], $size['height'], $size['crop'] );
	}
} );

add_action( 'image_size_names_choose', function( $size_names ) {
	global $my_custom_image_sizes;
	$custom_sizes = get_intermediate_image_sizes();
	foreach ( $custom_sizes as $custom_size ) {
		if ( isset( $my_custom_image_sizes[$custom_size]['selectable'] ) && $my_custom_image_sizes[$custom_size]['selectable'] ) {
			$size_names[$custom_size] = $my_custom_image_sizes[$custom_size]['name'];
		}
	}
	return $size_names;
} );

/*---------------------------------------------------------------------------
 * WordPress 管理画面に「WpTHK」を追加する
 *---------------------------------------------------------------------------*/
add_action( 'admin_menu', function() {
	get_template_part( 'inc/customize' );

	$customize = new wpthk_customize();

	$linefeed = get_locale() === 'ja' ? ' ' : '<br />';

	wpthk_menu_page(
		'WpTHK ' . __( 'Customizer', 'wpthk' ),
		'WpTHK',
		'manage_options',
		'wpthk',
		array( $customize, 'wpthk_custom_form' ),
		'dashicons-layout',
		59
	);
	wpthk_submenu_page(
		'wpthk',
		'WpTHK' . __( 'Customizer', 'wpthk' ),
		__( 'Customize', 'wpthk' ),
		'manage_options',
		'wpthk',
		array( $customize, 'wpthk_custom_form' )
	);
	wpthk_submenu_page(
		'wpthk',
		'WpTHK Customize',
		__( 'Customize', 'wpthk' ) . $linefeed .'(' . __( 'Appearance', 'wpthk' ) . ')',
		'manage_options',
		'customize.php?return=' . urlencode( wp_unslash( $_SERVER['REQUEST_URI'] ) ) . '&amp;wpthk=custom',
		''
	);
	wpthk_submenu_page(
		'wpthk',
		'SNS ' . __( 'Counter', 'wpthk' ),
		'SNS ' . __( 'Counter', 'wpthk' ),
		'manage_options',
		'wpthk_sns',
		array( $customize, 'wpthk_custom_form' )
	);
	wpthk_submenu_page(
		'wpthk',
		__( 'Child Theme Editor', 'wpthk' ),
		__( 'Child Theme Editor', 'wpthk' ),
		'manage_options',
		'wpthk_edit',
		array( $customize, 'wpthk_custom_form' )
	);
} );

/*---------------------------------------------------------------------------
 * カスタマイズ内容の変更反映
 *---------------------------------------------------------------------------*/
add_action( 'admin_init', function() {
	global $wpthk, $wpthk_defaults;
	require_once( INC . 'const.php' );

	if(
		isset( $_GET['page'] ) && isset( $_POST['_wpnonce'] ) && isset( $_POST['option_page'] ) &&
		( $_GET['page'] === 'wpthk' || substr( $_GET['page'], 0, 6 ) === 'wpthk_' )
	) {
		// options.php を経由してないので、ここで nonce のチェック
		if( check_admin_referer( $_POST['option_page'] . '-options', '_wpnonce' ) ) {
			$conf = new defConfig();
			$wpthk_defaults = $conf->default_custom_variables();
			$err = false;

			if( $_POST['option_page'] === 'header' ) {
				// SEO
				thk_customize_result_set( 'canonical_enable', 'checkbox' );
				thk_customize_result_set( 'next_prev_enable', 'checkbox' );
				thk_customize_result_set( 'rss_feed_enable', 'checkbox' );
				thk_customize_result_set( 'atom_feed_enable', 'checkbox' );
				thk_customize_result_set( 'top_description', 'text' );
				thk_customize_result_set( 'meta_keywords', 'radio' );
				thk_customize_result_set( 'nextpage_index', 'checkbox' );
				// OGP
				thk_customize_result_set( 'facebook_ogp_enable', 'checkbox' );
				thk_customize_result_set( 'facebook_admin', 'text' );
				thk_customize_result_set( 'facebook_app_id', 'text' );
				thk_customize_result_set( 'twitter_card_enable', 'checkbox' );
				thk_customize_result_set( 'twitter_card_type', 'select' );
				thk_customize_result_set( 'twitter_id', 'text' );
			}
			elseif( $_POST['option_page'] === 'title' ) {
				// Title
				thk_customize_result_set( 'title_sep', 'radio' );
				thk_customize_result_set( 'title_top_list', 'radio' );
				thk_customize_result_set( 'title_front_page', 'radio' );
				thk_customize_result_set( 'title_other', 'radio' );
			}
			elseif( $_POST['option_page'] === 'optimize' ) {
				// HTML
				thk_customize_result_set( 'html_compress', 'select' );
				thk_customize_result_set( 'child_css_compress', 'select' );
				thk_customize_result_set( 'child_js_compress', 'select' );
				thk_customize_result_set( 'child_js_file_1', 'text' );
				thk_customize_result_set( 'child_js_file_2', 'text' );
				thk_customize_result_set( 'child_js_file_3', 'text' );
			}
			elseif( $_POST['option_page'] === 'style' ) {
				// Mode select
				thk_customize_result_set( 'wpthk_mode_select', 'radio' );
				// Inline Style
				thk_customize_result_set( 'css_to_style', 'checkbox' );
				// Child CSS
				thk_customize_result_set( 'child_css', 'checkbox' );
				// Font Awesome
				thk_customize_result_set( 'awesome_css_type', 'radio' );
				thk_customize_result_set( 'awesome_load', 'select' );
				// Widget CSS
				thk_customize_result_set( 'css_search', 'checkbox' );
				thk_customize_result_set( 'css_archive', 'checkbox' );
				thk_customize_result_set( 'css_calendar', 'checkbox' );
				thk_customize_result_set( 'css_new_post', 'checkbox' );
				thk_customize_result_set( 'css_rcomments', 'checkbox' );
				thk_customize_result_set( 'css_follow_button', 'checkbox' );
				thk_customize_result_set( 'css_rss_feedly', 'checkbox' );
				thk_customize_result_set( 'css_qr_code', 'checkbox' );
			}
			elseif( $_POST['option_page'] === 'script' ) {
				// jQuery
				thk_customize_result_set( 'jquery_load', 'checkbox' );
				// jQuery defer
				thk_customize_result_set( 'jquery_defer', 'checkbox' );
				// Bootstrap Plugin
				thk_customize_result_set( 'bootstrap_js_load_type', 'select' );
				// Other Javascript
				thk_customize_result_set( 'jquery_migrate', 'radio' );
				thk_customize_result_set( 'child_script', 'checkbox' );
				thk_customize_result_set( 'html5shiv_load_type', 'checkbox' );
				thk_customize_result_set( 'respondjs_load_type', 'checkbox' );
				thk_customize_result_set( 'thk_emoji_disable', 'checkbox' );
				thk_customize_result_set( 'thk_embed_disable', 'checkbox' );
			}
			elseif( $_POST['option_page'] === 'search' ) {
				thk_customize_result_set( 'search_extract', 'radio' );
				thk_customize_result_set( 'search_match_method', 'radio' );
				thk_customize_result_set( 'comment_search', 'checkbox' );
				thk_customize_result_set( 'search_highlight', 'checkbox' );
				thk_customize_result_set( 'highlight_bold', 'checkbox' );
				thk_customize_result_set( 'highlight_oblique', 'checkbox' );
				thk_customize_result_set( 'highlight_bg', 'checkbox' );
				thk_customize_result_set( 'highlight_bg_color', 'text' );
				thk_customize_result_set( 'search_extract_length', 'text' );
				thk_customize_result_set( 'highlight_radius', 'text' );
			}
			elseif( $_POST['option_page'] === 'adsense' ) {
				thk_customize_result_set( 'adsense_visible', 'checkbox' );
				thk_customize_result_set( 'page_adsense_visible', 'checkbox' );
				thk_customize_result_set( 'preview_adsense_visible', 'checkbox' );
			}
			elseif( $_POST['option_page'] === 'others' ) {
				thk_customize_result_set( 'buffering_enable', 'checkbox' );
				thk_customize_result_set( 'add_role_attribute', 'checkbox' );
				thk_customize_result_set( 'remove_hentry_class', 'checkbox' );
				thk_customize_result_set( 'enable_mb_slug', 'checkbox' );
				thk_customize_result_set( 'user_scalable', 'radio' );
				thk_customize_result_set( 'categories_a_inner', 'checkbox' );
				thk_customize_result_set( 'archives_a_inner', 'checkbox' );
				thk_customize_result_set( 'parent_css_uncompress', 'checkbox' );
			}
			elseif( $_POST['option_page'] === 'restore' ) {
				if( isset( $_FILES['wpthk-restore'] ) ) {
					require_once( INC . 'optimize.php' );
					global $wp_filesystem;

					$filesystem = new thk_filesystem();
					if( $filesystem->init_filesystem( site_url() ) === false ) return false;

					$json_file = $_FILES['wpthk-restore']['tmp_name'];
					$json = $wp_filesystem->get_contents( $json_file );
					$json = @json_decode( $json );
					$json_error = json_error_code_to_msg( json_last_error() );

					if( $json_error === JSON_ERROR_NONE ) {
						$i = 0;
						remove_theme_mods();
						foreach( (array)$json as $key => $val ) {
							if( array_key_exists( $key, $wpthk_defaults ) && $wpthk_defaults[$key] != $val ) {
								set_theme_mod( $key, $val );
								++$i;
							}
						}
						add_settings_error( 'wpthk-custom', $_POST['option_page'],  sprintf( __( 'Restored %s settings', 'wpthk' ), $i ), 'updated' );
					}
					else {
						add_settings_error( 'wpthk-custom', $_POST['option_page'], __( 'Restore failed', 'wpthk' ) . $json_error, 'error' );
					}
				}
				else {
					add_settings_error( 'wpthk-custom', $_POST['option_page'], __( 'Not file selected', 'wpthk' ) . $json_error, 'error' );
				}
				$err = true;
			}
			elseif( $_POST['option_page'] === 'reset' ) {
				thk_customize_result_set( 'all_clear', 'checkbox' );
				thk_customize_result_set( 'sns_count_cache_cleanup', 'checkbox' );
			}
			elseif( $_POST['option_page'] === 'sns_setting' ) {
				thk_customize_result_set( 'sns_count_cache_enable', 'checkbox' );
				thk_customize_result_set( 'sns_count_cache_force', 'checkbox' );
				thk_customize_result_set( 'sns_count_cache_expire', 'select' );
				thk_customize_result_set( 'sns_count_weekly_cleanup', 'select' );
				thk_customize_result_set( 'sns_count_cache_cleanup', 'checkbox' );
			}
			elseif(
				$_POST['option_page'] === 'edit_style'  ||
				$_POST['option_page'] === 'edit_script' ||
				$_POST['option_page'] === 'edit_header' ||
				$_POST['option_page'] === 'edit_footer' ||
				$_POST['option_page'] === 'edit_functions'
			) {
				if( TPATH === SPATH ) return;

				require_once( INC . 'optimize.php' );
				global $wp_filesystem;

				$filesystem = new thk_filesystem();
				if( $filesystem->init_filesystem( site_url() ) === false ) return false;

				$save_file = null;
				$save_content = '';

				switch( $_POST['option_page'] ) {
					case 'edit_style':
						$save_file = SPATH . DSEP . 'style.css';
						break;
					case 'edit_script':
						$save_file = SPATH . DSEP . 'wpthkch.js';
						break;
					case 'edit_header':
						$save_file = SPATH . DSEP . 'add-header.php';
						break;
					case 'edit_footer':
						$save_file = SPATH . DSEP . 'add-footer.php';
						break;
					case 'edit_functions':
						$save_file = SPATH . DSEP . 'functions.php';
						break;
					default:
						$save_file = SPATH . DSEP . 'style.css';
						break;
				}

				$save_content .= isset( $_POST['newcontent'] ) ? $_POST['newcontent'] : '';
				$save_content = str_replace( "\r\n", "\n", $save_content );
				$save_content = stripslashes_deep( thk_convert( $save_content ) );

				$theme = wp_get_theme( get_stylesheet() );
				if( $_POST['option_page'] === 'edit_style' && $theme->errors() ) {
					add_settings_error( 'wpthk-custom', $_POST['option_page'], __( 'This theme is broken.', 'wpthk' ) . ' ' . $theme->errors()->get_error_message(), 'error' );
					$err = true;
				}
				if( $filesystem->file_save( $save_file, $save_content ) === false ) {
					add_settings_error( 'wpthk-custom', $_POST['option_page'], __( 'Error saving file.', 'wpthk' ), 'error' );
				}
			}

			if( $err === false ) {
				add_settings_error( 'wpthk-custom', $_POST['option_page'], __( 'Changes are properly reflected', 'wpthk' ), 'updated' );
			}

			thk_regenerate_files( true );
			add_filter( 'shutdown', 'thk_cleanup', 99 );
		}
	}
} );

add_action( 'init', function() {
	if( isset( $_GET['page'] ) && $_GET['page'] === 'wpthk' && isset( $_POST['_wpnonce'] ) && isset( $_POST['option_page'] ) ) {
		if( check_admin_referer( $_POST['option_page'] . '-options', '_wpnonce' ) ) {
			if( $_POST['option_page'] === 'backup' ) {
				$mods = get_theme_mods();
				foreach( (array)$mods as $key => $val ) {
					if( is_array( $val ) || is_numeric( $key ) ) unset( $mods[$key] );
				}
				$json = json_encode( $mods );
				if( $json === false || $json === 'false' ) $json = '';
				$file = 'wpthk-customize.json';
				@ob_start();
				@header( 'Content-Description: File Transfer' );
				@header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );
				@header( 'Content-Disposition: attachment; filename=' . basename( $file ) );
				echo $json;
				@ob_end_flush();
				//wp_send_json( $mods );
				exit;
			}
		}
	}
} );

/*---------------------------------------------------------------------------
 * カスタマイズ内容の変更を DB に書き込む (サニタイズ込み)
 *---------------------------------------------------------------------------*/
if( function_exists( 'thk_customize_result_set' ) === false ):
function thk_customize_result_set( $key, $type ) {
	global $wpthk_defaults;

	if( $type === 'checkbox' ) {
		if( isset( $_POST[$key] ) && $wpthk_defaults[$key] != true ) {
			set_theme_mod( $key, true );
		}
		elseif( !isset( $_POST[$key] ) && $wpthk_defaults[$key] != false ) {
			set_theme_mod( $key, false );
		}
		else {
			remove_theme_mod( $key );
		}
	}
	elseif( $type === 'text' ||  $type === 'radio' ||  $type === 'select' ) {
		if( isset( $_POST[$key] ) && $_POST[$key] != $wpthk_defaults[$key] ) {
			set_theme_mod( $key, esc_attr( $_POST[$key] ) );
		}
		else {
			remove_theme_mod( $key );
		}
	}
}
endif;

/*---------------------------------------------------------------------------
 * value の値 (もしくは checked や selected) をチェックして HTML に挿入
 *---------------------------------------------------------------------------*/
if( function_exists( 'thk_value_check' ) === false ):
function thk_value_check( $key, $type, $default = null ) {
	global $wpthk;
	$ret = '';

	if( $type === 'checkbox' ) {
		if( isset( $_POST[$key] ) ) {
			$ret = ' checked="checked"';
		}
		elseif( !isset( $_POST[$key] ) && isset( $_POST['action'] ) ) {
			$ret = '';
		}
		elseif( isset( $wpthk[$key] ) ) {
			$ret = ' checked="checked"';
		}
	}
	elseif( $type === 'radio' || $type === 'select' ) {
		if( isset( $_POST['action'] ) ) {
			if( isset( $_POST[$key] ) && $_POST[$key] == $default ) {
				$ret = $type === 'radio' ? ' checked="checked"' : ' selected="selected"';
			}
		}
		else {
			if( isset( $wpthk[$key] ) && $wpthk[$key] == $default ) {
				$ret = $type === 'radio' ? ' checked="checked"' : ' selected="selected"';
			}
		}
	}
	elseif( $type === 'text' ) {
		if( isset( $_POST[$key] ) ) {
			$ret = $_POST[$key];
		}
		else {
			$ret = isset( $wpthk[$key] ) ? $wpthk[$key] : '';
		}
		$ret = esc_attr( $ret );
	}
	echo $ret;
}
endif;

/*---------------------------------------------------------------------------
 * 管理画面で Widget などに変更が加わった時の処理
 *---------------------------------------------------------------------------*/
if( function_exists( 'thk_options_modify' ) === false ):
function thk_options_modify() {
	global $wpthk;

	require( INC . 'custom-css.php' );
	require( INC . 'compress.php' );

	add_filter( 'updated_option', 'thk_compress', 75 );
	add_filter( 'updated_option', 'thk_parent_css_bind', 80 );
	add_filter( 'updated_option', 'thk_child_js_comp', 80 );
	add_filter( 'updated_option', 'thk_create_inline_style', 85 );
}
endif;

/*---------------------------------------------------------------------------
 * 管理画面用の CSS 読み込み
 *---------------------------------------------------------------------------*/
/* メニューのアイコンを青くするだけのスタイル */
add_action( 'admin_print_styles', function() {
	wp_register_style( 'wpthk-admin', TURI . '/css/admin-menu.css', false, false );
        wp_enqueue_style( 'wpthk-admin' );
} );

/* WpTHK のカスタマイズ画面を開いた時だけ読み込み */
if( isset( $_GET['page'] ) && ( strpos( $_GET['page'], 'wpthk' ) !== false || strpos( $_GET['page'], 'wpthk_edit' ) !== false ) ) {
	add_action( 'admin_print_styles', function() {
        	wp_enqueue_style( 'wpthk-admin-customize', TURI . '/css/admin-customize.css', array( 'wpthk-admin' ), false, false );
	} );
}

/* SNS カウンターのページを開いたときだけ読み込み */
if( isset( $_GET['page'] ) && $_GET['page'] === 'wpthk_sns' ) {
	add_action( 'admin_print_styles', function() {
		wp_enqueue_style( 'wpthk-admin-sns-view', TURI . '/css/admin-sns-view.css', array( 'wpthk-admin' ), false, false );
	} );
}

/* Code Mirror */
if( isset( $_GET['page'] ) && $_GET['page'] === 'wpthk_edit' ) {
	add_action( 'admin_print_styles', function() {
		wp_enqueue_script( 'wpthk-codemirror', TURI . '/js/codemirror/lib/codemirror.min.js', array(), false, false );
		wp_enqueue_script( 'wpthk-codemirror-ui', TURI . '/js/codemirror/lib/codemirror-ui.js', array( 'wpthk-codemirror' ), false, false );
		wp_enqueue_script( 'wpthk-codemirror-searchcursor', TURI . '/js/codemirror/adon/search/searchcursor.js', array(), false, false );

		$code = array( 'css', 'xml', 'php', 'clike', 'htmlmixed', 'javascript' );

		foreach( $code as $val ) {
			wp_enqueue_script( 'wpthk-codem-' . $val, TURI . '/js/codemirror/mode/' . $val . '/' . $val . '.js', array( 'wpthk-codemirror' ), false, false );
		}

		wp_enqueue_style( 'wpthk-codemirror', TURI . '/css/codemirror.css', array( 'wpthk-admin' ), false, false );
		wp_enqueue_style( 'wpthk-codemirror-ui', TURI . '/css/codemirror-ui.css', array( 'wpthk-admin' ), false, false );
		wp_enqueue_style( 'wpthk-codemirror-wpthk', TURI . '/css/codemirror-wpthk.css', array( 'wpthk-admin' ), false, false );
	} );
}

/*---------------------------------------------------------------------------
 * JSON のエラーメッセージ ( json_last_error_msg だと日本語返してくれんから )
 *---------------------------------------------------------------------------*/
if( function_exists( 'json_error_code_to_msg' ) === false ):
function json_error_code_to_msg( $code ) {
	switch( $code ) {
		case JSON_ERROR_DEPTH:
			return ' : ' . __( 'Maximum stack depth exceeded.', 'wpthk' );
			break;
		case JSON_ERROR_STATE_MISMATCH:
			return ' : ' . __( 'Underflow or the modes mismatch.', 'wpthk' );
			break;
		case JSON_ERROR_CTRL_CHAR:
			return ' : ' . __( 'Unexpected control character found.', 'wpthk' );
			break;
		case JSON_ERROR_SYNTAX:
			return ' : ' . __( 'Syntax error, malformed JSON.', 'wpthk' );
			break;
		case JSON_ERROR_UTF8:
			return ' : ' . __( 'Malformed UTF-8 characters, possibly incorrectly encoded.', 'wpthk' );
			break;
		case JSON_ERROR_RECURSION:
			if( version_compare( PHP_VERSION, '5.5.0', '<' ) ) {
				return JSON_ERROR_NONE;
			}
			return ' : ' . __( 'One or more recursive references in the value to be encoded.', 'wpthk' );
			break;
		case JSON_ERROR_INF_OR_NAN:
			return ' : ' . __( 'One or more NAN or INF values in the value to be encoded.', 'wpthk' );
			break;
		case JSON_ERROR_UNSUPPORTED_TYPE:
			return ' : ' . __( 'A value of a type that cannot be encoded was given.', 'wpthk' );
			break;
		default:
			return JSON_ERROR_NONE;
			break;
	}
}
endif;

/*---------------------------------------------------------------------------
 * add menu page -> wpthk menu page
 *---------------------------------------------------------------------------*/
if( function_exists( 'wpthk_menu_page' ) === false ):
function wpthk_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function = '', $icon_url = '', $position = null ) {
	$func = 'add_' . 'menu_' . 'page';
	$hookname = $func( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
	return $hookname;
}
endif;

/*---------------------------------------------------------------------------
 * add submenu page -> wpthk submenu page
 *---------------------------------------------------------------------------*/
if( function_exists( 'wpthk_submenu_page' ) === false ):
function wpthk_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function = '' ) {
	$func = 'add_' . 'submenu_' . 'page';
	$hookname = $func( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function );
	return $hookname;
}
endif;

/*---------------------------------------------------------------------------
 * dummy function
 *---------------------------------------------------------------------------*/
if( function_exists('d_init') === false ):
function d_init() {
	//add_theme_support( 'post-formats' );
	add_theme_support( 'custom-header' );
	add_theme_support( 'custom-background' );
	get_post_format();
}
endif;

if( function_exists( 'd_pagination' ) === false ):
function d_pagination() {
	posts_nav_link();
	next_comments_link();
	previous_comments_link();
	paginate_comments_links();
}
endif;
