<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

define( 'THK_HOME_URL',	home_url('/') );
define( 'THK_SITENAME',	get_bloginfo( 'name' ) );
define( 'THK_DESCRIPTION', get_bloginfo( 'description' ) );
define( 'TURI', get_template_directory_uri() );
define( 'TDEL', pdel( TURI ) );
define( 'SURI', get_stylesheet_directory_uri() );
define( 'SDEL', pdel( SURI ) );
define( 'THK_COPY', 'http://thk.kanzae.net/' );

global $wpthk;
require_once( INC . 'defaults.php' );

$conf = new defConfig();
if( is_customize_preview() === true || is_admin() === true ) {
	if(
		is_customize_preview() === true ||
		( isset( $_GET['page'] ) && strpos( $_GET['page'], 'wpthk') !== false )
	) {
		$wpthk = $conf->const_custom( $conf->default_custom_variables() );
	}
}
else {
	$wpthk = wp_parse_args( get_theme_mods(), $conf->default_variables() );
}

//$wpthk = array_filter( $wpthk, 'strlen' );
$conf->optimize_wpthk_variable();
unset( $conf );
