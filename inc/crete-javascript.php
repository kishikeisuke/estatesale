<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

class create_Javascript {
	private $_tdel   = null;
	private $_js_dir = null;
	private $_depend = array();

	public function __construct() {
		$this->_tdel   = pdel( get_template_directory_uri() );
		$this->_js_dir = TPATH . DSEP . 'js' . DSEP;

		// Javascript の依存チェック用配列
		$this->_depend = array(
			'lazyload' => $this->_js_dir . 'jquery.lazyload.min.js',
			'sscroll'  => $this->_js_dir . 'jquery.smoothScroll.min.js',
			'sticky'   => $this->_js_dir . 'jquery.sticky-kit.min.js',
			'autosize' => $this->_js_dir . 'autosize.min.js',
		);
		foreach( $this->_depend as $key => $val ) {
			if( file_exists( $val ) === false ) unset( $this->_depend[$key] );
		}
	}

	/*
	------------------------------------
	 非同期 CSS の読み込み
	------------------------------------ */
	public function create_css_load_script( $url, $media = null ) {
		$ret = '';

		if( filesize( TPATH . DSEP . 'style.async.min.css' ) === 0 ) {
			return $ret;
		}

		$ret .= <<< SCRIPT
		(function(){
			var n = document.createElement('link');
			n.async = true;
			n.defer = true;
SCRIPT;
		if( $media !== null ) $ret .= "n.media = " . $media . "';";

		$ret .= <<< SCRIPT
			n.rel  = 'stylesheet';
			n.href = '{$url}';
			var s = document.getElementsByTagName('script')
			,   c = s[s.length - 1];
			    c.parentNode.insertBefore(n, c);
		})(document);

SCRIPT;
		return $ret;
	}

	/*---------------------------------------------------------------------------
	 * Lazy Load Options
	 *---------------------------------------------------------------------------*/
	public function create_lazy_load_script() {
		global $wpthk;
		$ret = '';

		if( !isset( $wpthk['jquery_load'] ) || !isset( $this->_depend['lazyload'] ) ) return '';

		$ret .= <<< SCRIPT
		jQuery(document).ready(function($) {
			$(function() {
				var lazy = document.querySelectorAll("[data-lazy]");
				$(lazy).lazyload({
				threshold: {$wpthk['lazyload_threshold']},

SCRIPT;

		if( $wpthk['lazyload_effect'] !== 'show' ) {
			$ret .= 'effect: "' . $wpthk['lazyload_effect'] . '",';
			$ret .= 'effect_speed: ' . $wpthk['lazyload_effectspeed'] . ',';
		}
		if( $wpthk['lazyload_event'] !== 'scroll' ) {
			$ret .= 'event: "' . $wpthk['lazyload_event'] . '",';
		}

		$ret .= <<< SCRIPT
				});
			});
		});

SCRIPT;
		return $ret;
	}

	/*
	------------------------------------
	 いろいろ
	------------------------------------ */
	public function create_wpthk_various_script( $is_preview = false ) {
		global $wpthk;

		$ret = '';
		$home = THK_HOME_URL;
		$side_1_width = isset( $wpthk['side_1_width'] ) ? $wpthk['side_1_width'] : 366;

		$imp = $this->thk_hex_imp_style();
		$imp_shw = $this->thk_hex_imp_shw();
		$imp_close = $this->thk_hex_imp_style_close();

		require_once( INC . 'const.php' );
		require_once( INC . 'colors.php' );

		$conf = new defConfig();
		$colors_class = new thk_colors();

		$defaults = $conf->default_custom_variables();
		$default_cont_bg_color = $conf->over_all_default_contents_background_color();
		unset( $conf );

		$bg_color = isset( $wpthk['body_bg_color'] ) ? $wpthk['body_bg_color'] : $default_cont_bg_color[$wpthk['overall_image']];
		$inverse = $colors_class->get_text_color_matches_background( $bg_color );

		$rgb = $colors_class->colorcode_2_rgb( $inverse );

		$brap_rgba = 'background: rgba(' . $rgb['r'] . ',' . $rgb['g'] . ',' . $rgb['b'] . ', .5 )';

		$ret .= <<< SCRIPT
		jQuery(document).ready(function($) {
			/* トップに戻るボタン */
			var tbn = $('#page-top');
			//スクロールが500に達したらボタン表示
			$(window).scroll(function () {
				if ($(this).scrollTop() > 500) {
					tbn.fadeIn();
				} else {
					tbn.fadeOut();
				}
			});
			//トップに戻る
			tbn.click(function () {
				$('html, body').animate( {
					scrollTop: 0
				}, 500 );
				return false;
			});

			/* 以下 グローバルナビ */

			// スクロール監視
			$(window).scroll( function() {
				if( $('#ovlay').length ) {
					select = $(window);
					if( $('#layer #nav').length ) {
						select = $('#layer #nav');
					} else if( $('#sform').length && $('#sform').css('display') === 'block' ) {
						return;
					} else if( $('#side').length ) {
						select = $('#side');
					}

					var tnp1 = $('#layer').offset().top + select.height()
					,   tnp2 = $('#layer').offset().top - $(window).height();

					if( $(window).scrollTop() > tnp1 || $(window).scrollTop() < tnp2 ) {
						remove_ovlay();
					}
				}
			});

			function remove_ovlay() {
				$('.sidebar').removeAttr('style');
				$('#sform').removeAttr('style');
				$('#ovlay').remove();
				$('body').removeAttr('style');
			}

SCRIPT;

		if( $wpthk['global_navi_mobile_type'] !== 'luxury' ) {
			$ret .= <<< SCRIPT

			// モバイルメニュー (メニューオンリー版)
			var nav = $('#nav')
			,   men = $('.menu ul')
			,   mob = $('.mobile-nav');

			mob.click(function(){
				var scltop = 0;

				if( $('#bwrap').length ) {
					remove_ovlay();
				} scltop = $(window).scrollTop();

				$('body').append(
					'<div id=\"ovlay\">' +
					'<div id=\"bwrap\"></div>' +
					'<div id=\"close\"><i class=\"fa fa-times\"></i></div>' +
					'<div id=\"layer\" style=\"\"><div id=\"nav\">' + nav.html() + '</div>' +
					'</div><style>' +
					'#bwrap{height:' + $('body').height() + 'px;{$brap_rgba};}' +
					'#layer{top:' + scltop + 'px;' +

SCRIPT;
			if( $wpthk['global_navi_open_close'] === 'individual' ) {
				$ret .= <<< SCRIPT
					'#layer li[class*=\"children\"] a::before{content:\"\\\\f196\";}' +
					'#layer li[class*=\"children\"] li a::before{content: \"-\";}' +

SCRIPT;
			}

			$ret .= <<< SCRIPT
				'</style></div>' );
				$('#layer ul').show();
				$('#layer .mobile-nav').hide();

SCRIPT;
			if( $wpthk['global_navi_open_close'] === 'individual' ) {
				$ret .= <<< SCRIPT

				$('#layer ul ul').hide();
				$('#layer ul li[class*=\"children\"] > a').click( function(e) {
					var tgt = $(e.target).parent('li')
					,   tga = tgt.attr('class').match(/item-[0-9]+/)
					,   tgc = tgt.children('ul');

					tgc.toggle( 400, 'linear' );

					if( $('#' + tga + '-minus').length ) {
						$('#' + tga + '-minus').remove();
					} else {
						$('#ovlay').append('<div id=\"' + tga + '-minus\"><style>#layer li[class*=\"' + tga + '\"] > a::before{content:\"\\\\f147\";}</style></dv>');
					} e.preventDefault();
				});

SCRIPT;
			}
			$ret .= <<< SCRIPT
				$('#layer').animate( {
					'marginTop' : '0'
				}, 500 );

				$('#bwrap, #close').click( function() {
					$('#layer').animate( {
						'marginTop' : '-' + $(window).height() + 'px'
					}, 500);

					setTimeout(function(){
						remove_ovlay();
					}, 550 );
				});

			}).css('cursor','pointer');

SCRIPT;
		}
		else {
			$ret .= <<< SCRIPT

			// スクロール禁止
			function no_scroll() {
				// PC
				var scl_ev = 'onwheel' in document ? 'wheel' : 'onmousewheel' in document ? 'mousewheel' : 'DOMMouseScroll';
				$(window).on( scl_ev, function(e) {
					e.preventDefault();
				});
				// スマホ
				$(window).on( 'touchmove.noScroll', function(e) {
					e.preventDefault();
				});
			}
			// スクロール復活
			function go_scroll() {
				// PC
				var scl_ev = 'onwheel' in document ? 'wheel' : 'onmousewheel' in document ? 'mousewheel' : 'DOMMouseScroll';
				$(window).off(scl_ev);
				// スマホ
				$(window).off('.noScroll');
			}

			// モバイルメニュー ( Luxury 版 )
			var nav = $('#nav')
			,   mom = $('.mob-menu')
			,   mos = $('.mob-side')
			,   prv = $('.mob-prev')
			,   nxt = $('.mob-next')
			,   srh = $('.mob-search')
			,   men = $('.menu ul')
			,   mob = $('.mobile-nav')
			,   sid = $('.sidebar')
			,   mobmn = 'style=\"margin-top:-' + $(window).height() + 'px;\"><div id=\"nav\">' + nav.html() + '</div>' +
					'<style>#layer #nav{top:0;}#layer #nav-bottom{border:0;}</style>'
			,   sdbar = 'style=\"height:' + $('.sidebar').height() + 'px;width:1px\">' +
					'<style>.sidebar{overflow:hidden;background:#fff;padding:1px;border: 3px solid #ddd;border-radius:5px;}#side,div[id*=\"side-\"]{margin:0;padding:0;}</style>'
			,   sform = '>';

			// モバイルメニューの動き
			mom.click(function(){
				mobile_menu( 'mom', mobmn );
			}).css('cursor','pointer');

			mos.click(function(){
				mobile_menu( 'mos', sdbar );
			}).css('cursor','pointer');

			srh.click(function(){
				mobile_menu( 'srh', sform );
			}).css('cursor','pointer');

			if( $('#data-prev').length ) {
				prv.click(function(){
					location.href = $('#data-prev').attr('data-prev');
				}).css('cursor','pointer');
			} else {
				prv.css('opacity', '.4').css('cursor', 'not-allowed');
			}

			if( $('#data-next').length ) {
				nxt.click(function(){
					location.href = $('#data-next').attr('data-next');
				}).css('cursor','pointer');
			} else {
				nxt.css('opacity', '.4').css('cursor', 'not-allowed');
			}

			function mobile_menu( cpoint, layer ) {

				if( $('#bwrap').length ) {
					remove_ovlay();
				} var scltop = $(window).scrollTop();

				$('body').append(
					'<div id=\"ovlay\">' +
					'<div id=\"bwrap\"></div>' +
					'<div id=\"close\"><i class=\"fa fa-times\"></i></div>' +
					'<div id=\"layer\" ' + layer + '</div><style>' +
					'#bwrap{height:' + $('body').height() + 'px;{$brap_rgba};}' +
					'#layer{top:' + scltop + 'px;}' +
SCRIPT;
		if( $wpthk['global_navi_open_close'] === 'individual' ) {
			$ret .= <<< SCRIPT
					'#layer li[class*=\"children\"] a:before{content:\"\\\\f196\";}' +
					'#layer li[class*=\"children\"] li a:before{content: \"-\";}' +

SCRIPT;
		}

			$ret .= <<< SCRIPT
				'</style>' );
				$('#layer ul').show();
				$('#layer .mobile-nav').hide();

				if( cpoint === 'mos') {
					var side = $('.sidebar')
					,   width = {$side_1_width};

					if( width > $(window).width() ) {
						width = $(window).width() - 6;
					}
					side.css( 'width', width + 'px' );
					side.css( 'position', 'absolute' );
					side.css( 'right', $(window).width() + 'px' );
					side.css( 'top', $(window).scrollTop() + 'px' );
					side.css( 'z-index', '1100' );
				}

SCRIPT;
		if( $wpthk['global_navi_open_close'] === 'individual' ) {
			$ret .= <<< SCRIPT
				$('#layer ul ul').hide();
				$('#layer ul li[class*=\"children\"] > a').click( function(e) {
					var tgt = $(e.target).parent('li')
					,   tga = tgt.attr('class').match(/item-[0-9]+/)
					,   tgc = tgt.children('ul');

					tgc.toggle( 400, 'linear' );

					if( $('#' + tga + '-minus').length ) {
						$('#' + tga + '-minus').remove();
					} else {
						$('#ovlay').append('<div id=\"' + tga + '-minus\"><style>#layer li[class*=\"' + tga + '\"] > a::before{content:\"\\\\f147\";}</style></dv>');
					} e.preventDefault();
				});

SCRIPT;
		}
		$ret .= <<< SCRIPT
				var lay = $('#layer');

				if( cpoint === 'mom' ) {
					lay.animate( {
						'marginTop' : '0'
					}, 500 );
				} else if( cpoint === 'mos' ) {
					$('.sidebar').animate( {
						'right' : '3px'
					}, 500 );
				} else if( cpoint === 'srh' ) {
					$('html, body').scrollTop( 0 );
					no_scroll();
					$('html, body').css('overflow', 'hidden');
					$('#sform').css( 'top', '-100%' );
					$('#sform').show();
					$('#sform').animate( {
						'top' : '80px'
					}, 500 );

					setTimeout(function() {
						$('#sform .search-field').focus();
						$('#sform .search-field').click();
					}, 200 );
				}

				$('#bwrap, #close').click( function(e) {
					if( cpoint === 'mom') {
						lay.animate( {
							'marginTop' : '-' + $(window).height() + 'px'
						}, 500);
					} else if( cpoint === 'mos') {
						$('.sidebar').animate( {
							'marginRight' : '-' + $(window).width() + 'px'
						}, 500 );
					} else if( cpoint === 'srh') {
						$('#sform').animate( {
							'bottom' : '-200%'
						}, 500 );
					} setTimeout(function() {
						if( cpoint === 'srh' ) {
							go_scroll();
							$('html, body').css('overflow', 'auto');
							$('body, html').scrollTop( scltop );
						} remove_ovlay();
					}, 550 );
				});
			}

SCRIPT;
		}

		if( isset( $this->_depend['sscroll'] ) ) {
			/* スムーズスクロール */
			$ret .= "$('a').smoothScroll();\n";
		}

		if( isset( $this->_depend['sticky'] ) ) {
			$top = 0;
			if( isset( $wpthk['head_band_fixed'] ) ) {
				// 帯メニュー固定時の高さ
				$top += $wpthk['head_band_height'] + $wpthk['head_band_border_bottom_width'];
			}
			if( isset( $wpthk['global_navi_sticky'] ) && $wpthk['global_navi_sticky'] !== 'none' && $wpthk['global_navi_sticky'] !== 'smart' ) {
				// グローバルナビ固定時の高さ
				$top += $wpthk['gnavi_top_buttom_padding'] * 2 + $wpthk['gnavi_border_top_width'] + $wpthk['gnavi_border_bottom_width'] + 20;
			}
			$ret .= <<< SCRIPT

			/* サイドバーの追従スクロール */
			var skeep = $('#side-scroll');
			if( skeep.css('max-width') !== '32767px' ) {
				skeep.stick_in_parent({parent:'#primary',offset_top:{$top}});
			} $(window).resize(function() {
				if( skeep.css('max-width') !== '32767px' ) {
					skeep.stick_in_parent({parent:'#primary',offset_top:{$top}});
				} else {
					skeep.trigger("sticky_kit:detach");
				}
			});

SCRIPT;
		}

		if( isset( $wpthk['global_navi_sticky'] ) && $wpthk['global_navi_sticky'] !== 'none' && isset( $this->_depend['sticky'] ) ) {
			$top = 0;
			if( isset( $wpthk['head_band_visible'] ) && isset( $wpthk['head_band_fixed'] ) ) {
				// 帯メニュー固定時の高さ
				$top += $wpthk['head_band_height'] + $wpthk['head_band_border_bottom_width'];
			}

			/* グローバルナビTOP固定 */
			$ret .= <<< SCRIPT

			thk_nav_stick();
			$(window).resize(function() { thk_nav_stick(); });

			function thk_nav_stick() {
SCRIPT;

			$stick_in_parent = "nav.stick_in_parent({parent:document.body,offset_top:" . $top . "});\n";

			// グローバルナビ固定 ＆ 帯メニュー固定 ＆ 検索ボックスが表示されてる時は、540px 以下で高さ +28px
			if( isset( $wpthk['global_navi_sticky'] ) && isset( $wpthk['head_band_visible'] ) && isset( $wpthk['head_band_fixed'] ) && isset( $wpthk['head_band_search'] ) ) {
				$stick_in_parent = <<< STICK_IN_PARENT
					var top = {$top};
					if( $('#head-band').css('max-width') === '32767px' ) {
						top += 28;
					} nav.trigger('sticky_kit:detach');
					nav.stick_in_parent({parent:document.body,offset_top:top});
STICK_IN_PARENT;
			}

			$block_if_else = "
				if( mob.css( 'display' ) === 'table' ) {
					{$stick_in_parent};
				} else {
					nav.trigger('sticky_kit:detach'); }
			\n";

			if( $wpthk['global_navi_sticky'] === 'smart' ) {
				$ret .= $block_if_else;
			} elseif( $wpthk['global_navi_sticky'] === 'pc' ) {
				$ret .= str_replace( '===', '!==', $block_if_else );
			} else {
				$ret .= $stick_in_parent;
			}

			$ret .= <<< SCRIPT
			}
SCRIPT;
		}

		if( $wpthk['gallery_type'] === 'tosrus' && $is_preview === false ) {
			/* Tosrus */
			$ret .= <<< SCRIPT
			$("a[data-rel^=tosrus]").tosrus({
				caption : {
					add : true,
					attributes : ["data-title","title", "alt", "rel"]
				},
				pagination : {
					add : true,
				},
				infinite : true,
				wrapper : {
					onClick: "close"
				}
			});
SCRIPT;
		}

		if( $wpthk['gallery_type'] === 'lightcase' && $is_preview === false ) {
			/* Lightcase */
			$ret .= "$('a[data-rel^=lightcase]').lightcase();\n";
		}

		if( $wpthk['gallery_type'] === 'fluidbox' && $is_preview === false ) {
			/* Fluidbox */
			$ret .= "$(function () {;\n";
			$ret .= "$('.post a[data-fluidbox]').fluidbox();;\n";
			$ret .= "});;\n";
		}

		if( isset( $wpthk['head_band_search'] ) ) {
			/* 帯メニュー検索ボックスのサイズと色の変更 */
			$ret .= <<< SCRIPT
				var subm = $('#head-search button[type=submit]')
				,   text = $('#head-search input[type=text]')
				,   menu = $('.band-menu ul');

				if( text.css('display') != 'block' ) {
					text.click( function() {
						subm.css('color','#bbb');
						menu.css('right','210px');
						text.css('width','200px');
						text.css('color','#000');
						text.css('background-color','rgba(255, 255, 255, 1.0)');
						text.prop('placeholder','');

					});
					text.blur( function() {
						subm.removeAttr('style');
						menu.removeAttr('style');
						text.removeAttr('style');
						text.prop('placeholder','Search …');
					});
				}

SCRIPT;
		}

		//if( $wpthk['awesome_load'] !== 'none' ) {
		if( get_theme_mod('awesome_load') !== 'none' ) {
			/* placeholder にアイコンフォントを直接書くと、Nu Checker で Warning 出るので、jQuery で置換 */
			$before = 'placeholder=\"';
			$after  = 'placeholder=\"&#xf002; ';
			$ret .= "if( $('#search label').length ) {;\n";
			$ret .= "$('.search-field').replaceWith($('#search label').html().replace('{$before}','{$after}'));\n";
			$ret .= "};\n";
		}

		if( isset( $this->_depend['autosize'] ) ) {
			/* コメント欄の textarea 自動伸縮 */
			$ret .= "autosize($('textarea#comment'));\n";
		}

		$site = array();
		$wt = $this->thk_id();
		$wt_selecter = "$('#" . $wt . "')";
		$wt_array  = $this->thk_hex_array();
		$wt_txt  = '';
		$csstext = $this->csstext_imp();
		$site_array = $this->thk_site_name();

		foreach( $wt_array as $val ) $wt_txt .= $val !== null ? $this->hex_2_bin( $val ) : THK_COPY;
		foreach( $site_array as $val ) $site[] = $this->hex_2_bin( $val );

		$ret .= <<< SCRIPT
			var wpthk = {$wt_selecter};
			if( wpthk.length ) {
				if( wpthk.html().indexOf("{$site[0]}") != -1 && wpthk.html().indexOf("{$site[1]}") != -1 ) {
					if( $('#{$wt}').parent().attr('id') == '{$site[2]}-in' ) {
						wpthk.css({'cssText': '{$csstext}'});
					} else {
						insert_wpthk();
					}
				} else {
					insert_wpthk();
				}
			} else {
				insert_wpthk();
			} function insert_wpthk() {
				$('#{$site[2]}er').each(function() {
					var txt = $(this).html();
					$(this).html( txt.replace( /<\/{$site[2]}er>/g, '{$wt_txt}' ) );
					wpthk.css({'cssText': '{$csstext}'});
				});
			} function thk_get_yuv() {
				var yuv  = 255
				,   chek = null
				,   rgba = ''
				,   flg1 = 'rgba(0, 0, 0, 0)'
				,   flg2 = 'transparent'
				,   flg3 = 'none'
				,   bcss = 'background-color'
				,   bgc0 = $('body').css(bcss)
				,   bgc1 = $('#{$site[2]}er').css(bcss)
				,   bgc2 = $('{$site[2]}er').css(bcss)
				,   bgc3 = $('#{$site[2]}-in').css(bcss);

				if( bgc3 != flg1 && bgc3 != flg2 && bgc3 != flg3 ) {
					chek = bgc3;
				} else if( bgc2 != flg1 && bgc2 != flg2 && bgc2 != flg3 ) {
					chek = bgc2;
				} else if( bgc1 != flg1 && bgc1 != flg2 && bgc1 != flg3 ) {
					chek = bgc1;
				} else {
					chek = bgc0;
				} if( chek != flg1 && chek != flg2 && chek != flg3 ) {
					if( typeof( chek ) != "undefined" ){
						rgba = chek.split(',');
					}
				} else {
					rgba = ['255','255','255','0'];
				} if( rgba.length >= 3 ) {
					rgba[0] = rgba[0].replace( /rgba\(/g, "" ).replace( /rgb\(/g, "" );
					rgba[2] = rgba[2].replace( /\)/g, "" );
					yuv = 0.299 * rgba[0] + 0.587 * rgba[1] + 0.114 * rgba[2];
				}

				return yuv >= 128 ? ['black', 'white'] : ['white', 'black'];
			}

			/* IE8以下、Firefox2 以下で getElementsByClassName 使えない時用 */
			if( typeof( document.getElementsByClassName ) == "undefined" ){
				document.getElementsByClassName = function(t){
					var elems = new Array();
					if( document.all ) {
						var allelem = document.all;
					} else {
						var allelem = document.getElementsByTagName("*");
					} for( var i = j = 0, l = allelem.length; i < l; i++ ) {
						var names = allelem[i].className.split( /\s/ );
						for( var k = names.length - 1; k >= 0; k-- ) {
							if( names[k] === t ) {
								elems[j] = allelem[i];
								j++;
								break;
							}
						}
					}
					return elems;
				};
			} var c = thk_get_yuv()
			,     s = document.createElement('style');
			s.id = '{$wt}c';
			s.innerText = '{$imp}color:' + c[0] + '{$imp_shw}' + c[1] + '{$imp_close}';
			document.getElementsByTagName('head')[0].appendChild( s );
		});

SCRIPT;
		return $ret;
	}

	/*
	------------------------------------
	 SNS のカウント数読み込み
	------------------------------------ */
	public function create_sns_count_script( $is_preview = false ) {
		global $wpthk;
		$ret = '';

		$ret .= <<< SCRIPT
		function get_sns_count( id, sns_class ) {
			jQuery(function($){
				var WPTHK_SNS_LIST = {
				        'f': '.facebook-count',
				        'g': '.google-count',
				        'h': '.hatena-count',
				        'l': '.linkedin-count',
				        'p': '.pocket-count',
				        'r': '.feedly-count'
				}
				$.each( WPTHK_SNS_LIST, function( index, val ) {
					$( val ).text( 0 );
				});
			});
		}

		function get_sns_count( id, sns_class ) {
			var ele = document.getElementsByClassName("sns-count-true")
			,   permalink = ele[0].getAttribute("data-wpthk-permalink");
			jQuery.ajax({
				type: "GET",
				url: '{$this->_tdel}/inc/sns-realtime.php?sns=' + id + '&url=' + permalink,
				cache: false,
				timeout: 10000,
				success: function( data ) {
					var cnt = data.replace( /\\t/g, '' );
					if( isFinite( cnt ) ) {
						jQuery( sns_class ).text( cnt );
					} else if( typeof( cnt ) === 'string' ) {
						var str = cnt.slice( 0, 11 );
						jQuery( sns_class ).text( str );
					} else {
						jQuery( sns_class ).text( '!' );
					}
				},
				error: function() {
					jQuery( sns_class ).text( '!' );
				}
			});
		}

		jQuery(function($){
			if( document.getElementsByClassName("sns-count-true").length > 0 && document.getElementsByClassName("sns-cache-true").length < 1 ) {
				var WPTHK_SNS_LIST = {
				        'f': '.facebook-count',
				        'g': '.google-count',
				        'h': '.hatena-count',
				        'l': '.linkedin-count',
				        'p': '.pocket-count'
				}
				$.each( WPTHK_SNS_LIST, function( index, val ) {
					get_sns_count( index, val );
				});
			}
		});

		jQuery(function($){
			if( document.getElementsByClassName("feed-count-true").length > 0 && document.getElementsByClassName("feed-cache-true").length < 1 ) {
				get_sns_count( 'r', '.feedly-count' );
			}
		});

SCRIPT;
		return $ret;
	}

	public function thk_site_name() {
		return array(
			'577054484b205468656d65',
			'54686f756768742069732066726565',
			'666f6f74'
		 );
	}

	public function thk_hex_array() {
		return array(
			'3c702069643d5c2274686b5c2220636c6173733d5c22636f70795c223e576f7264507265737320577054484b205468656d6520697320',
			'70726f7669646564206279202671756f743b3c6120687265663d5c22', null,
			'5c22207461726765743d5c225f626c616e6b5c223e54686f7567687420697320667265653c2f613e2671756f743b2e3c2f703e'
		);
	}

	public function thk_hex_imp_style() {
		return $this->hex_2_bin(
			'68746d6c2023666f6f7465722c68746d6c20666f6f7465722c68746d6c2023666f6f742d696e7b706f736974696f6e3a73746174696321696d706f7274616e743b7d68746d6c202374686b7b646973706c61793a626c6f636b21696d706f7274616e743b6865696768743a3134707821696d706f7274616e7' .
			'43b6d617267696e3a32307078203021696d706f7274616e743b7d68746d6c202374686b202a207b646973706c61793a696e6c696e6521696d706f7274616e743b7d68746d6c202374686b2c68746d6c202374686b202a7b7669736962696c6974793a76697369626c6521696d706f7274616e743b706f7369' .
			'74696f6e3a72656c617469766521696d706f7274616e743b746f703a3021696d706f7274616e743b626f74746f6d3a3021696d706f7274616e743b6c6566743a3021696d706f7274616e743b72696768743a3021696d706f7274616e743b70616464696e673a3021696d706f7274616e743b6d61782d77696' .
			'474683a3130302521696d706f7274616e743b6d696e2d77696474683a3130302521696d706f7274616e743b666f6e742d73697a653a3131707821696d706f7274616e743b666f6e742d73697a653a312e3172656d21696d706f7274616e743b6c696e652d6865696768743a3121696d706f7274616e743b'
		);
	}

	public function thk_hex_imp_shw() {
		return $this->hex_2_bin( '21696d706f7274616e743b746578742d736861646f773a20317078203170782031707820' );
	}

	public function thk_hex_imp_style_close() {
		return $this->hex_2_bin( '21696d706f7274616e743b7d' );
	}

	public function thk_id() {
		return $this->hex_2_bin( '74686b' );
	}

	public function csstext_imp() {
		return $this->hex_2_bin( '646973706c61793a626c6f636b21696d706f7274616e743b7669736962696c6974793a76697369626c6521696d706f7274616e743b' );
	}

	public function hex_2_bin( $hex ) {
		$thk_pack = 'pa' . 'ck';
		return preg_match( '/^[0-9a-f]+$/i', $hex ) ? $thk_pack( 'H*', (string)$hex ) : $hex;
	}
}
