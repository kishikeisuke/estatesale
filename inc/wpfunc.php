<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

/*---------------------------------------------------------------------------
 * CSS / Javascript の圧縮・結合で必要なファイルのインクルード
 * 条件によって、load header でもインクルードするので関数化
 *---------------------------------------------------------------------------*/
if( function_exists('thk_regenerate_files') === false ):
function thk_regenerate_files( $shutdown = false, $require_only = false ) {
	global $wpthk;
	require_once( INC . 'custom-css.php' );
	require_once( INC . 'compress.php' );

	$conf = new defConfig();
	$wpthk = $conf->const_custom( $conf->default_custom_variables() );
	$conf->optimize_wpthk_variable();
	unset( $conf );

	if( $require_only === true ) {
		return;
	}

	if( $shutdown === false ) {
		thk_compress();
		thk_parent_css_bind();
		thk_child_js_comp();
		thk_create_inline_style();
		thk_empty_remove();
	}
	else {
		add_filter( 'shutdown', 'thk_compress', 75 );
		add_filter( 'shutdown', 'thk_parent_css_bind', 80 );
		add_filter( 'shutdown', 'thk_child_js_comp', 80 );
		add_filter( 'shutdown', 'thk_create_inline_style', 85 );
		add_filter( 'shutdown', 'thk_empty_remove', 90 );
	}
}
endif;

/*---------------------------------------------------------------------------
 * タイトル修正
 *---------------------------------------------------------------------------*/
if( function_exists('thk_title_separator') === false ):
function thk_title_separator( $sep ) {
	global $wpthk;
	$sep = ( $wpthk['title_sep'] === 'hyphen' ) ? '-' : '|';
	return $sep;
}
add_filter( 'document_title_separator', 'thk_title_separator' );
endif;

add_filter( 'document_title_parts', function( $title ) {
	global $wpthk;
	$ret = $title;

	/* Memo: https://developer.wordpress.org/reference/hooks/document_title_parts/
	 *
	 * $title (array) The document title parts.
	 *	'title'   (string) Title of the viewed page.
	 *	'page'    (string) Optional. Page number if paginated.
	 *	'tagline' (string) Optional. Site description when on home page.
	 * 	'site'    (string) Optional. Site title when not on home page.
	 */

	switch( true ) {
		case is_home():
			if( $wpthk['title_top_list'] === 'site' ) {
				$ret = array( 'site' => THK_SITENAME );
				if( isset( $title['page'] ) ) $ret['page'] = $title['page'];
			}
			break;
		case is_front_page():
			if( $wpthk['title_front_page'] === 'site' ) {
				$ret = array( 'site' => THK_SITENAME );
				if( isset( $title['page'] ) ) $ret['page'] = $title['page'];
			}
			elseif( $wpthk['title_front_page'] === 'site_catch' ) {
				$ret = array( 'site' => THK_SITENAME, 'title' => THK_DESCRIPTION );
				if( isset( $title['page'] ) ) $ret['page'] = $title['page'];
			}
			elseif( $wpthk['title_front_page'] === 'site_title' ) {
				$ret = array( 'site' => THK_SITENAME, 'title' => get_the_title() );
				if( isset( $title['page'] ) ) $ret['page'] = $title['page'];
			}
			break;
		case is_singular():
			if( $wpthk['title_other'] === 'title' ) {
				//$ret = array( 'title' =>  get_the_title() );
				if( isset( $title['page'] ) ) $ret['page'] = $title['page'];
			}
			elseif( $wpthk['title_other'] === 'site_title' ) {
				$ret = array( 'site' => THK_SITENAME, 'title' => get_the_title() );
				if( isset( $title['page'] ) ) $ret['page'] = $title['page'];
			}
			break;
		default:
			if( $wpthk['title_other'] === 'title' ) {
				$ret = array( 'title' =>  current( $title ) );
				if( isset( $title['page'] ) ) $ret['page'] = $title['page'];
			}
			elseif( $wpthk['title_other'] === 'site_title' ) {
				$ret = array( 'site' => THK_SITENAME, 'title' => current( $title ) );
				if( isset( $title['page'] ) ) $ret['page'] = $title['page'];
			}
			break;
	}
	return $ret;
} );

/*---------------------------------------------------------------------------
 * グローバルナビにホームへのリンク追加
 *---------------------------------------------------------------------------*/
add_filter( 'wp_page_menu_args', function( $args ) {
	global $wpthk;

	if( isset( $wpthk['home_text'] ) ) {
		$args['show_home'] = $wpthk['home_text'];
	}
	elseif( !isset( $args['show_home'] ) ) {
		$args['show_home'] = true;
	}

	return $args;
} );

/*---------------------------------------------------------------------------
 * プロトコル消去
 *---------------------------------------------------------------------------*/
if( function_exists( 'pdel' ) === false ):
function pdel( $url ) {
	return str_replace( array( 'http:', 'https:' ), '', esc_url( $url ) );
}
endif;

/*---------------------------------------------------------------------------
 * ページネーション (bootstrap version)
 *---------------------------------------------------------------------------*/
if( function_exists( 'bootstrap_pagination' ) === false ):
function bootstrap_pagination( $flag = null ) {
	global $wp_query, $paged;

	$paged = (int)$wp_query->get( 'paged' );
	$posts_per_page = get_option('posts_per_page');

	if( ( !$paged || $paged < 2 ) && $wp_query->found_posts < $posts_per_page ) {
		return false;
	}
	elseif( $flag !== null ) {
		return true;
	}

	if( empty( $paged ) ) $paged = 1;

	$pages = (int)$wp_query->max_num_pages;
	if( empty( $pages ) ) $pages = 1;

	$range = 3; //左右に表示する件数
	$showitems = ( $range * 2 ) + 1;	// アイテム数 (current 1件、左右3件、計7件表示)

	if( $paged === 1 ) $range += 3;			// 1ページ目は右に + 3件
	elseif( $paged === 2 ) $range += 2;		// 2ページ目は右に + 2件
	elseif( $paged === 3 ) $range += 1;		// 3ページ目は右に + 1件
	elseif( $paged === $pages ) $range += 3;	// 最終ページは左に + 3件
	elseif( $paged === $pages - 1 ) $range += 2;	// 後ろから2ページ目は左に + 2件
	elseif( $paged === $pages - 2 ) $range += 1;	// 後ろから3ページ目は左に + 1件

	if( $pages !== 1 ) {
?>
<div id="paging">
<nav>
<ul class="pagination">
<?php
		if( $paged > 1 ) {
?>
<li><a href="<?php echo get_pagenum_link( 1 ); ?>"><i>&laquo;</i></a></li>
<li><a href="<?php echo get_pagenum_link( $paged - 1 ); ?>"><i>&lsaquo;</i></a></li>
<?php
		}
		else {
?>
<li class="not-allow"><span><i>&laquo;</i></span></li>
<li class="not-allow"><span><i>&lsaquo;</i></span></li>
<?php
		}

		$paginate = array();
		for( $i = 1, $j = 1; $i <= $pages; $i++ ) {
			if( $pages !== 1 &&( !( $i >= $paged + $range + 1 || $i <= $paged - $range - 1 ) || $pages <= $showitems ) ) {
				if( $paged == $i ) {
					$paginate[] = '<li class="active"><span class="current">' . $i . '</span></li>';
				}
				else {
					$paginate[] = '<li><a href="' . get_pagenum_link( $i ) . '" class="inactive">' . $i . '</a></li>';
				}
			}
		}

		$item_max = count( $paginate );
		foreach( $paginate as $key => $val ) {
			if(
				( $item_max >= $showitems && ( $key < 1 || $key >= $item_max - 1 ) ) ||
				( $item_max >= $showitems - 1 && $pages >= $item_max / 2 && $key < 1 ) ||
				( $item_max >= $showitems - 1 && $pages <= $item_max / 2 && $key >= $item_max - 1 )
			) {
				echo str_replace( '<li>', '<li class="bothends">', $val );
			}
			else {
				echo $val;
			}
		}

		if( $paged < $pages ) {
?>
<li><a href="<?php echo get_pagenum_link( $paged + 1 ); ?>"><i>&rsaquo;</i></a></li>
<li><a href="<?php echo get_pagenum_link( $pages ); ?>"><i>&raquo;</i></a></li>
<?php
		}
		else {
?>
<li class="not-allow"><span><i>&rsaquo;</i></span></li>
<li class="not-allow"><span><i>&raquo;</i></span></li>
<?php
		}
?>
</ul>
</nav>
</div>
<?php
	}
}
endif;

/*---------------------------------------------------------------------------
 * 投稿・固定ページのページネーション (bootstrap version)
 *---------------------------------------------------------------------------*/
if( function_exists( 'thk_link_pages' ) === false ):
function thk_link_pages() {
	$wp_link_pages = wp_link_pages( array(
		'before'           => '',
		'after'            => '',
		'link_before'      => '',
		'link_after'       => '',
		'next_or_number'   => 'number',
		'separator'        => '|',
		'nextpagelink'     => '',
		'previouspagelink' => '',
		'pagelink'         => '%',
		'echo'             => 0
	) );

	if( !empty( $wp_link_pages ) ) {
		echo '<div id="paging"><nav><ul class="pagination">';

		$link_pages = explode( '|', $wp_link_pages );

		$range = 3;				// 左右に表示する件数
		$showitems = ( $range * 2 ) + 1;	// アイテム数 (current 1件、左右3件、計7件表示)
		$paged = ( get_query_var('page') ) ? (int)get_query_var('page') : 1;	// 今いるページ番号
		$max_page = count( $link_pages );	// 最終ページ番号

		// 0から始まる添字を1から始まるように、添字振り直し(ついでに、前後空白削除)
		foreach( $link_pages as $key => $val) {
			$link_pages[$key + 1] = trim( $val );
		}
		unset( $link_pages[0] );

		if( $paged === 1 ) $range += 3;			// 1ページ目は右に + 3件
		elseif( $paged === 2 ) $range += 2;			// 2ページ目は右に + 2件
		elseif( $paged === 3 ) $range += 1;			// 3ページ目は右に + 1件
		elseif( $paged === $max_page ) $range += 3;		// 最終ページは左に + 3件
		elseif( $paged === $max_page - 1 ) $range += 2;	// 後ろから2ページ目は左に + 2件
		elseif( $paged === $max_page - 2 ) $range += 1;	// 後ろから3ページ目は左に + 1件

		$paginate = array();
		foreach( $link_pages as $key => $val ) {
			if( $max_page !== 1 &&( !( $key >= $paged + $range + 1 || $key <= $paged - $range - 1 ) || $max_page <= $showitems ) ) {
				$paginate[$key] = $val;
			}
		}

		$prv_page = $paged - 1;	// 前のページ番号
		$nxt_page = $paged + 1;	// 次のページ番号

		if( $paged > 1 ) {
?>
<li><?php echo str_replace( '>' . 1 . '<', '><i>&laquo;</i><', $link_pages[1] ); ?></li>
<li><?php echo str_replace( '>' . $prv_page . '<', '><i>&lsaquo;</i><', $link_pages[$prv_page] ); ?></li>
<?php
		}
		else {
?>
<li class="not-allow"><span><i>&laquo;</i></span></li>
<li class="not-allow"><span><i>&lsaquo;</i></span></li>
<?php
		}

		foreach( $paginate as $key => $val) {
			$bothends = '';
			if(
				( $max_page >= $showitems && ( $key < 1 || $key >= $max_page - 1 ) ) ||
				( $max_page >= $showitems - 1 && $paged >= $max_page / 2 && $key < 1 ) ||
				( $max_page >= $showitems - 1 && $paged <= $max_page / 2 && $key >= $max_page - 1 )
			) {
				$bothends = ' class="bothends"';
			}

			if( is_numeric( $val ) === false ) {
				echo '<li'.$bothends.'>', $val, '</li>';
			}
			else {
				echo '<li class="active"><span class="current">', $val, '</span></li>';
			}
		}

		if( $paged < $max_page ) {
?>
<li><?php echo str_replace( '>' . $nxt_page . '<', '><i style="font-weight:bold">&rsaquo;</i><', $link_pages[$nxt_page] ); ?></li>
<li><?php echo str_replace( '>' . $max_page . '<', '><i style="font-weight:bold">&raquo;</i><', $link_pages[$max_page] ); ?></li>
<?php
		}
		else {
?>
<li class="not-allow"><span><i>&rsaquo;</i></span></li>
<li class="not-allow"><span><i>&raquo;</i></span></li>
<?php
		}

		echo '</ul></nav></div>';
	}
}
endif;

/*---------------------------------------------------------------------------
 * スクリプト類に勝手に入ってくるバージョン番号消す
 *---------------------------------------------------------------------------*/
if( function_exists( 'remove_url_version' ) === false ):
function remove_url_version( $arg ) {
	if( strpos( $arg, 'ver=' ) !== false ) {
		$arg = esc_url( remove_query_arg( 'ver', $arg ) );
	}
	return $arg;
}
add_filter( 'style_loader_src', 'remove_url_version', 99 );
add_filter( 'script_loader_src', 'remove_url_version', 99 );
endif;

/*---------------------------------------------------------------------------
 * 改行するタイプの抜粋
 *---------------------------------------------------------------------------*/
if( function_exists( 'thk_excerpt' ) === false ):
function thk_excerpt( $content = null ) {
	global $wpthk, $more;
	$more = true;	// more タグ無視で指定した文字数まで出力( more で切る場合は false に)

	$length = $wpthk['excerpt_length'];	// 抜粋文字数(p、br タグを含む文字数)
	if( is_int( $length ) === false ) {
		$length = 120;
	}

	if( empty( $content ) )	{
		if( has_excerpt() === true && isset( $wpthk['excerpt_priority'] )  ) {
			$content = apply_filters( 'the_excerpt', get_the_excerpt('') );
		}
		else {
			$content = apply_filters( 'the_content', get_the_content('') );
		}
	}

	// 改行・タブ削除
	$content = preg_replace( '/\t|\r|\n/', '', trim( $content ) );
	// 連続スペースを1つに
	$content = preg_replace('/\s{2,}/', ' ', $content);

	// </li>タグを<br>に変換（後から<br />に再度変換する）
	$content = str_replace( '</li>', '<br>', $content );

	// <p><br>タグは残して、他のタグを削除
	$content = preg_replace( '/<script.*<\/script>/', '', $content ) ;
	$content = strip_tags( $content, '<p><br>' );

	$content = str_replace( array( '/ ', ' /' ), '/', $content );
	$content = str_replace( array( ' >', '> ' ), '>', $content );
	$content = str_replace( array( '< ', ' <' ), '<', $content );

	// <p> についてる class や style 等を削除
	$content = preg_replace( '/<p[^>]+?>/', '<p>', $content );

	// <img ～>などを<p>で囲ってた場合、<p></p> の形で残るので削除
	$content = str_replace( '<p></p>', '', $content );

	$content = mb_substr( $content, 0, $length );	//文字列を指定した長さで切り取る

	// <p><br>タグの途中で文字列が切れた場合、中途半端に残ったタグを < が出てくるまで後ろから1文字づつ削除
	while( strrpos( $content, '<' ) > strrpos( $content, '>' ) ) {
		$content = substr( $content, 0, -1 );
	}
	// 最後が<br>だったら削除
	$content = str_replace( '<br/>', '<br />', $content );
	$content = str_replace( '<br>', '<br />', $content );
	$content = str_replace( '<p><br />', '<p>', $content );
	$content = str_replace( '<br /></p>', '</p>', $content );
	if( substr( $content, -6 ) === '<br />' ) {
		$content = substr( $content, 0, -6 );
	}

	// 三点リーダー付ける
	$three_point = $length > 0 && mb_strlen( $content ) >= $length ? ' ...' : '';

	if( substr( $content, -4 ) === '</p>' ) {
		$content = substr( $content, 0, -4 );
		$content .= $three_point . '</p>';
	}
	else {
		$content .= $three_point;
	}

	// <p>タグの終了タグが無くなってた場合は終了タグを補完
	if( strrpos( $content, '<p>' ) > strrpos( $content, '</p>' ) ) {
		$content .= '</p>';
	}
	return $content;
}
endif;

/*---------------------------------------------------------------------------
 * 改行しないタイプの抜粋
 * wp_excerpt() だとゴミが混じるので改良版
 *---------------------------------------------------------------------------*/
if( function_exists( 'thk_excerpt_no_break' ) === false ):
function thk_excerpt_no_break( $length, $content = null ) {
	global $wpthk, $post;
	if( is_int( $length ) === false ) {
		$length = 70;
	}
	if( has_excerpt() === true && isset( $wpthk['excerpt_priority'] )  ) {
		$content = apply_filters( 'the_excerpt', get_the_excerpt('') );
	}
	else {
		$content = apply_filters( 'the_content', get_the_content('') );
	}
	$content = preg_replace( '/<script.*<\/script>/', '', $content ) ;
	$content = strip_tags( $content );
	$content = strip_shortcodes( $content );
	$content = preg_replace( '/((?:https?|ftp|ed2k):\/\/[-_.!~*\'()a-zA-Z0-9%?@&=+,#$:;\/]+)/', '', $content );

	$three_point = $length > 0 && mb_strlen( $content ) >= $length ? ' ...' : '';

	return wp_html_excerpt( $content, $length, $three_point);
}
endif;

/*---------------------------------------------------------------------------
 * ヘッダーに canonical 追加
 *---------------------------------------------------------------------------*/
if( function_exists( 'thk_rel_canonical' ) === false && is_admin() === false ):
function thk_rel_canonical() {
	global $paged, $page, $wp_query;

	$canonical_url = null;

	switch( true ) {
		case is_home():
			if( get_option('page_for_posts') ){
				$canonical_url = canonical_paged_uri( get_page_link( get_option('page_for_posts') ) );
			}
			else {
				$canonical_url = canonical_paged_uri( THK_HOME_URL );
			}
			break;
		case is_front_page():
			$canonical_url = canonical_paged_uri( THK_HOME_URL );
			break;
		case is_category():
			$canonical_url = canonical_paged_uri( get_category_link( get_query_var('cat') ) );
			break;
		case is_tag():
			$canonical_url = canonical_paged_uri( get_tag_link( get_query_var('tag_id') ) );
			break;
		case is_author():
			$canonical_url = canonical_paged_uri( get_author_posts_url( get_query_var( 'author' ), get_query_var( 'author_name' ) ) );
			break;
		case is_year():
			$canonical_url = canonical_paged_uri( get_year_link( get_the_time('Y') ) );
			break;
		case is_month():
			$canonical_url = canonical_paged_uri( get_month_link( get_the_time('Y'), get_the_time('m') ) );
			break;
		case is_day():
			$canonical_url = canonical_paged_uri( get_day_link( get_the_time('Y'), get_the_time('m'), get_the_time('d') ) );
			break;
		case is_post_type_archive() :
			$post_type = get_query_var( 'post_type' );
			if( is_array( $post_type ) === true ) { $post_type = reset( $post_type ); }
			$canonical_url = canonical_paged_uri( get_post_type_archive_link( $post_type ) );
			break;
		default:
			break;
	}

	if( $canonical_url !== null ):
?>
<link rel="canonical" href="<?php echo esc_url( $canonical_url ); ?>" />
<?php
	endif;
}
endif;

if( function_exists( 'canonical_paged_uri' ) === false ):
function canonical_paged_uri( $canonical_url ) {
	global $paged, $page, $wp_rewrite;

	if( $paged >= 2 || $page >= 2 ) {
		// パーマリンクが設定されてる場合
		if( is_object( $wp_rewrite ) === true && $wp_rewrite->using_permalinks() ) {
			if( substr( $canonical_url, -1 ) === '/' ) {
				$canonical_url .= 'page/' . max( $paged, $page ) . '/';
			}
			else {
				$canonical_url .= '/page/' . max( $paged, $page );
			}
		}
		// パーマリンクがデフォルト設定(動的URL)の場合
		else {
			if( is_front_page() === true ) {
				$canonical_url .= '?paged=' . max( $paged, $page );
			}
			else {
				$canonical_url .= '&amp;paged=' . max( $paged, $page );
			}
		}
	}
	return $canonical_url;
}
endif;

/*---------------------------------------------------------------------------
 * ヘッダーに next / prev 追加
 *---------------------------------------------------------------------------*/
if( function_exists( 'thk_rel_next_prev' ) === false && is_admin() === false ):
function thk_rel_next_prev() {
	global $paged, $post, $wp_query;

	if( is_singular() === false ) {
		$max_page = (int)$wp_query->max_num_pages;

		if( empty( $paged ) ) {
			$paged = 1;
		}
		$nextpage = (int)$paged + 1;
		if( $nextpage <= $max_page ) {
?>
<link rel="next" href="<?php echo next_posts( $max_page, false ); ?>" />
<?php
		}
		if( $paged > 1 ) {
?>
<link rel="prev" href="<?php echo previous_posts( false ); ?>" />
<?php
		}
	}
	else {
		$pages = count( explode('<!--nextpage-->', $post->post_content) );

		if( $pages > 1 ) {
			$prev = singular_nextpage_link( 'prev', $pages );
			$next = singular_nextpage_link( 'next', $pages );

			if( !empty( $prev ) ) {
?>
<link rel="prev" href="<?php echo $prev; ?>" />
<?php
			}
			if( !empty( $next ) ) {
?>
<link rel="next" href="<?php echo $next; ?>" />
<?php
			}
		}
	}
}
endif;

/*---------------------------------------------------------------------------
 * 投稿・固定ページを <!--nextpage--> で分割した場合の next / prev 追加関数
 *---------------------------------------------------------------------------*/
if( function_exists( 'singular_nextpage_link' ) === false && is_admin() === false ):
function singular_nextpage_link( $rel = 'prev', $pages ) {
	global $post, $page;
	$url = '';

	if( $pages > 1 ) {
		$i = $rel === 'prev' ? $page - 1 : $page + 1;
		if( $i >= 0 && $i <= $pages ) {
			if( 1 === $i ) {
				if( $rel === 'prev' ) {
					$url = get_permalink();
				}
				else {
					$url = trailingslashit( get_permalink() ) . user_trailingslashit( $i + 1, 'single_paged' );
				}
			}
			else {
				$opt = get_option('permalink_structure');
				if( empty( $opt ) || in_array( $post->post_status, array('draft', 'pending') ) ) {
					$url = add_query_arg( 'page', $i, get_permalink() );
				}
				else {
					$url = trailingslashit( get_permalink() ) . user_trailingslashit( $i, 'single_paged' );
				}
			}
		}
	}
	return $url;
}
endif;

/*---------------------------------------------------------------------------
 * サイドバーのカラム数を決めて、サイドバーを呼び出す
 *---------------------------------------------------------------------------*/
if( function_exists( 'thk_call_sidebar' ) === false ):
function thk_call_sidebar() {
	global $wpthk;

	// 1カラムの時はサイドバー表示しない
	if( $wpthk['column_style'] === '1column' ) return;

	if( $wpthk['column_style'] === '3column' ) {
		if( isset( $wpthk['column3_reverse'] ) ) {
			thk_sidebar();
		}
		else {
			thk_sidebar('col3');
		}
?>
</div><!--/#field-->
<?php
		if( !isset( $wpthk['column3_reverse'] ) ) {
			thk_sidebar();
		}
		else {
			thk_sidebar('col3');
		}
	}
	else {
		thk_sidebar();
	}
}
endif;

/*---------------------------------------------------------------------------
 * ヘッダー、サイドバー、その他の書き換え
 *---------------------------------------------------------------------------*/
// header
if( function_exists( 'thk_head' ) === false ):
function thk_head() {
	if( is_feed() === true ) return;

	global $wpthk;
	remove_action( 'wp_head', 'index_rel_link' );
	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'feed_links', 2 );
	remove_action( 'wp_head', 'feed_links_extra', 3 );
	remove_action( 'wp_head', 'wp_generator' );
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );

	// canonical と shortlink は SEO で重要なので meta tag の上位に表示されるように、いったん消す
	remove_action( 'wp_head', 'rel_canonical' );
	remove_action( 'wp_head', 'wp_shortlink_wp_head' );

	// 絵文字スクリプト (非同期 の Javascript を優先させるため、wp_head の後に挿入するのでいったん消して再挿入)
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles', 10 );

	if( !isset( $wpthk['thk_emoji_disable'] ) ) {
		add_action( 'wp_head', 'print_emoji_detection_script', 109 );
		add_action( 'wp_head', 'print_emoji_styles', 110 );
	}

	// embed
	if( isset( $wpthk['thk_embed_disable'] ) ) {
		remove_action( 'wp_head', 'rest_output_link_wp_head' );
		remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
		remove_action( 'wp_head', 'wp_oembed_add_host_js' );
	}

	ob_start();
	wp_head();
	$head = ob_get_clean();

	$head = str_replace( "'", '"', $head );
	$head = str_replace( array( ' type="text/javascript"', ' type="text/css"' ), '', $head );

	if( $wpthk['html_compress'] !== 'none' ) {
		$head = preg_replace( '/\n\s+</', "\n".'<', $head );
		$head = str_replace( "\t", '', $head );
		$head = thk_html_format( $head );
	}
	echo $head;

	if( isset( $wpthk['buffering_enable'] ) ) {
		if( ob_get_level() < 1 ) ob_start();
		if( ob_get_length() !== false ) {
		       	ob_flush();
		       	flush();
			ob_end_flush();
		}
	}
}
endif;

// sidebar
if( function_exists( 'thk_sidebar' ) === false ):
function thk_sidebar( $col=null ) {
	if( is_feed() === true ) return;

	global $wpthk;

	if( isset( $wpthk['buffering_enable'] ) ) {
		if( ob_get_level() < 1 ) ob_start();
		if( ob_get_length() !== false ) {
		       	ob_flush();
		       	flush();
			ob_end_flush();
		}
	}

	ob_start();
	if( empty( $col ) ) {
		get_sidebar();
	}
	else {
		// 3カラム用サイドバー
		get_sidebar('left');
	}
	$side = ob_get_clean();

	if( isset( $wpthk['preview_adsense_visible'] ) && ( is_preview() === true || is_customize_preview() === true ) ) {
		// サイドバーに Adsense があった場合、プレビュー画面で消す
		$side = preg_replace( '/<script.+?adsbygoogle\.js\"><\/script>.+?adsbygoogle.+?<\/script>/sm', '', $side );
	}

	if( $wpthk['html_compress'] !== 'none' ) {
		$side = thk_html_format( $side );
	}
	echo $side;

	if( isset( $wpthk['buffering_enable'] ) ) {
		if( ob_get_level() < 1 ) ob_start();
		if( ob_get_length() !== false ) {
		       	ob_flush();
		       	flush();
			ob_end_flush();
		}
	}
}
endif;

// footer
if( function_exists( 'thk_footer' ) === false ):
function thk_footer() {
	if( is_feed() === true ) return;

	global $wpthk;
	if( ob_get_level() > 0 ) ob_end_flush();
	ob_start();
	get_footer();
	$foot = ob_get_clean();

	$foot = str_replace( "'", '"', $foot );
	$foot = str_replace( ' type="text/javascript"', '', $foot );

	if( $wpthk['html_compress'] !== 'none' ) {
		$foot = str_replace( "\t", '', $foot );
		$foot = thk_html_format( $foot );
	}
	echo $foot;
}
endif;

// common
if( function_exists( 'thk_html_format' ) === false && is_admin() === false ):
function thk_html_format( $contents ) {
	global $wpthk;

	// 連続改行削除
	$contents = preg_replace( '/(\n|\r|\r\n)+/us',"\n", $contents );
	// 行頭の余計な空白削除
	$contents = preg_replace( '/\n+\s*</', "\n".'<', $contents );

	// タグ間の余計な空白や改行の削除
	if( $wpthk['html_compress'] === 'low' ) {
		$contents = preg_replace( '/>[\t| ]+?</', '><', $contents );
		$contents = preg_replace( '/\n+<\/([^b|^h])/', '</$1', $contents );
	}
	elseif( $wpthk['html_compress'] === 'high' ) {
		$contents = preg_replace( '/>\s*?</', '><', $contents );
	}

	return $contents;
}
add_filter( 'wp_nav_menu', 'thk_html_format', 10, 2 );
endif;

/*---------------------------------------------------------------------------
 * スクリプト書き換え
 *---------------------------------------------------------------------------*/
add_filter( 'script_loader_tag', function( $ret ) {
	global $wpthk;

	if(
		is_feed() === true ||
		is_admin() === true ||
		is_customize_preview() === true
	) return $ret;

	$ret = str_replace( "'", '"', $ret );

	// dummy.js
	if( stripos( $ret, 'wpthk.dummy-' ) ) {
		$ret = null;
	}

	// jquery bind
	if( isset( $wpthk['jquery_load'] ) && isset( $wpthk['jquery_migrate'] ) && $wpthk['jquery_migrate'] !== 'not' ) {
		if( stripos( $ret, 'jquery/jquery.js' ) ) {
			if( $wpthk['jquery_migrate'] === 'migrate' ) {
				$ret = '<script src="' . TDEL . '/js/jquery.bind.min.js"></script>' . "\n";
			}
			else {
				$ret = '<script src="' . TDEL . '/js/jquery.wpthk.min.js?v=' . $_SERVER['REQUEST_TIME'] . '"></script>' . "\n";
			}
		}
		if( stripos( $ret, 'jquery/jquery-migrate.min.js' ) ) {
			$ret = null;
		}
	}

	// bootstrap.min.js
	if( isset( $wpthk['jquery_load'] ) && $wpthk['bootstrap_js_load_type'] !== 'none' ) {
		if( stripos( $ret, 'bootstrap.min.js' ) ) {
			if( $wpthk['bootstrap_js_load_type'] === 'sync' ) {
				$bootstrap_js = '';
			}
			elseif( $wpthk['bootstrap_js_load_type'] === 'asyncdefer' ) {
				$bootstrap_js = ' async defer';
			}
			else {
				$bootstrap_js = ' ' . $wpthk['bootstrap_js_load_type'];
			}
			$ret = str_replace( '><', $bootstrap_js . '><', $ret );
		}
	}

	// wpthk.async.min.js
	if( stripos( $ret, 'wpthk.async.min.js' ) ) {
		$ret = str_replace( '><', ' async defer><', $ret );
	}

	// jquery defer
	if( isset( $wpthk['jquery_defer'] ) && stripos( $ret, 'async' ) === false && stripos( $ret, 'defer' ) === false ) {
		if( stripos( $ret, '/js/jquery.wpthk.min.js' ) ) {
			$ret = str_replace( '><', ' async defer><', $ret );
		}
		elseif( stripos( $ret, '/js/jquery.bind.min.js' ) ) {
			$ret = str_replace( '><', ' async defer><', $ret );
		}
		else {
			$ret = str_replace( '><', ' defer><', $ret );
		}
	}
	elseif( stripos( $ret, '/js/wpthk.min.js' ) ) {
		$ret = str_replace( '><', ' async defer><', $ret );
	}

	$ret = str_replace( array( 'http:', 'https:' ), '', $ret );

	return str_replace( '  ', ' ', $ret );
} );

/*---------------------------------------------------------------------------
 * スタイルシート書き換え
 *---------------------------------------------------------------------------*/
add_filter( 'style_loader_tag', function( $ret ) {
	global $wpthk;

	if(
		is_feed() === true ||
		is_admin() === true ||
		is_customize_preview() === true
	) return $ret;

	$ret = str_replace( array( 'http:', 'https:' ), '', $ret );
	$ret = str_replace( "'", '"', $ret );

	if( strpos( $ret, 'id="async-css"' ) !== false ) {
		$ret = '<noscript>' . trim( $ret ) . '</noscript>' . "\n";
	}

	if( isset( $wpthk['css_to_style'] ) ) {
		if( stripos( $ret, 'id="wpthk-css"' ) !== false || stripos( $ret, 'id="wpthkch-css"' ) !== false ) {
			$ret = null;
		}
	}

	if( strpos( $ret, 'id="wpthk2-css"' ) !== false || strpos( $ret, 'id="wpthk3-css"' ) !== false ) {
		$ret = null;
	}

	return str_replace( '  ', ' ', $ret );
} );

/*---------------------------------------------------------------------------
 * Lazy Load 用の img タグ置換
 *---------------------------------------------------------------------------*/
if( function_exists( 'add_lazyload' ) === false ):
function add_lazyload( $content ) {
	if( is_feed() === true || wp_is_mobile() ) {
		return $content;
	}

	global $wpthk;

	if( isset( $wpthk['lazyload_crawler'] ) ) {
		if( is_crawler( isset( $_SERVER['HTTP_USER_AGENT'] ) ? $_SERVER['HTTP_USER_AGENT'] : null ) === true ) {
			return $content;
		}
	}

	if( stripos( $content, 'data-lazy=' ) === false && stripos( $content, 'data-original=' ) === false  ) {
		$content = preg_replace(
			'/<img ([^>]*?)src=[\'\"]([^>]+?(\.jpg|\.png|\.gif))[\'\"]([^>]+?)>/i',
			'<img src="' . TDEL . '/images/trans.png" data-lazy="1" data-original="${2}"${1}${4}><noscript><img src="${2}"${1}${4}></noscript>',
			$content
		);
		$content = preg_replace(
			'/(<img [^>]*?height=[\'\"]([^>]+?)[\'\"])([^>]+?)><noscript>/i',
			'${1} style="height:${2}px"${3}><noscript>',
			$content
		);
	}

	return $content;
}
endif;

/*---------------------------------------------------------------------------
 * Tosrus 用の a タグ置換
 *---------------------------------------------------------------------------*/
if( function_exists( 'add_tosrus' ) === false ):
function add_tosrus( $content ) {
	if( is_feed() === true ) return $content;

	if( stripos( $content, 'data-rel="tosrus"' ) !== false ) {
		return $content;
	}

	$content = preg_replace(
		'/(<a[^>]+?href[^>]+?(\.jpg|\.png|\.gif)[\'\"][^>]*?)>\s*(<img[^>]+?(alt=[\'\"](.*?)[\'\"]|[^>]+?)+[^>]+?>)\s*<\/a>/i',
		'${1} data-rel="tosrus" data-title="${5}">${3}</a>',
		$content
	);

	return $content;
}
endif;

/*---------------------------------------------------------------------------
 * Lightcase 用の a タグ置換
 *---------------------------------------------------------------------------*/
if( function_exists( 'add_lightcase' ) === false ):
function add_lightcase( $content ) {
	if( is_feed() === true ) return $content;

	if( stripos( $content, 'data-rel="lightcase"' ) !== false ) {
		return $content;
	}

	$content = preg_replace(
		'/(<a[^>]+?href[^>]+?(\.jpg|\.png|\.gif)[\'\"][^>]*?)>\s*(<img[^>]+?>)\s*<\/a>/i',
		'${1} data-rel="lightcase:myCollection">${3}</a>',
		$content
	);

	return $content;
}
endif;

/*---------------------------------------------------------------------------
 * Fluidbox 用の a タグ置換
 *---------------------------------------------------------------------------*/
if( function_exists( 'add_fluidbox' ) === false ):
function add_fluidbox( $content ) {
	if( is_feed() === true ) return $content;

	if( stripos( $content, 'data-fluidbox' ) !== false ) {
		return $content;
	}

	$content = preg_replace(
		'/(<a[^>]+?href[^>]+?(\.jpg|\.png|\.gif)[\'\"][^>]*?)>\s*(<img[^>]+?>)\s*<\/a>/i',
		'${1} data-fluidbox>${3}</a>',
		$content
	);

	return $content;
}
endif;

/*---------------------------------------------------------------------------
 * クローラー判定 ( Lazy Load 用 )
 *---------------------------------------------------------------------------*/
if( function_exists( 'is_crawler' ) === false ) :
function is_crawler( $user_agent ) {
	$crawler = array(
		'Y!J',			// Yahoo!
		'Slurp',		// Yahoo!
		'Googlebot',		// Google
		'adsence-Google',	// Google
		'Mediapartners',	// Google
		'msnbot',		// Microsoft
		'bingbot',		// Microsoft
		'Yeti/',		// Never
		'NaverBot',		// Never
		'Baidu',		// Baidu (百度)
		'ichiro',		// goo
		'Hatena',		// Hatena
		'YPBot',		// イエローページ
		'BecomeBot',		// Become.com (アメリカ)
		'YandexBot',		// ロシアの画像検索
		'heritr',		// オープンソース
		'Spider',
		'crawl'
	);

	foreach( $crawler as $val ) {
		if( stripos( $user_agent, $val ) ) {
			return true;
		}
	}
	return false;
}
endif;

/*---------------------------------------------------------------------------
 * 「記事を読む」の後ろに短いタイトル追加
 *---------------------------------------------------------------------------*/
if( function_exists( 'read_more_title_add' ) === false ):
function read_more_title_add( $word = '' ) {
	global $wpthk;
	$more_title = the_title_attribute('echo=0');

	$length = $wpthk['short_title_length']; // タイトルの抜粋文字数
	if( is_int( $length ) === false ) {
		$length = 16;
	}
	if( mb_strlen( $more_title ) > $length ) {
		$more_title = mb_strimwidth( $more_title, 0, $length ) . ' ...';
	}
	return $word . ' <i class="fa fa-angle-double-right"></i>&nbsp; ' . $more_title;
}
endif;

/*---------------------------------------------------------------------------
 * more タグ除去（オリジナルのものに変えるので要らない）
 *---------------------------------------------------------------------------*/
add_filter( 'the_content_more_link', function( $more ) {
	return null;
} );

/*---------------------------------------------------------------------------
 * インラインフレーム (Youtube とか Google Map 等) の responsive 対応
 * 外部リンクに external や icon 追加
 *---------------------------------------------------------------------------*/
add_filter( 'the_content', function( $contents ) {
	global $wpthk;

 	/***
	 * インラインフレーム
	 ***/
	if( !empty( $contents ) ) {
		// インラインフレームで、且つ embed を含むものを探す
		$i_frame = 'i' . 'frame';
		preg_match_all( "/<\s*${i_frame}[^>]+?embed[^>]+?>[^<]*?<\/${i_frame}>/i", $contents, $frame_array );

		// 置換する
		foreach( array_unique( $frame_array[0] ) as $value ) {
			$replaced = '';

			// WordPress だと、ほぼ自動で p で囲まれるため、div でなく、あえて span (display:block) を使う
			if( stripos( $value, 'youtube.com' ) !== false || stripos( $value, '.google.com/maps' ) !== false ) {
				$replaced = str_replace( "<$i_frame", "<span class=\"i-video\"><$i_frame", $value );
				$replaced = str_replace( "</$i_frame>", "</$i_frame></span>", $replaced );
			}
			else {
				$replaced = str_replace( "<$i_frame", "<span class=\"i-embed\"><$i_frame", $value );
				$replaced = str_replace( "</$i_frame>", "</$i_frame></span>", $replaced );
			}
			$contents = str_replace( $value, $replaced, $contents );
		}

	 	/***
		 * 外部リンクに external や icon 追加
		 ***/
		if(
			isset( $wpthk['add_target_blank'] ) ||
			isset( $wpthk['add_rel_nofollow'] ) ||
			isset( $wpthk['add_class_external'] ) ||
			isset( $wpthk['add_external_icon'] )
		) {
			preg_match_all( '/<a[^>]+?href[^>]+?>.+?<\/a>/i', $contents, $link_array );
			//$my_url = preg_quote( rtrim( THK_HOME_URL, '/' ) . '/', '/' );

			foreach( array_unique( $link_array[0] ) as $link ) {
				$replaced = '';
				$last = '';

				$compare = str_replace( array( "'", '"', ' ' ), '', $link );

				if( stripos( $compare, '://' ) === false && stripos( $compare, 'href=//' ) === false ) continue;
				if( stripos( $compare, '\\' ) !== false ) continue;

				//if( !preg_match( '/href=[\'|\"]?\s?' . $my_url . '[^>]+?[\'|\"]/i', $link ) ) {
				if( stripos( $compare, 'href=' . THK_HOME_URL ) === false ) {
					$atag = preg_split( '/>/i', $link );
					$atag = array_filter( $atag );

					// target="_blank"
					if( isset( $wpthk['add_target_blank'] ) && stripos( $atag[0], 'target' ) === false ) {
						$atag[0] .= ' target="_blank"';
					}
					// rel="nofollow"
					if( isset( $wpthk['add_rel_nofollow'] ) && stripos( $atag[0], 'nofollow' ) === false ) {
						$atag[0] .= ' rel="nofollow"';
					}
					// class="external"
					if( isset( $wpthk['add_class_external'] ) ) {
						$atag[0] .= ' class="external"';
					}

					foreach( $atag as $key => $value ) $atag[$key] = $value . '>';

					// external icon
					if( isset( $wpthk['add_external_icon'] ) ) {
						$last = end( $atag );
						$last .= '<span class="ext_icon"></span>';
						array_pop( $atag );

					}

					foreach( $atag as $value ) $replaced .= $value;
					$replaced .= $last;

					$contents = str_replace( $link, $replaced, $contents );

					if( isset( $wpthk['add_external_icon'] ) ) {
						// img の時はアイコン消す（class="external" は残す）
						$contents = preg_replace(
							'/(<a[^>]+?href[^>]+?external[^>]+?>\s*?<\s?img[^>]+?src[^>]+?><\/a>)<span class=\"ext_icon\"><\/span>/is',
							'$1', $contents
						);
					}
				}
			}
		}
	}
	return $contents;
}, 12 );

/*---------------------------------------------------------------------------
 * カスタマイズで hentry 削除にチェックがついてたら hentry 削除
 *---------------------------------------------------------------------------*/
if( function_exists( 'thk_remove_hentry' ) === false ):
function thk_remove_hentry( $ret ) {
	$ret = array_diff( $ret, array('hentry') );
	return $ret;
}
endif;

/*---------------------------------------------------------------------------
 * 全体イメージの CSS ファイル名取得
 *---------------------------------------------------------------------------*/
if( function_exists( 'get_overall_image' ) === false ):
function get_overall_image() {
	$overall = get_theme_mod( 'overall_image', 'white' );
	if( $overall !== null && $overall !== 'white' ) {
		$overall = 'styles/style-' . $overall . '.css';
	}
	else {
		$overall = 'style.css';
	}
	return $overall;
}
endif;

/*---------------------------------------------------------------------------
 * CSS を HTML に直接埋め込む場合 (パス変換済みの CSS を require する)
 * または、テンプレートごとにカラム数が違う場合
 *---------------------------------------------------------------------------*/
if( function_exists( 'thk_direct_style' ) === false ):
function thk_direct_style( $require_file ) {
	if( file_exists( $require_file ) === false ) return;

	ob_start();
	require( $require_file );
	$css = ob_get_clean();

	return $css;
}
endif;

/*---------------------------------------------------------------------------
 * サイトマップ用インラインスタイル
 *---------------------------------------------------------------------------*/
if( function_exists( 'thk_sitemap_inline_style' ) === false ):
function thk_sitemap_inline_style() {
	return <<< STYLE
#sitemap .sitemap-home {
	margin: 0 0 0 20px;
}
#sitemap ul {
	margin: 0 0 30px 0x;
}
#sitemap ul ul,
#sitemap ul ul ul,
#sitemap ul ul ul ul {
	margin: 0 0 0 3px;
	padding: 0;
}
#sitemap li {
	line-height: 1.7;
	margin-left: 10px;
	padding: 0 0 0 22px;
	border-left: 1px solid #000;
	list-style-type: none;
}
#sitemap li:before {
	content: "-----";
	font-size: 14px; font-size: 1.4rem;
	margin-left: -23px;
	margin-right: 12px;
	letter-spacing: -3px;
}
#sitemap .sitemap-home a,
#sitemap li a {
	text-decoration: none;
}
STYLE;
}
endif;

/*---------------------------------------------------------------------------
 * 1週間に1度 SNS のカウントキャッシュを全クリア ( transient に登録)
 *---------------------------------------------------------------------------*/
if( function_exists( 'set_transient_sns_count_cache_weekly_cleanup' ) === false ):
function set_transient_sns_count_cache_weekly_cleanup() {
	if( get_transient( 'sns_count_cache_weekly_cleanup' ) === false ) {
		global $wpdb;

		$wpdb->query( "DELETE FROM `$wpdb->options` WHERE `option_name` LIKE ('_transient_wpthk-sns-%')" );
		delete_transient( 'sns_count_cache_weekly_cleanup' );
		set_transient( 'sns_count_cache_weekly_cleanup', 1, WEEK_IN_SECONDS );

		sns_count_cache_cleanup( false, false, true );
	}
}
endif;

/*---------------------------------------------------------------------------
 * SNS のカウントキャッシュ削除処理
 *---------------------------------------------------------------------------*/
/* 削除処理 */
if( function_exists( 'sns_count_cache_cleanup' ) === false ):
function sns_count_cache_cleanup( $rm_dir = false, $del_transient = false, $weekly = true ) {
	require_once( INC . 'optimize.php' );
	global $wp_filesystem;

	$target = $weekly === true ? get_theme_mod( 'sns_count_weekly_cleanup', 'dust' ) : '';

	$filesystem = new thk_filesystem();
	if( $filesystem->init_filesystem( site_url() ) === false ) return false;

	$wp_upload_dir = wp_upload_dir();
	$cache_dir = $wp_upload_dir['basedir'] . '/wpthk-sns/';

	if( $wp_filesystem->is_dir( $cache_dir ) === true ) {
		if( $rm_dir === true ) {
			// ディレクトリごと消す場合
			if( $wp_filesystem->delete( $cache_dir, true ) === false ) {
				$result = new WP_Error( 'rmdir failed', __( 'Could not delete cache directory.', 'wpthk' ), $cache_dir );
				thk_error_msg( $result );
			}
		}
		else {
			// ファイルだけ消す場合
			$dirlist = $wp_filesystem->dirlist( $cache_dir );

			if( $target === 'dust' ) {
				// 明らかなゴミだけ削除する場合
				foreach( (array)$dirlist as $filename => $fileinfo ) {
					$size = filesize( $cache_dir . $filename );
					if( ctype_xdigit( $filename ) === false || strlen( $filename ) !== 32 || $size < 14 || $size > 8200 ) {
						$wp_filesystem->delete( $cache_dir . $filename );
					}
				}
			}
			elseif( $target === 'all' ) {
				// 全ファイル削除する場合
				foreach( (array)$dirlist as $filename => $fileinfo ) {
					$wp_filesystem->delete( $cache_dir . $filename );
				}
			}
		}
	}

	// transient も全部消す場合
	if( $del_transient === true || $target === 'all' ) {
		global $wpdb;
		$wpdb->query( "DELETE FROM `$wpdb->options` WHERE `option_name` LIKE ('_transient_wpthk-sns-%')" );
		delete_transient( 'sns_count_cache_weekly_cleanup' );
	}
}
add_action( 'sns_count_cache_weekly_cleanup', 'sns_count_cache_cleanup' );
endif;

/*---------------------------------------------------------------------------
 * SNS カウントキャッシュの transient 登録
 *---------------------------------------------------------------------------*/
/* 空ファイル作成 (template_redirect に add_filter) */
if( function_exists( 'touch_sns_count_cache' ) === false ):
function touch_sns_count_cache() {
	$url = is_front_page() === true ? THK_HOME_URL : get_permalink();
	$sns = new sns_cache();
	$sns->touch_sns_count_cache( esc_url( $url ) );
}
endif;

/* transient 登録 (shutdown に add_filter) */
if( function_exists( 'set_transient_sns_count_cache' ) === false ):
function set_transient_sns_count_cache() {
	$url = is_front_page() === true ? THK_HOME_URL : get_permalink();
	$sns = new sns_cache();
	$sns->set_transient_sns_count_cache( 'sns_count_cache', esc_url( $url ) );
}
endif;

/* カウント数取得 (shutdown に add_filter) */
add_action( 'sns_count_cache', function( $url = null ){
	$sns = new sns_cache();
	$sns->create_sns_cache( esc_url( $url ) );
} );

/*---------------------------------------------------------------------------
 * Feedly カウントキャッシュの transient 登録
 *---------------------------------------------------------------------------*/
/* 空ファイル作成 (template_redirect に add_filter) */
if( function_exists( 'touch_feedly_cache' ) === false ):
function touch_feedly_cache() {
	$sns = new sns_cache();
	$sns->touch_sns_count_cache( esc_url( get_bloginfo( 'rss2_url' ) ) );
}
endif;

/* transient 登録 (shutdown に add_filter) */
if( function_exists( 'transient_register_feedly_cache' ) === false ):
function transient_register_feedly_cache() {
	$sns = new sns_cache();
	$sns->set_transient_sns_count_cache( 'feedly_count_cache', esc_url( get_bloginfo( 'rss2_url' ) ) );
}
endif;

/* カウント数取得 (shutdown に add_filter) */
add_action( 'feedly_count_cache', function( $url = null ){
	$sns = new sns_cache();
	$sns->create_feedly_cache();
} );

/*---------------------------------------------------------------------------
 * SNS カウントキャッシュの中身取得 (初回と失敗時は ajax で取得)
 *---------------------------------------------------------------------------*/
if( function_exists( 'thk_get_sns_count_cache' ) === false ):
function thk_get_sns_count_cache() {
	$id_cnt = array( 'f' => '', 'g' => '', 'h' => '', 'l' => '', 'p' => '' );

	foreach( $id_cnt as $key => $val ) {
		$id_cnt[$key] = '<i class="fa fa-spinner fa-spin"></i>';
	}

	$url = is_front_page() === true ? esc_url( THK_HOME_URL ) : esc_url( get_permalink() );

	$wp_upload_dir = wp_upload_dir();
	$cache_dir = $wp_upload_dir['basedir'] . '/wpthk-sns/';
	$sns_count_cache = $cache_dir . md5( $url );

	if( file_exists( $sns_count_cache ) === true ) {
		global $wp_filesystem;

		$cache = '';
		if( method_exists( $wp_filesystem, 'get_contents' ) === true ) {
			$cache = $wp_filesystem->get_contents( $sns_count_cache );
		}

		if( !empty( $cache ) && strpos( $cache, $url ) !== false ) {
			$ids = explode( "\n", $cache );
			array_shift( $ids );
			foreach( (array)$ids as $value ) {
				foreach( (array)$id_cnt as $key => $val ) {
					if( strpos( $value, $key . ':' ) !== false ) {
						$value = trim( $value, $key . ':' );
						if( ctype_digit( $value ) === true ) {
							$id_cnt[$key] = $value;
						}
					}
				}
			}
		}
	}
	return $id_cnt;
}
endif;

/*---------------------------------------------------------------------------
 * Feedly カウントキャッシュの中身取得 (初回と失敗時は ajax で取得)
 *---------------------------------------------------------------------------*/
if( function_exists( 'thk_get_feedly_count_cache' ) === false ):
function thk_get_feedly_count_cache() {
	$feedly_count = '<i class="fa fa-spinner fa-spin"></i>';

	$url = esc_url( get_bloginfo( 'rss2_url' ) );

	$wp_upload_dir = wp_upload_dir();
	$cache_dir = $wp_upload_dir['basedir'] . '/wpthk-sns/';
	$feedly_count_cache = $cache_dir . md5( $url );

	if( file_exists( $feedly_count_cache ) === true ) {
		global $wp_filesystem;

		$cache = '';
		if( method_exists( $wp_filesystem, 'get_contents' ) === true ) {
			$cache = $wp_filesystem->get_contents( $feedly_count_cache );
		}

		if( !empty( $cache ) && strpos( $cache, $url ) !== false ) {
			$cnt = explode( "\nr:", $cache );
			if( ctype_digit( trim( $cnt[1] ) ) === true ) {
				$feedly_count = trim( $cnt[1] );
			}
		}
	}
	return $feedly_count;
}
endif;

/*---------------------------------------------------------------------------
 * 画像の URL から attachemnt_id を取得する
 *---------------------------------------------------------------------------*/
/***
 Source: https://philipnewcomer.net/2012/11/get-the-attachment-id-from-an-image-url-in-wordpress/
 ***/
if( function_exists( 'pn_get_attachment_id_from_url' ) === false ):
function pn_get_attachment_id_from_url( $attachment_url = '' ) {
	global $wpdb;
	$attachment_id = false;

	// If there is no url, return.
	if( '' == $attachment_url ) return;

	// Get the upload directory paths
	$upload_dir_paths = wp_upload_dir();

	// Make sure the upload path base directory exists in the attachment URL, to verify that we're working with a media library image
	if( false !== strpos( $attachment_url, $upload_dir_paths['baseurl'] ) ) {
		// If this is the URL of an auto-generated thumbnail, get the URL of the original image
		$attachment_url = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $attachment_url );

		// Remove the upload path base directory from the attachment URL
		$attachment_url = str_replace( $upload_dir_paths['baseurl'] . '/', '', $attachment_url );

		// Finally, run a custom database query to get the attachment ID from the modified attachment URL
		$attachment_id = $wpdb->get_var( $wpdb->prepare( "SELECT wposts.ID FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta WHERE wposts.ID = wpostmeta.post_id AND wpostmeta.meta_key = '_wp_attached_file' AND wpostmeta.meta_value = '%s' AND wposts.post_type = 'attachment'", $attachment_url ) );
	}
	return $attachment_id;
}
endif;

/*---------------------------------------------------------------------------
 * 画像の URL から srcset 付きの img タグを生成する
 *---------------------------------------------------------------------------*/
if( function_exists( 'thk_create_srcset_img_tag' ) === false ):
function thk_create_srcset_img_tag( $url, $alt = null ) {
	global $wpthk;

	$aid  = pn_get_attachment_id_from_url( $url );
	$meta = wp_get_attachment_metadata( $aid );

	if( $alt === null ) $alt = get_post( $aid )->post_title;
	$width = isset( $meta['width'] ) ? $meta['width'] : '';
	$height = isset( $meta['height'] ) ? $meta['height'] : '';

	$content = '<img src="' . $url . '" alt="' . $alt . '" width="' . $width . '" height="' . $height . '" />';

	return wp_image_add_srcset_and_sizes( $content, $meta, $aid );
}
endif;

/*---------------------------------------------------------------------------
 * URL Encode と Convert
 *---------------------------------------------------------------------------*/
if( function_exists( 'thk_encode' ) === false ):
function thk_encode( $value ){
	return rawurlencode( thk_convert( $value ) );
}
endif;

if( function_exists( 'thk_convert' ) === false ):
function thk_convert( $value ){
	if( empty( $value ) ) return;
	if( strpos( $value, null ) !== false ) return;
	mb_language( 'Japanese' );
	$charcode = check_charcode( $value );
	if( $charcode !== null ) $encoding = $charcode;
	return mb_convert_encoding( $value, 'UTF-8', $encoding );
}
endif;

// mb_detect_encoding でうまくいかない場合用
if( function_exists( 'check_charcode' ) === false ):
function check_charcode( $value ) {
	if( empty( $value ) ) return;
	$codes = array( 'UTF-8','SJIS','EUC-JP','ASCII','JIS' );
	foreach( $codes as $charcode ){
		if( mb_convert_encoding( $value, $charcode, $charcode ) === $value ){
			return $charcode;
		}
	}
	return null;
}
endif;

/*---------------------------------------------------------------------------
 * URL Decode
 *---------------------------------------------------------------------------*/
if( function_exists( 'thk_decode' ) === false ):
function thk_decode( $value ){
	while( $value !== rawurldecode( $value ) ) {
		$value = rawurldecode( $value );
	}
	return $value;
}
endif;

/*---------------------------------------------------------------------------
 * Punycode Encode
 *---------------------------------------------------------------------------*/
if( function_exists( 'puny_encode' ) === false ):
function puny_encode( $value ) {
	if( version_compare( PHP_VERSION, '5.3', '<' ) === true ) {
		return $value;
	}

	if( class_exists('Punycode') === true ) {
		$Punycode = new Punycode();

		if( method_exists( $Punycode, 'encode' ) === true ) {
			$parse = parse_url( $value );

			if( isset( $parse['host'] ) ) {
				$parse['host'] = $Punycode->encode( $parse['host'] );
				$value = http_build_url( $value, $parse );
			}
			else {
				$value = $Punycode->encode( $value );
			}
		}
	}

	return $value;
}
endif;

/*---------------------------------------------------------------------------
 * Error message
 *---------------------------------------------------------------------------*/
if( function_exists( 'thk_error_msg' ) === false ):
function thk_error_msg( $result ) {
	echo '<div style="margin:50px 100px;font-weight:bold">';
	echo '<p>' . $result->get_error_message() . '</p>';
	echo '<p>' . $result->get_error_data() . '</p>';
	echo '</div>';
}
endif;

/*---------------------------------------------------------------------------
 * URLを組み立て( PECL の http_build_url 代替版)
 *---------------------------------------------------------------------------*/
/***
 Source: http://stackoverflow.com/questions/7751679/php-http-build-url-and-pecl-install
 ***/

if( function_exists( 'http_build_url' ) === false ) {
	define( 'HTTP_URL_REPLACE', 1 );		// Replace every part of the first URL when there's one of the second URL
	define( 'HTTP_URL_JOIN_PATH', 2 );		// Join relative paths
	define( 'HTTP_URL_JOIN_QUERY', 4 );		// Join query strings
	define( 'HTTP_URL_STRIP_USER', 8 );		// Strip any user authentication information
	define( 'HTTP_URL_STRIP_PASS', 16 );		// Strip any password authentication information
	define( 'HTTP_URL_STRIP_AUTH', 32 );		// Strip any authentication information
	define( 'HTTP_URL_STRIP_PORT', 64 );		// Strip explicit port numbers
	define( 'HTTP_URL_STRIP_PATH', 128 );		// Strip complete path
	define( 'HTTP_URL_STRIP_QUERY', 256 );		// Strip query string
	define( 'HTTP_URL_STRIP_FRAGMENT', 512 );	// Strip any fragments (#identifier)
	define( 'HTTP_URL_STRIP_ALL', 1024 );		// Strip anything but scheme and host

	// Build an URL
	// The parts of the second URL will be merged into the first according to the flags argument. 
	// 
	// @param mixed	(Part(s) of) an URL in form of a string or associative array like parse_url() returns
	// @param mixed	Same as the first argument
	// @param int	A bitmask of binary or'ed HTTP_URL constants (Optional)HTTP_URL_REPLACE is the default
	// @param array	If set, it will be filled with the parts of the composed url like parse_url() would return 
	function http_build_url( $url, $parts=array(), $flags=HTTP_URL_REPLACE, &$new_url=false ) {
		$keys = array( 'user', 'pass', 'port', 'path', 'query', 'fragment' );

		// HTTP_URL_STRIP_ALL becomes all the HTTP_URL_STRIP_Xs
		if( $flags & HTTP_URL_STRIP_ALL ) {
			$flags |= HTTP_URL_STRIP_USER;
			$flags |= HTTP_URL_STRIP_PASS;
			$flags |= HTTP_URL_STRIP_PORT;
			$flags |= HTTP_URL_STRIP_PATH;
			$flags |= HTTP_URL_STRIP_QUERY;
			$flags |= HTTP_URL_STRIP_FRAGMENT;
		}
		// HTTP_URL_STRIP_AUTH becomes HTTP_URL_STRIP_USER and HTTP_URL_STRIP_PASS
		elseif( $flags & HTTP_URL_STRIP_AUTH ) {
			$flags |= HTTP_URL_STRIP_USER;
			$flags |= HTTP_URL_STRIP_PASS;
		}

		// Parse the original URL
		$parse_url = parse_url( $url );

		// Scheme and Host are always replaced
		if( isset( $parts['scheme'] ) ) $parse_url['scheme'] = $parts['scheme'];
		if( isset( $parts['host'] ) ) $parse_url['host'] = $parts['host'];

		// (If applicable) Replace the original URL with it's new parts
		if( $flags & HTTP_URL_REPLACE ) {
			foreach( $keys as $key ) {
				if( isset( $parts[$key] ) ) $parse_url[$key] = $parts[$key];
			}
		}
		else {
			// Join the original URL path with the new path
			if( isset( $parts['path'] ) && ( $flags & HTTP_URL_JOIN_PATH ) ) {
				if( isset( $parse_url['path'] ) ) {
					$parse_url['path'] = rtrim( str_replace( basename( $parse_url['path'] ), '', $parse_url['path'] ), '/' ) . '/' . ltrim( $parts['path'], '/' );
				}
				else {
					$parse_url['path'] = $parts['path'];
				}
			}

			// Join the original query string with the new query string
			if( isset( $parts['query'] ) && ( $flags & HTTP_URL_JOIN_QUERY ) ) {
				if( isset($parse_url['query'] ) ) {
					$parse_url['query'] .= '&' . $parts['query'];
				}
				else {
					$parse_url['query'] = $parts['query'];
				}
			}
		}

		// Strips all the applicable sections of the URL
		// Note: Scheme and Host are never stripped
		foreach( $keys as $key ) {
			if( $flags & (int)constant( 'HTTP_URL_STRIP_' . strtoupper( $key ) ) ) unset( $parse_url[$key] );
		}
		$new_url = $parse_url;

		return 
			 ( ( isset($parse_url['scheme'] ) ) ? $parse_url['scheme'] . '://' : '' )
			.( ( isset($parse_url['user'] ) ) ? $parse_url['user'] . ( ( isset( $parse_url['pass'] ) ) ? ':' . $parse_url['pass'] : '' ) .'@' : '' )
			.( ( isset($parse_url['host'] ) ) ? $parse_url['host'] : '' )
			.( ( isset($parse_url['port'] ) ) ? ':' . $parse_url['port'] : '' )
			.( ( isset($parse_url['path'] ) ) ? $parse_url['path'] : '' )
			.( ( isset($parse_url['query'] ) ) ? '?' . $parse_url['query'] : '' )
			.( ( isset($parse_url['fragment'] ) ) ? '#' . $parse_url['fragment'] : '' )
		;
	}
}
