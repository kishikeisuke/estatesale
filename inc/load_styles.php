<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

/*---------------------------------------------------------------------------
 * CSS ＆ Javascrpt をヘッダーに追加
 *---------------------------------------------------------------------------*/
global $wpthk;

/*---------------------------------------------------------------------------
 * Status check of Functions for CSS & Javascript Files
 *---------------------------------------------------------------------------*/
if( function_exists( 'thk_file_status_check' ) === false ):
function thk_file_status_check() {
	global $wpthk;

	// File name definition array
	$load_files = array(
		// Stylesheet
		'p-style'	=> array( TPATH . DSEP . 'style.css', TDEL . '/style.css' ),
		'p-style-min'	=> array( TPATH . DSEP . 'style.min.css', TDEL . '/style.min.css' ),
		'p-async'	=> array( TPATH . DSEP . 'style.async.min.css', TDEL . '/style.async.min.css'),
		'p-replace'	=> array( TPATH . DSEP . 'style.replace.min.css', TDEL . '/style.replace.min.css' ),
		'p-2col'	=> array( TPATH . DSEP . 'style.2col.min.css', TDEL . '/style.2col.min.css' ),
		'p-3col'	=> array( TPATH . DSEP . 'style.3col.min.css', TDEL . '/style.3col.min.css' ),
		'c-style'	=> array( SPATH . DSEP . 'style.css', SDEL . '/style.css' ),
		'c-style-min'	=> array( SPATH . DSEP . 'style.min.css', SDEL . '/style.min.css' ),
		'c-replace'	=> array( SPATH . DSEP . 'style.replace.min.css', SDEL . '/style.replace.min.css' ),

		// jQuery
		'jquery'	=> array( ABSPATH . WPINC . DSEP . 'js' . DSEP . 'jquery' . DSEP . 'jquery.js', null ),
		'jquery-bind'	=> array( TPATH . DSEP . 'js' . DSEP . 'jquery.bind.min.js', TDEL . '/js/jquery.bind.min.js' ),
		// Javascript
		'jquery-wpthk'	=> array( TPATH . DSEP . 'js' . DSEP . 'jquery.wpthk.min.js', TDEL . '/js/jquery.wpthk.min.js' ),

		'p-wpthk-min'	=> array( TPATH . DSEP . 'js' . DSEP . 'wpthk.min.js', TDEL . '/js/wpthk.min.js' ),
		'p-wpthk-async'	=> array( TPATH . DSEP . 'js' . DSEP . 'wpthk.async.min.js', TDEL . '/js/wpthk.async.min.js' ),
		'c-wpthkch'	=> array( SPATH . DSEP . 'wpthkch.js', SDEL . '/js/wpthkch.js' ),
		'c-wpthkch-min'	=> array( SPATH . DSEP . 'wpthkch.min.js', SDEL . '/js/wpthkch.js' )
	);

	$status = false;

	// Array for file status check
	$files = array(
		'pstyle'	=> array(
			'check'		=> $load_files['p-style-min'][0],
			'compare'	=> $load_files['p-style'][0]
		),
		'cstyle'	=> array(
			'check'		=> $load_files['c-style-min'][0],
			'compare'	=> $load_files['c-style'][0]
		),
		'creplace'	=> array(
			'check'		=> $load_files['c-replace'][0],
			'compare'	=> $load_files['c-style'][0]
		),
		'jbind'		=> array(
			'check'		=> $load_files['jquery-bind'][0],
			'compare'	=> $load_files['jquery'][0]
		),
		'jwpthk'	=> array(
			'check'		=> $load_files['jquery-wpthk'][0],
			'compare'	=> $load_files['jquery'][0]
		),
		'pwpthk'	=> array(
			'check'		=> $load_files['p-wpthk-min'][0],
			'compare'	=> $load_files['p-style'][0]
		),
		'cwpthkch'	=> array(
			'check'		=> $load_files['c-wpthkch-min'][0],
			'compare'	=> $load_files['c-wpthkch'][0]
		)
	);

	// Status check of CSS & Javascript Files
	if( file_exists( $files['pstyle']['check'] ) === true && file_exists( $files['pstyle']['compare'] ) === true ) {
		if( filemtime( $files['pstyle']['check'] ) !== filemtime( $files['pstyle']['compare'] ) ) {
			$status = true;
		}
	}

	if( file_exists( $files['cstyle']['check'] ) === true && file_exists( $files['cstyle']['compare'] ) === true ) {
		if( filemtime( $files['cstyle']['check'] ) !== filemtime( $files['cstyle']['compare'] ) ) {
			$status = true;
		}
	}

	if( file_exists( $files['pwpthk']['check'] ) === true && file_exists( $files['pwpthk']['compare'] ) === true ) {
		if( filemtime( $files['pwpthk']['check'] ) !== filemtime( $files['pwpthk']['compare'] ) ) {
			$status = true;
		}
	}

	if( file_exists( $files['cwpthkch']['check'] ) === true && file_exists( $files['cwpthkch']['compare'] ) === true ) {
		if( filemtime( $files['cwpthkch']['check'] ) !== filemtime( $files['cwpthkch']['compare'] ) ) {
			$status = true;
		}
	}

	if( file_exists( $files['creplace']['check'] ) === true && file_exists( $files['creplace']['compare'] ) === true ) {
		if( filemtime( $files['creplace']['check'] ) !== filemtime( $files['creplace']['compare'] ) ) {
			$status = true;
		}
	}

	// Status check of jquery.js
	if( file_exists( $files['jbind']['check'] ) === true && file_exists( $files['jbind']['compare'] ) === true ) {
		if( filemtime( $files['jbind']['check'] ) !== filemtime( $files['jbind']['compare'] ) ) {
			$status = true;
		}
	}

	if( file_exists( $files['jwpthk']['check'] ) === true && file_exists( $files['jwpthk']['compare'] ) === true ) {
		if( filemtime( $files['jwpthk']['check'] ) !== filemtime( $files['jwpthk']['compare'] ) ) {
			$status = true;
		}
	}

	// File exists check
	if( $wpthk['child_css_compress'] !== 'bind' ) {
		if( file_exists( $load_files['p-style-min'][0] ) === false ) {
			$status = true;
		}
		if( isset( $wpthk['css_to_style'] ) ) {
			if( file_exists( $load_files['p-replace'][0] ) === false ) {
				$status = true;
			}
		}
	}

	if( isset( $wpthk['child_css'] ) && TDEL !== SDEL ) {
		if( $wpthk['child_css_compress'] !== 'none' ) {
			if( file_exists( $load_files['c-style-min'][0] ) === false ) {
				$status = true;
			}
			if( isset( $wpthk['css_to_style'] ) ) {
				if( file_exists( $load_files['c-replace'][0] ) === false ) {
					$status = true;
				}
			}
		}
	}

	if( $wpthk['column_default'] === false ) {
		if( $wpthk['column_style'] === '2column' ) {
			if( file_exists( $load_files['p-2col'][0] ) === false ) {
				$status = true;
			}
		}
		if( $wpthk['column_style'] === '3column' ) {
			if( file_exists( $load_files['p-3col'][0] ) === false ) {
				$status = true;
			}
		}
	}
	if( file_exists( $load_files['p-async'][0] ) === false ) {
		$status = true;
	}

	if( isset( $wpthk['jquery_load'] ) ) {
		if( isset( $wpthk['jquery_migrate'] ) && $wpthk['jquery_migrate'] !== 'wpthk' ) {
			if( file_exists( $load_files['p-wpthk-min'][0] ) === false ) {
				$status = true;
			}
		}
	}

	if( isset( $wpthk['jquery_migrate'] ) && $wpthk['jquery_migrate'] !== 'wpthk' ) {
		if( file_exists( $load_files['p-wpthk-async'][0] ) === false ) {
			$status = true;
		}
	}

	if( !isset( $wpthk['child_script'] ) ) {
		if( $wpthk['child_js_compress'] === 'comp' ) {
			if( file_exists( $load_files['c-wpthkch-min'][0] ) === false ) {
				$status = true;
			}
		}
	}

	if( $status === true && is_customize_preview() === false ) {
		thk_regenerate_files();
		thk_touch_files( $files );
	}

	return $load_files;
}
endif;

// Touch CSS & Javascript Files
if( function_exists( 'thk_touch_files' ) === false ):
function thk_touch_files( $files ) {
	global $wp_filesystem;

	thk_regenerate_files( false, true );
	$filesystem = new thk_filesystem();
	$filesystem->init_filesystem();

	foreach( $files as $key => $value ) {
		if( file_exists( $value['check'] ) === true && file_exists( $value['compare'] ) === true ) {
			if( $wp_filesystem->touch( $value['check'], filemtime( $value['compare'] ) ) === false ) {
				$result = new WP_Error( 'File updating failed', 'Could not touch the file.', $value['check'] );
				thk_error_msg( $result );
			}
		}
	}
}
endif;

/*---------------------------------------------------------------------------
 * CSS、Javascript のステータスチェック
 *---------------------------------------------------------------------------*/
$load_files = thk_file_status_check();

/*---------------------------------------------------------------------------
 * カスタマイズ画面プレビュー用の CSS
 *---------------------------------------------------------------------------*/
if( is_customize_preview() === true ) {
	require_once( INC . 'optimize.php' );

	$optimize = new thk_optimize();
	$files = $optimize->css_optimize_init();

	foreach( $files as $key => $val ) {
		$val = str_replace( DSEP, '/', str_replace( TPATH, TDEL, $val ) );
		wp_enqueue_style( $key, $val . '?v=' . $_SERVER['REQUEST_TIME'], false, array(), 'all' );
	}

	/* アイコンフォント (プレビュー) */
	if( $wpthk['awesome_load'] === 'async' ) {
		wp_enqueue_style( 'awesome', TDEL . '/css/fontawesome.css', false, array(), 'all' );
		wp_enqueue_style( 'icomoon', TDEL . '/css/iconmoon.css', false, array(), 'all' );
	}

	/* カスタマイズした CSS (プレビュー用) */
	end( $files );
	$last_css = key( $files );
	wp_add_inline_style( $last_css, thk_custom_css() );
}
/*---------------------------------------------------------------------------
 * 実体の CSS
 *---------------------------------------------------------------------------*/
else {
	/* 親テーマの CSS 読み込み(実体) */
	if( $wpthk['child_css_compress'] !== 'bind' ) {
		if( file_exists( $load_files['p-style-min'][0] ) === true ) {
			wp_enqueue_style( 'wpthk', $load_files['p-style-min'][1] . '?v=' . $_SERVER['REQUEST_TIME'], false, array(), 'all' );
			if( isset( $wpthk['css_to_style'] ) ) {
				wp_add_inline_style( 'wpthk', thk_direct_style( $load_files['p-replace'][0] ) );
			}
		}
	}

	/* 子テーマの CSS はプラグインの CSS より後に読み込むので load_header.php で処理 */

	// 非同期で読み込む CSS ( noscript 用 )
	if( file_exists( $load_files['p-async'][0] ) === true && filesize( $load_files['p-async'][0] ) !== 0 ) {
		wp_enqueue_style( 'async', $load_files['p-async'][1] . '?v=' . $_SERVER['REQUEST_TIME'], array(), 'all' );
	}
}

/* テンプレートごとに違うカラム数にしてる場合の 3カラム CSS
 * (親子 CSS 結合時はデザインが崩れるため、子テーマより後に読み込ませる -> load_header.php で処理 )
 */
if( $wpthk['child_css_compress'] !== 'bind' ) {
	if( $wpthk['column_default'] === false ) {
		if( $wpthk['column_style'] === '2column' ) {
			wp_enqueue_style( 'wpthk2', $load_files['p-2col'][1], false, array(), 'all' );
			wp_add_inline_style( 'wpthk2', thk_direct_style( $load_files['p-2col'][0] ) );
		}
		if( $wpthk['column_style'] === '3column' ) {
			wp_enqueue_style( 'wpthk3', $load_files['p-3col'][1], false, array(), 'all' );
			wp_add_inline_style( 'wpthk3', thk_direct_style( $load_files['p-3col'][0] ) );
		}
	}
}

/*---------------------------------------------------------------------------
 * Javascript 類
 *---------------------------------------------------------------------------*/

/* Asynchronous Javascript ( jQuery と結合してる場合は読み込まない) */
if( isset( $wpthk['jquery_migrate'] ) && $wpthk['jquery_migrate'] !== 'wpthk' ) {
	if( file_exists( $load_files['p-wpthk-async'][0] ) === true && filesize( $load_files['p-wpthk-async'][0] ) !== 0 ) {
		wp_enqueue_script( 'async', $load_files['p-wpthk-async'][1] . '?v=' . $_SERVER['REQUEST_TIME'], array(), false, false );
	}
}

/* jQuery */
if( isset( $wpthk['jquery_load'] ) ) {
	wp_enqueue_script( 'jquery' );

	/* wpthk.min.js ( jQuery と結合してる場合は読み込まない )
	 * カスタマイズプレビューはフッターで各スクリプトごとに読み込み (カスタマイズ内容によって読み込むスクリプトが異なるため)
	 */
	if( is_customize_preview() === false ) {
		if( isset( $wpthk['jquery_migrate'] ) && $wpthk['jquery_migrate'] !== 'wpthk' ) {
			wp_enqueue_script( 'wpthk', $load_files['p-wpthk-min'][1] . '?v=' . $_SERVER['REQUEST_TIME'], array('jquery'), false, false );
		}
	}

	if( $wpthk['bootstrap_js_load_type'] !== 'none' ) {
		/* Load bootstrap.js */
		wp_enqueue_script( 'bootstrap', TDEL . '/js/bootstrap.min.js', array('jquery'), false, false );
	}

	/* Image Gallery */
	if( $wpthk['gallery_type'] === 'tosrus' ) {
		add_filter( 'the_content', 'add_tosrus', 11 );
	}
	elseif( $wpthk['gallery_type'] === 'lightcase' ) {
		add_filter( 'the_content', 'add_lightcase', 11 );
	}
	elseif( $wpthk['gallery_type'] === 'fluidbox' ) {
		add_filter( 'the_content', 'add_fluidbox', 11 );
	}

	/* Lazy Load */
	if( isset( $wpthk['lazyload_enable'] ) ) {
		remove_filter( 'the_content', 'wp_make_content_images_responsive' );	// remove srcset
		/*** WordPress の srcset を全部無効化する場合は以下2行
			add_filter( 'wp_calculate_image_srcset', '__return_false' );
			add_filter( 'wp_calculate_image_sizes', '__return_false' );
		 ***/

		add_filter( 'the_content', 'add_lazyload', 12 );
	}

	/* Search Highlight */
	if( is_search() === true && isset( $wpthk['search_highlight'] ) ) {
		wp_enqueue_script( 'thk-highlight', TDEL . '/js/thk-highlight.min.js?v=' . $_SERVER['REQUEST_TIME'], array('jquery'), false, false );
	}
}

/* カスタマイズで hentry 削除にチェックがついてたら hentry 削除 */
if( isset( $wpthk['remove_hentry_class'] ) ) {
	add_filter( 'post_class', 'thk_remove_hentry' );
}

/* カテゴリとアーカイブの件数 A タグを内側にするかどうかのやつ */
if( isset( $wpthk['categories_a_inner'] ) ) {
	add_filter( 'wp_list_categories', 'thk_list_categories_archives', 10, 2 );
}
if( isset( $wpthk['archives_a_inner'] ) ) {
	add_filter( 'get_archives_link', 'thk_list_categories_archives', 10, 2 );
}
