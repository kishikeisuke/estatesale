<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

/*---------------------------------------------------------------------------
 * Optimize
 *---------------------------------------------------------------------------*/
class thk_optimize {
	private $_thk_files	= null;
	private $_filesystem	= null;
	private $_js_dir	= null;
	private $_css_dir	= null;
	private $_tmpl_dir	= null;

	public function __construct() {
		require_once( INC . 'files.php' );

		$this->_js_dir   = TPATH . DSEP . 'js' . DSEP;
		$this->_css_dir  = TPATH . DSEP . 'css' . DSEP;
		$this->_tmpl_dir = TPATH . DSEP . 'styles' . DSEP;

		$this->_thk_files = new thk_files();

		// filesystem initialization
		$this->_filesystem = new thk_filesystem();
		if( $this->_filesystem->init_filesystem() === false ) return false;
	}

	/*
	 * CSS Optimize initialization
	 */
	public function css_optimize_init() {
		global $wpthk;

		$files = $this->_thk_files->styles();

		// get overall image
		if( get_theme_mod( 'all_clear', false ) === false ) {
			$files['style_thk'] = TPATH . DSEP . get_overall_image();
		}

		// file exists check
		foreach( $files as $key => $val ) {
			if( file_exists( $val ) === false ) unset( $files[$key] );
		}

		// determining the conditions
		if( $wpthk['wpthk_mode_select'] === 'wpthk' ) {
			unset( $files['bootstrap'] );
		}
		else {
			unset( $files['wpthk-common'] );
		}

		// Font Awesome
		if( $wpthk['awesome_load'] !== 'sync' ) {
			unset( $files['awesome'] );
			unset( $files['awesome-minimum'] );
			unset( $files['icomoon'] );
		}
		else {
			if( $wpthk['awesome_css_type'] === 'full' ) {
				unset( $files['awesome-minimum'] );
			}
			else {
				unset( $files['awesome'] );
			}
		}

		if( isset( $wpthk['sns_tops_enable'] ) || isset( $wpthk['sns_bottoms_enable'] ) ) {
			if( $wpthk['sns_tops_type'] !== 'color' && $wpthk['sns_tops_type'] !== 'white' && $wpthk['sns_bottoms_type'] !== 'color' && $wpthk['sns_bottoms_type'] !== 'white' ) {
				unset( $files['sns'] );
			}
			if( $wpthk['sns_tops_type'] !== 'flatc' && $wpthk['sns_tops_type'] !== 'flatw' && $wpthk['sns_bottoms_type'] !== 'flatc' && $wpthk['sns_bottoms_type'] !== 'flatw' ) {
				unset( $files['sns-flat'] );
			}
			if( $wpthk['sns_tops_type'] !== 'iconc' && $wpthk['sns_tops_type'] !== 'iconw' && $wpthk['sns_bottoms_type'] !== 'iconc' && $wpthk['sns_bottoms_type'] !== 'iconw' ) {
				unset( $files['sns-icon'] );
			}
			if( $wpthk['sns_tops_type'] !== 'normal' && $wpthk['sns_bottoms_type'] !== 'normal' ) {
				unset( $files['sns-normal'] );
			}
		}

		if( !isset( $wpthk['css_search'] ) ) {
			unset( $files['search'] );
		}
		if( !isset( $wpthk['css_archive'] ) ) {
			unset( $files['archive'] );
		}
		if( !isset( $wpthk['css_calendar'] ) ) {
			unset( $files['calendar'] );
		}
		if( !isset( $wpthk['css_new_post'] ) ) {
			unset( $files['new-post'] );
		}
		if( !isset( $wpthk['css_rcomments'] ) ) {
			unset( $files['rcomments'] );
		}
		if( !isset( $wpthk['css_follow_button'] ) ) {
			unset( $files['follow-button'] );
		}
		if( !isset( $wpthk['css_rss_feedly'] ) ) {
			unset( $files['rss-feedly'] );
		}
		if( !isset( $wpthk['css_qr_code'] ) ) {
			unset( $files['qr-code'] );
		}

		if( !isset( $wpthk['global_navi_mobile_type'] ) ) {
			unset( $files['mobile-common'] );
			unset( $files['mobile-menu'] );
			unset( $files['mobile-luxury'] );
		}
		elseif( isset( $wpthk['global_navi_mobile_type'] ) && $wpthk['global_navi_mobile_type'] === 'luxury' ) {
			unset( $files['mobile-menu'] );
		}
		else {
			unset( $files['mobile-luxury'] );
		}

		if( !isset( $wpthk['head_band_search'] ) ) {
			unset( $files['head-search'] );
		}

		if( $wpthk['wpthk_mode_select'] !== 'bootstrap' && $wpthk['gallery_type'] !== 'bootstrap' ) {
			unset( $files['blueimp'] );
			unset( $files['bootimgg'] );
		}

		if( $wpthk['gallery_type'] !== 'lightcase' ) {
			unset( $files['lightcase'] );
		}
		if( $wpthk['gallery_type'] !== 'fluidbox' ) {
			unset( $files['fluidbox'] );
		}
		if( $wpthk['gallery_type'] !== 'lightbox' ) {
			unset( $files['lightbox'] );
		}

		return array_filter( $files, 'strlen' );
	}

	/*
	 * CSS Optimize
	 */
	public function css_optimize( $files = array(), $name = 'style.min.css', $dir_replace_flag = false ) {
		global $wpthk, $wp_filesystem;
		$contents = array();

		$style_min = TPATH . DSEP . $name;

		// get stylesheet file content
		$replaces = $this->_thk_files->dir_replace();
		foreach( $files as $key => $val ) {
			if( isset( $replaces[$key] ) && $dir_replace_flag === true ) {
				$contents[$key] = str_replace( '../', './', $wp_filesystem->get_contents( $val ) );
			}
			else {
				$contents[$key] = $wp_filesystem->get_contents( $val );
			}
		}

		// get wpthk customizer css
		if( get_theme_mod( 'all_clear', false ) === false ) {
			$files['style_thk'] = TPATH . DSEP . get_overall_image();

			// 管理画面でのカスタマイズ内容
			$contents['customize'] = trim( str_replace( array( '<style>', '</style>' ), '', thk_custom_css() ) );
			if( $contents['customize'] === '/*! wpthk customizer css */' ) {
				$contents['customize'] = '';
			}
			else {
				$contents['customize'] = str_replace( '/*! wpthk customizer css */' . "\n", '/*! wpthk customizer css */', $contents['customize'] );
			}
		}

		// css bind
		$save = '';
		foreach( $contents as $value ) {
			$save .= $value . "\n";
		}

		// css compression and save
		if( isset( $wpthk['parent_css_uncompress'] ) ) {
			if( $this->_filesystem->file_save( $style_min, $save ) === false ) return false;
		}
		else {
			if( $this->_filesystem->file_save( $style_min, thk_cssmin( $save ) ) === false ) return false;
		}

		return true;
	}

	/*
	 * Asynchronous CSS Optimize initialization
	 */
	public function css_async_optimize_init() {
		global $wpthk;

		$files = $this->_thk_files->styles_async();

		// file exists check
		foreach( $files as $key => $val ) {
			if( file_exists( $val ) === false ) unset( $files[$key] );
		}
		return array_filter( $files, 'strlen' );
	}

	/*
	 * Asynchronous CSS Optimize
	 */
	public function css_async_optimize( $files = array(), $dir_replace_flag = false ) {
		global $wpthk, $wp_filesystem;
		$contents = array();

		$style_min = TPATH . DSEP . 'style.async.min.css';

		// Font Awesome
		if( $wpthk['awesome_load'] !== 'async' ) {
			unset( $files['awesome'] );
			unset( $files['awesome-minimum'] );
			unset( $files['icomoon'] );
		}
		else {
			if( $wpthk['awesome_css_type'] === 'full' ) {
				unset( $files['awesome-minimum'] );
			}
			else {
				unset( $files['awesome'] );
			}
		}

		// tosrus
		if( $wpthk['gallery_type'] !== 'tosrus' ) {
			unset( $files['tosrus'] );
		}
		// lightcase
		if( $wpthk['gallery_type'] !== 'lightcase' ) {
			unset( $files['lightcase'] );
		}
		// fluidbox
		if( $wpthk['gallery_type'] !== 'fluidbox' ) {
			unset( $files['fluidbox'] );
		}

		// get stylesheet file content
		$replaces = $this->_thk_files->dir_replace();
		foreach( $files as $key => $val ) {
			if( isset( $replaces[$key] ) && $dir_replace_flag === true ) {
				$contents[$key] = str_replace( '../', './', $wp_filesystem->get_contents( $val ) );
			}
			else {
				$contents[$key] = $wp_filesystem->get_contents( $val );
			}
		}

		// css bind
		$save = '';
		foreach( $contents as $value ) {
			$save .= $value . "\n";
		}

		// css compression and save
		if( isset( $wpthk['parent_css_uncompress'] ) ) {
			if( $this->_filesystem->file_save( $style_min, $save ) === false ) return false;
		}
		else {
			if( $this->_filesystem->file_save( $style_min, thk_cssmin( $save ) ) === false ) return false;
		}

		return true;
	}

	/*
	 * Synchronous Javascript Optimize
	 */
	public function js_defer_optimize() {
		global $wpthk, $wp_filesystem;
		$contents = array();

		$wpthk_min = $this->_js_dir . 'wpthk.min.js';

		$files = $this->_thk_files->scripts_defer();

		// file exists check
		foreach( $files as $key => $val ) {
			if( $val === true ) continue;
			if( file_exists( $val ) === false ) unset( $files[$key] );
		}

		// column3
		if( $wpthk['column3'] !== '3column' ) {
			unset( $files['column3'] );
		}

		// jquery.lazyload ( jquery defer )
		if( !isset( $wpthk['jquery_load'] ) || !isset( $wpthk['jquery_defer'] ) || !isset( $wpthk['lazyload_enable'] ) ) {
			unset( $files['lazyload'] );
		}

		// tosrus
		if( $wpthk['gallery_type'] !== 'tosrus' ) {
			unset( $files['tosrus'] );
		}
		// lightcase
		if( $wpthk['gallery_type'] !== 'lightcase' ) {
			unset( $files['lightcase'] );
		}
		// fluidbox
		if( $wpthk['gallery_type'] !== 'fluidbox' ) {
			unset( $files['fluidbox'] );
			unset( $files['throttle'] );
		}

		$files = array_filter( $files, 'strlen' );

		// get javascript file content
		$jscript = new create_Javascript();

		foreach( $files as $key => $val ) {
			if( $val !== null ) {
				$contents[$key] = $wp_filesystem->get_contents( $val );
			}

			// Lazy Load Options ( jquery defer )
			if( $key === 'lazyload' && isset( $wpthk['jquery_defer'] ) ) {
				$contents[$key] .= $jscript->create_lazy_load_script();
			}

			// WpTHK script
			if( $key === 'wpthk' ) {
				$contents[$key] .= $jscript->create_wpthk_various_script();
				// SNS Count script
				if(
					( isset( $wpthk['sns_tops_enable'] ) && isset( $wpthk['sns_tops_count'] ) ) ||
					( isset( $wpthk['sns_bottoms_enable'] ) && isset( $wpthk['sns_bottoms_count'] ) ) ||
					( isset( $wpthk['sns_toppage_view'] ) && isset( $wpthk['sns_bottoms_count'] ) )
				){
					$contents[$key] .= $jscript->create_sns_count_script();
				}
			}
		}

		// javascript bind
		$save = '';
		foreach( $contents as $value ) {
			$save .= $value . "\n";
		}
		if( $this->_filesystem->file_save( $wpthk_min, thk_jsmin( $save ) ) === false ) return false;

		return true;
	}

	/*
	 * Asynchronous Javascript Optimize
	 */
	public function js_async_optimize() {
		global $wpthk, $wp_filesystem;
		$contents = array();

		$wpthk_async = $this->_js_dir . 'wpthk.async.min.js';

		$files = $this->_thk_files->scripts_async();

		// file exists check
		foreach( $files as $key => $val ) {
			if( $val === true ) continue;
			if( file_exists( $val ) === false ) unset( $files[$key] );
		}

		// jquery.lazyload ( jquery no defer )
		if( !isset( $wpthk['jquery_load'] ) ||  isset( $wpthk['jquery_defer'] ) || !isset( $wpthk['lazyload_enable'] ) ) {
			unset( $files['lazyload'] );
		}

		$files = array_filter( $files, 'strlen' );

		// get Asynchronous javascript file content
		$jscript = new create_Javascript();
		$tdel =pdel( get_template_directory_uri() );
		$sdel =pdel( get_stylesheet_directory_uri() );

		foreach( $files as $key => $val ) {
			if( $key === 'async' ) {
				$contents[$key] = $jscript->create_css_load_script( $tdel . '/style.async.min.css' );
				continue;
			}

			$contents[$key] = $wp_filesystem->get_contents( $val );

			// Lazy Load Options ( jquery no defer )
			if( $key === 'lazyload' && !isset( $wpthk['jquery_defer'] ) ) {
				$contents[$key] .= $jscript->create_lazy_load_script();
			}
		}

		// Asynchronous javascript bind
		$save = '';
		foreach( $contents as $value ) {
			$save .= $value . "\n";
		}
		if( $this->_filesystem->file_save( $wpthk_async, thk_jsmin( $save ) ) === false ) return false;

		return true;
	}

	/*
	 * Search highlight script Optimize
	 */
	public function js_search_highlight() {
		global $wp_filesystem;

		$contents = array();
		$files = $this->_thk_files->scripts_search_highlight();

		$wpthk_search_highlight = $this->_js_dir . 'thk-highlight.min.js';

		foreach( $files as $key => $val ) {
			$contents[$key] = $wp_filesystem->get_contents( $val );
		}

		$save = '';

		$save .= <<< JQUERY_CHECK
// jQuery が読み込まれてなかったら、実行遅らせる
// 参考 URL： http://jsfiddle.net/ocfzf3bb/2/
var checkReady = function(callback) {
	if( window.jQuery ) {
		callback(jQuery);
	} else {
		window.setTimeout( function() {
			checkReady(callback);
		}, 100 );
	}
};
checkReady( function($) {

JQUERY_CHECK;

		foreach( $contents as $value ) {
			$save .= $value . "\n";
		}

		$save .= '});';

		if( $this->_filesystem->file_save( $wpthk_search_highlight, thk_jsmin( $save ) ) === false ) return false;

		return true;
	}

	/*
	 * jQuery and bootstrap Optimize
	 */
	public function jquery_optimize() {
		global $wpthk, $wp_filesystem;
		$contents = array();

		$bind = array(
			$this->_js_dir . 'jquery.bind.min.js',
			$this->_js_dir . 'jquery.wpthk.min.js'
		);

		$jquery_migrate = $bind[0];
		if( $wpthk['jquery_migrate'] === 'wpthk' ) {
			$jquery_migrate = $bind[1];
		}

		$files = $this->_thk_files->jquery();

		// file exists check
		foreach( $files as $key => $val ) {
			if( file_exists( $val ) === false ) unset( $files[$key] );
		}

		// get script files
		if( isset( $wpthk['jquery_load'] ) ) {
			if( isset( $wpthk['jquery_migrate'] ) && $wpthk['jquery_migrate'] !== 'not' && file_exists( $files['jquery'] ) === true ) {
				// jquery
				$contents['jquery'] = $wp_filesystem->get_contents( $files['jquery'] );
				// wpthk.async.min.js
				$wpthk_async = $this->_js_dir . 'wpthk.async.min.js';
				// wpthk.min.js
				$wpthk_min = $this->_js_dir . 'wpthk.min.js';

				if( file_exists( $files['migrate'] ) === true ) {
					// jquery-migrate
					$contents['migrate'] = $wp_filesystem->get_contents( $files['migrate'] );
				}

				if( $wpthk['jquery_migrate'] === 'wpthk' ) {
					if( file_exists( $wpthk_async ) === true ) {
						// wpthk.async.min.js
						$contents['migrate'] .= $wp_filesystem->get_contents( $wpthk_async );
					}
					if( file_exists( $wpthk_min ) === true ) {
						// wpthk.min.js
						$contents['migrate'] .= $wp_filesystem->get_contents( $wpthk_min );
					}

					$del_file = $bind[0];
				}
				else {
					$del_file = $bind[1];
				}

				if( $wp_filesystem->delete( $del_file ) === false ) {
					$this->_filesystem->file_save( $del_file, null );
				}
			}
		}

		if(
			get_theme_mod( 'all_clear', false ) === true ||
			!isset( $wpthk['jquery_migrate'] ) ||
			( isset( $wpthk['jquery_migrate'] ) && $wpthk['jquery_migrate'] === 'not' )
		) {
			foreach( $bind as $val ) {
				if( $wp_filesystem->delete( $val ) === false ) {
					$this->_filesystem->file_save( $val, null );
				}
			}
			return true;
		}

		// javascript compression and save
		$save = '';
		foreach( $contents as $value ) {
			$save .= $value . "\n";
		}
		if( $this->_filesystem->file_save( $jquery_migrate, thk_jsmin( $save ) ) === false ) return false;

		return true;
	}
}

/*---------------------------------------------------------------------------
 * ファイル操作
 *---------------------------------------------------------------------------*/
class thk_filesystem {
	/* save */
	public function file_save( $file=THK_STYLE_TMP_CSS, $txt='' ) {
		global $wp_filesystem;

		add_filter( 'request_filesystem_credentials', '__return_true' );

		$this->init_filesystem();
		if( false === $wp_filesystem->put_contents( $file , $txt, FS_CHMOD_FILE ) ) {
			//echo "error saving file!";
			$result = new WP_Error( 'error saving file', __( 'Error saving file.', 'wpthk' ), $file );
			thk_error_msg( $result );
			return false;
		}
		return;
	}

	/* init */
	public function init_filesystem( $url = null ) {
		global $wp_filesystem;
		require_once( ABSPATH . 'wp-admin/includes/file.php' );

		if( $url === null ) {
			$url = wp_nonce_url( 'customize.php?return=' . urlencode( wp_unslash( $_SERVER['REQUEST_URI'] ) ) );
		}
		$creds = request_filesystem_credentials( $url, '', false, false, null );

		// Writable or Check
		if( false === ( $creds = request_filesystem_credentials( $url, '', false, false, null ) ) ) {
			return false;
		}
		// WP_Filesystem_Base init
		if( false === WP_Filesystem( $creds ) ) {
			request_filesystem_credentials( $url, '', true, false, null );
			return false;
		}
		return;
	}
}
