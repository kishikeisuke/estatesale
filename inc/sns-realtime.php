<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

$wpload = dirname( __FILE__ );
for( $i = 0; $i < 4; ++$i ) $wpload = dirname( $wpload );
$wpload .= DIRECTORY_SEPARATOR . 'wp-load.php';

if( file_exists( $wpload ) === false ){
	echo '!';
	exit;
}
require_once( $wpload );

if( version_compare( PHP_VERSION, '5.3', '<' ) === false ) {
	if( class_exists( 'Punycode' ) === false ) {
		require( INC . 'punycode.php' );
		//use TrueBV\Punycode;
	}
}

$hst = isset( $_SERVER['HTTP_HOST'] ) ? puny_encode( thk_convert( $_SERVER['HTTP_HOST'] ) ) : null;
$url = isset( $_GET['url'] ) ? rawurlencode( puny_encode( esc_url( thk_convert( $_GET['url'] ) ) ) ) : rawurlencode( puny_encode( esc_url( thk_convert( ( home_url('/') ) ) ) ) );
$id  = isset( $_GET['sns'] ) ? $_GET['sns'] : null;
$cnt = '';

if( stripos( $url, $hst ) === false ) {
	echo '!';
	exit;
}

require_once( INC . 'sns-count.php' );
$getcnt = new getSnsCount();

switch( $id ) {
	case 'f':
		$cnt = $getcnt->facebookCount( $url );
		break;
	case 'g':
		$cnt = $getcnt->googleCount( $url );
		break;
	case 'h':
		$cnt = $getcnt->hatenaCount( $url );
		break;
	case 'l':
		$cnt = $getcnt->linkedinCount( $url );
		break;
	case 'p':
		$cnt = $getcnt->pocketCount( $url );
		break;
	case 'r':
		$cnt = $getcnt->feedlyCount( rawurlencode( esc_url( get_bloginfo( 'rss2_url' ) ) ) );
		break;
	default:
		break;
}
echo $cnt;
