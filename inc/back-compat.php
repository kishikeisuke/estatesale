<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

define( 'PHP_LEAST_VER', '5.3' );
define( 'WP_LEAST_VER', '4.4' );

function thk_after_switch_theme() {
	switch_theme( WP_DEFAULT_THEME, WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'thk_upgrade_notice' );
}
add_action( 'after_switch_theme', 'thk_after_switch_theme' );

function thk_load_customize() {
	if( version_compare( PHP_VERSION, PHP_LEAST_VER, '<' ) ) {
		wp_die( sprintf( __( 'WpTHK requires at least %s version %s. You are running version %s. Please upgrade and try again.', 'wpthk' ), 'PHP', PHP_LEAST_VER, PHP_VERSION ), '', array( 'back_link' => true ) );
	}
	else {
		wp_die( sprintf( __( 'WpTHK requires at least %s version %s. You are running version %s. Please upgrade and try again.', 'wpthk' ), 'WordPress', WP_LEAST_VER, $GLOBALS['wp_version'] ), '', array( 'back_link' => true ) );
	}
}
add_action( 'load-customize.php', 'thk_load_customize' );

function thk_template_redirect() {
	if ( isset( $_GET['preview'] ) ) {
		if( version_compare( PHP_VERSION, PHP_LEAST_VER, '<' ) ) {
			wp_die( sprintf( __( 'WpTHK requires at least %s version %s. You are running version %s. Please upgrade and try again.', 'wpthk' ), 'PHP', PHP_LEAST_VER, PHP_VERSION ) );
		}
		else {
			wp_die( sprintf( __( 'WpTHK requires at least %s version %s. You are running version %s. Please upgrade and try again.', 'wpthk' ), 'WordPress', WP_LEAST_VER, $GLOBALS['wp_version'] ) );
		}
	}
}
add_action( 'template_redirect', 'thk_template_redirect' );

function thk_upgrade_notice() {
	$message = '';

	if( version_compare( PHP_VERSION, PHP_LEAST_VER, '<' ) ) {
		$message = sprintf( __( 'WpTHK requires at least %s version %s. You are running version %s. Please upgrade and try again.', 'wpthk' ), 'PHP', PHP_LEAST_VER, PHP_VERSION );
	}
	else {
		$message = sprintf( __( 'WpTHK requires at least %s version %s. You are running version %s. Please upgrade and try again.', 'wpthk' ), 'WordPress', WP_LEAST_VER, $GLOBALS['wp_version'] );
	}

	printf( '<div class="error"><p>%s</p></div>', $message );
}
