<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

class wpthk_customize {
	private $_active	= '';
	private $_page		= '';
	private $_tabs		= array();

	public function __construct() {
	}

	public function wpthk_custom_form() {
		$this->_page = isset( $_GET['page'] ) ? $_GET['page'] : 'wpthk';

		echo '<div class="wrap narrow">';

		if( $this->_page === 'wpthk' ) {
			$this->_active = isset( $_GET['active'] ) ? $_GET['active'] : 'header';

			$this->_tabs = array(
				'header'	=> 'Head ' . __( 'tag', 'wpthk' ),
				'title'		=> 'Title ' . __( 'tag', 'wpthk' ),
				'optimize'	=> __( 'Compression and optimization', 'wpthk' ),
				'style'		=> 'CSS',
				'script'	=> 'Javascript',
				'search'	=> __( 'Search', 'wpthk' ),
				'adsense'	=> __( 'Advertisement', 'wpthk' ),
				'others'	=> __( 'Others', 'wpthk' ),
				'htaccess'	=> __( 'htaccess for speed boost', 'wpthk' ),
				'backup'	=> __( 'Backup', 'wpthk' ),
				'reset'		=> __( 'Reset', 'wpthk' ),
				'version'	=> __( 'Version', 'wpthk' ),
			);

			echo	'<h2 class="wpthk-customize-title">', esc_html( str_replace( array( '<h2>', '</h2>' ), '', get_admin_page_title() ) ),
				'<spna class="wpthk-title-button-block"><span class="wpthk-title-button"><a href="', esc_url( admin_url( 'customize.php?return=/wp-admin/admin.php?page=' . $this->_page . '&wpthk=custom' ) ) ,'" ',
				'class="button button-primary">', __( 'Customizing the Appearance', 'wpthk' ), '</a></span>',
				'<span class="wpthk-title-button"><a href="', esc_url( admin_url( 'admin.php?page=wpthk_sns' ) ) ,'" ',
				'class="button button-primary">SNS ' . __( 'Counter', 'wpthk' ) . '</a></span></span></h2>';
		}
		elseif( $this->_page === 'wpthk_sns' ) {
			$this->_active = isset( $_GET['active'] ) ? $_GET['active'] : 'sns_post';

			$this->_tabs = array(
				'sns_post'	=> __( 'Post page', 'wpthk' ),
				'sns_page'	=> __( 'Static page', 'wpthk' ),
				'sns_home'	=> __( 'Top page', 'wpthk' ),
				'sns_setting'	=> __( 'Settings', 'wpthk' ),
			);

			echo	'<h2>SNS ' . __( 'Counter', 'wpthk' ) . ' ' . __( 'Cache', 'wpthk' ) . ' ' . __( 'Control', 'wpthk' ) . '</h2>';
		}
		else {
			$this->_active = isset( $_GET['active'] ) ? $_GET['active'] : 'edit_style';

			$this->_tabs = array(
				'edit_style'		=> 'style.css',
				'edit_script'		=> 'Javascript',
				'edit_header'		=> 'Head ' . __( 'tag', 'wpthk' ),
				'edit_footer'		=> __( 'Footer', 'wpthk' ),
				'edit_functions'	=> 'functions.php',
			);

			echo	'<h2>' . __( 'Child Theme Editor', 'wpthk' ) . '</h2>';
		}

		echo	'<h2 class="nav-tab-wrapper">';

		foreach( $this->_tabs as $key => $val ) {
			register_setting( $key, $key, 'esc_attr' );
			echo	'<a href="', esc_url( admin_url( 'admin.php?page=' . $this->_page . '&active=' . $key ) ) ,'" ',
				'class="nav-tab', $this->_active === $key ? ' nav-tab-active' : '', '">', esc_html( $val ), '</a>';
		}
		echo	'</h2>';

		settings_errors( 'wpthk-custom' );

		$form = false;
		if(
			$this->_active !== 'backup'   &&
			$this->_active !== 'htaccess' &&
			$this->_active !== 'version'  &&
			$this->_active !== 'sns_post' &&
			$this->_active !== 'sns_page' &&
			$this->_active !== 'sns_home'
		) $form = true;

		// options.php は経由しないので、nonce のチェックは check_admin_referer でやる
		if( $form === true ) echo '<form id="wpthk-customize" method="post" action="">';

		$func = '_' . $this->_active . '_section';
		if( method_exists( $this, $func ) === true ) {
			$this->$func();
		}
		else {
			$this->_empty_section();
		}

		settings_fields( $this->_active );

		ob_start();
		do_settings_sections( $this->_active );
		$settings = ob_get_clean();

		$settings = str_replace( '<h2>', '<fieldset class="wpthk-field"><legend><h2 class="wpthk-field-title">', $settings );
		$settings = str_replace( '</h2>', '</h2></legend>', $settings );
		echo $settings;

		if( $_GET['page'] === 'wpthk_edit' ) {
			submit_button( '', 'primary', 'edit_save', true, array( 'disabled' => 1 ) );
			echo '</form>';
		}
		elseif( $form === true ) {
			submit_button( '', 'primary', 'save', true, array( 'disabled' => 1 ) );
			echo '</form>';
		}
?>
</div>
<script>
jQuery(document).ready(function($) {
	$('#wpthk-customize').bind('keyup change', function() {
		$("#save").prop("disabled", false);
	});
	$('.wpthk-field-title').click(function() {
		$(this).parent().nextAll().toggle();
	});
});
</script>
<?php
	}

	public function sections( $args ) {
		get_template_part( 'inc/sections/' . $args['id'] );
		echo '</fieldset>';
	}

	private function _header_section() {
		$suffix = get_locale() === 'ja' ? ' 関連' : '';
		add_settings_section( 'seo', 'SEO' . $suffix, array( $this, 'sections' ), $this->_active );
		add_settings_section( 'ogp', 'OGP' . $suffix, array( $this, 'sections' ), $this->_active );
	}

	private function _title_section() {
		add_settings_section( 'title', sprintf( __( 'Setting of %s', 'wpthk' ), 'Title ' . __( 'tag', 'wpthk' ) ), array( $this, 'sections' ), $this->_active );
	}

	private function _optimize_section() {
		add_settings_section( 'optimize-html', __( 'Compression of HTML', 'wpthk' ), array( $this, 'sections' ), $this->_active );
		add_settings_section( 'optimize-css', __( 'Optimization of CSS', 'wpthk' ), array( $this, 'sections' ), $this->_active );
		add_settings_section( 'optimize-script', __( 'Optimization of Javascript', 'wpthk' ), array( $this, 'sections' ), $this->_active );
	}

	private function _style_section() {
		add_settings_section( 'mode-select', __( 'Mode select', 'wpthk' ), array( $this, 'sections' ), $this->_active );
		add_settings_section( 'css-to-style', __( 'Direct output of external CSS', 'wpthk' ), array( $this, 'sections' ), $this->_active );
		add_settings_section( 'child-css', __( 'CSS of child theme', 'wpthk' ), array( $this, 'sections' ), $this->_active );
		add_settings_section( 'fontawesome', __( 'CSS of icon fonts', 'wpthk' ), array( $this, 'sections' ), $this->_active );
		add_settings_section( 'widget-css', __( 'CSS of widgets', 'wpthk' ), array( $this, 'sections' ), $this->_active );
	}

	private function _script_section() {
		add_settings_section( 'jquery', 'jQuery', array( $this, 'sections' ), $this->_active );
		add_settings_section( 'bootstrap', 'Bootstrap ' . __( 'Plugins', 'wpthk' ), array( $this, 'sections' ), $this->_active );
		add_settings_section( 'script', __( 'Other setting of Javascript', 'wpthk' ), array( $this, 'sections' ), $this->_active );
	}

	private function _search_section() {
		add_settings_section( 'search', sprintf( __( 'Setting of %s', 'wpthk' ), __( 'Search Widget', 'wpthk' ) ), array( $this, 'sections' ), $this->_active );
	}

	private function _adsense_section() {
		add_settings_section( 'adsense', __( 'Advertisement', 'wpthk' ) . ' ( ' . __( 'Adsense', 'wpthk' ) . ' )', array( $this, 'sections' ), $this->_active );
	}

	private function _others_section() {
		add_settings_section( 'others', sprintf( __( 'Setting of %s', 'wpthk' ), __( 'Others', 'wpthk' ) ), array( $this, 'sections' ), $this->_active );
	}

	private function _htaccess_section() {
		add_settings_section( 'htaccess', __( 'htaccess for speed boost', 'wpthk' ), array( $this, 'sections' ), $this->_active );
	}

	private function _backup_section() {
		add_settings_section( 'backup', __( 'Back up WpTHK', 'wpthk' ), array( $this, 'sections' ), $this->_active );
		add_settings_section( 'restore', __( 'Restore WpTHK', 'wpthk' ), array( $this, 'sections' ), $this->_active );
	}

	private function _reset_section() {
		add_settings_section( 'all_clear', __( 'RESET all the customizations of WpTHK', 'wpthk' ), array( $this, 'sections' ), $this->_active );
		add_settings_section( 'sns-cache-cleanup', __( 'Clean up of SNS count cache', 'wpthk' ), array( $this, 'sections' ), $this->_active );
	}

	private function _version_section() {
		add_settings_section( 'version', '', array( $this, 'sections' ), $this->_active );
	}

	private function _sns_setting_section() {
		add_settings_section( 'sns-cache-setting', sprintf( __( 'Setting of %s', 'wpthk' ), __( 'cache', 'wpthk' ) ), array( $this, 'sections' ), $this->_active );
		add_settings_section( 'sns-cache-cleanup', __( 'Clean up of SNS count cache', 'wpthk' ), array( $this, 'sections' ), $this->_active );
	}

	private function _sns_post_section() {
		get_template_part( 'inc/sns-count-view' );
		$cache_view = new cache_control();
		add_settings_section( 'sns-cache-list', '', array( $cache_view, 'sns_cache_list' ), $this->_active );
	}

	private function _sns_page_section() {
		$this->_sns_post_section();
	}

	private function _sns_home_section() {
		$this->_sns_post_section();
	}

	private function _edit_style_section() {
		get_template_part( 'inc/theme-editor' );
	}

	private function _edit_script_section() {
		$this->_edit_style_section();
	}

	private function _edit_header_section() {
		$this->_edit_style_section();
	}

	private function _edit_footer_section() {
		$this->_edit_style_section();
	}

	private function _edit_functions_section() {
		$this->_edit_style_section();
	}

	private function _empty_section() {
		return;
	}
}
