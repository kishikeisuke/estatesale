<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

add_action( 'customize_register', function( $wp_customize ) {
	//---------------------------------------------------------------------------
	// サイト情報 / サイトアイコン
	//---------------------------------------------------------------------------
	$wp_customize->add_section( 'title_tagline', array(
		'title'    => __( 'Site Identity / Site Icon', 'wpthk' ),
		'priority' => 20
	) );

	//---------------------------------------------------------------------------
	// 全体レイアウト (1)
	//---------------------------------------------------------------------------
	$wp_customize->add_section( 'the_overall_image', array(
		'title'		=> __( 'The Entire Layout', 'wpthk' ) . ' (1)',
		'priority'	=> 21
	) );

	// 全体イメージ
	$wp_customize->add_setting( 'overall_image', array(
		'default'	=> 'white',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'overall_image', array(
		'settings'	=> 'overall_image',
		'label'		=> __( 'The entire image', 'wpthk' ),
		'description'	=> __( 'Pure has almost no color assigned other than border, therefore if you want o customize sytle.css by yourself, go for this one!', 'wpthk' ),
		'section'	=> 'the_overall_image',
		'type'		=> 'select',
		'choices'	=> array(
			'white'		=> __( 'White Contrast', 'wpthk' ),
			'pearl'		=> __( 'Pearl White', 'wpthk' ),
			'coffee'	=> __( 'Coffee', 'wpthk' ),
			'dark'		=> __( 'Dark', 'wpthk' ),
			'black'		=> __( 'Black', 'wpthk' ),
			'pure'		=> __( 'Pure ( if you want to manually write style.css)', 'wpthk' )
		),
		'priority'	=> 10
	) );

	// 記事一覧の表示方法
	$wp_customize->add_setting( 'list_view', array(
		'default'	=> 'excerpt',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'list_view', array(
		'settings'	=> 'list_view',
		'label'		=> __( 'Display layout of the article list', 'wpthk' ),
		'section'	=> 'the_overall_image',
		'type'		=> 'select',
		'choices'	=> array(
			'excerpt'	=> __( 'Excerpt + Thumbnail display', 'wpthk' ),
			'content'	=> __( 'Full article display (Until more tag)', 'wpthk' )
		),
		'priority'	=> 25
	) );

	// 先頭固定の投稿は本文表示にする
	$wp_customize->add_setting( 'sticky_no_excerpt', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'sticky_no_excerpt', array(
		'settings'	=> 'sticky_no_excerpt',
		'label'		=> __( 'Sticky post to show all content', 'wpthk' ),
		'description'	=> '<p class="f09em mm23l">' . __( '* Use this feature when you want to show Latest Updates or News as your first post.', 'wpthk' ) . '</p>',
		'section'	=> 'the_overall_image',
		'type'		=> 'checkbox',
		'priority'	=> 30
	) );

	// ページャーの表示有無
	$wp_customize->add_setting( 'pagination_visible', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'pagination_visible', array(
		'settings'	=> 'pagination_visible',
		'label'		=> __( 'Display Pager', 'wpthk' ),
		'description'	=> '<p class="bold snormal f11em mm23l mm10b">' . __( 'Post page', 'wpthk' ) . '</p>',
		'section'	=> 'the_overall_image',
		'type'		=> 'checkbox',
		'priority'	=> 60
	) );

	// 関連記事の表示有無
	$wp_customize->add_setting( 'related_visible', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'related_visible', array(
		'settings'	=> 'related_visible',
		'label'		=> __( 'Display Related Articles', 'wpthk' ),
		'section'	=> 'the_overall_image',
		'type'		=> 'checkbox',
		'priority'	=> 65
	) );

	// コメント欄の表示有無
	$wp_customize->add_setting( 'comment_visible', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'comment_visible', array(
		'settings'	=> 'comment_visible',
		'label'		=> __( 'Display Comment', 'wpthk' ),
		'section'	=> 'the_overall_image',
		'type'		=> 'checkbox',
		'priority'	=> 70
	) );

	// トラックバック URL の表示有無
	$wp_customize->add_setting( 'trackback_visible', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'trackback_visible', array(
		'settings'	=> 'trackback_visible',
		'label'		=> __( 'Display Trackback URL', 'wpthk' ),
		'description'	=> '<p class="bold snormal f11em mm23l mm10b">' . __( 'Static page', 'wpthk' ) . '</p>',
		'section'	=> 'the_overall_image',
		'type'		=> 'checkbox',
		'priority'	=> 75
	) );

	// コメント欄（固定ページ）の表示有無
	$wp_customize->add_setting( 'comment_page_visible', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'comment_page_visible', array(
		'settings'	=> 'comment_page_visible',
		'label'		=> __( 'Display Comment', 'wpthk' ),
		'section'	=> 'the_overall_image',
		'type'		=> 'checkbox',
		'priority'	=> 80
	) );

	// トラックバック URL（固定ページ）の表示有無
	$wp_customize->add_setting( 'trackback_page_visible', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'trackback_page_visible', array(
		'settings'	=> 'trackback_page_visible',
		'label'		=> __( 'Display Trackback URL', 'wpthk' ),
		'section'	=> 'the_overall_image',
		'type'		=> 'checkbox',
		'priority'	=> 85
	) );

	// フッターウィジェットエリア表示数
	$wp_customize->add_setting( 'foot_widget', array(
		'default'	=> 3,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'foot_widget', array(
		'settings'	=> 'foot_widget',
		'label'		=> __( 'Footer widget area display', 'wpthk' ),
		'section'	=> 'the_overall_image',
		'type'		=> 'select',
		'choices'	=> array(
			3	=> __( '3 rows (left, center, and right)', 'wpthk' ),
			2	=> __( '2 rows (left and right)', 'wpthk' ),
			1	=> __( 'Horizontal row (center)', 'wpthk' ),
			0	=> __( 'Hide', 'wpthk' )
		),
		'priority'	=> 90
	) );

	//---------------------------------------------------------------------------
	// 全体レイアウト (2)
	//---------------------------------------------------------------------------
	$wp_customize->add_section( 'layout_section', array(
		'title'		=> __( 'The Entire Layout', 'wpthk' ) . ' (2)',
		'priority'	=> 25
	) );

	// コンテナの最大幅
	$wp_customize->add_setting( 'container_max_width', array(
		'default'	=> 1170,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'container_max_width', array(
		'settings'	=> 'container_max_width',
		'label'		=> __( 'The maximum width of the container', 'wpthk' ),
		'description'	=> __( '* 0 would be full width', 'wpthk' ) . '<br />' . __( '* default value of Bootstrap 1170', 'wpthk' ),
		'section'	=> 'layout_section',
		'type'		=> 'number',
		'priority'	=> 5
	) );

	// タイトルの配置
	$wp_customize->add_setting( 'title_position', array(
		'default'	=> 'left',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'title_position', array(
		'settings'	=> 'title_position',
		'label'		=> __( 'Title layout', 'wpthk' ),
		'section'	=> 'layout_section',
		'type'		=> 'select',
		'choices'	=> array(
			'left'		=> __( 'left', 'wpthk' ),
			'center'	=> __( 'center', 'wpthk' ),
			'right'		=> __( 'right', 'wpthk' )
		),
		'priority'	=> 5
	) );

	// ヘッダーの位置
	$wp_customize->add_setting( 'bootstrap_header', array(
		'default'	=> 'out',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'bootstrap_header', array(
		'settings'	=> 'bootstrap_header',
		'label'		=> __( 'Position of the header', 'wpthk' ),
		'section'	=> 'layout_section',
		'type'		=> 'select',
		'choices'	=> array(
			'in'		=> __( 'Inside the Container', 'wpthk' ),
			'out'		=> __( 'Outside the Container', 'wpthk' )
		),
		'priority'	=> 15
	) );

	// ヘッダーを枠線で囲む
	$wp_customize->add_setting( 'header_border', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'header_border', array(
		'settings'	=> 'header_border',
		'label'		=> __( 'Surround the header with border', 'wpthk' ),
		'section'	=> 'layout_section',
		'type'		=> 'checkbox',
		'priority'	=> 20
	) );

	// ヘッダーを枠線で囲む
	$wp_customize->add_setting( 'header_border_wide', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'header_border_wide', array(
		'settings'	=> 'header_border_wide',
		'label'		=> __( 'Full width for the header border', 'wpthk' ),
		'description'	=> __( '(Valid only when outside the container)', 'wpthk' ),
		'section'	=> 'layout_section',
		'type'		=> 'checkbox',
		'priority'	=> 21
	) );

	// フッターの位置
	$wp_customize->add_setting( 'bootstrap_footer', array(
		'default'	=> 'out',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'bootstrap_footer', array(
		'settings'	=> 'bootstrap_footer',
		'label'		=> __( 'Position of footer', 'wpthk' ),
		'section'	=> 'layout_section',
		'type'		=> 'select',
		'choices'	=> array(
			'in'		=> __( 'Inside the Container', 'wpthk' ),
			'out'		=> __( 'Outside the Container', 'wpthk' )
		),
		'priority'	=> 25
	) );

	// フッターを枠線で囲む
	$wp_customize->add_setting( 'footer_border', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'footer_border', array(
		'settings'	=> 'footer_border',
		'label'		=> __( 'Surround the footer with border', 'wpthk' ),
		'section'	=> 'layout_section',
		'type'		=> 'checkbox',
		'priority'	=> 30
	) );

	// フッター上部に太線を入れる
	$wp_customize->add_setting( 'footer_border_top', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'footer_border_top', array(
		'settings'	=> 'footer_border_top',
		'label'		=> __( 'Put a bold line above the footer', 'wpthk' ),
		'section'	=> 'layout_section',
		'type'		=> 'checkbox',
		'priority'	=> 35
	) );

	// ヘッダー margin-top
	$wp_customize->add_setting( 'head_margin_top', array(
		'default'	=> 0,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'head_margin_top', array(
		'settings'	=> 'head_margin_top',
		'label'		=> __( 'Header margin', 'wpthk' ),
		'description'	=> __( 'Header ', 'wpthk' ) . __( 'top', 'wpthk' ) . ' ( 0px )',
		'section'	=> 'layout_section',
		'type'		=> 'number',
		'priority'	=> 45
	) );

	// ヘッダー padding-top
	$wp_customize->add_setting( 'head_padding_top', array(
		'default'	=> 20,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'head_padding_top', array(
		'settings'	=> 'head_padding_top',
		'label'		=> __( 'Header padding', 'wpthk' ),
		'description'	=> __( 'Header ', 'wpthk' ) . __( 'top', 'wpthk' ) . ' ( 20px )',
		'section'	=> 'layout_section',
		'type'		=> 'number',
		'priority'	=> 50
	) );

	// ヘッダー padding-right
	$wp_customize->add_setting( 'head_padding_right', array(
		'default'	=> 10,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'head_padding_right', array(
		'settings'	=> 'head_padding_right',
		'description'	=> __( 'Header ', 'wpthk' ) . __( 'right', 'wpthk' ) . ' ( 10px )',
		'section'	=> 'layout_section',
		'type'		=> 'number',
		'priority'	=> 55
	) );

	// ヘッダー padding-bottom
	$wp_customize->add_setting( 'head_padding_bottom', array(
		'default'	=> 20,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'head_padding_bottom', array(
		'settings'	=> 'head_padding_bottom',
		'description'	=> __( 'Header ', 'wpthk' ) . __( 'bottom', 'wpthk' ) . ' ( 20px )',
		'section'	=> 'layout_section',
		'type'		=> 'number',
		'priority'	=> 60
	) );

	// ヘッダー padding-left
	$wp_customize->add_setting( 'head_padding_left', array(
		'default'	=> 10,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'head_padding_left', array(
		'settings'	=> 'head_padding_left',
		'description'	=> __( 'Header ', 'wpthk' ) . __( 'left', 'wpthk' ) . ' ( 10px )',
		'section'	=> 'layout_section',
		'type'		=> 'number',
		'priority'	=> 65
	) );

	//---------------------------------------------------------------------------
	// カラム操作
	//---------------------------------------------------------------------------
	$wp_customize->add_section( 'columns_section', array(
		'title'		=> __( 'Column adjustment', 'wpthk' ),
		'priority'	=> 27
	) );

	// カラム数 (全体デフォルト)
	$wp_customize->add_setting( 'column3', array(
		'default'	=> '2column',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'column3', array(
		'settings'	=> 'column3',
		'label'		=> __( 'Number of column', 'wpthk' ),
		'description'	=> '<p class="f09em">' . __( 'default', 'wpthk' ) . '</p>',
		'section'	=> 'columns_section',
		'type'		=> 'select',
		'choices'	=> array(
			'1column'	=> '1 ' . __( 'column', 'wpthk' ),
			'2column'	=> '2 ' . __( 'column', 'wpthk' ),
			'3column'	=> '3 ' . __( 'column', 'wpthk' )
		),
		'priority'	=> 5
	) );

	// カラム数 (フロントページ)
	$wp_customize->add_setting( 'column_home', array(
		'default'	=> 'default',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'column_home', array(
		'settings'	=> 'column_home',
		//'label'		=> __( 'カラム数 (フロントページ)', 'wpthk' ),
		'description'	=> '<p class="f09em m0t">' . __( 'Number of columns in each template', 'wpthk' ) . '</p>',
		'section'	=> 'columns_section',
		'type'		=> 'select',
		'choices'	=> array(
			'default'	=> __( 'Front page', 'wpthk' ),
			'1column'	=> '1 ' . __( 'column', 'wpthk' ),
			'2column'	=> '2 ' . __( 'column', 'wpthk' ),
			'3column'	=> '3 ' . __( 'column', 'wpthk' )
		),
		'priority'	=> 10
	) );

	// カラム数 (投稿ページ)
	$wp_customize->add_setting( 'column_post', array(
		'default'	=> 'default',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'column_post', array(
		'settings'	=> 'column_post',
		//'label'		=> __( 'カラム数 (投稿ページ)', 'wpthk' ),
		'section'	=> 'columns_section',
		'type'		=> 'select',
		'choices'	=> array(
			'default'	=> __( 'Posts page', 'wpthk' ),
			'1column'	=> '1 ' . __( 'column', 'wpthk' ),
			'2column'	=> '2 ' . __( 'column', 'wpthk' ),
			'3column'	=> '3 ' . __( 'column', 'wpthk' )
		),
		'priority'	=> 15
	) );

	// カラム数 (固定ページ)
	$wp_customize->add_setting( 'column_page', array(
		'default'	=> 'default',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'column_page', array(
		'settings'	=> 'column_page',
		//'label'		=> __( 'カラム数 (固定ページ)', 'wpthk' ),
		'section'	=> 'columns_section',
		'type'		=> 'select',
		'choices'	=> array(
			'default'	=> __( 'Static page', 'wpthk' ),
			'1column'	=> '1 ' . __( 'column', 'wpthk' ),
			'2column'	=> '2 ' . __( 'column', 'wpthk' ),
			'3column'	=> '3 ' . __( 'column', 'wpthk' )
		),
		'priority'	=> 20
	) );

	// カラム数 (アーカイブページ)
	$wp_customize->add_setting( 'column_archive', array(
		'default'	=> 'default',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'column_archive', array(
		'settings'	=> 'column_archive',
		//'label'		=> __( 'カラム数 (アーカイブページ)', 'wpthk' ),
		'section'	=> 'columns_section',
		'type'		=> 'select',
		'choices'	=> array(
			'default'	=> __( 'Category', 'wpthk' ) . ' / ' . __( 'Archive', 'wpthk' ),
			'1column'	=> '1 ' . __( 'column', 'wpthk' ),
			'2column'	=> '2 ' . __( 'column', 'wpthk' ),
			'3column'	=> '3 ' . __( 'column', 'wpthk' )
		),
		'priority'	=> 25
	) );

	// サイドバーの位置 (2カラム)
	$wp_customize->add_setting( 'side_position', array(
		'default'	=> 'right',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'side_position', array(
		'settings'	=> 'side_position',
		'label'		=> __( 'Position of the side bar', 'wpthk' ),
		'description'	=> __( '2 column', 'wpthk' ),
		'section'	=> 'columns_section',
		'type'		=> 'select',
		'choices'	=> array(
			'right'		=> __( 'Right sidebar', 'wpthk' ),
			'left'		=> __( 'Left sidebar', 'wpthk' )
		),
		'priority'	=> 30
	) );

	// サイドバーの位置 (3カラム)
	$wp_customize->add_setting( 'column3_position', array(
		'default'	=> 'center',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'column3_position', array(
		'settings'	=> 'column3_position',
		'description'	=> __( '3 column', 'wpthk' ),
		'section'	=> 'columns_section',
		'type'		=> 'select',
		'choices'	=> array(
			'center'	=> __( 'Both sides sidebar', 'wpthk' ),
			'right'		=> __( 'Right sidebar', 'wpthk' ),
			'left'		=> __( 'Left sidebar', 'wpthk' )
		),
		'priority'	=> 35
	) );

	// 3カラム左右サイドバー反転
	$wp_customize->add_setting( 'column3_reverse', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'column3_reverse', array(
		'settings'	=> 'column3_reverse',
		'label'		=> __( '3 column left and right side bar reversal', 'wpthk' ),
		'section'	=> 'columns_section',
		'type'		=> 'checkbox',
		'priority'	=> 36
	) );

	// パンくずリンクの配置
	$wp_customize->add_setting( 'breadcrumb_view', array(
		'default'	=> 'outer',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'breadcrumb_view', array(
		'settings'	=> 'breadcrumb_view',
		'label'		=> __( 'Position of breadcrumb link', 'wpthk' ),
		'section'	=> 'columns_section',
		'type'		=> 'select',
		'choices'	=> array(
			'outer'		=> __( 'Outside of the content area', 'wpthk' ),
			'inner'		=> __( 'Inside of the content area', 'wpthk' ),
			'none'		=> __( 'Do not show', 'wpthk' )
		),
		'priority'	=> 40
	) );

	// コンテンツ領域の分離・結合
	$wp_customize->add_setting( 'content_discrete', array(
		'default'	=> 'discrete',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'content_discrete', array(
		'settings'	=> 'content_discrete',
		'label'		=> __( 'Merge and separate areas', 'wpthk' ),
		'description'	=> '<p class="bold snormal mm5b">' . __( 'Category', 'wpthk' ) . ' / ' . __( 'Archive type pages', 'wpthk' ) . '</p>',
		'section'	=> 'columns_section',
		'type'		=> 'radio',
		'choices'	=> array(
			'discrete'	=> __( 'Separate content area for each post', 'wpthk' ),
			'indiscrete'	=> __( 'Merge content area', 'wpthk' )
		),
		'priority'	=> 45
	) );

	// サイドバー領域の分離と結合
	$wp_customize->add_setting( 'side_discrete', array(
		'default'	=> 'indiscrete',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'side_discrete', array(
		'settings'	=> 'side_discrete',
		'label'		=> __( 'Sidebar ', 'wpthk' ),
		'section'	=> 'columns_section',
		'type'		=> 'radio',
		'choices'	=> array(
			'discrete'	=> __( 'Separate the side bar for each element', 'wpthk' ),
			'indiscrete'	=> __( 'Merge the elements in side bar', 'wpthk' )
		),
		'priority'	=> 50
	) );

	// コンテンツ領域とサイドバーの分離・結合
	$wp_customize->add_setting( 'content_side_discrete', array(
		'default'	=> 'discrete',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'content_side_discrete', array(
		'settings'	=> 'content_side_discrete',
		'label'		=> __( 'Merge and separate content area and side bar', 'wpthk' ),
		'section'	=> 'columns_section',
		'type'		=> 'radio',
		'choices'	=> array(
			'discrete'	=> __( 'Separate content area and side bar', 'wpthk' ),
			'indiscrete'	=> __( 'Merge content area and side bar', 'wpthk' )
		),
		'priority'	=> 55
	) );

	$wp_customize->add_setting( 'dummy1', array( 'sanitize_callback' => 'thk_sanitize' ) );
	$wp_customize->add_control( 'dummy1', array(
		'settings'	=> 'dummy1',
		'description'	=> '<p class="f09em">' . __( '* When they are merged, some configuration of content area will overwrite the side bar configuration.', 'wpthk' ) . '</p>',
		'section'	=> 'columns_section',
		'priority'	=> 60
	) );

	//---------------------------------------------------------------------------
	// コンテンツ領域とサイドバー
	//---------------------------------------------------------------------------
	$wp_customize->add_section( 'cont_side_section', array(
		'title'		=> __( 'Content area and side bar', 'wpthk' ),
		'description'	=> '<p class="bold f11em mm15b">' . __( 'Content area', 'wpthk' ) . '</p>',
		'priority'	=> 28
	) );

	// コンテンツ領域に枠線をつける
	$wp_customize->add_setting( 'contents_border', array(
		'default' 	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'contents_border', array(
		'settings'	=> 'contents_border',
		'label'		=> __( 'Put border around the content area', 'wpthk' ),
		'section'	=> 'cont_side_section',
		'type'		=> 'checkbox',
		'priority'	=> 5
	) );

	// ページャー表示領域に枠線(コンテンツ領域分離時のみ)
	$wp_customize->add_setting( 'pagination_area_border', array(
		'default' 	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'pagination_area_border', array(
		'settings'	=> 'pagination_area_border',
		'label'		=> __( 'Apply frame border around pagination area', 'wpthk' ),
		'description'	=> __( '* Only when content area is separated', 'wpthk' ),
		'section'	=> 'cont_side_section',
		'type'		=> 'checkbox',
		'priority'	=> 6
	) );

	// コンテンツ領域枠線の丸み
	$wp_customize->add_setting( 'cont_border_radius', array(
		'default'	=> 0,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'cont_border_radius', array(
		'settings'	=> 'cont_border_radius',
		'label'		=> __( 'Border radius', 'wpthk' ),
		'section'	=> 'cont_side_section',
		'type'		=> 'number',
		'priority'	=> 10
	) );

	// コンテンツ領域 padding-top
	$wp_customize->add_setting( 'cont_padding_top', array(
		'default'	=> 35,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'cont_padding_top', array(
		'settings'	=> 'cont_padding_top',
		'label'		=> __( 'Content area padding', 'wpthk' ),
		'description'	=> __( 'Content ', 'wpthk' ) . __( 'top', 'wpthk' ) . ' ( 35px )',
		'section'	=> 'cont_side_section',
		'type'		=> 'number',
		'priority'	=> 15
	) );

	// コンテンツ領域 padding-right
	$wp_customize->add_setting( 'cont_padding_right', array(
		'default'	=> 25,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'cont_padding_right', array(
		'settings'	=> 'cont_padding_right',
		'description'	=> __( 'Content ', 'wpthk' ) . __( 'right', 'wpthk' ) . ' ( 25px )',
		'section'	=> 'cont_side_section',
		'type'		=> 'number',
		'priority'	=> 20
	) );

	// コンテンツ領域 padding-bottom
	$wp_customize->add_setting( 'cont_padding_bottom', array(
		'default'	=> 0,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'cont_padding_bottom', array(
		'settings'	=> 'cont_padding_bottom',
		'description'	=> __( 'Content ', 'wpthk' ) . __( 'bottom', 'wpthk' ) . ' ( 0px )',
		'section'	=> 'cont_side_section',
		'type'		=> 'number',
		'priority'	=> 25
	) );

	// コンテンツ領域 padding-left
	$wp_customize->add_setting( 'cont_padding_left', array(
		'default'	=> 25,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'cont_padding_left', array(
		'settings'	=> 'cont_padding_left',
		'description'	=> __( 'Content ', 'wpthk' ) . __( 'left', 'wpthk' ) . ' ( 25px )',
		'section'	=> 'cont_side_section',
		'type'		=> 'number',
		'priority'	=> 30
	) );

	// サイドバーの幅
	$wp_customize->add_setting( 'side_1_width', array(
		'default'	=> 366,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'side_1_width', array(
		'settings'	=> 'side_1_width',
		'label'		=> __( 'Sidebar width', 'wpthk' ),
		'description'	=> __( 'Second column', 'wpthk' ),
		'section'	=> 'cont_side_section',
		'type'		=> 'number',
		'priority'	=> 35
	) );

	// サイドバーの幅 (3カラム目)
	$wp_customize->add_setting( 'side_2_width', array(
		'default'	=> 280,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'side_2_width', array(
		'settings'	=> 'side_2_width',
		'description'	=> __( 'Third column', 'wpthk' ),
		'section'	=> 'cont_side_section',
		'type'		=> 'number',
		'priority'	=> 36
	) );

	// サイドバーの位置
	$wp_customize->add_setting( 'side_position_dummy', array( 'sanitize_callback' => 'thk_sanitize' ) );
	$wp_customize->add_control( 'side_position_dummy', array(
		'settings'	=> 'side_position_dummy',
		'label'		=> __( 'Position of the side bar', 'wpthk' ),
		'description'	=> __( '* You can change the side bar position in the Column adjustment menu.', 'wpthk' ),
		'section'	=> 'cont_side_section',
		'priority'	=> 40
	) );

	// サイドバーを枠線で囲む
	$wp_customize->add_setting( 'sidebar_border', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'sidebar_border', array(
		'settings'	=> 'sidebar_border',
		'label'		=> __( 'Surround the sidebar with border', 'wpthk' ),
		'section'	=> 'cont_side_section',
		'type'		=> 'checkbox',
		'priority'	=> 45
	) );

	// サイドバー枠線の丸み
	$wp_customize->add_setting( 'side_border_radius', array(
		'default'	=> 0,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'side_border_radius', array(
		'settings'	=> 'side_border_radius',
		'label'		=> __( 'Border radius', 'wpthk' ),
		'section'	=> 'cont_side_section',
		'type'		=> 'number',
		'priority'	=> 50
	) );

	//---------------------------------------------------------------------------
	// 細部の見た目
	//---------------------------------------------------------------------------
	$wp_customize->add_section( 'view_section', array(
		'title'		=> __( 'Style Details', 'wpthk' ),
		'priority'	=> 30
	) );

	// 記事一覧の抜粋の文字数
	$wp_customize->add_setting( 'excerpt_length', array(
		'default' 	=> 120,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'excerpt_length', array(
		'settings'	=> 'excerpt_length',
		'label'		=> __( 'Number of characters in excerpt on article list', 'wpthk' ),
		'section'	=> 'view_section',
		'type'		=> 'number',
		'priority'	=> 10
	) );

	// 記事一覧の抜粋を改行するかどうか
	$wp_customize->add_setting( 'break_excerpt', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'break_excerpt', array(
		'settings'	=> 'break_excerpt',
		'label'		=> __( 'No line break in excerpt on article list', 'wpthk' ),
		'section'	=> 'view_section',
		'type'		=> 'checkbox',
		'priority'	=> 15
	) );

	// 投稿・編集画面の抜粋を優先表示
	$wp_customize->add_setting( 'excerpt_priority', array(
		'default' 	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'excerpt_priority', array(
		'settings'	=> 'excerpt_priority',
		'label'		=> __( 'Excerpt on each post will be prioritized and displayed', 'wpthk' ),
		'description'	=> '<p class="f09em mm23l">' . __( '* Description will always prioritize and display the excerpts from the blog post.', 'wpthk' ) . '</p>',
		'section'	=> 'view_section',
		'type'		=> 'checkbox',
		'priority'	=> 20
	) );

	// 「記事を読む」の文言
	$wp_customize->add_setting( 'read_more_text', array(
		'default' 	=> __( 'Read more', 'wpthk' ),
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'read_more_text', array(
		'settings'	=> 'read_more_text',
		'label'		=> __( 'Text for the &quot;Read More&quot;', 'wpthk' ),
		'description'	=> '<p class="f09em">' . __( '* Can hide the link if kept blank.', 'wpthk' ) . '</p>',
		'section'	=> 'view_section',
		'type'		=> 'text',
		'priority'	=> 22
	) );

	// 「記事を読む」のリンクに短いタイトル付ける
	$wp_customize->add_setting( 'read_more_short_title', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'read_more_short_title', array(
		'settings'	=> 'read_more_short_title',
		'label'		=> __( 'Put a short title to the link of &quot;Read more&quot;', 'wpthk' ),
		'section'	=> 'view_section',
		'type'		=> 'checkbox',
		'priority'	=> 25
	) );

	// 「記事を読む」のタイトルの文字数
	$wp_customize->add_setting( 'short_title_length', array(
		'default' 	=> 16,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'short_title_length', array(
		'settings'	=> 'short_title_length',
		'description'	=> __( 'Number of characters in the short title', 'wpthk' ),
		'section'	=> 'view_section',
		'type'		=> 'number',
		'priority'	=> 30
	) );

	// ヘッダーのキャッチフレーズを変更
	$wp_customize->add_setting( 'header_catchphrase_change', array(
		'default'	=> get_bloginfo('description'),
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'header_catchphrase_change', array(
		'settings'	=> 'header_catchphrase_change',
		'label'		=> __( 'Tagline change on header', 'wpthk' ),
		'description'	=> '<p class="f09em">' . __( '* To overwrite the tagline on the header to something other than what is set on the Wordpress General Settings.', 'wpthk' ) . '</p>',
		'section'	=> 'view_section',
		'type'		=> 'text',
		'priority'	=> 35
	) );

	// ヘッダーのキャッチフレーズ表示
	$wp_customize->add_setting( 'header_catchphrase_visible', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'header_catchphrase_visible', array(
		'settings'	=> 'header_catchphrase_visible',
		'label'		=> __( 'Tagline display on header', 'wpthk' ),
		'section'	=> 'view_section',
		'type'		=> 'checkbox',
		'priority'	=> 40
	) );

	// 「ホーム」の名称
	$wp_customize->add_setting( 'home_text', array(
		'default'	=> __( 'Home', 'wpthk' ),
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'home_text', array(
		'settings'	=> 'home_text',
		'label'		=> __( 'Text for &quot;Home&quot;', 'wpthk' ),
		'section'	=> 'view_section',
		'type'		=> 'text',
		'priority'	=> 42
	) );

	// PAGE TOP ボタン
	$wp_customize->add_setting( 'page_top_text', array(
		'default'	=> 'PAGE TOP',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'page_top_text', array(
		'settings'	=> 'page_top_text',
		'label'		=> __( 'Text for &quot;PAGE TOP&quot; scroll button', 'wpthk' ),
		'description'	=> '<p class="f09em">' . __( '* Only icon will be displayed if kept blank.', 'wpthk' ) . '</p>',
		'section'	=> 'view_section',
		'type'		=> 'text',
		'priority'	=> 45
	) );

	// PAGE TOP ボタンのアイコンの種類
	$wp_customize->add_setting( 'page_top_icon', array(
		'default'	=> 'fa_caret_square_o_up',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'page_top_icon', array(
		'settings'	=> 'page_top_icon',
		'label'		=> __( 'Type of &quot;PAGE TOP&quot; button icon', 'wpthk' ),
		'section'	=> 'view_section',
		'type'		=> 'radio',
		'choices'	=> array(
			'fa_caret_square_o_up'	=> 'fa-caret-square-o-up',
			'fa_arrow_up'		=> 'fa-arrow-up',
			'fa_caret_up'		=> 'fa-caret-up',
			'fa_chevron_up'		=> 'fa-chevron-up',
			'fa_chevron_circle_up'	=> 'fa-chevron-circle-up',
			'fa_arrow_circle_up'	=> 'fa-arrow-circle-up',
			'fa_arrow_circle_o_up'	=> 'fa-arrow-circle-o-up'
		),
		'priority'	=> 50
	) );

	// PAGE TOP 文字色
	$wp_customize->add_setting( 'page_top_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page_top_color', array(
		'settings'	=> 'page_top_color',
		'label'		=> 'PAGE TOP ' . __( 'Text color', 'wpthk' ),
		'section'	=> 'view_section',
		'priority'	=> 55
	) ) );

	// PAGE TOP 背景色
	$wp_customize->add_setting( 'page_top_bg_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'page_top_bg_color', array(
		'settings'	=> 'page_top_bg_color',
		'label'		=> 'PAGE TOP ' . __( 'Background color', 'wpthk' ),
		'section'	=> 'view_section',
		'priority'	=> 60
	) ) );

	//---------------------------------------------------------------------------
	// メタ情報の表示位置
	//---------------------------------------------------------------------------
	$wp_customize->add_section( 'meta_section', array(
		'title'		=> __( 'Display position of meta information', 'wpthk' ),
		'description'	=> '<p class="bold snormal f11em">' . __( 'Post and Static page', 'wpthk' ) . '<br /><p class="f09em">' . __( '* hiding either the publish date or the update date might cause error in the Google Webmasters tool.', 'wpthk' ) . '</p><p class="bold snormal f11em">' . __( 'What meta information to dsiplay under the article title', 'wpthk' ) . '</p>',
		'priority'	=> 31
	) );

	// 投稿日時表示
	$wp_customize->add_setting( 'post_date_visible', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'post_date_visible', array(
		'settings'	=> 'post_date_visible',
		'label'		=> __( 'Published date and time', 'wpthk' ),
		'section'	=> 'meta_section',
		'type'		=> 'checkbox',
		'priority'	=> 5
	) );

	// 更新日時表示
	$wp_customize->add_setting( 'mod_date_visible', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'mod_date_visible', array(
		'settings'	=> 'mod_date_visible',
		'label'		=> __( 'Updated date and time', 'wpthk' ),
		'section'	=> 'meta_section',
		'type'		=> 'checkbox',
		'priority'	=> 10
	) );

	// カテゴリー名表示
	$wp_customize->add_setting( 'category_meta_visible', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'category_meta_visible', array(
		'settings'	=> 'category_meta_visible',
		'label'		=> __( 'Category name', 'wpthk' ),
		'section'	=> 'meta_section',
		'type'		=> 'checkbox',
		'priority'	=> 15
	) );

	// タグ表示
	$wp_customize->add_setting( 'tag_meta_visible', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'tag_meta_visible', array(
		'settings'	=> 'tag_meta_visible',
		'label'		=> __( 'Tag', 'wpthk' ),
		'section'	=> 'meta_section',
		'type'		=> 'checkbox',
		'priority'	=> 20
	) );

	// タクソノミー表示
	$wp_customize->add_setting( 'tax_meta_visible', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'tax_meta_visible', array(
		'settings'	=> 'tax_meta_visible',
		'label'		=> __( 'Taxonomy', 'wpthk' ),
		'description'	=> '<p class="bold snormal f11em m23t mm10b mm23l">' . __( 'What meta information to dsiplay under article', 'wpthk' ) . '</p>',
		'section'	=> 'meta_section',
		'type'		=> 'checkbox',
		'priority'	=> 20
	) );

	// 投稿日時表示 (記事下)
	$wp_customize->add_setting( 'post_date_u_visible', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'post_date_u_visible', array(
		'settings'	=> 'post_date_u_visible',
		'label'		=> __( 'Published date and time', 'wpthk' ),
		'section'	=> 'meta_section',
		'type'		=> 'checkbox',
		'priority'	=> 25
	) );

	// 更新日時表示 (記事下)
	$wp_customize->add_setting( 'mod_date_u_visible', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'mod_date_u_visible', array(
		'settings'	=> 'mod_date_u_visible',
		'label'		=> __( 'Updated date and time', 'wpthk' ),
		'section'	=> 'meta_section',
		'type'		=> 'checkbox',
		'priority'	=> 30
	) );

	// カテゴリー名表示 (記事下)
	$wp_customize->add_setting( 'category_meta_u_visible', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'category_meta_u_visible', array(
		'settings'	=> 'category_meta_u_visible',
		'label'		=> __( 'Category name', 'wpthk' ),
		'section'	=> 'meta_section',
		'type'		=> 'checkbox',
		'priority'	=> 35
	) );

	// タグ表示 (記事下)
	$wp_customize->add_setting( 'tag_meta_u_visible', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'tag_meta_u_visible', array(
		'settings'	=> 'tag_meta_u_visible',
		'label'		=> __( 'Tag', 'wpthk' ),
		'section'	=> 'meta_section',
		'type'		=> 'checkbox',
		'priority'	=> 40
	) );

	// タクソノミー表示 (記事下)
	$wp_customize->add_setting( 'tax_meta_u_visible', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'tax_meta_u_visible', array(
		'settings'	=> 'tax_meta_u_visible',
		'label'		=> __( 'Taxonomy', 'wpthk' ),
		'description'	=> '<hr><p class="bold snormal f11em m23t mm10b mm23l">' . __( 'List type page', 'wpthk' ) . '</p><p class="bold snormal f11em m23t mm10b mm23l">' . __( 'What meta information to dsiplay under the article title', 'wpthk' ) . '</p>',
		'section'	=> 'meta_section',
		'type'		=> 'checkbox',
		'priority'	=> 42
	) );

	// リストページ 投稿日時表示
	$wp_customize->add_setting( 'list_post_date_visible', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'list_post_date_visible', array(
		'settings'	=> 'list_post_date_visible',
		'label'		=> __( 'Published date and time', 'wpthk' ),
		'section'	=> 'meta_section',
		'type'		=> 'checkbox',
		'priority'	=> 45
	) );

	// リストページ 更新日時表示
	$wp_customize->add_setting( 'list_mod_date_visible', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'list_mod_date_visible', array(
		'settings'	=> 'list_mod_date_visible',
		'label'		=> __( 'Updated date and time', 'wpthk' ),
		'section'	=> 'meta_section',
		'type'		=> 'checkbox',
		'priority'	=> 50
	) );

	// リストページ カテゴリー名表示
	$wp_customize->add_setting( 'list_category_meta_visible', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'list_category_meta_visible', array(
		'settings'	=> 'list_category_meta_visible',
		'label'		=> __( 'Category name', 'wpthk' ),
		'section'	=> 'meta_section',
		'type'		=> 'checkbox',
		'priority'	=> 55
	) );

	// リストページ タグ表示
	$wp_customize->add_setting( 'list_tag_meta_visible', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'list_tag_meta_visible', array(
		'settings'	=> 'list_tag_meta_visible',
		'label'		=> __( 'Tag', 'wpthk' ),
		'section'	=> 'meta_section',
		'type'		=> 'checkbox',
		'priority'	=> 60
	) );

	// リストページ タクソノミー表示
	$wp_customize->add_setting( 'list_tax_meta_visible', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'list_tax_meta_visible', array(
		'settings'	=> 'list_tax_meta_visible',
		'label'		=> __( 'Taxonomy', 'wpthk' ),
		'description'	=> '<p class="bold snormal f11em m23t mm10b mm23l">' . __( 'Meta information displayed above excerpt', 'wpthk' ) . '</p>',
		'section'	=> 'meta_section',
		'type'		=> 'checkbox',
		'priority'	=> 62
	) );

	// リストページ 投稿日時表示 (抜粋下)
	$wp_customize->add_setting( 'list_post_date_u_visible', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'list_post_date_u_visible', array(
		'settings'	=> 'list_post_date_u_visible',
		'label'		=> __( 'Published date and time', 'wpthk' ),
		'section'	=> 'meta_section',
		'type'		=> 'checkbox',
		'priority'	=> 65
	) );

	// リストページ 更新日時表示 (抜粋下)
	$wp_customize->add_setting( 'list_mod_date_u_visible', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'list_mod_date_u_visible', array(
		'settings'	=> 'list_mod_date_u_visible',
		'label'		=> __( 'Updated date and time', 'wpthk' ),
		'section'	=> 'meta_section',
		'type'		=> 'checkbox',
		'priority'	=> 70
	) );

	// リストページ カテゴリー名表示 (抜粋下)
	$wp_customize->add_setting( 'list_category_meta_u_visible', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'list_category_meta_u_visible', array(
		'settings'	=> 'list_category_meta_u_visible',
		'label'		=> __( 'Category name', 'wpthk' ),
		'section'	=> 'meta_section',
		'type'		=> 'checkbox',
		'priority'	=> 75
	) );

	// リストページ タグ表示 (抜粋下)
	$wp_customize->add_setting( 'list_tag_meta_u_visible', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'list_tag_meta_u_visible', array(
		'settings'	=> 'list_tag_meta_u_visible',
		'label'		=> __( 'Tag', 'wpthk' ),
		'section'	=> 'meta_section',
		'type'		=> 'checkbox',
		'priority'	=> 80
	) );

	// リストページ タクソノミー表示 (抜粋下)
	$wp_customize->add_setting( 'list_tax_meta_u_visible', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'list_tax_meta_u_visible', array(
		'settings'	=> 'list_tax_meta_u_visible',
		'label'		=> __( 'Taxonomy', 'wpthk' ),
		'section'	=> 'meta_section',
		'type'		=> 'checkbox',
		'priority'	=> 85
	) );

	//---------------------------------------------------------------------------
	// サムネイル (アイキャッチ) 
	//---------------------------------------------------------------------------
	$wp_customize->add_section( 'thumbnail_section', array(
		'title'		=> __( 'Thumbnail (Featured Image)', 'wpthk' ),
		'description'	=> '<p class="bold f11em mm15b">' . __( 'Thumbnail dispay on/off', 'wpthk' ) . '</p>',
		'priority'	=> 32
	) );

	// サムネイルを表示する
	$wp_customize->add_setting( 'thumbnail_visible', array(
		'default' 	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'thumbnail_visible', array(
		'settings'	=> 'thumbnail_visible',
		'label'		=> __( 'Display thumbnail', 'wpthk' ),
		'section'	=> 'thumbnail_section',
		'type'		=> 'checkbox',
		'priority'	=> 5
	) );

	// No Image のサムネイルを表示する
	$wp_customize->add_setting( 'noimage_visible', array(
		'default' 	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'noimage_visible', array(
		'settings'	=> 'noimage_visible',
		'label'		=> __( 'Thumbnail display when No Image', 'wpthk' ),
		'section'	=> 'thumbnail_section',
		'type'		=> 'checkbox',
		'priority'	=> 10
	) );

	// 画像に対するテキスト(抜粋)の配置
	$wp_customize->add_setting( 'thumbnail_layout', array(
		'default' 	=> 'right',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'thumbnail_layout', array(
		'settings'	=> 'thumbnail_layout',
		'label'		=> __( 'Where to place the text (excerpt) around the image', 'wpthk' ),
		'description'	=> '<p class="f09em">' . __( '* It will always wrap around the image in small devices.', 'wpthk' ) . '</p>',
		'section'	=> 'thumbnail_section',
		'type'		=> 'radio',
		'choices'	=> array(
			'right'	=> __( 'Text right to image (do not allow the text wrap below the image)', 'wpthk' ),
			'flow'	=> __( 'Text right to image (allow the text wrap below the image)', 'wpthk' ),
			'under'	=> __( 'Text below the image', 'wpthk' )
		),
		'priority'	=> 15
	) );

	// 投稿画面のアイキャッチ画像で設定した画像の大きさのまま表示
	$wp_customize->add_setting( 'thumbnail_is_size', array(
		'default' 	=> 'generate',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'thumbnail_is_size', array(
		'settings'	=> 'thumbnail_is_size',
		'label'		=> __( 'Thumbnail display size', 'wpthk' ),
		'section'	=> 'thumbnail_section',
		'type'		=> 'select',
		'choices'	=> array(
			'generate'	=> __( 'Thumbnails created below', 'wpthk' ),
			'thumbnail'	=> 'normal ( 150px )',
			'medium'	=> 'medium ( 300px )',
			'large'		=> 'large ( 640px )',
			'full'		=> 'full ( ' . __( 'Original size', 'wpthk' ) . ' )'
		),
		'priority'	=> 20
	) );

	// サムネイル画像の幅
	$wp_customize->add_setting( 'thumbnail_width', array(
		'default' 	=> 150,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'thumbnail_width', array(
		'settings'	=> 'thumbnail_width',
		'label'		=> '',
		'description'	=> '<p class="f11em bold snormal">' . __( 'Thumbnail size that would be auto created when registered in Media Library', 'wpthk' ) . '</p><p class="f09em">' . __( '* It will create the thumbnail in the below size.', 'wpthk' ) . '</p><p class="f09em">' . __( '* It will auto create a small thumbnail with a size of 66.6&#x25; for small devices.', 'wpthk' ) . '</p><p class="f09em">' . __( '* Pre registered images will not be auto generating the thumbnails.  For such purpose, please use plugins that will allow to re-generate thumbnails from your whole Media Library.', 'wpthk' ) . '</p><p class="f11em bold snormal">' . __( 'Width of image', 'wpthk' ) . ' (' . __( 'default', 'wpthk' ) . ': 150px)</p>',
		'section'	=> 'thumbnail_section',
		'type'		=> 'number',
		'priority'	=> 25
	) );

	// サムネイル画像の高さ
	$wp_customize->add_setting( 'thumbnail_height', array(
		'default' 	=> 150,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'thumbnail_height', array(
		'settings'	=> 'thumbnail_height',
		'label'		=> __( 'Height of image', 'wpthk' ) . ' (' . __( 'default', 'wpthk' ) . ': 150px)',
		'section'	=> 'thumbnail_section',
		'type'		=> 'number',
		'priority'	=> 30
	) );

	// 実際に表示する時の画像の高さ
	$wp_customize->add_setting( 'thumbnail_view_height', array(
		'default' 	=> 'fixed',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'thumbnail_view_height', array(
		'settings'	=> 'thumbnail_view_height',
		'label'		=> __( 'The height used in actual page', 'wpthk' ),
		'section'	=> 'thumbnail_section',
		'type'		=> 'select',
		'choices'	=> array(
			'auto'	=> __( 'Keep the aspect ratio', 'wpthk' ),
			'fixed'	=> __( 'Fixed (apply the height input above)', 'wpthk' )
		),
		'priority'	=> 35
	) );

	//---------------------------------------------------------------------------
	// 文字色
	//---------------------------------------------------------------------------
	$wp_customize->add_section( 'color_section', array(
		'title'		=> __( 'Text color', 'wpthk' ),
		'priority'	=> 35
	) );

	// 文字色 ( Body )
	$wp_customize->add_setting( 'body_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'body_color', array(
		'settings'	=> 'body_color',
		'label'		=> __( 'Text color', 'wpthk' ) . ' ( Body )',
		'section'	=> 'color_section',
		'priority'	=> 10
	) ) );

	// リンク色 ( Body )
	$wp_customize->add_setting( 'body_link_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'body_link_color', array(
		'settings'	=> 'body_link_color',
		'label'		=> __( 'Link color', 'wpthk' ) . ' ( Body )',
		'section'	=> 'color_section',
		'priority'	=> 15
	) ) );

	// リンクホバー色 ( Body )
	$wp_customize->add_setting( 'body_hover_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'body_hover_color', array(
		'settings'	=> 'body_hover_color',
		'label'		=> __( 'Link hover color', 'wpthk' ) . ' ( Body )',
		'section'	=> 'color_section',
		'priority'	=> 20
	) ) );

	// パンくずリンク文字色
	$wp_customize->add_setting( 'breadcrumb_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'breadcrumb_color', array(
		'settings'	=> 'breadcrumb_color',
		'label'		=> __( 'Breadcrumb menu link color', 'wpthk' ),
		'section'	=> 'color_section',
		'priority'	=> 25
	) ) );

	// ヘッダー文字色
	$wp_customize->add_setting( 'head_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'head_color', array(
		'settings'	=> 'head_color',
		'label'		=> __( 'Header ', 'wpthk' ) . __( 'Text color', 'wpthk' ),
		'section'	=> 'color_section',
		'priority'	=> 35
	) ) );

	// ヘッダーリンク色
	$wp_customize->add_setting( 'head_link_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'head_link_color', array(
		'settings'	=> 'head_link_color',
		'label'		=> __( 'Header ', 'wpthk' ) . __( 'Link color', 'wpthk' ),
		'section'	=> 'color_section',
		'priority'	=> 40
	) ) );

	// ヘッダーリンクホバー色
	$wp_customize->add_setting( 'head_hover_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'head_hover_color', array(
		'settings'	=> 'head_hover_color',
		'label'		=> __( 'Header ', 'wpthk' ) . __( 'Link hover color', 'wpthk' ),
		'section'	=> 'color_section',
		'priority'	=> 45
	) ) );

	// フッター文字色
	$wp_customize->add_setting( 'foot_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'foot_color', array(
		'settings'	=> 'foot_color',
		'label'		=> __( 'Footer ', 'wpthk' ) . __( 'Text color', 'wpthk' ),
		'section'	=> 'color_section',
		'priority'	=> 55
	) ) );

	// フッターリンク色
	$wp_customize->add_setting( 'foot_link_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'foot_link_color', array(
		'settings'	=> 'foot_link_color',
		'label'		=> __( 'Footer ', 'wpthk' ) . __( 'Link color', 'wpthk' ),
		'section'	=> 'color_section',
		'priority'	=> 60
	) ) );

	// フッターホバーリンク色
	$wp_customize->add_setting( 'foot_hover_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'foot_hover_color', array(
		'settings'	=> 'foot_hover_color',
		'label'		=> __( 'Footer ', 'wpthk' ) . __( 'Link hover color', 'wpthk' ),
		'section'	=> 'color_section',
		'priority'	=> 65
	) ) );

	//---------------------------------------------------------------------------
	// 背景色・枠線色
	//---------------------------------------------------------------------------
	$wp_customize->add_section( 'bg_color_section', array(
		'title'		=> __( 'Background color', 'wpthk' ) . ' / ' . __( 'Border color', 'wpthk' ),
		'priority'	=> 40
	) );

	// 背景色 ( Body )
	$wp_customize->add_setting( 'body_bg_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'body_bg_color', array(
		'settings'	=> 'body_bg_color',
		'label'		=> __( 'Background color', 'wpthk' ) . ' ( Body )',
		'section'	=> 'bg_color_section',
		'priority'	=> 25
	) ) );

	// 背景透過 ( Body )
	$wp_customize->add_setting( 'body_transparent', array(
		'default'	=> 100,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'body_transparent', array(
		'settings'	=> 'body_transparent',
		'label'		=> __( 'Background transparent', 'wpthk' ) . ' ( Body )',
		'section'	=> 'bg_color_section',
		'type'		=> 'range',
		'priority'	=> 27
	) );

	// コンテンツ領域背景色
	$wp_customize->add_setting( 'cont_bg_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'cont_bg_color', array(
		'settings'	=> 'cont_bg_color',
		'label'		=> __( 'Content area ', 'wpthk' ) . __( 'Background color', 'wpthk' ),
		'section'	=> 'bg_color_section',
		'priority'	=> 30
	) ) );

	// コンテンツ領域枠線色
	$wp_customize->add_setting( 'cont_border_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'cont_border_color', array(
		'settings'	=> 'cont_border_color',
		'label'		=> __( 'Content area ', 'wpthk' ) . __( 'Border color', 'wpthk' ),
		'section'	=> 'bg_color_section',
		'priority'	=> 35
	) ) );

	// コンテンツ領域背景透過
	$wp_customize->add_setting( 'cont_transparent', array(
		'default'	=> 100,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'cont_transparent', array(
		'settings'	=> 'cont_transparent',
		'label'		=> __( 'Content area ', 'wpthk' ) . __( 'Background transparent', 'wpthk' ),
		'section'	=> 'bg_color_section',
		'type'		=> 'range',
		'priority'	=> 37
	) );

	// サイドバー背景色
	$wp_customize->add_setting( 'side_bg_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'side_bg_color', array(
		'settings'	=> 'side_bg_color',
		'label'		=> __( 'Sidebar ', 'wpthk' ) . __( 'Background color', 'wpthk' ),
		'section'	=> 'bg_color_section',
		'priority'	=> 40
	) ) );

	// サイドバー枠線色
	$wp_customize->add_setting( 'side_border_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'side_border_color', array(
		'settings'	=> 'side_border_color',
		'label'		=> __( 'Sidebar ', 'wpthk' ) . __( 'Border color', 'wpthk' ),
		'section'	=> 'bg_color_section',
		'priority'	=> 42
	) ) );

	// サイドバー背景透過
	$wp_customize->add_setting( 'side_transparent', array(
		'default'	=> 100,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'side_transparent', array(
		'settings'	=> 'side_transparent',
		'label'		=> __( 'Sidebar ', 'wpthk' ) . __( 'Background transparent', 'wpthk' ),
		'section'	=> 'bg_color_section',
		'type'		=> 'range',
		'priority'	=> 43
	) );

	// ヘッダー背景色
	$wp_customize->add_setting( 'head_bg_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'head_bg_color', array(
		'settings'	=> 'head_bg_color',
		'label'		=> __( 'Header ', 'wpthk' ) . __( 'Background color', 'wpthk' ),
		'section'	=> 'bg_color_section',
		'priority'	=> 45
	) ) );

	// ヘッダー枠線色
	$wp_customize->add_setting( 'head_border_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'head_border_color', array(
		'settings'	=> 'head_border_color',
		'label'		=> __( 'Header ', 'wpthk' ) . __( 'Border color', 'wpthk' ),
		'section'	=> 'bg_color_section',
		'priority'	=> 50
	) ) );

	// ヘッダー背景透過
	$wp_customize->add_setting( 'head_transparent', array(
		'default'	=> 100,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'head_transparent', array(
		'settings'	=> 'head_transparent',
		'label'		=> __( 'Header ', 'wpthk' ) . __( 'Background transparent', 'wpthk' ),
		'section'	=> 'bg_color_section',
		'type'		=> 'range',
		'priority'	=> 52
	) );

	// フッター背景色
	$wp_customize->add_setting( 'foot_bg_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'foot_bg_color', array(
		'settings'	=> 'foot_bg_color',
		'label'		=> __( 'Footer ', 'wpthk' ) . __( 'Background color', 'wpthk' ),
		'section'	=> 'bg_color_section',
		'priority'	=> 55
	) ) );

	// フッター枠線色
	$wp_customize->add_setting( 'foot_border_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'foot_border_color', array(
		'settings'	=> 'foot_border_color',
		'label'		=> __( 'Footer ', 'wpthk' ) . __( 'Border color', 'wpthk' ),
		'section'	=> 'bg_color_section',
		'priority'	=> 60
	) ) );

	// フッター背景透過
	$wp_customize->add_setting( 'foot_transparent', array(
		'default'	=> 100,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'foot_transparent', array(
		'settings'	=> 'foot_transparent',
		'label'		=> __( 'Footer ', 'wpthk' ) . __( 'Background transparent', 'wpthk' ),
		'section'	=> 'bg_color_section',
		'type'		=> 'range',
		'priority'	=> 65
	) );

	//---------------------------------------------------------------------------
	// 背景・タイトル・ロゴ画像
	//---------------------------------------------------------------------------
	$wp_customize->add_section( 'img_section', array(
		'title'		=> __( 'Images (Background / Title / Logo)', 'wpthk' ),
		'priority'	=> 45
	) );

	// タイトル画像
	$wp_customize->add_setting( 'title_img', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_url'
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'title_img', array(
		'settings'	=> 'title_img',
		'label'		=> __( 'Use image as title', 'wpthk' ),
		'section'	=> 'img_section',
		'priority'	=> 10
	) ) );

	// 背景画像
	$wp_customize->add_setting( 'body_bg_img', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_url'
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'body_bg_img', array(
		'settings'	=> 'body_bg_img',
		'label'		=> __( 'Background image', 'wpthk' ),
		'section'	=> 'img_section',
		'priority'	=> 15
	) ) );

	// 背景画像透過
	$wp_customize->add_setting( 'body_img_transparent', array(
		'default'	=> 0,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'body_img_transparent', array(
		'settings'	=> 'body_img_transparent',
		'label'		=> __( 'Background image transparent', 'wpthk' ),
		'section'	=> 'img_section',
		'type'		=> 'range',
		'priority'	=> 20
	) );

	// 背景画像を固定
	$wp_customize->add_setting( 'body_img_fixed', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'body_img_fixed', array(
		'settings'	=> 'body_img_fixed',
		'label'		=> __( 'Fixed background', 'wpthk' ),
		'section'	=> 'img_section',
		'type'		=> 'checkbox',
		'priority'	=> 25
	) );

	// 背景画像を垂直位置
	$wp_customize->add_setting( 'body_img_vertical', array(
		'default'	=> 'top',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'body_img_vertical', array(
		'settings'	=> 'body_img_vertical',
		'label'		=> __( 'Background image layout', 'wpthk' ),
		'section'	=> 'img_section',
		'type'		=> 'select',
		'choices'	=> array(
			'top'		=> 'Top',
			'middle'	=> 'Middle',
			'bottom'	=> 'Bottom'
		),
		'priority'	=> 30
	) );

	// 背景画像の左右位置
	$wp_customize->add_setting( 'body_img_horizontal', array(
		'default'	=> 'left',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'body_img_horizontal', array(
		'settings'	=> 'body_img_horizontal',
		'section'	=> 'img_section',
		'type'		=> 'select',
		'choices'	=> array(
			'left'		=> 'Left',
			'center'	=> 'Center',
			'right'		=> 'Right'
		),
		'priority'	=> 32
	) );

	// 背景画像の配置方法 ( repeat )
	$wp_customize->add_setting( 'body_img_repeat', array(
		'default'	=> 'repeat',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'body_img_repeat', array(
		'settings'	=> 'body_img_repeat',
		'section'	=> 'img_section',
		'type'		=> 'select',
		'choices'	=> array(
			'repeat'	=> 'repeat',
			'no-repeat'	=> 'no-repeat'
		),
		'priority'	=> 33
	) );

	// 背景画像の配置方法 ( size )
	$wp_customize->add_setting( 'body_img_size', array(
		'default'	=> 'auto',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'body_img_size', array(
		'settings'	=> 'body_img_size',
		'section'	=> 'img_section',
		'type'		=> 'select',
		'choices'	=> array(
			'auto'		=> 'auto',
			'contain'	=> 'contain',
			'cover'		=> 'cover',
			'adjust'	=> '100% auto',
			'adjust2'	=> 'auto 100%',
			'adjust3'	=> '100% 100%'
		),
		'priority'	=> 35
	) );

	// サイドバー背景画像
	$wp_customize->add_setting( 'side_bg_img', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_url'
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'side_bg_img', array(
		'settings'	=> 'side_bg_img',
		'label'		=> __( 'Sidebar ', 'wpthk' ) . __( 'Background image', 'wpthk' ),
		'section'	=> 'img_section',
		'priority'	=> 40
	) ) );

	// ヘッダー背景画像
	$wp_customize->add_setting( 'head_bg_img', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_url'
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'head_bg_img', array(
		'settings'	=> 'head_bg_img',
		'label'		=> __( 'Header ', 'wpthk' ) . __( 'Background image', 'wpthk' ),
		'description'	=> __( '* In responsive style, we recommend you to use the Logo Image setting below this page, rather than using background as logo.', 'wpthk' ),
		'section'	=> 'img_section',
		'priority'	=> 45
	) ) );

	// ヘッダー背景画像を横いっぱいに
	$wp_customize->add_setting( 'head_img_width_max', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'head_img_width_max', array(
		'settings'	=> 'head_img_width_max',
		'label'		=> __( 'Full width', 'wpthk' ),
		'section'	=> 'img_section',
		'type'		=> 'checkbox',
		'priority'	=> 50
	) );

	// ヘッダー背景画像を固定 (無理、やめた)
/*
	$wp_customize->add_setting( 'head_img_fixed', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'head_img_fixed', array(
		'settings'	=> 'head_img_fixed',
		'label'		=> __( 'Fixed background', 'wpthk' ),
		'section'	=> 'img_section',
		'type'		=> 'checkbox',
		'priority'	=> 51
	) );
*/

	// ヘッダー背景画像を垂直位置
	$wp_customize->add_setting( 'head_img_vertical', array(
		'default'	=> 'top',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'head_img_vertical', array(
		'settings'	=> 'head_img_vertical',
		'label'		=> __( 'Header background image layout', 'wpthk' ),
		'description'	=> __( '* display area of ​​the image , please adjust the width and height in the &quot;header padding adjustment&quot; of &quot;the entire layout&quot;.', 'wpthk' ) . '<br />' . __( '* Please check the look in mobile phones by yourself :-)', 'wpthk' ),
		'section'	=> 'img_section',
		'type'		=> 'select',
		'choices'	=> array(
			'top'		=> 'Top',
			'middle'	=> 'Middle',
			'bottom'	=> 'Bottom'
		),
		'priority'	=> 55
	) );

	// ヘッダー背景画像の左右位置
	$wp_customize->add_setting( 'head_img_horizontal', array(
		'default'	=> 'left',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'head_img_horizontal', array(
		'settings'	=> 'head_img_horizontal',
		'section'	=> 'img_section',
		'type'		=> 'select',
		'choices'	=> array(
			'left'		=> 'Left',
			'center'	=> 'Center',
			'right'		=> 'Right'
		),
		'priority'	=> 60
	) );

	// ヘッダー背景画像の配置方法 ( repeat )
	$wp_customize->add_setting( 'head_img_repeat', array(
		'default'	=> 'repeat',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'head_img_repeat', array(
		'settings'	=> 'head_img_repeat',
		'section'	=> 'img_section',
		'type'		=> 'select',
		'choices'	=> array(
			'repeat'	=> 'repeat',
			'no-repeat'	=> 'no-repeat'
		),
		'priority'	=> 65
	) );

	// ヘッダー背景画像の配置方法 ( size )
	$wp_customize->add_setting( 'head_img_size', array(
		'default'	=> 'auto',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'head_img_size', array(
		'settings'	=> 'head_img_size',
		'section'	=> 'img_section',
		'type'		=> 'select',
		'choices'	=> array(
			'auto'		=> 'auto',
			'contain'	=> 'contain',
			'cover'		=> 'cover',
			'adjust'	=> '100% auto',
			'adjust2'	=> 'auto 100%',
			'adjust3'	=> '100% 100%'
		),
		'priority'	=> 70
	) );

	// ロゴ画像
	$wp_customize->add_setting( 'logo_img', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_url'
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo_img', array(
		'settings'	=> 'logo_img',
		'label'		=> __( 'Logo image', 'wpthk' ),
		'section'	=> 'img_section',
		'priority'	=> 75
	) ) );

	// ロゴ画像の位置
	$wp_customize->add_setting( 'logo_img_up', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'logo_img_up', array(
		'settings'	=> 'logo_img_up',
		'label'		=> __( 'On top of the global navigation', 'wpthk' ),
		'section'	=> 'img_section',
		'type'		=> 'checkbox',
		'priority'	=> 80
	) );

	//---------------------------------------------------------------------------
	// グローバルナビ (ヘッダーナビ)
	//---------------------------------------------------------------------------
	$wp_customize->add_section( 'gnavi_section', array(
		'title'		=> __( 'Global Nav (Header Nav)', 'wpthk' ),
		'priority'	=> 50
	) );

	// グローバルナビの表示有無
	$wp_customize->add_setting( 'global_navi_visible', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'global_navi_visible', array(
		'settings'	=> 'global_navi_visible',
		'label'		=> __( 'Display Global Navigation', 'wpthk' ),
		'section'	=> 'gnavi_section',
		'type'		=> 'checkbox',
		'priority'	=> 5
	) );

	// グローバルナビの位置
	$wp_customize->add_setting( 'global_navi_position', array(
		'default'	=> 'under',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'global_navi_position', array(
		'settings'	=> 'global_navi_position',
		'label'		=> __( 'The position of the Global Nav', 'wpthk' ),
		'section'	=> 'gnavi_section',
		'type'		=> 'select',
		'choices'	=> array(
			'under'		=> __( 'Below header  ( Normal )', 'wpthk' ),
			'upper'		=> __( 'Above header', 'wpthk' )
		),
		'priority'	=> 10
	) );

	// モバイルのメニュー種類
	$wp_customize->add_setting( 'global_navi_mobile_type', array(
		'default'	=> 'luxury',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'global_navi_mobile_type', array(
		'settings'	=> 'global_navi_mobile_type',
		'label'		=> __( 'Type of mobile menu', 'wpthk' ),
		'section'	=> 'gnavi_section',
		'type'		=> 'select',
		'choices'	=> array(
			'luxury'	=> __( 'Luxury version', 'wpthk' ),
			'global'	=> __( 'Global menu only', 'wpthk' )
		),
		'priority'	=> 11
	) );

	// モバイルメニュー開閉方法
	$wp_customize->add_setting( 'global_navi_open_close', array(
		'default'	=> 'individual',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'global_navi_open_close', array(
		'settings'	=> 'global_navi_open_close',
		'label'		=> __( 'How to open and close the mobile menu', 'wpthk' ),
		'section'	=> 'gnavi_section',
		'type'		=> 'select',
		'choices'	=> array(
			'individual'	=> __( 'Individually opening and closing the parent and child', 'wpthk' ),
			'all'		=> __( 'All opening and closing', 'wpthk' )
		),
		'priority'	=> 12
	) );

	// ナビをスクロールで最上部に固定する
	$wp_customize->add_setting( 'global_navi_sticky', array(
		'default'	=> 'none',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'global_navi_sticky', array(
		'settings'	=> 'global_navi_sticky',
		'label'		=> __( 'Make it sticky', 'wpthk' ),
		'description'	=> __( '* may not work properly with old Android (2.x system)', 'wpthk' ),
		'section'	=> 'gnavi_section',
		'type'		=> 'select',
		'choices'	=> array(
			'none'	=> __( 'Not make it sticky', 'wpthk' ),
			'all'	=> __( 'Make it sticky', 'wpthk' ),
			'smart'	=> __( 'Make it sticky on small devices', 'wpthk' ),
			'pc'	=> __( 'Make it sticky on PC', 'wpthk' )
		),
		'priority'	=> 15
	) );

	// スクロールで固定したとき半透明にする
	$wp_customize->add_setting( 'global_navi_translucent', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'global_navi_translucent', array(
		'settings'	=> 'global_navi_translucent',
		'label'		=> __( 'Semi transparent when sticky', 'wpthk' ),
		'section'	=> 'gnavi_section',
		'type'		=> 'checkbox',
		'priority'	=> 20
	) );

	// セパレーターを付ける
	$wp_customize->add_setting( 'global_navi_add_sep', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'global_navi_add_sep', array(
		'settings'	=> 'global_navi_add_sep',
		'label'		=> __( 'Add separator line', 'wpthk' ),
		'section'	=> 'gnavi_section',
		'type'		=> 'checkbox',
		'priority'	=> 22
	) );

	// 横幅の大きさ
	$wp_customize->add_setting( 'global_navi_auto_resize', array(
		'default'	=> 'auto',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'global_navi_auto_resize', array(
		'settings'	=> 'global_navi_auto_resize',
		'label'		=> __( 'Size of width', 'wpthk' ),
		'section'	=> 'gnavi_section',
		'type'		=> 'select',
		'choices'	=> array(
			'auto'	=> __( 'Automatically resize the width', 'wpthk' ),
			'full'	=> __( 'Automatically resize the width', 'wpthk' ) . '( ' . __( 'full width', 'wpthk' ) . ' )',
			'same'	=> __( 'All the same width', 'wpthk' ),
		),
		'priority'	=> 25
	) );

	// ナビ文字色
	$wp_customize->add_setting( 'gnavi_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'gnavi_color', array(
		'settings'	=> 'gnavi_color',
		'label'		=> __( 'Navigation ', 'wpthk' ) . __( 'Text color', 'wpthk' ),
		'section'	=> 'gnavi_section',
		'priority'	=> 30
	) ) );

	// ナビバー背景色
	$wp_customize->add_setting( 'gnavi_bar_bg_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'gnavi_bar_bg_color', array(
		'settings'	=> 'gnavi_bar_bg_color',
		'label'		=> __( 'Navigation bar ', 'wpthk' ) . __( 'Background color', 'wpthk' ),
		'section'	=> 'gnavi_section',
		'priority'	=> 32
	) ) );

	// ナビ背景色
	$wp_customize->add_setting( 'gnavi_bg_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'gnavi_bg_color', array(
		'settings'	=> 'gnavi_bg_color',
		'label'		=> __( 'Navigation ', 'wpthk' ) . __( 'Background color', 'wpthk' ),
		'section'	=> 'gnavi_section',
		'priority'	=> 35
	) ) );

	// ナビ背景ホバー色
	$wp_customize->add_setting( 'gnavi_bg_hover_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'gnavi_bg_hover_color', array(
		'settings'	=> 'gnavi_bg_hover_color',
		'label'		=> __( 'Navigation ', 'wpthk' ) . __( 'Link hover color', 'wpthk' ),
		'section'	=> 'gnavi_section',
		'priority'	=> 40
	) ) );

	// ナビ背景カレント色
	$wp_customize->add_setting( 'gnavi_bg_current_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'gnavi_bg_current_color', array(
		'settings'	=> 'gnavi_bg_current_color',
		'label'		=> __( 'Navigation ', 'wpthk' ) . __( 'Current color', 'wpthk' ),
		'section'	=> 'gnavi_section',
		'priority'	=> 45
	) ) );

	// ナビ上の線の色
	$wp_customize->add_setting( 'gnavi_border_top_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'gnavi_border_top_color', array(
		'settings'	=> 'gnavi_border_top_color',
		'label'		=> __( 'Color of the lines above the Nav', 'wpthk' ),
		'section'	=> 'gnavi_section',
		'priority'	=> 50
	) ) );

	// ナビ下の線の色
	$wp_customize->add_setting( 'gnavi_border_bottom_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'gnavi_border_bottom_color', array(
		'settings'	=> 'gnavi_border_bottom_color',
		'label'		=> __( 'Color of the line bottom to the Nav', 'wpthk' ),
		'section'	=> 'gnavi_section',
		'priority'	=> 55
	) ) );

	// セパレーター（区切り線）の色
	$wp_customize->add_setting( 'gnavi_separator_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'gnavi_separator_color', array(
		'settings'	=> 'gnavi_separator_color',
		'label'		=> __( 'Color of the separator line', 'wpthk' ),
		'section'	=> 'gnavi_section',
		'priority'	=> 58
	) ) );

	// ナビ上の線の太さ
	$wp_customize->add_setting( 'gnavi_border_top_width', array(
		'default'	=> 1,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'gnavi_border_top_width', array(
		'settings'	=> 'gnavi_border_top_width',
		'label'		=> __( 'Thickness of the line above the Nav', 'wpthk' ) . ' ( px )',
		'section'	=> 'gnavi_section',
		'type'		=> 'number',
		'priority'	=> 60
	) );

	// ナビ下の線の太さ
	$wp_customize->add_setting( 'gnavi_border_bottom_width', array(
		'default'	=> 4,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'gnavi_border_bottom_width', array(
		'settings'	=> 'gnavi_border_bottom_width',
		'label'		=> __( 'Thickness of the line bottom to the Nav', 'wpthk' ) . ' ( px )',
		'section'	=> 'gnavi_section',
		'type'		=> 'number',
		'priority'	=> 65
	) );

	// ナビ上下のパディング
	$wp_customize->add_setting( 'gnavi_top_buttom_padding', array(
		'default'	=> 8,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'gnavi_top_buttom_padding', array(
		'settings'	=> 'gnavi_top_buttom_padding',
		'label'		=> __( 'Padding of Nav for above and below', 'wpthk' ) . ' ( px )',
		'section'	=> 'gnavi_section',
		'type'		=> 'number',
		'priority'	=> 70
	) );

	//---------------------------------------------------------------------------
	// ヘッダー上の帯状メニュー
	//---------------------------------------------------------------------------
	$wp_customize->add_section( 'head_band_section', array(
		'title'		=> __( 'Header Band Menu', 'wpthk' ),
		'description'	=> '<p class="f09em">' . __( '* Menu settings is under &quot;Appearance -&gt; Menus&quot;.  Please create and save your desired menu,and make sure to select for the location &quot;Header Band Menu&quot;.', 'wpthk' ) . '</p>',
		'priority'	=> 55
	) );

	// ヘッダーの上に帯状のメニューを表示
	$wp_customize->add_setting( 'head_band_visible', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'head_band_visible', array(
		'settings'	=> 'head_band_visible',
		'label'		=> __( 'Display the band menu above the header', 'wpthk' ),
		'section'	=> 'head_band_section',
		'type'		=> 'checkbox',
		'priority'	=> 0
	) );

	// ヘッダーの上の帯状メニューを常に横幅いっぱいにする
	$wp_customize->add_setting( 'head_band_wide', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'head_band_wide', array(
		'settings'	=> 'head_band_wide',
		'label'		=> __( 'Band menu to be full width', 'wpthk' ),
		'section'	=> 'head_band_section',
		'type'		=> 'checkbox',
		'priority'	=> 5
	) );

	// 帯状メニューを固定表示にする
	$wp_customize->add_setting( 'head_band_fixed', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'head_band_fixed', array(
		'settings'	=> 'head_band_fixed',
		'label'		=> __( 'Make band menu sticky', 'wpthk' ),
		'description'	=> '<p class="f09em mm23l">' . __( '* may not work properly with old Android (2.x system)', 'wpthk' ) . '</p>',
		'section'	=> 'head_band_section',
		'type'		=> 'checkbox',
		'priority'	=> 10
	) );

	// 帯状メニューの高さ
	$wp_customize->add_setting( 'head_band_height', array(
		'default'	=> 28,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'head_band_height', array(
		'settings'	=> 'head_band_height',
		'label'		=> __( 'Appearance of band menu', 'wpthk' ),
		'description'	=> __( 'Height of band menu', 'wpthk' ) . ' ( px )',
		'section'	=> 'head_band_section',
		'type'		=> 'number',
		'priority'	=> 15
	) );

	// 帯状メニュー文字色
	$wp_customize->add_setting( 'head_band_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'head_band_color', array(
		'settings'	=> 'head_band_color',
		'label'		=> __( 'Band menu ', 'wpthk' ) . __( 'Text color', 'wpthk' ),
		'section'	=> 'head_band_section',
		'priority'	=> 20
	) ) );

	// 帯状メニューリンクホバー色
	$wp_customize->add_setting( 'head_band_hover_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'head_band_hover_color', array(
		'settings'	=> 'head_band_hover_color',
		'label'		=> __( 'Band menu ', 'wpthk' ) . __( 'Link hover color', 'wpthk' ),
		'section'	=> 'head_band_section',
		'priority'	=> 25
	) ) );

	// 帯状メニュー背景色
	$wp_customize->add_setting( 'head_band_bg_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'head_band_bg_color', array(
		'settings'	=> 'head_band_bg_color',
		'label'		=> __( 'Band menu ', 'wpthk' ) . __( 'Background color', 'wpthk' ),
		'section'	=> 'head_band_section',
		'priority'	=> 30
	) ) );

	// 帯状メニューの下線の色
	$wp_customize->add_setting( 'head_band_border_bottom_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'head_band_border_bottom_color', array(
		'settings'	=> 'head_band_border_bottom_color',
		'label'		=> __( 'Color of the line bottom of the band menu', 'wpthk' ),
		'section'	=> 'head_band_section',
		'priority'	=> 35
	) ) );

	// 帯状メニューの下線の太さ
	$wp_customize->add_setting( 'head_band_border_bottom_width', array(
		'default'	=> 1,
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'head_band_border_bottom_width', array(
		'settings'	=> 'head_band_border_bottom_width',
		'label'		=> __( 'Thickness of the line bottom of the band menu', 'wpthk' ) . ' ( px )',
		'section'	=> 'head_band_section',
		'type'		=> 'number',
		'priority'	=> 40
	) );

	// 帯状メニューに検索ボックス
	$wp_customize->add_setting( 'head_band_search', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'head_band_search', array(
		'settings'	=> 'head_band_search',
		'label'		=> __( 'Display Search Box', 'wpthk' ),
		'section'	=> 'head_band_section',
		'type'		=> 'checkbox',
		'priority'	=> 41
	) );

	// 帯状メニューに検索ボックス文字色
	$wp_customize->add_setting( 'head_search_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'head_search_color', array(
		'settings'	=> 'head_search_color',
		'label'		=> __( 'Search Box ', 'wpthk' ) . __( 'Text color', 'wpthk' ),
		'section'	=> 'head_band_section',
		'priority'	=> 42
	) ) );

	// 帯状メニューに検索ボックス背景色
	$wp_customize->add_setting( 'head_search_bg_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'head_search_bg_color', array(
		'settings'	=> 'head_search_bg_color',
		'label'		=> __( 'Search Box ', 'wpthk' ) . __( 'Background color', 'wpthk' ),
		'section'	=> 'head_band_section',
		'priority'	=> 43
	) ) );

	// 帯状メニューに検索ボックス背景透過
	$wp_customize->add_setting( 'head_search_transparent', array(
		'default'	=> 30,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'head_search_transparent', array(
		'settings'	=> 'head_search_transparent',
		'label'		=> __( 'Search Box ', 'wpthk' ) . __( 'Background transparent', 'wpthk' ),
		'section'	=> 'head_band_section',
		'type'		=> 'range',
		'priority'	=> 44
	) );

	// フォローボタンの表示方法
	$wp_customize->add_setting( 'head_band_follow_icon', array(
		'default' 	=> 'icon_name',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'head_band_follow_icon', array(
		'settings'	=> 'head_band_follow_icon',
		'label'		=> __( 'Display style of social buttons', 'wpthk' ),
		'section'	=> 'head_band_section',
		'type'		=> 'radio',
		'choices'	=> array(
			'icon_only'	=> __( 'Icon only', 'wpthk' ),
			'icon_name'	=> __( 'Icon + SNS name', 'wpthk' )
		),
		'priority'	=> 45
	) );

	// フォローボタンをカラーにする
	$wp_customize->add_setting( 'head_band_follow_color', array(
		'default' 	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'head_band_follow_color', array(
		'settings'	=> 'head_band_follow_color',
		'label'		=> __( 'Apply color on the follow buttons', 'wpthk' ),
		'section'	=> 'head_band_section',
		'type'		=> 'checkbox',
		'priority'	=> 50
	) );

	// Twitter フォローボタン表示
	$wp_customize->add_setting( 'head_band_twitter', array(
		'default' 	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'head_band_twitter', array(
		'settings'	=> 'head_band_twitter',
		'label'		=> 'Twitter ' . __( 'follow button display', 'wpthk' ),
		'section'	=> 'head_band_section',
		'type'		=> 'checkbox',
		'priority'	=> 55
	) );

	// Twitter ID
	$wp_customize->add_setting( 'follow_twitter_id', array(
		'default' 	=> null,
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'follow_twitter_id', array(
		'settings'	=> 'follow_twitter_id',
		'label'		=> 'Twitter ID',
		'description'	=> __( '* ', 'wpthk' ) . 'http://twitter.com/<span style="font-weight:bold">XXXXX</span><br />' . __( '&nbsp;&nbsp;&nbsp;Enter the XXXXX part', 'wpthk' ),
		'section'	=> 'head_band_section',
		'type'		=> 'text',
		'priority'	=> 60
	) );

	// Facebook フォローボタン表示
	$wp_customize->add_setting( 'head_band_facebook', array(
		'default' 	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'head_band_facebook', array(
		'settings'	=> 'head_band_facebook',
		'label'		=> 'Facebook ' . __( 'follow button display', 'wpthk' ),
		'section'	=> 'head_band_section',
		'type'		=> 'checkbox',
		'priority'	=> 65
	) );

	// Facebook ID
	$wp_customize->add_setting( 'follow_facebook_id', array(
		'default' 	=> null,
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'follow_facebook_id', array(
		'settings'	=> 'follow_facebook_id',
		'label'		=> 'Facebook ID',
		'description'	=> __( '* ', 'wpthk' ) . 'http://www.facebook.com/<span style="font-weight:bold">XXXXX</span><br />' . __( '&nbsp;&nbsp;&nbsp;Enter the XXXXX part', 'wpthk' ),
		'section'	=> 'head_band_section',
		'type'		=> 'text',
		'priority'	=> 70
	) );

	// はてなブックマーク フォローボタン表示
	$wp_customize->add_setting( 'head_band_hatena', array(
		'default' 	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'head_band_hatena', array(
		'settings'	=> 'head_band_hatena',
		'label'		=> __( 'Hatena Bookmark', 'wpthk' ) . ' ' . __( 'follow button display', 'wpthk' ),
		'section'	=> 'head_band_section',
		'type'		=> 'checkbox',
		'priority'	=> 75
	) );

	// はてなブックマーク ID
	$wp_customize->add_setting( 'follow_hatena_id', array(
		'default' 	=> null,
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'follow_hatena_id', array(
		'settings'	=> 'follow_hatena_id',
		'label'		=> __( 'Hatena Bookmark', 'wpthk' ) . ' ID',
		'description'	=> __( '* ', 'wpthk' ) . 'http://b.hatena.ne.jp/<span style="font-weight:bold">XXXXX</span><br />' . __( '&nbsp;&nbsp;&nbsp;Enter the XXXXX part', 'wpthk' ),
		'section'	=> 'head_band_section',
		'type'		=> 'text',
		'priority'	=> 80
	) );

	// Google+ フォローボタン表示
	$wp_customize->add_setting( 'head_band_google', array(
		'default' 	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'head_band_google', array(
		'settings'	=> 'head_band_google',
		'label'		=> 'Google+ ' . __( 'follow button display', 'wpthk' ),
		'section'	=> 'head_band_section',
		'type'		=> 'checkbox',
		'priority'	=> 85
	) );

	// Google+ ID
	$wp_customize->add_setting( 'follow_google_id', array(
		'default' 	=> null,
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'follow_google_id', array(
		'settings'	=> 'follow_google_id',
		'label'		=> 'Google+ ID',
		'description'	=> __( '* ', 'wpthk' ) . 'http://plus.google.com/<span style="font-weight:bold">XXXXX</span><br />' . __( '&nbsp;&nbsp;&nbsp;Enter the XXXXX part', 'wpthk' ),
		'section'	=> 'head_band_section',
		'type'		=> 'text',
		'priority'	=> 90
	) );

	// Youtube フォローボタン表示
	$wp_customize->add_setting( 'head_band_youtube', array(
		'default' 	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'head_band_youtube', array(
		'settings'	=> 'head_band_youtube',
		'label'		=> 'Youtube ' . __( 'follow button display', 'wpthk' ),
		'section'	=> 'head_band_section',
		'type'		=> 'checkbox',
		'priority'	=> 95
	) );

	// Youtube ID
	$wp_customize->add_setting( 'follow_youtube_id', array(
		'default' 	=> null,
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'follow_youtube_id', array(
		'settings'	=> 'follow_youtube_id',
		'label'		=> 'Youtube ID',
		'description'	=> __( '* ', 'wpthk' ) . 'http://www.youtube.com/<span style="font-weight:bold">XXXXX</span><br />' . __( '&nbsp;&nbsp;&nbsp;Enter the XXXXX part', 'wpthk' ),
		'section'	=> 'head_band_section',
		'type'		=> 'text',
		'priority'	=> 100
	) );

	// LINE フォローボタン表示
	$wp_customize->add_setting( 'head_band_line', array(
		'default' 	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'head_band_line', array(
		'settings'	=> 'head_band_line',
		'label'		=> 'LINE ' . __( 'follow button display', 'wpthk' ),
		'section'	=> 'head_band_section',
		'type'		=> 'checkbox',
		'priority'	=> 102
	) );

	// LINE ID
	$wp_customize->add_setting( 'follow_line_id', array(
		'default' 	=> null,
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'follow_line_id', array(
		'settings'	=> 'follow_line_id',
		'label'		=> 'LINE ID',
		'description'	=> __( '* ', 'wpthk' ) . 'http://line.naver.jp/ti/p/<span style="font-weight:bold">XXXXX</span><br />' . __( '&nbsp;&nbsp;&nbsp;Enter the XXXXX part', 'wpthk' ),
		'section'	=> 'head_band_section',
		'type'		=> 'text',
		'priority'	=> 103
	) );

	// RSS ボタン表示
	$wp_customize->add_setting( 'head_band_rss', array(
		'default' 	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'head_band_rss', array(
		'settings'	=> 'head_band_rss',
		'label'		=> 'RSS ' . __( 'button display', 'wpthk' ),
		'section'	=> 'head_band_section',
		'type'		=> 'checkbox',
		'priority'	=> 105
	) );

	// Feedly ボタン表示
	$wp_customize->add_setting( 'head_band_feedly', array(
		'default' 	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'head_band_feedly', array(
		'settings'	=> 'head_band_feedly',
		'label'		=> 'Feedly ' . __( 'button display', 'wpthk' ),
		'section'	=> 'head_band_section',
		'type'		=> 'checkbox',
		'priority'	=> 110
	) );

	//---------------------------------------------------------------------------
	// アニメーション
	//---------------------------------------------------------------------------
	$wp_customize->add_section( 'animation_section', array(
		'title'		=> __( 'Animation', 'wpthk' ),
		'description'	=> __( 'Enable animation effects', 'wpthk' ),
		'priority'	=> 60
	) );

	// サイト名のズーム効果
	$wp_customize->add_setting( 'anime_sitename', array(
		'default'	=> 'none',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'anime_sitename', array(
		'settings'	=> 'anime_sitename',
		'label'		=> __( 'Site name', 'wpthk' ),
		'section'	=> 'animation_section',
		'type'		=> 'radio',
		'choices'	=> array(
			'none'		=> __( 'Without animation effect', 'wpthk' ),
			'zoomin'	=> __( 'Zoom in', 'wpthk' ),
			'zoomout'	=> __( 'Zoom out', 'wpthk' ),
		),
		'priority'	=> 10
	) );

	// 記事一覧サムネイルのズーム効果
	$wp_customize->add_setting( 'anime_thumbnail', array(
		'default'	=> 'none',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'anime_thumbnail', array(
		'settings'	=> 'anime_thumbnail',
		'label'		=> __( 'Posts List thumbnail', 'wpthk' ),
		'section'	=> 'animation_section',
		'type'		=> 'radio',
		'choices'	=> array(
			'none'		=> __( 'Without animation effect', 'wpthk' ),
			'zoomin'	=> __( 'Zoom in', 'wpthk' ),
			'zoomout'	=> __( 'Zoom out', 'wpthk' ),
		),
		'priority'	=> 15
	) );

	// SNS シェアボタンのズーム効果
	$wp_customize->add_setting( 'anime_sns_buttons', array(
		'default'	=> 'none',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'anime_sns_buttons', array(
		'settings'	=> 'anime_sns_buttons',
		'label'		=> __( 'SNS share buttons', 'wpthk' ),
		'section'	=> 'animation_section',
		'type'		=> 'radio',
		'choices'	=> array(
			'none'		=> __( 'Without animation effect', 'wpthk' ),
			'upward'	=> __( 'Upward movement', 'wpthk' ),
			'zoomin'	=> __( 'Zoom in', 'wpthk' ),
			'zoomout'	=> __( 'Zoom out', 'wpthk' ),
		),
		'priority'	=> 20
	) );

	// グローバルナビの上方移動効果
	$wp_customize->add_setting( 'anime_global_navi', array(
		'default'	=> 'none',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'anime_global_navi', array(
		'settings'	=> 'anime_global_navi',
		'label'		=> __( 'Global Nav', 'wpthk' ),
		'section'	=> 'animation_section',
		'type'		=> 'radio',
		'choices'	=> array(
			'none'		=> __( 'Without animation effect', 'wpthk' ),
			'upward'	=> __( 'Upward movement', 'wpthk' ),
		),
		'priority'	=> 25
	) );

	//---------------------------------------------------------------------------
	// Lazy Load (画像の遅延読み込み)
	//---------------------------------------------------------------------------
	$wp_customize->add_section( 'lazyload_section', array(
		'title'		=> 'Lazy Load (' . __( 'Lazy loading of image', 'wpthk' ) . ')',
		'priority'	=> 79
	) );

	// 投稿・固定ページの Lazy Load 有効化
	$wp_customize->add_setting( 'lazyload_enable', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'lazyload_enable', array(
		'settings'	=> 'lazyload_enable',
		'label'		=> __( 'Enable Lazy Load for post and pages.', 'wpthk' ),
		'description'	=> '<p class="f09em mm10b mm23l">' . __( '* srcset of the posts will be disabled.', 'wpthk' ) . '</p>',
		'section'	=> 'lazyload_section',
		'type'		=> 'checkbox',
		'priority'	=> 10
	) );

	// 投稿・固定ページの Lazy Load 有効化
	$wp_customize->add_setting( 'lazyload_enable', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'lazyload_enable', array(
		'settings'	=> 'lazyload_enable',
		'label'		=> __( 'Enbale Lazy Load for post and pages.', 'wpthk' ),
		'description'	=> '<p class="f09em mm10b mm23l">' . __( '* srcset of the posts will be disabled.', 'wpthk' ) . '</p>',
		'section'	=> 'lazyload_section',
		'type'		=> 'checkbox',
		'priority'	=> 10
	) );

	// クローラーに対しては Lazy Load を無効化する
	$wp_customize->add_setting( 'lazyload_crawler', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'lazyload_crawler', array(
		'settings'	=> 'lazyload_crawler',
		'label'		=> __( 'Disable Lazy Load for search engine crawlers', 'wpthk' ),
		'description'	=> '<p class="f09em mm23l">' . __( '* minor search engies are not supported.', 'wpthk' ) . '</p>',
		'section'	=> 'lazyload_section',
		'type'		=> 'checkbox',
		'priority'	=> 15
	) );

	// Lazy Load プレースホルダー
	$wp_customize->add_setting( 'lazyload_placeholder', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'lazyload_placeholder', array(
		'settings'	=> 'lazyload_placeholder',
		'label'		=> __( 'Color of the placeholder', 'wpthk' ),
		'description'	=> '<p class="f09em">' . __( '* Placeholder is an image which will be temporarily displayed until full image is displayed.', 'wpthk' ) . '</p>',
		'section'	=> 'lazyload_section',
		'priority'	=> 20
	) ) );

	// Lazy Load プレースホルダーにスピナー
	$wp_customize->add_setting( 'lazyload_spinner', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'lazyload_spinner', array(
		'settings'	=> 'lazyload_spinner',
		'label'		=> __( 'Add spinner to placeholder', 'wpthk' ),
		'description'	=> '<p class="f09em mm10b mm23l">' . __( '* Spinner is the spinning image which is shown until the full image is displayed.', 'wpthk' ) . '</p>',
		'section'	=> 'lazyload_section',
		'type'		=> 'checkbox',
		'priority'	=> 25
	) );

	// Lazy Load 閾値
	$wp_customize->add_setting( 'lazyload_threshold', array(
		'default'	=> 0,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'lazyload_threshold', array(
		'settings'	=> 'lazyload_threshold',
		'label'		=> __( 'threshold amount ( px )', 'wpthk' ),
		'description'	=> '<p class="f09em">' . __( '* If you want to start loading the images before apperaing, please define the length of pixels outside the viewport.', 'wpthk' ) . '</p>',
		'section'	=> 'lazyload_section',
		'type'		=> 'number',
		'priority'	=> 30
	) );

	// Lazy Load エフェクト・効果
	$wp_customize->add_setting( 'lazyload_effect', array(
		'default'	=> 'fadeIn',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'lazyload_effect', array(
		'settings'	=> 'lazyload_effect',
		'label'		=> __( 'Effect', 'wpthk' ),
		'section'	=> 'lazyload_section',
		'type'		=> 'radio',
		'choices'	=> array(
			'fadeIn'	=> __( 'Fade-in', 'wpthk' ),
			'show'		=> __( 'Show', 'wpthk' )
		),
		'priority'	=> 35
	) );

	// Lazy Load フィードインの速度
	$wp_customize->add_setting( 'lazyload_effectspeed', array(
		'default'	=> 1000,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'lazyload_effectspeed', array(
		'settings'	=> 'lazyload_effectspeed',
		'label'		=> __( 'Speed of Fade-in ( ms )', 'wpthk' ),
		'description'	=> '<p class="f09em">' . __( '* Unit : milliseconds.', 'wpthk' ) . '<br />' . __( 'e.g. Input 3000 for 3 seconds.', 'wpthk' ) . '</p>',
		'section'	=> 'lazyload_section',
		'type'		=> 'number',
		'priority'	=> 40
	) );

	// イベント
	$wp_customize->add_setting( 'lazyload_event', array(
		'default'	=> 'scroll',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'lazyload_event', array(
		'settings'	=> 'lazyload_event',
		'label'		=> __( 'Events', 'wpthk' ),
		'description'	=> '<p class="f09em">' . __( '* Will display image when following event ocurs.', 'wpthk' ) . '</p>',
		'section'	=> 'lazyload_section',
		'type'		=> 'radio',
		'choices'	=> array(
			'scroll'	=> 'Scroll',
			'mouseover'	=> 'Mouseover',
			'click'		=> 'Click'
		),
		'priority'	=> 45
	) );

	//---------------------------------------------------------------------------
	// 画像ギャラリーの設定
	//---------------------------------------------------------------------------
	$wp_customize->add_section( 'gallery_section', array(
		'title'		=> __( 'Image gallery', 'wpthk' ),
		'priority'	=> 80
	) );

	// 画像ギャラリーの種類
	$wp_customize->add_setting( 'gallery_type', array(
		'default'	=> 'none',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'gallery_type', array(
		'settings'	=> 'gallery_type',
		'label'		=> __( 'Type of image gallery', 'wpthk' ),
		'section'	=> 'gallery_section',
		'type'		=> 'radio',
		'choices'	=> array(
			'tosrus'	=> 'Tosrus ( ' . __( 'Responsive', 'wpthk' ) . ' / ' . __( 'Most Recommended', 'wpthk' ) . ' )',
			'lightcase'	=> 'Lightcase ( ' . __( 'Responsive', 'wpthk' ) . ' / ' . __( 'Recommend', 'wpthk' ) . ' )',
			'fluidbox'	=> 'Fluidbox ( ' . __( 'Responsive', 'wpthk' ) . ' / ' . __( 'Recommend', 'wpthk' ) . ' / ' . __( 'Cannot coexist with &quot;Lazy Load&quot;', 'wpthk' ) . ' )',
			'none'		=> __( 'Do not use the image gallery', 'wpthk' )
		),
		'priority'	=> 10
	) );

	//---------------------------------------------------------------------------
	// 外部リンクの設定
	//---------------------------------------------------------------------------
	$wp_customize->add_section( 'external_link_section', array(
		'title'		=> __( 'External link', 'wpthk' ),
		'priority'	=> 85
	) );

	// 記事内の外部リンクに class="external" 付ける
	$wp_customize->add_setting( 'add_class_external', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'add_class_external', array(
		'settings'	=> 'add_class_external',
		'label'		=> sprintf( __( 'Add %s in the external links of the article', 'wpthk' ), ' class="external" ' ),
		'section'	=> 'external_link_section',
		'type'		=> 'checkbox',
		'priority'	=> 10
	) );

	// 記事内の外部リンクにアイコン付ける
	$wp_customize->add_setting( 'add_external_icon', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'add_external_icon', array(
		'settings'	=> 'add_external_icon',
		'label'		=> sprintf( __( 'Add %s in the external links of the article', 'wpthk' ), __( ' icon ', 'wpthk' ) ),
		'section'	=> 'external_link_section',
		'type'		=> 'checkbox',
		'priority'	=> 20
	) );

	// 外部リンクアイコンのタイプ
	$wp_customize->add_setting( 'external_icon_type', array(
		'default'	=> 'normal',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'external_icon_type', array(
		'settings'	=> 'external_icon_type',
		'label'		=> __( 'Type of external link icon', 'wpthk' ),
		'section'	=> 'external_link_section',
		'type'		=> 'radio',
		'choices'	=> array(
			'normal'	=> 'fa-external-link',
			'square'	=> 'fa-external-link-square'
		),
		'priority'	=> 30
	) );

	// 外部リンクアイコンの色
	$wp_customize->add_setting( 'external_icon_color', array(
		'default'	=> null,
		'sanitize_callback' => 'thk_sanitize_color'
	) );
	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'external_icon_color', array(
		'settings'	=> 'external_icon_color',
		'label'		=> __( 'The color of external link icon', 'wpthk' ),
		'section'	=> 'external_link_section',
		'priority'	=> 40
	) ) );

	// 記事内の外部リンクに target="_blank" 付ける
	$wp_customize->add_setting( 'add_target_blank', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'add_target_blank', array(
		'settings'	=> 'add_target_blank',
		'label'		=> sprintf( __( 'Add %s in the external links of the article', 'wpthk' ), ' target="_blank" ' ),
		'section'	=> 'external_link_section',
		'type'		=> 'checkbox',
		'priority'	=> 50
	) );

	// 記事内の外部リンクに rel="nofollow" 付ける
	$wp_customize->add_setting( 'add_rel_nofollow', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'add_rel_nofollow', array(
		'settings'	=> 'add_rel_nofollow',
		'label'		=> sprintf( __( 'Add %s in the external links of the article', 'wpthk' ), ' rel="nofollow" ' ),
		'description'	=> '<p class="mm23l f09em">' . __( '* Not recommended! (such nofollow link in your articles could be a breach in manners.)', 'wpthk' ) . '</p>',
		'section'	=> 'external_link_section',
		'type'		=> 'checkbox',
		'priority'	=> 50
	) );

	//---------------------------------------------------------------------------
	// author (記事投稿者) の設定
	//---------------------------------------------------------------------------
	$wp_customize->add_section( 'author_section', array(
		'title'		=> __( 'Author (post contributor)', 'wpthk' ),
		'priority'	=> 90
	) );
	// author (記事投稿者) を表示
	$wp_customize->add_setting( 'author_visible', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'author_visible', array(
		'settings'	=> 'author_visible',
		'label'		=> __( 'Display author (post contributor)', 'wpthk' ),
		'section'	=> 'author_section',
		'type'		=> 'checkbox',
		'priority'	=> 10
	) );
	// author に張る URL の選択
	$wp_customize->add_setting( 'author_page_type', array(
		'default'	=> 'auth',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'author_page_type', array(
		'settings'	=> 'author_page_type',
		'label'		=> __( 'URL spanned the author', 'wpthk' ),
		'section'	=> 'author_section',
		'type'		=> 'radio',
		'choices'	=> array(
			'auth'		=> __( 'Link to the contributor archive page', 'wpthk' ),
			'other'		=> __( 'Link to the URL you desire', 'wpthk' )
		),
		'priority'	=> 15
	) );
	// 自分で作成したプロフィールページの URL
	$wp_customize->add_setting( 'thk_author_url', array(
		'default' 	=> home_url('/'),
		'sanitize_callback' => 'thk_sanitize_url'
	) );
	$wp_customize->add_control( 'thk_author_url', array(
		'settings'	=> 'thk_author_url',
		'label'		=> __( 'The URL you desire!', 'wpthk' ),
		'section'	=> 'author_section',
		'type'		=> 'text',
		'priority'	=> 20
	) );

	//---------------------------------------------------------------------------
	// SNS シェアボタン (1)
	//---------------------------------------------------------------------------
	$wp_customize->add_section( 'sns_section', array(
		'title'		=> __( 'SNS share buttons', 'wpthk' ) . ' (1)',
		'priority'	=> 95
	) );

	// SNS のカウントをキャッシュする
	$wp_customize->add_setting( 'sns_count_cache_enable', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'sns_count_cache_enable', array(
		'settings'	=> 'sns_count_cache_enable',
		'label'		=> __( 'Eanable cache for SNS counter', 'wpthk' ) . ' (' . __( 'Recommend', 'wpthk' ) . ')',
		'description'	=> '<p class="mm23l f09em">' . __( '* will not function if normal SNS button is selected.', 'wpthk' ) . '</p><p class="mm23l f09em">' . __( '* by enabling cache, the SNS counter will work even on WAF enabled servers.', 'wpthk' ) . '</p>',
		'section'	=> 'sns_section',
		'type'		=> 'checkbox',
		'priority'	=> 5
	) );

	// ブログで SNS カウントを非表示にしていても、カウント数を取得してキャッシュする
	$wp_customize->add_setting( 'sns_count_cache_force', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'sns_count_cache_force', array(
		'settings'	=> 'sns_count_cache_force',
		'label'		=> __( 'Count and cache the SNS counts even NO display is selected.', 'wpthk' ),
		'section'	=> 'sns_section',
		'type'		=> 'checkbox',
		'priority'	=> 6
	) );

	// SNS カウントキャッシュ再構築までのインターバル
	$wp_customize->add_setting( 'sns_count_cache_expire', array(
		'default'	=> 600,
		'sanitize_callback' => 'thk_sanitize_integer'
	) );
	$wp_customize->add_control( 'sns_count_cache_expire', array(
		'settings'	=> 'sns_count_cache_expire',
		'label'		=> __( 'Interval for cache restructure', 'wpthk' ),
		'section'	=> 'sns_section',
		'type'		=> 'select',
		'choices'	=> array(
			60	=> sprintf( __( '%s seconds', 'wpthk' ), 60 ),
			600	=> sprintf( __( '%s minutes', 'wpthk' ), 10 ),
			1800	=> sprintf( __( '%s minutes', 'wpthk' ), 30 ),
			3600	=> sprintf( __( '%s hour', 'wpthk' ), 1 ),
			10800	=> sprintf( __( '%s hours', 'wpthk' ), 3 ),
			21600	=> sprintf( __( '%s hours', 'wpthk' ), 6 ),
			43200	=> sprintf( __( '%s hours', 'wpthk' ), 12 ),
			86400	=> sprintf( __( '%s day', 'wpthk' ), 1 )
		),
		'priority'	=> 7
	) );

	// 週間自動キャッシュ整理
	$wp_customize->add_setting( 'sns_count_weekly_cleanup', array(
		'default'	=> 'dust',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'sns_count_weekly_cleanup', array(
		'settings'	=> 'sns_count_weekly_cleanup',
		'label'		=> __( 'Weekly cache cleaning', 'wpthk' ),
		'section'	=> 'sns_section',
		'type'		=> 'select',
		'choices'	=> array(
			'dust'	=> __( 'Delete trash considered cache', 'wpthk' ),
			'all'	=> __( 'Delete all cache', 'wpthk' ),
			'none'	=> __( 'Do nothing', 'wpthk' )
		),
		'priority'	=> 8
	) );

	// 記事上の SNS ボタンの種類と配置
	$wp_customize->add_setting( 'sns_tops_type', array(
		'default'	=> 'color',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'sns_tops_type', array(
		'settings'	=> 'sns_tops_type',
		'label'		=> __( 'SNS button above article type and layout', 'wpthk' ),
		'section'	=> 'sns_section',
		'type'		=> 'select',
		'choices'	=> array(
			'normal'	=> __( 'Normal button', 'wpthk' ),
			'color'		=> __( 'Color type', 'wpthk' ),
			'white'		=> __( 'White type', 'wpthk' ),
			'flatc'		=> __( 'Variable width flat type', 'wpthk' ) . ' (' . __( 'Color', 'wpthk' ) . ')',
			'flatw'		=> __( 'Variable width flat type', 'wpthk' ) . ' (' . __( 'White', 'wpthk' ) . ')',
			'iconc'		=> __( 'Nameless icon type', 'wpthk' ) . ' (' . __( 'Color', 'wpthk' ) . ')',
			'iconw'		=> __( 'Nameless icon type', 'wpthk' ) . ' (' . __( 'White', 'wpthk' ) . ')'
		),
		'priority'	=> 10
	) );
	// 記事上の SNS ボタンの配置
	$wp_customize->add_setting( 'sns_tops_position', array(
		'default'	=> 'left',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'sns_tops_position', array(
		'settings'	=> 'sns_tops_position',
		'section'	=> 'sns_section',
		'type'		=> 'select',
		'choices'	=> array(
			'left'		=> __( 'left', 'wpthk' ),
			'center'	=> __( 'center', 'wpthk' ),
			'right'		=> __( 'right', 'wpthk' )
		),
		'priority'	=> 15
	) );
	// 記事上の SNS ボタン表示
	$wp_customize->add_setting( 'sns_tops_enable', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'sns_tops_enable', array(
		'settings'	=> 'sns_tops_enable',
		'label'		=> __( 'Display SNS button above the articles', 'wpthk' ),
		'section'	=> 'sns_section',
		'type'		=> 'checkbox',
		'priority'	=> 18
	) );
	// 記事上の SNS カウント表示
	$wp_customize->add_setting( 'sns_tops_count', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'sns_tops_count', array(
		'settings'	=> 'sns_tops_count',
		'label'		=> __( 'Show the counts on SNS buttons(above)', 'wpthk' ),
		'section'	=> 'sns_section',
		'type'		=> 'checkbox',
		'priority'	=> 20
	) );

	// 記事上 SNS ボタン2段組表示
	$wp_customize->add_setting( 'sns_tops_multiple', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'sns_tops_multiple', array(
		'settings'	=> 'sns_tops_multiple',
		'label'		=> __( 'Show SNS Button in 2 rows(above)', 'wpthk' ),
		'section'	=> 'sns_section',
		'type'		=> 'checkbox',
		'priority'	=> 25
	) );

	// 記事下の SNS ボタンの種類
	$wp_customize->add_setting( 'sns_bottoms_type', array(
		'default'	=> 'color',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'sns_bottoms_type', array(
		'settings'	=> 'sns_bottoms_type',
		'label'		=> __( 'SNS button beneath article type and layout', 'wpthk' ),
		'section'	=> 'sns_section',
		'type'		=> 'select',
		'choices'	=> array(
			'normal'	=> __( 'Normal button', 'wpthk' ),
			'color'		=> __( 'Color type', 'wpthk' ),
			'white'		=> __( 'White type', 'wpthk' ),
			'flatc'		=> __( 'Variable width flat type', 'wpthk' ) . ' (' . __( 'Color', 'wpthk' ) . ')',
			'flatw'		=> __( 'Variable width flat type', 'wpthk' ) . ' (' . __( 'White', 'wpthk' ) . ')',
			'iconc'		=> __( 'Nameless icon type', 'wpthk' ) . ' (' . __( 'Color', 'wpthk' ) . ')',
			'iconw'		=> __( 'Nameless icon type', 'wpthk' ) . ' (' . __( 'White', 'wpthk' ) . ')'
		),
		'priority'	=> 30
	) );
	// 記事下の SNS ボタンの配置
	$wp_customize->add_setting( 'sns_bottoms_position', array(
		'default'	=> 'left',
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'sns_bottoms_position', array(
		'settings'	=> 'sns_bottoms_position',
		'section'	=> 'sns_section',
		'type'		=> 'select',
		'choices'	=> array(
			'left'		=> __( 'left', 'wpthk' ),
			'center'	=> __( 'center', 'wpthk' ),
			'right'		=> __( 'right', 'wpthk' )
		),
		'priority'	=> 35
	) );
	// 記事下の SNS ボタン表示
	$wp_customize->add_setting( 'sns_bottoms_enable', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'sns_bottoms_enable', array(
		'settings'	=> 'sns_bottoms_enable',
		'label'		=> __( 'Display SNS button below the articles', 'wpthk' ),
		'section'	=> 'sns_section',
		'type'		=> 'checkbox',
		'priority'	=> 40
	) );
	// 記事下の SNS カウント表示
	$wp_customize->add_setting( 'sns_bottoms_count', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'sns_bottoms_count', array(
		'settings'	=> 'sns_bottoms_count',
		'label'		=> __( 'Show the counts on SNS buttons (below article)', 'wpthk' ),
		'section'	=> 'sns_section',
		'type'		=> 'checkbox',
		'priority'	=> 45
	) );

	// 記事下 SNS ボタン2段組表示
	$wp_customize->add_setting( 'sns_bottoms_multiple', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'sns_bottoms_multiple', array(
		'settings'	=> 'sns_bottoms_multiple',
		'label'		=> __( 'Show SNS Button in 2 rows (below article)', 'wpthk' ),
		'section'	=> 'sns_section',
		'type'		=> 'checkbox',
		'priority'	=> 50
	) );

	// 記事下の SNS シェアメッセージ
	$wp_customize->add_setting( 'sns_bottoms_msg', array(
		'default'	=> __( 'Please share this if you liked it!', 'wpthk' ),
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'sns_bottoms_msg', array(
		'settings'	=> 'sns_bottoms_msg',
		'label'		=> __( 'Tagline for the SNS buttons under articles', 'wpthk' ),
		'section'	=> 'sns_section',
		'type'		=> 'text',
		'priority'	=> 55
	) );
	// リスト型のトップページ下に SNS ボタン表示
	$wp_customize->add_setting( 'sns_toppage_view', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'sns_toppage_view', array(
		'settings'	=> 'sns_toppage_view',
		'label'		=> __( 'Dispplay SNS button at the bottom of top page list', 'wpthk' ),
		'section'	=> 'sns_section',
		'type'		=> 'checkbox',
		'priority'	=> 60
	) );
	// 固定ページの SNS ボタン表示
	$wp_customize->add_setting( 'sns_page_view', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'sns_page_view', array(
		'settings'	=> 'sns_page_view',
		'label'		=> __( 'Display SNS button on static pages', 'wpthk' ),
		'section'	=> 'sns_section',
		'type'		=> 'checkbox',
		'priority'	=> 65
	) );
	// Facebook app_id
	$wp_customize->add_setting( 'sns_fb_appid', array(
		'default' 	=> null,
		'sanitize_callback' => 'thk_sanitize'
	) );
	$wp_customize->add_control( 'sns_fb_appid', array(
		'settings'	=> 'sns_fb_appid',
		'label'		=> 'Facebook fb:app_id',
		'description'	=> '<span class="bold">(' . __( 'used for getting count on Facebook', 'wpthk' ) . ')</span><p class="f09em">' . __( '* current API allows to get the count without it, but it may become mandatory in future.', 'wpthk' ) . '</p>',
		'section'	=> 'sns_section',
		'type'		=> 'text',
		'priority'	=> 70
	) );

	//---------------------------------------------------------------------------
	// SNS シェアボタン (2)
	//---------------------------------------------------------------------------
	$wp_customize->add_section( 'sns_section_2', array(
		'title'		=> __( 'SNS share buttons', 'wpthk' ) . ' (2)',
		'description'	=> '<span style="font-weight:bold;font-size:1.1em">' . __( 'Display / non-display SNS button above articles', 'wpthk' ) . '</span>',
		'priority'	=> 96
	) );

	// Twitter ボタン表示
	$wp_customize->add_setting( 'twitter_share_tops_button', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'twitter_share_tops_button', array(
		'settings'	=> 'twitter_share_tops_button',
		'label'		=> sprintf( __( '%s button display', 'wpthk' ), 'Twitter ' ),
		'section'	=> 'sns_section_2',
		'type'		=> 'checkbox',
		'priority'	=> 10
	) );

	// Facebook ボタン表示
	$wp_customize->add_setting( 'facebook_share_tops_button', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'facebook_share_tops_button', array(
		'settings'	=> 'facebook_share_tops_button',
		'label'		=> sprintf( __( '%s button display', 'wpthk' ), 'Facebook ' ),
		'section'	=> 'sns_section_2',
		'type'		=> 'checkbox',
		'priority'	=> 15
	) );

	// Google+ ボタン表示
	$wp_customize->add_setting( 'google_share_tops_button', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'google_share_tops_button', array(
		'settings'	=> 'google_share_tops_button',
		'label'		=> sprintf( __( '%s button display', 'wpthk' ), 'Google+ ' ),
		'section'	=> 'sns_section_2',
		'type'		=> 'checkbox',
		'priority'	=> 20
	) );

	// LinkedIn ボタン表示
	$wp_customize->add_setting( 'linkedin_share_tops_button', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'linkedin_share_tops_button', array(
		'settings'	=> 'linkedin_share_tops_button',
		'label'		=> sprintf( __( '%s button display', 'wpthk' ), 'LinkedIn ' ),
		'section'	=> 'sns_section_2',
		'type'		=> 'checkbox',
		'priority'	=> 25
	) );

	// はてブ ボタン表示
	$wp_customize->add_setting( 'hatena_share_tops_button', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'hatena_share_tops_button', array(
		'settings'	=> 'hatena_share_tops_button',
		'label'		=> sprintf( __( '%s button display', 'wpthk' ), __( 'Hatena Bookmark', 'wpthk' ) . ' ' ),
		'section'	=> 'sns_section_2',
		'type'		=> 'checkbox',
		'priority'	=> 30
	) );

	// Pocket ボタン表示
	$wp_customize->add_setting( 'pocket_share_tops_button', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'pocket_share_tops_button', array(
		'settings'	=> 'pocket_share_tops_button',
		'label'		=> sprintf( __( '%s button display', 'wpthk' ), 'Pocket ' ),
		'section'	=> 'sns_section_2',
		'type'		=> 'checkbox',
		'priority'	=> 35
	) );

	// LINE ボタン表示
	$wp_customize->add_setting( 'line_share_tops_button', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'line_share_tops_button', array(
		'settings'	=> 'line_share_tops_button',
		'label'		=> sprintf( __( '%s button display', 'wpthk' ), 'LINE ' ),
		'section'	=> 'sns_section_2',
		'type'		=> 'checkbox',
		'priority'	=> 37
	) );

	// RSS ボタン表示
	$wp_customize->add_setting( 'rss_share_tops_button', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'rss_share_tops_button', array(
		'settings'	=> 'rss_share_tops_button',
		'label'		=> sprintf( __( '%s button display', 'wpthk' ), 'RSS ' ),
		'section'	=> 'sns_section_2',
		'type'		=> 'checkbox',
		'priority'	=> 40
	) );

	// Feedly ボタン表示
	$wp_customize->add_setting( 'feedly_share_tops_button', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'feedly_share_tops_button', array(
		'settings'	=> 'feedly_share_tops_button',
		'label'		=> sprintf( __( '%s button display', 'wpthk' ), 'Feedly ' ),
		'description'	=> '<p class="mm23l mm10b f11em bold snormal">' . __( 'Display / non-display SNS button below articles', 'wpthk' ) . '</p>',
		'section'	=> 'sns_section_2',
		'type'		=> 'checkbox',
		'priority'	=> 45
	) );

	// Twitter ボタン表示
	$wp_customize->add_setting( 'twitter_share_bottoms_button', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'twitter_share_bottoms_button', array(
		'settings'	=> 'twitter_share_bottoms_button',
		'label'		=> sprintf( __( '%s button display', 'wpthk' ), 'Twitter ' ),
		'section'	=> 'sns_section_2',
		'type'		=> 'checkbox',
		'priority'	=> 50
	) );

	// Facebook ボタン表示
	$wp_customize->add_setting( 'facebook_share_bottoms_button', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'facebook_share_bottoms_button', array(
		'settings'	=> 'facebook_share_bottoms_button',
		'label'		=> sprintf( __( '%s button display', 'wpthk' ), 'Facebook ' ),
		'section'	=> 'sns_section_2',
		'type'		=> 'checkbox',
		'priority'	=> 55
	) );

	// Google+ ボタン表示
	$wp_customize->add_setting( 'google_share_bottoms_button', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'google_share_bottoms_button', array(
		'settings'	=> 'google_share_bottoms_button',
		'label'		=> sprintf( __( '%s button display', 'wpthk' ), 'Google+ ' ),
		'section'	=> 'sns_section_2',
		'type'		=> 'checkbox',
		'priority'	=> 60
	) );

	// LinkedIn ボタン表示
	$wp_customize->add_setting( 'linkedin_share_bottoms_button', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'linkedin_share_bottoms_button', array(
		'settings'	=> 'linkedin_share_bottoms_button',
		'label'		=> sprintf( __( '%s button display', 'wpthk' ), 'LinkedIn ' ),
		'section'	=> 'sns_section_2',
		'type'		=> 'checkbox',
		'priority'	=> 65
	) );

	// はてブ ボタン表示
	$wp_customize->add_setting( 'hatena_share_bottoms_button', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'hatena_share_bottoms_button', array(
		'settings'	=> 'hatena_share_bottoms_button',
		'label'		=> sprintf( __( '%s button display', 'wpthk' ), __( 'Hatena Bookmark', 'wpthk' ) . ' ' ),
		'section'	=> 'sns_section_2',
		'type'		=> 'checkbox',
		'priority'	=> 70
	) );

	// Pocket ボタン表示
	$wp_customize->add_setting( 'pocket_share_bottoms_button', array(
		'default'	=> true,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'pocket_share_bottoms_button', array(
		'settings'	=> 'pocket_share_bottoms_button',
		'label'		=> sprintf( __( '%s button display', 'wpthk' ), 'Pocket ' ),
		'section'	=> 'sns_section_2',
		'type'		=> 'checkbox',
		'priority'	=> 75
	) );

	// LINE ボタン表示
	$wp_customize->add_setting( 'line_share_bottoms_button', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'line_share_bottoms_button', array(
		'settings'	=> 'line_share_bottoms_button',
		'label'		=> sprintf( __( '%s button display', 'wpthk' ), 'LINE ' ),
		'section'	=> 'sns_section_2',
		'type'		=> 'checkbox',
		'priority'	=> 77
	) );

	// RSS ボタン表示
	$wp_customize->add_setting( 'rss_share_bottoms_button', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'rss_share_bottoms_button', array(
		'settings'	=> 'rss_share_bottoms_button',
		'label'		=> sprintf( __( '%s button display', 'wpthk' ), 'RSS ' ),
		'section'	=> 'sns_section_2',
		'type'		=> 'checkbox',
		'priority'	=> 80
	) );

	// Feedly ボタン表示
	$wp_customize->add_setting( 'feedly_share_bottoms_button', array(
		'default'	=> false,
		'sanitize_callback' => 'thk_sanitize_boolean'
	) );
	$wp_customize->add_control( 'feedly_share_bottoms_button', array(
		'settings'	=> 'feedly_share_bottoms_button',
		'label'		=> sprintf( __( '%s button display', 'wpthk' ), 'Feedly ' ),
		'section'	=> 'sns_section_2',
		'type'		=> 'checkbox',
		'priority'	=> 85
	) );
});

/*---------------------------------------------------------------------------
 * カスタマイズ画面の CSS
 *---------------------------------------------------------------------------*/
add_action( 'admin_enqueue_scripts', function() {
	wp_register_style( 'thk_admin_menu_css', get_template_directory_uri() . '/css/admin-custom-menu.css', false, false );
        wp_enqueue_style( 'thk_admin_menu_css' );
});

/*---------------------------------------------------------------------------
 * sanitize
 *---------------------------------------------------------------------------*/
// 文字列型
if( function_exists('thk_sanitize') === false ):
function thk_sanitize( $value ) {
	if( is_string( $value ) === true ) {
		//return htmlspecialchars( $value );
		return esc_attr( $value );
	}
	return $value;
}
endif;

// INT型
if( function_exists('thk_sanitize_integer') === false ):
function thk_sanitize_integer( $value ) {
	if( ctype_digit( $value ) ) {
		return (int)$value;
	}
	return 0;
}
endif;

// BOOL型
if( function_exists('thk_sanitize_boolean') === false ):
function thk_sanitize_boolean( $value ) {
	if( $value === true ) {
		return true;
	}
	return false;
}
endif;

// URL
if( function_exists('thk_sanitize_url') === false ):
function thk_sanitize_url( $value ) {
	return esc_url_raw( $value );
}
endif;

// 色コード
if( function_exists('thk_sanitize_color') === false ):
function thk_sanitize_color( $value ) {
	return sanitize_hex_color( maybe_hash_hex_color( $value ) );
}
endif;
