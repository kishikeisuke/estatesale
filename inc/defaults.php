<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

if( class_exists( 'defConfig' ) === false ):
class defConfig {
	public function __construct() {
	}

	public function default_variables() {
		$share_msg = __( 'Please share this if you liked it!', 'wpthk' );
		$read_more = __( 'Read more', 'wpthk' );
		$home_text = __( 'Home', 'wpthk' );

		if( get_locale() === 'ja' ) {
			$share_msg = 'よろしければシェアお願いします';
			$read_more = '記事を読む';
			$home_text = 'ホーム';
		}

		$defaults = array(
			'bootstrap_header'	=> 'out',
			'bootstrap_footer'	=> 'out',
			'foot_widget'		=> 3,
			'pagination_visible'	=> true,
			'related_visible'	=> true,
			'comment_visible'	=> true,
			'trackback_visible'	=> true,
			'comment_page_visible'	=> true,
			'trackback_page_visible'=> true,
			'thumbnail_width'	=> 150,
			'thumbnail_height'	=> 150,
			'thumbnail_is_size'	=> 'generate',
			'title_sep'		=> 'pipe',
			'title_top_list'	=> 'site_catch',
			'title_front_page'	=> 'site_catch',
			'title_other'		=> 'title_site',
			'canonical_enable'	=> true,
			'next_prev_enable'	=> true,
			'rss_feed_enable'	=> true,
			'atom_feed_enable'	=> true,
			'meta_keywords'		=> 'none',
			'twitter_card_type'	=> 'summary',
			'child_css'		=> true,
			'bootstrap_js_load_type' => 'none',
			'jquery_load'		=> true,
			'jquery_migrate'	=> 'not',
			'awesome_css_type'	=> 'minimum',
			'awesome_load'		=> 'sync',
			'html_compress'		=> 'none',
			'child_css_compress'	=> 'none',
			'child_js_compress'	=> 'none',
			'list_view'		=> 'excerpt',
			'breadcrumb_view'	=> 'outer',
			'content_side_discrete'	=> 'discrete',
			'content_discrete'	=> 'discrete',
			'thumbnail_visible'	=> true,
			'noimage_visible'	=> true,
			'excerpt_length'	=> 120,
			'excerpt_priority'	=> true,
			'read_more_text'	=> $read_more,
			'short_title_length'	=> 16,
			'read_more_short_title'	=> true,
			'header_catchphrase_visible'	=> true,
			'home_text'			=> $home_text,
			'page_top_text'			=> 'PAGE TOP',
			'page_top_icon'			=> 'fa_caret_square_o_up',
			'post_date_visible'		=> true,
			'mod_date_visible'		=> true,
			'category_meta_u_visible'	=> true,
			'tag_meta_u_visible'		=> true,
			'list_post_date_visible'	=> true,
			'list_category_meta_visible'	=> true,
			'list_mod_date_u_visible'	=> true,
			'global_navi_visible'		=> true,
			'global_navi_position'		=> 'under',
			'global_navi_mobile_type'	=> 'luxury',
			'head_band_visible'	=> true,
			'head_band_follow_icon'	=> 'icon_name',
			'head_band_rss'		=> true,
			'head_band_feedly'	=> true,
			'gallery_type'		=> 'none',
			'add_class_external'	=> true,
			'twitter_share_tops_button'	=> true,
			'facebook_share_tops_button'	=> true,
			'google_share_tops_button'	=> true,
			'linkedin_share_tops_button'	=> true,
			'hatena_share_tops_button'	=> true,
			'pocket_share_tops_button'	=> true,
			'twitter_share_bottoms_button'	=> true,
			'facebook_share_bottoms_button'	=> true,
			'google_share_bottoms_button'	=> true,
			'linkedin_share_bottoms_button'	=> true,
			'hatena_share_bottoms_button'	=> true,
			'pocket_share_bottoms_button'	=> true,
			'search_extract'	=> 'word',
			'search_extract_length'	=> 140,
			'search_match_method'	=> 'default',
			'search_highlight'	=> true,
			'highlight_bold'	=> true,
			'highlight_oblique'	=> true,
			'highlight_bg'		=> true,
			'highlight_bg_color'	=> '#ffd700',
			'highlight_radius'	=> 6,
			'sns_count_cache_expire'	=> 600,
			'sns_count_weekly_cleanup'	=> 'dust',
			'sns_tops_enable'	=> true,
			'sns_tops_type'		=> 'color',
			'sns_bottoms_enable'	=> true,
			'sns_bottoms_type'	=> 'color',
			'sns_bottoms_msg'	=> $share_msg,
			'sns_toppage_view'	=> true,
			'sns_page_view'		=> true,
			'adsense_visible'	=> true,
			'preview_adsense_visible'	=> true,
			'user_scalable'		=> 'yes',
			'author_visible'	=> true,
			'author_page_type'	=> 'auth'
		);
		return $defaults;
	}

	public function default_custom_variables() {
		$this->default_variables();
		$defaults = $this->default_variables() + array(
			'wpthk_mode_select'	=> 'wpthk',
			'nextpage_index'	=> false,
			'css_to_style'		=> false,
			'jquery_defer'		=> false,
			'column3'		=> '2column',
			'column_home'		=> 'default',
			'column_post'		=> 'default',
			'column_page'		=> 'default',
			'column_archive'	=> 'default',
			'side_1_width'		=> 366,
			'side_2_width'		=> 280,
			'side_position'		=> 'right',
			'column3_position'	=> 'center',
			'column3_reverse'	=> false,
			'sticky_no_excerpt'	=> false,
			'container_max_width'	=> 1170,
			'title_position'	=> 'left',
			'contents_border'	=> true,
			'pagination_area_border'=> true,
			'cont_border_radius'	=> 0,
			'sidebar_border'	=> true,
			'side_border_radius'	=> 0,
			'header_border'		=> false,
			'header_border_wide'	=> false,
			'footer_border'		=> false,
			'footer_border_top'	=> true,
			'head_margin_top'	=> 0,
			'head_padding_top'	=> 20,
			'head_padding_right'	=> 10,
			'head_padding_bottom'	=> 20,
			'head_padding_left'	=> 10,
			'side_discrete'		=> 'indiscrete',
			'cont_padding_top'	=> 35,
			'cont_padding_right'	=> 25,
			'cont_padding_bottom'	=> 0,
			'cont_padding_left'	=> 25,
			'break_excerpt'		=> false,
			'header_catchphrase_change'	=> THK_DESCRIPTION,
			'page_top_color'		=> null,
			'page_top_bg_color'		=> null,
			'category_meta_visible'		=> false,
			'tag_meta_visible'		=> false,
			'tax_meta_visible'		=> false,
			'post_date_u_visible'		=> false,
			'mod_date_u_visible'		=> false,
			'tax_meta_u_visible'		=> false,
			'list_mod_date_visible'		=> false,
			'list_tag_meta_visible'		=> false,
			'list_tax_meta_visible'		=> false,
			'list_post_date_u_visible'	=> false,
			'list_category_meta_u_visible'	=> false,
			'list_tag_meta_u_visible'	=> false,
			'list_tax_meta_u_visible'	=> false,
			'thumbnail_layout'	=> 'right',
			'thumbnail_view_height'	=> 'fixed',
			'top_description'	=> THK_DESCRIPTION,
			'facebook_ogp_enable'	=> false,
			'facebook_admin'	=> null,
			'facebook_app_id'	=> null,
			'twitter_card_enable'	=> false,
			'twitter_id'		=> null,
			'child_script'		=> false,
			'html5shiv_load_type'	=> false,
			'respondjs_load_type'	=> false,
			'thk_emoji_disable'	=> false,
			'thk_embed_disable'	=> false,
			'css_search'		=> true,
			'css_archive'		=> true,
			'css_calendar'		=> true,
			'css_new_post'		=> true,
			'css_rcomments'		=> true,
			'css_follow_button'	=> true,
			'css_rss_feedly'	=> true,
			'css_qr_code'		=> true,
			'child_js_file_1'	=> null,
			'child_js_file_2'	=> null,
			'child_js_file_3'	=> null,
			'overall_image'		=> 'white',
			'body_color'		=> null,
			'body_link_color'	=> null,
			'body_hover_color'	=> null,
			'breadcrumb_color'	=> null,
			'head_color'		=> null,
			'head_link_color'	=> null,
			'head_hover_color'	=> null,
			'foot_color'		=> null,
			'foot_link_color'	=> null,
			'foot_hover_color'	=> null,
			'body_bg_color'		=> null,
			'body_transparent'	=> 100,
			'cont_bg_color'		=> null,
			'cont_border_color'	=> null,
			'cont_transparent'	=> 100,
			'side_bg_color'		=> null,
			'side_border_color'	=> null,
			'side_transparent'	=> 100,
			'head_bg_color'		=> null,
			'head_border_color'	=> null,
			'head_transparent'	=> 100,
			'foot_bg_color'		=> null,
			'foot_border_color'	=> null,
			'foot_transparent'	=> 100,
			'title_img'		=> null,
			'body_bg_img'		=> null,
			'body_img_vertical'	=> 'top',
			'body_img_horizontal'	=> 'left',
			'body_img_fixed'	=> false,
			'body_img_transparent'	=> 0,
			'body_img_repeat'	=> 'repeat',
			'body_img_size'		=> 'auto',
			'side_bg_img'		=> null,
			'head_bg_img'		=> null,
			'head_img_width_max'	=> false,
			'head_img_vertical'	=> 'top',
			'head_img_horizontal'	=> 'left',
			//'head_img_fixed'	=> false,
			'head_img_repeat'	=> 'repeat',
			'head_img_size'		=> 'auto',
			'logo_img'		=> null,
			'logo_img_up'		=> false,
			'global_navi_open_close'	=> 'individual',
			'global_navi_sticky'		=> 'none',
			'global_navi_translucent'	=> false,
			'global_navi_add_sep'		=> false,
			'global_navi_auto_resize'	=> 'auto',
			'gnavi_color'			=> null,
			'gnavi_bar_bg_color'		=> null,
			'gnavi_bg_color'		=> null,
			'gnavi_bg_hover_color'		=> null,
			'gnavi_bg_current_color'	=> null,
			'gnavi_border_top_color'	=> null,
			'gnavi_border_bottom_color'	=> null,
			'gnavi_separator_color'		=> null,
			'gnavi_border_top_width'	=> 1,
			'gnavi_border_bottom_width'	=> 4,
			'gnavi_top_buttom_padding'	=> 8,
			'head_band_wide'	=> false,
			'head_band_fixed'	=> false,
			'head_band_height'	=> 28,
			'head_band_color'	=> null,
			'head_band_hover_color'	=> null,
			'head_band_bg_color'	=> null,
			'head_band_border_bottom_color'	=> null,
			'head_band_border_bottom_width'	=> 1,
			'head_band_follow_color'	=> false,
			'head_band_twitter'		=> false,
			'head_band_search'		=> false,
			'head_search_color'		=> null,
			'head_search_bg_color'		=> null,
			'head_search_transparent'	=> 30,
			'follow_twitter_id'		=> null,
			'head_band_facebook'		=> false,
			'follow_facebook_id'		=> null,
			'head_band_hatena'		=> false,
			'follow_hatena_id'		=> null,
			'head_band_google'		=> false,
			'follow_google_id'		=> null,
			'head_band_youtube'		=> false,
			'follow_youtube_id'		=> null,
			'head_band_line'		=> false,
			'follow_line_id'		=> null,
			'parent_css_uncompress'	=> false,
			'anime_sitename'	=> 'none',
			'anime_thumbnail'	=> 'none',
			'anime_sns_buttons'	=> 'none',
			'anime_global_navi'	=> 'none',
			'lazyload_enable'	=> false,
			'lazyload_crawler'	=> false,
			'lazyload_placeholder'	=> null,
			'lazyload_spinner'	=> true,
			'lazyload_threshold'	=> 0,
			'lazyload_effect'	=> 'fadeIn',
			'lazyload_effectspeed'	=> 1000,
			'lazyload_event'	=> 'scroll',
			'external_icon_type'	=> 'normal',
			'external_icon_color'	=> null,
			'add_external_icon'	=> false,
			'add_target_blank'	=> false,
			'add_rel_nofollow'	=> false,
			'line_share_tops_button'=> false,
			'rss_share_tops_button'	=> false,
			'feedly_share_tops_button'	=> false,
			'line_share_bottoms_button'	=> false,
			'rss_share_bottoms_button'	=> false,
			'feedly_share_bottoms_button'	=> false,
			'sns_count_cache_enable'	=> false,
			'sns_count_cache_force'		=> false,
			'sns_tops_position'	=> 'left',
			'sns_tops_count'	=> false,
			'sns_tops_multiple'	=> false,
			'sns_bottoms_position'	=> 'left',
			'sns_bottoms_count'	=> false,
			'sns_bottoms_multiple'	=> false,
			'sns_fb_appid'		=> null,
			'page_adsense_visible'	=> false,
			'comment_search'	=> false,
			'buffering_enable'	=> false,
			'add_role_attribute'	=> false,
			'remove_hentry_class'	=> false,
			'enable_mb_slug'	=> false,
			'categories_a_inner'	=> false,
			'archives_a_inner'	=> false,
			'sns_count_cache_cleanup'	=> false,
			'thk_author_url'		=> THK_HOME_URL,
			'all_clear'			=> false
		);
		return $defaults;
	}

	public function over_all_default_contents_background_color() {
		return array(
			'white'		=> '#ffffff',
			'pearl'		=> '#ffffff',
			'coffee'	=> '#ffffff',
			'dark'		=> '#ffffff',
			'black'		=> '#000000',
			'pure'		=> 'inherit'
		);
	}

	public function const_custom( $defaults = array() ) {
		$mods = wp_parse_args( get_theme_mods(), $defaults );
		foreach( $mods as $key => $val ) {
			if( ctype_digit( $val ) && $val == (int)$val ) {
				$mods[$key] = (int)apply_filters( 'theme_mod_' . $key, $val );
			}
			else {
				$mods[$key] = apply_filters( 'theme_mod_' . $key, $val );
			}
		}
		return( $mods );
	}

	public function optimize_wpthk_variable() {
		global $wpthk;
		foreach( $wpthk as $key => $val ) {
			if( empty( $val ) && $val !== 0 ) unset( $wpthk[$key] );
			if( ctype_digit( $val ) && $val == (int)$val ) {
				$wpthk[$key] = (int)$val;
				if( $key === 'facebook_admin' || $key === 'facebook_app_id' ||$key ===  'twitter_id' ) {
					$wpthk[$key] = (string)$val;
				}
			}
		}

		// カラム操作で別々のカラムになってるかどうかの判別 Global 変数追加
		$wpthk['column_default'] = true;
		$wpthk['column_style'] = isset( $wpthk['column3'] ) ? $wpthk['column3'] : '2column';

		$column_home = isset( $wpthk['column_home'] ) ? $wpthk['column_home'] : 'default';
		$column_post = isset( $wpthk['column_post'] ) ? $wpthk['column_post'] : 'default';
		$column_page = isset( $wpthk['column_page'] ) ? $wpthk['column_page'] : 'default';
		$column_archive = isset( $wpthk['column_archive'] ) ? $wpthk['column_archive'] : 'default';

		$columns = array(
			$column_home,
			$column_post,
			$column_page,
			$column_archive
		);

		foreach( $columns as $col ) {
			if( $wpthk['column_style'] !== $col && $col !== 'default' ) {
				$wpthk['column_default'] = false;
				break;
			}
		}

		// アクセスされたテンプレートのカラム数を格納する Global 変数追加
		if( is_front_page() === true && isset( $wpthk['column_home'] ) && $wpthk['column_home'] !== 'default' ) {
			$wpthk['column_style'] = $wpthk['column_home'];
		}
		elseif( is_single() === true && isset( $wpthk['column_post'] ) && $wpthk['column_post'] !== 'default' ) {
			$wpthk['column_style'] = $wpthk['column_post'];
		}
		elseif( ( is_page() === true || is_404() === true ) && isset( $wpthk['column_page'] ) && $wpthk['column_page'] !== 'default' ) {
			$wpthk['column_style'] = $wpthk['column_page'];
		}
		elseif( ( is_archive() === true || is_search() === true ) && isset( $wpthk['column_archive'] ) && $wpthk['column_archive'] !== 'default' ) {
			$wpthk['column_style'] = $wpthk['column_archive'];
		}
	}
}
endif;
