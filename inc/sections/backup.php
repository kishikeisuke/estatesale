<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

?>
<form id="wpthk-customize" method="post" action="">
<?php
settings_fields( 'backup' );
?>
<ul>
<li>
<div>
<p class="f09em"><?php echo __( '* Will create the back up for all the WpTHK customized data.', 'wpthk' ); ?></p>
<p class="f09em"><?php echo __( '* File will be saved in JSON format.', 'wpthk' ); ?></p>
<div class="wpthk-backup">
<?php
submit_button( __( 'Backup', 'wpthk' ), 'secondary', 'backup', true, array() );
?>
</div>
</div>
</li>
</ul>
</form>
