<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

?>
<ul>
<li>
<p class="control-title"><?php echo __( 'To add tags', 'wpthk' ); ?></p>
<p class="checkbox">
<input type="checkbox" value="" name="canonical_enable"<?php thk_value_check( 'canonical_enable', 'checkbox' ); ?> />
<?php printf( __( 'Add %s', 'wpthk' ), 'canonical' . ' ' ); ?>
</p>
</li>
<li>
<p class="checkbox">
<input type="checkbox" value="" name="next_prev_enable"<?php thk_value_check( 'next_prev_enable', 'checkbox' ); ?> />
<?php printf( __( 'Add %s', 'wpthk' ), 'next / prev' . ' ' ); ?>
</p>
</li>
<li>
<p class="checkbox">
<input type="checkbox" value="" name="rss_feed_enable"<?php thk_value_check( 'rss_feed_enable', 'checkbox' ); ?> />
<?php printf( __( 'Add %s', 'wpthk' ), 'RSS Feed' . ' ' ); ?>
</p>
</li>
<li>
<p class="checkbox">
<input type="checkbox" value="" name="atom_feed_enable"<?php thk_value_check( 'atom_feed_enable', 'checkbox' ); ?> />
<?php printf( __( 'Add %s', 'wpthk' ), 'Atom Feed' . ' ' ); ?>
</p>
</li>
<li>
<div>
<p class="control-title"><?php echo __( 'The front page meta description', 'wpthk' ); ?></p>
<input type="text" value="<?php thk_value_check( 'top_description', 'text' ); ?>" name="top_description" />
</div>
</li>
<li>
<p class="control-title"><?php printf( __( 'Setting of %s', 'wpthk' ), 'Meta keywords ' ); ?></p>
<p class="radio">
<input type="radio" value="tags" name="meta_keywords"<?php thk_value_check( 'meta_keywords', 'radio', 'tags' ); ?> />
<?php echo __( 'Put tags and category names into meta keywords', 'wpthk' ); ?>
</p>
<p class="radio">
<input type="radio" value="none" name="meta_keywords"<?php thk_value_check( 'meta_keywords', 'radio', 'none' ); ?> />
<?php echo __( 'Do not need any meta keywords!', 'wpthk' ); ?>
</p>
</li>
<li>
<p class="control-title"><?php echo __( 'Splitting Content for blog posts and pages', 'wpthk' ); ?></p>
<p class="checkbox">
<input type="checkbox" value="" name="nextpage_index"<?php thk_value_check( 'nextpage_index', 'checkbox' ); ?> />
<?php echo __( 'Prohibit crawlers to index second page onward when contents are split using &lt;!--nextpage--&gt; tag.', 'wpthk' ); ?>
</p>
</li>
</ul>
