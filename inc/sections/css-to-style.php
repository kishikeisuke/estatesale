<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

?>
<ul>
<li>
<div>
<input type="checkbox" value="" name="css_to_style"<?php thk_value_check( 'css_to_style', 'checkbox' ); ?> />
<?php echo __( 'Include CSS in HTML', 'wpthk' ); ?><?php echo ' ( ', __( 'Accelerate', 'wpthk' ), ' )'; ?></div>
<p class="f09em"><?php echo __( '* It will include the style.css code directly in the HTML file. You can reduce HTTP requests.', 'wpthk' ); ?></p>
</li>
<li>
<div>
</ul>
