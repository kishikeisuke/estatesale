<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

?>
<ul>
<li>
<div>
<p class="label-title"><?php echo __( 'How to load bootstrap.js', 'wpthk' ); ?></p>
<select name="bootstrap_js_load_type">
<option value="asyncdefer"<?php thk_value_check( 'bootstrap_js_load_type', 'select', 'asyncdefer' ); ?>><?php echo __( 'Asynchronous', 'wpthk' ), ' (async defer)'; ?></option>
<option value="async"<?php thk_value_check( 'bootstrap_js_load_type', 'select', 'async' ); ?>><?php echo __( 'Asynchronous', 'wpthk' ), ' (async)'; ?></option>
<option value="defer"<?php thk_value_check( 'bootstrap_js_load_type', 'select', 'defer' ); ?>><?php echo __( 'Asynchronous', 'wpthk' ), ' (defer)'; ?></option>
<option value="sync"<?php thk_value_check( 'bootstrap_js_load_type', 'select', 'sync' ); ?>><?php echo __( 'Synchronism', 'wpthk' ); ?></option>
<option value="none"<?php thk_value_check( 'bootstrap_js_load_type', 'select', 'none' ); ?>><?php echo __( 'Not required (no load)', 'wpthk' ); ?></option>
</select>
</div>
</li>
</ul>
