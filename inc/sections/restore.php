<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

?>
<form enctype="multipart/form-data" id="wpthk-customize" method="post" action="">
<?php
settings_fields( 'restore' );
?>
<ul>
<li>
<div>
<p class="f09em"><?php echo __( '* Will restore WpTHK customized setting from JSON backed up file.', 'wpthk' ); ?></p>
<label class="button secondary"><?php echo __( 'Restore', 'wpthk' ); ?>
<input type="file" id="wpthk-restore" name="wpthk-restore" style="display:none" />
</label>
<div style="display:none">
<?php
submit_button( __( 'Restore', 'wpthk' ), 'secondary', 'restore', true, array() );
?>
</div>
</div>
</li>
</ul>
</form>
<script>
jQuery(function($) {
	$("#wpthk-restore").change(function () {
		$(this).closest("form").submit();
	});
});
</script>
