<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

?>
<ul>
<li>
<p class="control-title"><?php printf( __( 'Setting of %s', 'wpthk' ), 'Facebook OGP' . ' ' ); ?></p>
<p class="checkbox">
<input type="checkbox" value="" name="facebook_ogp_enable"<?php thk_value_check( 'facebook_ogp_enable', 'checkbox' ); ?> />
<?php printf( __( 'Add %s', 'wpthk' ), 'Facebook OGP' . ' ' ); ?>
</p>
</li>
<li>
<div>
<p class="label-title">fb:admins (<?php echo __( 'Depreciated: Unnecessary if app_id exists', 'wpthk' ); ?>)</p>
<input type="text" value="<?php thk_value_check( 'facebook_admin', 'text' ); ?>" name="facebook_admin" />
</div>
</li>
<li>
<div>
<p class="label-title">fb:app_id (<?php echo __( 'Recommended', 'wpthk' ); ?>)</p>
<input type="text" value="<?php thk_value_check( 'facebook_app_id', 'text' ); ?>" name="facebook_app_id" />
</div>
</li>
<li>
<p class="control-title"><?php printf( __( 'Setting of %s', 'wpthk' ), __( 'Twitter Card', 'wpthk' ) ); ?></p>
<p class="checkbox">
<input type="checkbox" value="" name="twitter_card_enable"<?php thk_value_check( 'twitter_card_enable', 'checkbox' ); ?> />
<?php printf( __( 'Add %s', 'wpthk' ), __( 'Twitter Card', 'wpthk' ) ); ?>
</p>
</li>
<li>
<div>
<p class="label-title"><?php printf( __( 'Type of %s', 'wpthk' ), __( 'Twitter Card', 'wpthk' ) ); ?></p>
<select name="twitter_card_type">
<option value="summary"<?php thk_value_check( 'twitter_card_type', 'select', 'summary' ); ?>><?php echo __( 'summary', 'wpthk' ); ?></option>
<option value="large"<?php thk_value_check( 'twitter_card_type', 'select', 'largee' ); ?>><?php echo __( 'summary_large_image', 'wpthk' ); ?></option>
<option value="photo"<?php thk_value_check( 'twitter_card_type', 'select', 'photo' ); ?>><?php echo __( 'photo', 'wpthk' ); ?></option>
</select>
</div>
</li>
<li>
<div>
<p class="label-title">Twitter ID</p>
<input type="text" value="<?php thk_value_check( 'twitter_id', 'text' ); ?>" name="twitter_id" />
</div>
</li>
</ul>
