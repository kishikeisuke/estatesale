<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

?>
<ul>
<li>
<p class="f09em"><?php echo __( '* Both has the same design layout but WpTHK mode is faster and lighter.  Bootstrap mode should be chosen if you want to use the functions or classes of Bootstrap.', 'wpthk' ); ?></p>
<p class="radio">
<input type="radio" value="wpthk" name="wpthk_mode_select"<?php thk_value_check( 'wpthk_mode_select', 'radio', 'wpthk' ); ?> />
WpTHK Mode
</p>
<p class="radio">
<input type="radio" value="bootstrap" name="wpthk_mode_select"<?php thk_value_check( 'wpthk_mode_select', 'radio', 'bootstrap' ); ?> />
Bootstrap Mode
</p>
</li>
</ul>


