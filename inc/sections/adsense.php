<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

?>
<ul>
<li>
<p class="checkbox">
<input type="checkbox" value="" name="adsense_visible"<?php thk_value_check( 'adsense_visible', 'checkbox' ); ?> />
<?php echo __( 'Display Ad widgets uder articles', 'wpthk' ); ?>
</p>
</li>
<li>
<p class="checkbox">
<input type="checkbox" value="" name="page_adsense_visible"<?php thk_value_check( 'page_adsense_visible', 'checkbox' ); ?> />
<?php echo __( 'Advertisement displayed on static pages', 'wpthk' ); ?>
</p>
</li>
<li>
<p class="checkbox">
<input type="checkbox" value="" name="preview_adsense_visible"<?php thk_value_check( 'preview_adsense_visible', 'checkbox' ); ?> />
<?php echo __( 'Do not show ads in preview screens - editing', 'wpthk' ); ?>
</p>
</li>
</ul>
