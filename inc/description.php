<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

/*---------------------------------------------------------------------------
 * オリジナルディスクリプション生成
 *---------------------------------------------------------------------------*/
if( function_exists( 'thk_create_description' ) === false ):
function thk_create_description() {
	global $wpthk, $post;
	$desc = '';

	if( is_front_page() === true || is_home() === true ) { // フロントページの時
		$desc = isset( $wpthk['top_description'] ) ? $wpthk['top_description'] : THK_DESCRIPTION;
	}
	elseif( is_singular() === true ) { // 個別記事と固定ページ
		if( $post->post_excerpt ){ // 抜粋がある場合
			$desc = str_replace( array( "\r", "\n", "\t" ), '', $post->post_excerpt );

			// ページ分割してる場合は、2ページ目以降に No 付ける
			if( stripos( $post->post_content, '<!--nextpage-->' ) !== 0 ) {
				$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;

				if( $paged > 1 ) {
					$desc .= ' | ' . 'NO:' . $paged;
				}
			}
		}
		else {
			$content = '';

			// ページ分割してる場合は「そのページで表示されてる本文」の先頭 100 文字
			if( stripos( $post->post_content, '<!--nextpage-->' ) !== 0 ) {
				$contents = explode('<!--nextpage-->', $post->post_content );
				$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
				$content = trim( $contents[$paged - 1] );
			}
			else {
				$content = $post->post_content;
			}

			// 抜粋がない場合、記事の最初の100文字を description にする
			$length = 100;
			$summary = strip_tags( $content );
			$summary = strip_shortcodes( $summary );
			$summary = preg_replace( '/\s+/', ' ', $summary );
			$summary = esc_html( mb_substr( $summary, 0, 100 ) ); // 抜粋文字数
			$summary .= mb_strlen( $content ) >= $length ? '...' : '';
			$desc = trim( $summary );
		}
	}
	elseif( is_category() === true ) { // カテゴリページ
		$desc = get_bloginfo('name') . ' | ' . sprintf( __('%s Category', 'wpthk' ), single_cat_title( '', false ) );
	}
	else { // その他のページ ( description の重複を避けるため NO を付ける)
		$id = get_the_ID();
		$desc = get_bloginfo('name') . ' | ' . THK_DESCRIPTION;
		if( !empty( $id) ) $desc .= ' | ' . 'NO:' . $id;
	}

	return $desc;
}
endif;

/*---------------------------------------------------------------------------
 * タグ名やカテゴリ名を meta keyords に変換
 *---------------------------------------------------------------------------*/
if( function_exists( 'tag_to_keywords' ) === false ):
function tag_to_keywords() {
	global $wp_query;
	$keys = '';

	if( is_single() === true ) {
		$cat_array = get_the_category( $wp_query->post->ID );
		if( is_array( $cat_array ) === true ) {
			foreach( $cat_array as $value) {
				$keys .= esc_html( $value->cat_name ) . ', ';
			}
			$tag_array = get_the_tags( $wp_query->post->ID );
		}
		if( is_array( $tag_array ) === true ) {
			foreach ( $tag_array as $value ) {
				$keys .= esc_html( $value->name ) . ', ';
			}
		}
		$keys = rtrim( $keys, ', ' );
	}
	elseif( get_query_var('cat') ) {
		$keys .= single_cat_title( '', false );
	}
	elseif( is_tag() === true ) {
		$keys .= single_tag_title( '', false );
	}
	elseif( is_month() === true ) {
		$keys .= get_the_date('Y年m月');
	}
	return $keys;
}
endif;

/*---------------------------------------------------------------------------
 * オリジナルディスクリプション挿入
 *---------------------------------------------------------------------------*/
add_filter( 'wp_head', function() {
	global $wpthk;
	$desc = thk_create_description();
?>
<meta name="description" content="<?php echo $desc; ?>" />
<?php
	if( $wpthk['meta_keywords'] !== 'none' ) {
		if( is_single() === true || is_category() === true || is_tag() === true || is_month() === true ) {
			$keywords = tag_to_keywords( 'tags' );
?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php
		}
	}
}, 3 );

/*---------------------------------------------------------------------------
 * OGP 挿入
 *---------------------------------------------------------------------------*/
add_filter( 'wp_head', function() {
	global $wpthk, $post;

	if( isset( $wpthk['facebook_ogp_enable'] ) || isset( $wpthk['twitter_card_enable'] ) ) {
		$url = '';
		$title = wp_get_document_title();
		$blog_name = get_bloginfo('name');
		$site_name = $blog_name . ' ' . thk_title_separator( '|' ) . ' ' . THK_DESCRIPTION;
		if( is_singular() ) {
			$cont = $post->post_content;
			$preg = '/<img.*?src=(["\'])(.+?)\1.*?>/i';

			$url = get_the_permalink();

			$og_img = get_post_meta( $post->ID, 'og_img', true );
			$post_thumbnail = has_post_thumbnail();
			if( !empty( $og_img ) ) {
				$image = $og_img;
			}
			elseif( !empty( $post_thumbnail ) ) {
				$img_id = get_post_thumbnail_id();
				$img_arr = wp_get_attachment_image_src( $img_id, 'full');
				$image = $img_arr[0];
			}
			else if( preg_match( $preg, $cont, $img_url ) ) {
				$image = $img_url[2];
			}
			else {
				$image = get_stylesheet_directory_uri() . '/images/og.png';
			}
		}
		else {
			$url = THK_HOME_URL;

			if( get_header_image() === true ){
				$image = get_header_image();
			}
			else {
				$image = get_stylesheet_directory_uri() . '/images/og.png';
			}
		}
		$desc = thk_create_description();
	}
	if( isset( $wpthk['facebook_ogp_enable'] ) ) {
?>
<meta property="og:type" content="<?php echo (is_singular() ? 'article' : 'website'); ?>" />
<meta property="og:url" content="<?php echo $url; ?>" />
<meta property="og:title" content="<?php echo $title; ?>" />
<meta property="og:description" content="<?php echo $desc; ?>" />
<meta property="og:image" content="<?php echo $image; ?>" />
<meta property="og:site_name" content="<?php echo $site_name; ?>" />
<meta property="og:locale" content="ja_JP" />
<?php if( isset( $wpthk['facebook_admin'] ) ) { ?>
<meta property="fb:admins" content="<?php echo $wpthk['facebook_admin']; ?>" />
<?php } ?>
<?php if( isset( $wpthk['facebook_app_id'] ) ) { ?>
<meta property="fb:app_id" content="<?php echo $wpthk['facebook_app_id']; ?>" />
<?php } ?>
<?php
		if( is_singular() === true ) {
			$published_time = get_post( $post->ID )->post_date;
			$published_time = str_replace( ' ', 'T', $published_time ) . 'Z';
			$modified_time = get_post( $post->ID )->post_modified;
			$modified_time = str_replace( ' ', 'T', $modified_time ) . 'Z';
?>
<meta property="article:published_time" content="<?php echo $published_time ?>" />
<meta property="article:modified_time" content="<?php echo $modified_time ?>" />
<?php
		}
	}
	if( isset( $wpthk['twitter_card_enable'] ) ) {
?>
<meta name="twitter:card" content="<?php echo $wpthk['twitter_card_type']; ?>" />
<meta name="twitter:url" content="<?php echo $url; ?>" />
<meta name="twitter:title" content="<?php echo $title; ?>" />
<meta name="twitter:description" content="<?php echo $desc; ?>" />
<meta name="twitter:image" content="<?php echo $image; ?>" />
<meta name="twitter:domain" content="<?php echo $_SERVER['SERVER_NAME']; ?>" />
<?php if( isset( $wpthk['twitter_id'] ) ) { ?>
<meta name="twitter:creator" content="@<?php echo $wpthk['twitter_id']; ?>" />
<meta name="twitter:site" content="@<?php echo $wpthk['twitter_id']; ?>" />
<?php } ?>
<?php
	}
}, 4 );
