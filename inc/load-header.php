<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

/*---------------------------------------------------------------------------
 * wp_head に追加するヘッダー (CSS や Javascrpt の追加など)
 *---------------------------------------------------------------------------*/
if( function_exists( 'thk_load_header' ) === false && is_admin() === false ):
add_filter( 'wp_head', 'thk_load_header', 6 );

function thk_load_header() {
	global $wpthk;

	if( isset( $wpthk['canonical_enable'] ) ) {
		if( is_singular() === true ) {
			rel_canonical();
		}
		else {
			thk_rel_canonical();
		}
	}

	wp_shortlink_wp_head();

	if( isset( $wpthk['next_prev_enable'] ) ) {
		thk_rel_next_prev();
	}
?>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	if( isset( $wpthk['author_visible'] ) && is_singular() === true ) {
		if( $wpthk['author_page_type'] === 'auth' ) {
			$auth = get_users();
?>
<link rel="author" href="<?php echo get_author_posts_url( $auth[0]->ID ); ?>" />
<?php
		}
		else {
?>
<link rel="author" href="<?php echo isset( $wpthk['thk_author_url'] ) ? $wpthk['thk_author_url'] : THK_HOME_URL; ?>" />
<?php
		}
	}

	// RSS Feed
	if( isset( $wpthk['rss_feed_enable'] ) ) {
?>
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<?php
	}
	// Atom Feed
	if( isset( $wpthk['atom_feed_enable'] ) ) {
?>
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
<?php
	}

	// Site Icon
	if( has_site_icon() === false ) {
		// favicon.ico
		if( file_exists( SPATH . DSEP . 'images' . DSEP . 'favicon.ico' ) ) {
?>
<link rel="icon" href="<?php echo SURI; ?>/images/favicon.ico" />
<?php
		}
		else {
?>
<link rel="icon" href="<?php echo TURI; ?>/images/favicon.ico" />
<?php
		}

		// Apple Touch icon
		if( file_exists( SPATH . DSEP . 'images' . DSEP . 'apple-touch-icon-precomposed.png' ) ) {
?>
<link rel="apple-touch-icon-precomposed" href="<?php echo SURI; ?>/images/apple-touch-icon-precomposed.png" />
<?php
		}
		else {
?>
<link rel="apple-touch-icon-precomposed" href="<?php echo TURI; ?>/images/apple-touch-icon-precomposed.png" />
<?php
		}
	}

	if( is_customize_preview() === true ) {
		/* 子テーマの CSS (プレビュー) */
		if( isset( $wpthk['child_css'] ) && TDEL !== SDEL ) {
			wp_enqueue_style( 'wpthkch', SDEL . '/style.css?v=' . $_SERVER['REQUEST_TIME'], false, array(), 'all' );
		}
	}
	else {
		/* 子テーマの CSS (実体) */
		if( isset( $wpthk['child_css'] ) && TDEL !== SDEL ) {
			// 子テーマ圧縮してる場合
			if( $wpthk['child_css_compress'] !== 'none' ) {
				if( file_exists( SPATH . DSEP . 'style.min.css' ) === true && filesize( SPATH . DSEP . 'style.min.css' ) !== 0 ) {
					wp_enqueue_style( 'wpthkch', SDEL . '/style.min.css' . '?v=' . $_SERVER['REQUEST_TIME'], false, array(), 'all' );
					if( isset( $wpthk['css_to_style'] ) ) {
						wp_add_inline_style( 'wpthkch', thk_direct_style( SPATH . DSEP . 'style.replace.min.css' ) );
					}
				}
			}
			// 子テーマ圧縮してない
			else {
				if( file_exists( SPATH . DSEP . 'style.css' ) === true ) {
					wp_enqueue_style( 'wpthkch', SDEL . '/style.css?v=' . $_SERVER['REQUEST_TIME'], false, array(), 'all' );
					if( isset( $wpthk['css_to_style'] ) ) {
						wp_add_inline_style( 'wpthkch', thk_direct_style( SPATH . DSEP . 'style.replace.min.css' ) );
					}
				}
			}
		}
	}

	/* テンプレートごとに違うカラム数にしてる場合の 3カラム CSS
	 * (親子 CSS 非結合時は子テーマより先に読み込ませる -> load_styles.php で処理 )
	 */
	if( $wpthk['child_css_compress'] === 'bind' ) {
		if( $wpthk['column_default'] === false ) {
			if( $wpthk['column_style'] === '2column' ) {
				wp_enqueue_style( 'wpthk2', TDEL . '/style.2col.min.css?v=' . $_SERVER['REQUEST_TIME'], false, array(), 'all' );
				wp_add_inline_style( 'wpthk2', thk_direct_style( TPATH . DSEP . 'style.2col.min.css' ) );
			}
			if( $wpthk['column_style'] === '3column' ) {
				wp_enqueue_style( 'wpthk3', TDEL . '/style.3col.min.css?v=' . $_SERVER['REQUEST_TIME'], false, array(), 'all' );
				wp_add_inline_style( 'wpthk3', thk_direct_style( TPATH . DSEP . 'style.3col.min.css' ) );
			}
		}
	}

	// サイトマップ用インラインスタイル
	if( is_page_template( 'pages/sitemap.php' ) === true ) {
		if( isset( $wpthk['child_css'] ) && TDEL !== SDEL ) {
			wp_add_inline_style( 'wpthkch', thk_sitemap_inline_style() );
		}
		else {
			wp_add_inline_style( 'wpthk', thk_sitemap_inline_style() );
		}
	}

	// 検索結果のハイライト用インラインスタイル
	if( is_search() === true && isset( $wpthk['search_highlight'] ) ) {
		if( isset( $wpthk['child_css'] ) && TDEL !== SDEL ) {
			wp_add_inline_style( 'wpthkch', thk_search_highlight_inline_style() );
		}
		else {
			wp_add_inline_style( 'wpthk', thk_search_highlight_inline_style() );
		}
	}

	/* WordPress の管理バーが見えてる場合のヘッダー上部の帯メニュー位置調整 */
	if( is_admin_bar_showing() === true ) {
?>
<style type="text/css" media="screen">
	.band { top: 32px !important; }
	@media screen and ( max-width: 782px ) {
		.band { top: 46px !important; }
	}
</style>
<?php
	}
}
endif;
