<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

//---------------------------------------------------------------------------
// QRコード
//---------------------------------------------------------------------------
if( class_exists( 'thk_qr_code' ) === false ):
class thk_qr_code extends WP_Widget {
	function __construct() {
		$widget_ops = array( 'classname' => 'widget-qr', 'description' => __( 'QR Code', 'wpthk' ) );
		parent::__construct( 'qr', __( 'QR Code', 'wpthk' ) . ' (' . __( 'by WpTHK', 'wpthk' ) . ')', $widget_ops );
	}

	function widget( $args, $instance ) {
		$title = ( !empty( $instance['title'] ) ) ? $instance['title'] : '';
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );
		$width_height = isset( $instance['size'] ) ? (int)$instance['size'] : 250;
		$description = ( isset( $instance['description'] ) ) ? esc_attr( $instance['description'] ) : __( 'QR Code', 'wpthk' );
		$format = '<img src="http://chart.apis.google.com/chart?chs=%1$dx%1$d&amp;cht=qr&amp;chld=%2$s|2&amp;chl=%3$s" width="%1$d" height="%1$d" alt="QR Code | ' . get_bloginfo('name') . '" />';

		echo $args['before_widget'];
		if( !empty( $title ) ) echo $args['before_title'] . $title . $args['after_title'];
		echo sprintf( $format, $width_height, 'L', rawurlencode( home_url() ) );
		echo $args['after_widget'];
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = esc_attr( $new_instance['title'] );
		$new_instance = wp_parse_args( (array)$new_instance, array( 'size' => 250 ) );
		$instance['size'] = (int)$new_instance['size'];
		return $instance;
	}

	function form( $instance ) {
		$title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : __( 'QR Code', 'wpthk' );
		$instance = wp_parse_args( (array)$instance, array( 'posturl' => '', 'size' => 250 ) );
		$posturl = $instance['posturl'] ? 'checked="checked"' : '';
		$size = isset( $instance['size'] ) ? (int)$instance['size'] : 250;
		$description = isset( $instance['description'] ) ? esc_attr( $instance['description'] ) : __( 'QR Code', 'wpthk' );
?>
<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php echo __( 'Title:', 'wpthk' ); ?></label> <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" /></p>
<p><label for="<?php echo $this->get_field_id('size'); ?>"><?php echo __( 'Size of QR Code', 'wpthk' ); ?></label> <input class="widefat" id="<?php echo $this->get_field_id('size'); ?>" name="<?php echo $this->get_field_name('size'); ?>" type="text" value="<?php echo $size; ?>" /></p>
<?php
	}
}
endif;

//---------------------------------------------------------------------------
// サイドバーのコメント一覧（THK オリジナル）
//---------------------------------------------------------------------------
if( class_exists( 'thk_recent_comments' ) === false ):
class thk_recent_comments extends WP_Widget {
	function __construct() {
		$widget_ops = array( 'classname' => 'thk_recent_comments', 'description' => __( 'Recent Comments (by WpTHK)', 'wpthk' ) );
		parent::__construct( 'thk_recent_comments', __( 'Recent Comments', 'wpthk' ) . ' (' . __( 'by WpTHK', 'wpthk' ) . ')', $widget_ops );
	}

	function widget( $args, $instance ) {
		global $comments, $comment;

		$title = ( !empty( $instance['title'] ) ) ? $instance['title'] : '';
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number = ( !empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
		if( empty( $number ) ) $number = 5;

		$comments = get_comments( apply_filters( 'widget_comments_args', array(
			'number' => $number,		// 表示件数
			'orderby' => 'comment_date',	// コメント日付でソート
			'order' => 'desc',		// 降順ソート
			'status' => 'approve',		// 承認されたものだけ
			'post_status' => 'publish'	// 公開済みの記事だけ
		) ) );

		echo $args['before_widget'];
		if( !empty( $title ) ) echo $args['before_title'], $title, $args['after_title'];
?>
<ul id="thk-rcomments">
<?php
		if( is_array( $comments ) === true ) {
			$title_length = '36';	// 表示する記事タイトルの最大文字数
			$auth_length  = '26';

			foreach( (array)$comments as $comment ) {
				$excerpt = $this->com_excerpt( $comment->comment_content );

				if( strlen( get_the_title( $comment->comment_post_ID ) ) >= $title_length ) {
					$post_title = mb_strimwidth( get_the_title( $comment->comment_post_ID ), 0, $title_length ) . "...";
				}
				else {
					$post_title = get_the_title( $comment->comment_post_ID );
				}

				if( strlen( $comment->comment_author ) >= $auth_length ) {
					$comment_author = mb_strimwidth( $comment->comment_author, 0, $auth_length ) . "...";
				}
				else {
					$comment_author = $comment->comment_author;
				}

				$suffix = get_locale() === 'ja' ? ' さん' : '';
				if( $comment->comment_author_url ) {
					$author_link = '<a href="' . $comment->comment_author_url . '"';
					if( strpos( $comment->comment_author_url, home_url() ) !== false ) {
						$author_link .= ' class="url">' . $comment_author . '</a>';
					}
					else {
						$author_link .= ' rel="external nofollow" class="url">' . $comment_author . '</a>' . $suffix;
					}
				}
				else {
					$author_link = $comment_author . $suffix;
				}
?>
<li class="recentcomments">
<div class="widget_comment_author">
<?php
				$avatar_id = ( !empty( $comment->user_id ) ) ? $comment->user_id : $comment->comment_author_email;
				echo str_replace( "'", '"', get_avatar( $avatar_id, 32, 'mm', $comment_author ) );

				// コメント位置までまでスクロールしたくない場合は、
				// get_comment_link($comment->comment_ID) やめて get_permalink($comment->comment_post_ID) にする
?>
<span class="comment_date"><?php echo get_comment_date( __( 'F j, Y', 'wpthk' ) ); ?></span>
<span class="author_link"><?php echo $author_link; ?></span>
</div>
<div class="comment_excerpt"><i class="fa fa-comment-o fa-fw"></i><?php echo $excerpt; ?></div>
<span class="comment_post"><i class="fa fa-angle-double-right fa-fw"></i><a href="<?php echo get_comment_link($comment->comment_ID); ?>"><?php echo $post_title; ?></a></span>
</li>
<?php
			}
 		}
?>
</ul>
<?php
		echo $args['after_widget'];
	}

	private function com_excerpt( $comment ) {
		$length = 60;
		$excerpt = strip_tags( trim( $comment) );
		$excerpt = str_replace( array("\r", "\n"), '', $excerpt );
		$excerpt = str_replace( "\t", '', $excerpt );
		$excerpt = mb_substr( $excerpt, 0, $length );
		$excerpt .= $length > 0 && mb_strlen( $excerpt ) >= $length ? ' ...' : '';

		return $excerpt;
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['number'] = absint( $new_instance['number'] );

		return $instance;
	}

	public function form( $instance ) {
		$title  = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : __( 'The latest comments to all posts', 'wpthk' );
		$number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
?>
<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php echo __( 'Title:', 'wpthk' ); ?></label>
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>
<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php echo __( 'Number of comments to show:', 'wpthk' ); ?></label>
<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>
<?php
	}
}
endif;

//---------------------------------------------------------------------------
// 新着記事
//---------------------------------------------------------------------------
if( class_exists( 'thk_recent_posts' ) === false ):
class thk_recent_posts extends WP_Widget {
	function __construct() {
		$widget_ops = array( 'classname' => 'thk_recent_posts', 'description' => __( 'Recent posts (by WpTHK)', 'wpthk' ) );
		parent::__construct( 'thk_recent_posts', __( 'Recent posts', 'wpthk' ) . ' (' . __( 'by WpTHK', 'wpthk' ) . ')', $widget_ops );
	}

	function widget( $args, $instance ) {
		$title = !empty( $instance['title'] ) ? $instance['title'] : '';
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number = !empty( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		if( empty( $number ) ) $number = 5;

		$thumbnail = isset( $instance['thumbnail'] ) ? $instance['thumbnail'] : 0;

		echo $args['before_widget'];
		if( !empty( $title ) ) echo $args['before_title'], $title, $args['after_title'];
?>
<div id="thk-new">
<?php
		$arr = array( 'posts_per_page' => $number );
		$st_query = new WP_Query( $arr );

		if( $st_query->have_posts() === true ) {
			while( $st_query->have_posts() === true ) {
				$st_query->the_post();
?>
<div class="toc clearfix">
<?php if( empty( $thumbnail ) ): ?>
<div class="term"><a href="<?php the_permalink() ?>"><?php
				$post_thumbnail = has_post_thumbnail();
				if( !empty( $post_thumbnail ) ):	// サムネイル
					the_post_thumbnail( 'thumb100' );
				else:
?><img src="<?php echo get_template_directory_uri(); ?>/images/no-img-100x100.png" alt="No Image" title="No Image" width="100" height="100" /><?php
				endif;
?></a>
</div>
<?php endif; ?>
<div class="excerpt"<?php if( !empty( $thumbnail ) ) echo ' style="padding:0 10px"'; ?>>
<p class="new-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
<p><?php
echo thk_excerpt_no_break( 40 );
?></p>
</div>
</div>
<?php
			}
		}
		else {
?>
<p><?php echo __( 'No new posts', 'wpthk' ); ?></p>
<?php
		}
		wp_reset_postdata();
?>
</div>
<?php
		echo $args['after_widget'];
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['number'] = absint( $new_instance['number'] );
		$instance['thumbnail'] = $new_instance['thumbnail'];

		return $instance;
	}

	public function form( $instance ) {
		$title  = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : __( 'Recent posts', 'wpthk' );
		$number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$thumbnail = isset( $instance['thumbnail'] ) ? $instance['thumbnail'] : 0;
?>
<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php echo __( 'Title:', 'wpthk' ); ?></label>
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>
<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php echo __( 'Number of posts to show:', 'wpthk' ); ?></label>
<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>
<p><input class="checkbox" id="<?php echo $this->get_field_id( 'thumbnail' ); ?>" name="<?php echo $this->get_field_name( 'thumbnail' ); ?>" type="checkbox" value="1" <?php checked( $thumbnail, 1 ); ?> />
<label for="<?php echo $this->get_field_id( 'thumbnail' ); ?>"><?php echo __( 'No Thumbnail', 'wpthk' ); ?></label></p>
<?php
	}
}
endif;

//---------------------------------------------------------------------------
// フォローボタン
//---------------------------------------------------------------------------
if( class_exists( 'thk_follow_button' ) === false ):
class thk_follow_button extends WP_Widget {
	function __construct() {
		$widget_ops = array( 'classname' => 'thk_rss_feedly', 'description' => __( 'SNS Follow Button (by WpTHK)', 'wpthk' ) );
		parent::__construct( 'thk_follow_button', __( 'SNS Follow Button (by WpTHK)', 'wpthk' ), $widget_ops );
	}

	function widget( $args, $instance ) {
		$title = ( !empty( $instance['title'] ) ) ? $instance['title'] : '';
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		echo $args['before_widget'];
		if( !empty( $title ) ) echo $args['before_title'], $title, $args['after_title'];

		if(
			isset( $instance['twitter'] )	||
			isset( $instance['facebook'] )	||
			isset( $instance['hatena'] )	||
			isset( $instance['google'] )	||
			isset( $instance['youtube'] )	||
			isset( $instance['line'] )	||
			isset( $instance['rss'] )	||
			isset( $instance['feedly'] )
		) {
?>
<div id="thk-follow">
<ul>
<?php
	if( isset( $instance['twitter'] ) ):
?><li><span class="snsf twitter"><a href="//twitter.com/<?php echo isset($instance['twitter_id']) ? rawurlencode( rawurldecode( $instance['twitter_id'] ) ) : ''; ?>" target="blank" title="Twitter" rel="nofollow" itemprop="sameAs url">&nbsp;<i class="fa fa-twitter"></i>&nbsp;<?php if( $instance['icon'] ) echo '<span class="fname">Twitter</span>'; ?></a></span></li>
<?php
	endif;
	if( isset( $instance['facebook'] ) ):
?><li><span class="snsf facebook"><a href="//www.facebook.com/<?php echo isset($instance['facebook_id']) ? rawurlencode( rawurldecode( $instance['facebook_id'] ) ) : ''; ?>" target="blank" title="Facebook" rel="nofollow" itemprop="sameAs url">&nbsp;<i class="fa fa-facebook"></i>&nbsp;<?php if( $instance['icon'] ) echo '<span class="fname">Facebook</span>'; ?></a></span></li>
<?php
	endif;
	if( isset( $instance['hatena'] ) ):
?><li><span class="snsf hatena"><a href="//b.hatena.ne.jp/<?php echo isset($instance['hatena_id']) ? rawurlencode( rawurldecode( $instance['hatena_id'] ) ) : ''; ?>" target="blank" title="<?php echo __( 'Hatena Bookmark', 'wpthk' ); ?>" rel="nofollow" itemprop="sameAs url">B!<?php if( $instance['icon'] ) echo '<span class="fname">Hatena</span>'; ?></a></span></li>
<?php
	endif;
	if( isset( $instance['google'] ) ):
?><li><span class="snsf google"><a href="//plus.google.com/<?php echo isset($instance['google_id']) ? rawurlencode( rawurldecode( $instance['google_id'] ) ) : ''; ?>" target="blank" title="Google+" rel="nofollow" itemprop="sameAs url">&nbsp;<i class="fa fa-google-plus"></i>&nbsp;<?php if( $instance['icon'] ) echo '<span class="fname">Google+</span>'; ?></a></span></li>
<?php
	endif;
	if( isset( $instance['youtube'] ) ):
?><li><span class="snsf youtube"><a href="//www.youtube.com/<?php echo isset($instance['youtube_id']) ? rawurlencode( rawurldecode( $instance['youtube_id'] ) ) : ''; ?>" target="blank" title="YouTube" rel="nofollow" itemprop="sameAs url">&nbsp;<i class="fa fa-youtube"></i>&nbsp;<?php if( $instance['icon'] ) echo '<span class="fname">YouTube</span>'; ?></a></span></li>
<?php
	endif;
	if( isset( $instance['line'] ) ):
?><li><span class="snsf line"><a href="//line.naver.jp/ti/p/<?php echo isset($instance['line_id']) ? rawurlencode( rawurldecode( $instance['line_id'] ) ) : ''; ?>" target="blank" title="LINE" rel="nofollow" itemprop="sameAs url">&nbsp;<i class="fa ico-line"></i>&nbsp;<?php if( $instance['icon'] ) echo '<span class="fname">LINE</span>'; ?></a></span></li>
<?php
	endif;
	if( isset( $instance['rss'] ) ):
?><li><span class="snsf rss"><a href="<?php echo get_bloginfo('rss2_url'); ?>" target="_blank" title="RSS" rel="nofollow" itemprop="sameAs url">&nbsp;<i class="fa fa-rss"></i>&nbsp;<?php if( $instance['icon'] ) echo '<span class="fname">RSS</span>'; ?></a></span></li>
<?php
	endif;
	if( isset( $instance['feedly'] ) ):
?><li><span class="snsf feedly"><a href="//feedly.com/index.html#subscription/feed/<?php echo rawurlencode( get_bloginfo('rss2_url') ); ?>" target="blank" title="Feedly" rel="nofollow" itemprop="sameAs url">&nbsp;<i class="ico-feedly"></i>&nbsp;<?php if( $instance['icon'] ) echo '<span class="fname">Feedly</span>'; ?></a></span></li>
<?php
	endif;
?></ul>
<div class="clearfix"></div>
</div>
<?php
		}
		echo $args['after_widget'];
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['icon'] = $new_instance['icon'];

		$instance['twitter'] = $new_instance['twitter'];
		$instance['facebook'] = $new_instance['facebook'];
		$instance['hatena'] = $new_instance['hatena'];
		$instance['google'] = $new_instance['google'];
		$instance['youtube'] = $new_instance['youtube'];
		$instance['line'] = $new_instance['line'];
		$instance['rss'] = $new_instance['rss'];
		$instance['feedly'] = $new_instance['feedly'];

		$instance['twitter_id'] = $new_instance['twitter_id'];
		$instance['facebook_id'] = $new_instance['facebook_id'];
		$instance['hatena_id'] = $new_instance['hatena_id'];
		$instance['google_id'] = $new_instance['google_id'];
		$instance['youtube_id'] = $new_instance['youtube_id'];
		$instance['line_id'] = $new_instance['line_id'];

		return $instance;
	}

	public function form( $instance ) {
		$title  = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : __( 'Follow me!', 'wpthk' );
		$twitter_id  = isset( $instance['twitter_id'] ) ? esc_attr( $instance['twitter_id'] ) : '';
		$facebook_id  = isset( $instance['facebook_id'] ) ? esc_attr( $instance['facebook_id'] ) : '';
		$hatena_id  = isset( $instance['hatena_id'] ) ? esc_attr( $instance['hatena_id'] ) : '';
		$google_id  = isset( $instance['google_id'] ) ? esc_attr( $instance['google_id'] ) : '';
		$youtube_id  = isset( $instance['youtube_id'] ) ? esc_attr( $instance['youtube_id'] ) : '';
		$line_id  = isset( $instance['line_id'] ) ? esc_attr( $instance['line_id'] ) : '';
?>
<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php echo __( 'Title:', 'wpthk' ); ?></label>
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

<p><?php echo __( 'Button appearance:', 'wpthk' ); ?><br />
<input name="<?php echo $this->get_field_name( 'icon' ); ?>" type="radio" value="0" <?php echo !isset( $instance['icon'] ) || !$instance['icon'] ? 'checked="checked"' : ''; ?> />
<label for="<?php echo $this->get_field_id( 'icon' ); ?>"><?php echo __( 'Icon only', 'wpthk' ); ?></label><br />
<input name="<?php echo $this->get_field_name( 'icon' ); ?>" type="radio" value="1"  <?php echo isset( $instance['icon'] ) && $instance['icon'] ? 'checked="checked"' : ''; ?> />
<label for="<?php echo $this->get_field_id( 'icon' ); ?>"><?php echo __( 'Icon + SNS name', 'wpthk' ); ?></label>
</p>

<p><input class="checkbox" id="<?php echo $this->get_field_id( 'twitter' ); ?>" name="<?php echo $this->get_field_name( 'twitter' ); ?>" type="checkbox" value="1" <?php checked( isset( $instance['twitter'] ) ? $instance['twitter'] : 0 ); ?> />
<label for="<?php echo $this->get_field_id( 'twitter' ); ?>"><?php echo __( 'Twitter', 'wpthk' ); ?></label></p>
<p style="background:#f5f5f5;padding: 3% 5%">Twitter ID ( http://twitter.com/XXXXX ):<br />
<input class="widefat" style="width:90%" id="<?php echo $this->get_field_id( 'twitter_id' ); ?>" name="<?php echo $this->get_field_name( 'twitter_id' ); ?>" type="text" value="<?php echo $twitter_id; ?>" /></p>

<p><input class="checkbox" id="<?php echo $this->get_field_id( 'facebook' ); ?>" name="<?php echo $this->get_field_name( 'facebook' ); ?>" type="checkbox" value="1" <?php checked( isset( $instance['facebook'] ) ? $instance['facebook'] : 0 ); ?> />
<label for="<?php echo $this->get_field_id( 'facebook' ); ?>"><?php echo __( 'Facebook', 'wpthk' ); ?></label></p>
<p style="background:#f5f5f5;padding: 3% 5%">Facebook ID ( http://www.facebook.com/XXXXX ):<br />
<input class="widefat" style="width:90%" id="<?php echo $this->get_field_id( 'facebook_id' ); ?>" name="<?php echo $this->get_field_name( 'facebook_id' ); ?>" type="text" value="<?php echo $facebook_id; ?>" /></p>

<p><input class="checkbox" id="<?php echo $this->get_field_id( 'hatena' ); ?>" name="<?php echo $this->get_field_name( 'hatena' ); ?>" type="checkbox" value="1" <?php checked( isset( $instance['hatena'] ) ? $instance['hatena'] : 0 ); ?> />
<label for="<?php echo $this->get_field_id( 'hatena' ); ?>"><?php echo __( 'Hatena Bookmark ', 'wpthk' ); ?></label></p>
<p style="background:#f5f5f5;padding: 3% 5%">Hatena ID ( http://b.hatena.ne.jp/XXXXX ):<br />
<input class="widefat" style="width:90%" id="<?php echo $this->get_field_id( 'hatena_id' ); ?>" name="<?php echo $this->get_field_name( 'hatena_id' ); ?>" type="text" value="<?php echo $hatena_id; ?>" /></p>

<p><input class="checkbox" id="<?php echo $this->get_field_id( 'google' ); ?>" name="<?php echo $this->get_field_name( 'google' ); ?>" type="checkbox" value="1" <?php checked( isset( $instance['google'] ) ? $instance['google'] : 0 ); ?> />
<label for="<?php echo $this->get_field_id( 'google' ); ?>"><?php echo __( 'Google+', 'wpthk' ); ?></label></p>
<p style="background:#f5f5f5;padding: 3% 5%">Google+ ID ( http://plus.google.com/XXXXX ):<br />
<input class="widefat" style="width:90%" id="<?php echo $this->get_field_id( 'google_id' ); ?>" name="<?php echo $this->get_field_name( 'google_id' ); ?>" type="text" value="<?php echo $google_id; ?>" /></p>

<p><input class="checkbox" id="<?php echo $this->get_field_id( 'youtube' ); ?>" name="<?php echo $this->get_field_name( 'youtube' ); ?>" type="checkbox" value="1" <?php checked( isset( $instance['youtube'] ) ? $instance['youtube'] : 0 ); ?> />
<label for="<?php echo $this->get_field_id( 'youtube' ); ?>"><?php echo __( 'YouTube', 'wpthk' ); ?></label></p>
<p style="background:#f5f5f5;padding: 3% 5%">YouTube ID ( http://www.youtube.com/XXXXX ):<br />
<input class="widefat" style="width:90%" id="<?php echo $this->get_field_id( 'youtube_id' ); ?>" name="<?php echo $this->get_field_name( 'youtube_id' ); ?>" type="text" value="<?php echo $youtube_id; ?>" /></p>

<p><input class="checkbox" id="<?php echo $this->get_field_id( 'line' ); ?>" name="<?php echo $this->get_field_name( 'line' ); ?>" type="checkbox" value="1" <?php checked( isset( $instance['line'] ) ? $instance['line'] : 0 ); ?> />
<label for="<?php echo $this->get_field_id( 'line' ); ?>"><?php echo __( 'LINE', 'wpthk' ); ?></label></p>
<p style="background:#f5f5f5;padding: 3% 5%">LINE ID ( http://line.naver.jp/ti/p/XXXXX ):<br />
<input class="widefat" style="width:90%" id="<?php echo $this->get_field_id( 'line_id' ); ?>" name="<?php echo $this->get_field_name( 'line_id' ); ?>" type="text" value="<?php echo $line_id; ?>" /></p>

<p><input class="checkbox" id="<?php echo $this->get_field_id( 'rss' ); ?>" name="<?php echo $this->get_field_name( 'rss' ); ?>" type="checkbox" value="1" <?php checked( isset( $instance['rss'] ) ? $instance['rss'] : 0 ); ?> />
<label for="<?php echo $this->get_field_id( 'rss' ); ?>"><?php echo __( 'RSS ', 'wpthk' ); ?></label></p>

<p><input class="checkbox" id="<?php echo $this->get_field_id( 'feedly' ); ?>" name="<?php echo $this->get_field_name( 'feedly' ); ?>" type="checkbox" value="1" <?php checked( isset( $instance['feedly'] ) ? $instance['feedly'] : 0 ); ?> />
<label for="<?php echo $this->get_field_id( 'feedly' ); ?>"><?php echo __( 'Feedly ', 'wpthk' ); ?></label></p>
<?php
	}
}
endif;

//---------------------------------------------------------------------------
// RSS / Feedly 購読ボタン
//---------------------------------------------------------------------------
if( class_exists( 'thk_rss_feedly' ) === false ):
class thk_rss_feedly extends WP_Widget {
	function __construct() {
		$widget_ops = array( 'classname' => 'thk_rss_feedly', 'description' => __( 'RSS / Feedly Button (by WpTHK)', 'wpthk' ) );
		parent::__construct( 'thk_rss_feedly', __( 'RSS / Feedly Button (by WpTHK) ', 'wpthk' ), $widget_ops );
	}

	function widget( $args, $instance ) {
		$title = ( !empty( $instance['title'] ) ) ? $instance['title'] : '';
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		echo $args['before_widget'];
		if( !empty( $title ) ) echo $args['before_title'], $title, $args['after_title'];
?>
<div id="thk-rss-feedly">
<ul>
<li><a href="<?php echo get_bloginfo('rss2_url'); ?>" class="icon-rss-button" target="_blank" title="RSS" rel="nofollow"><i class="fa fa-rss"></i><span>RSS</span></a></li>
<li><a href="//feedly.com/index.html#subscription/feed/<?php echo rawurlencode( get_bloginfo('rss2_url') ); ?>" class="icon-feedly-button" target="blank" title="feedly" rel="nofollow"><i class="ico-feedly"></i><span>Feedly</span></a></li>
</ul>
<div class="clearfix"></div>
</div>
<?php
		echo $args['after_widget'];
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );

		return $instance;
	}

	public function form( $instance ) {
		$title  = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : __( 'Subscribe to this blog', 'wpthk' );
?>
<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php echo __( 'Title:', 'wpthk' ); ?></label>
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>
<?php
	}
}
endif;

//---------------------------------------------------------------------------
// カテゴリ・アーカイブウィジェットのaタグをリストの内側にする
//---------------------------------------------------------------------------
if( function_exists('thk_list_categories_archives') === false ):
function thk_list_categories_archives( $out ) {
	$out = str_replace( '&nbsp;', ' ', $out );
	$out = str_replace( "\t", '', $out);
	$out = str_replace( "'", '"', $out );
	//$out = preg_replace( '/>\s*?<\//', '></', $out );
	//$out = str_replace( "\n</li>", "</li>\n", $out );
	$out = preg_replace( '/<\/a> \(([0-9]*)\)/', ' (${1})</a>', $out );
	return $out;
}
endif;

//---------------------------------------------------------------------------
// widgets init
//---------------------------------------------------------------------------
add_action( 'widgets_init', function() {
	// recentcomments のインライン消す (style.css に一応書いとく)
	global $wp_widget_factory;
	remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );

	register_sidebars( 1, array(
		'id'		=> 'side-h3',
		'name'		=> __( 'General-purpose sidebar', 'wpthk' ) . ' (' . sprintf( __( 'title %s type', 'wpthk' ), 'H3' ) . ')',
		'description'	=> sprintf( __( 'If you want the title to have %s tag.', 'wpthk' ), 'h3' ),
		'before_widget'	=> '<div id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<h3 class="side-title">',
		'after_title'	=> '</h3>'
	));
	register_sidebars( 1, array(
		'id'		=> 'side-h4',
		'name'		=> __( 'General-purpose sidebar', 'wpthk' ) . ' (' . sprintf( __( 'title %s type', 'wpthk' ), 'H4' ) . ')',
		'description'	=> sprintf( __( 'If you want the title to have %s tag.', 'wpthk' ), 'h4' ),
		'before_widget'	=> '<div id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<h4 class="side-title">',
		'after_title'	=> '</h4>'
	));
	register_sidebars( 1, array(
		'id'		=> 'side-top-h3',
		'name'		=> __( 'Front page sidebar', 'wpthk' ) . ' (' . sprintf( __( 'title %s type', 'wpthk' ), 'H3' ) . ')',
		'description'	=> __( 'Front page dedicated.', 'wpthk' ) . sprintf( __( 'If you want the title to have %s tag.', 'wpthk' ), 'h3' ),
		'before_widget'	=> '<div id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<h3 class="side-title">',
		'after_title'	=> '</h3>'
	));
	register_sidebars( 1, array(
		'id'		=> 'side-top-h4',
		'name'		=> __( 'Front page sidebar', 'wpthk' ) . ' (' . sprintf( __( 'title %s type', 'wpthk' ), 'H4' ) . ')',
		'description'	=> __( 'Front page dedicated.', 'wpthk' ) . sprintf( __( 'If you want the title to have %s tag.', 'wpthk' ), 'h4' ),
		'before_widget'	=> '<div id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<h4 class="side-title">',
		'after_title'	=> '</h4>'
	));
	register_sidebars( 1, array(
		'id'		=> 'side-no-top-h3',
		'name'		=> __( 'Other than the front page sidebar', 'wpthk' ) . ' (' . sprintf( __( 'title %s type', 'wpthk' ), 'H3' ) . ')',
		'description'	=> __( 'Pages other than the front page.', 'wpthk' ) . sprintf( __( 'If you want the title to have %s tag.', 'wpthk' ), 'h3' ),
		'before_widget'	=> '<div id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<h3 class="side-title">',
		'after_title'	=> '</h3>'
	));
	register_sidebars( 1, array(
		'id'		=> 'side-no-top-h4',
		'name'		=> __( 'Other than the front page sidebar', 'wpthk' ) . ' (' . sprintf( __( 'title %s type', 'wpthk' ), 'H4' ) . ')',
		'description'	=> __( 'Pages other than the front page.', 'wpthk' ) . sprintf( __( 'If you want the title to have %s tag.', 'wpthk' ), 'h4' ),
		'before_widget'	=> '<div id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<h4 class="side-title">',
		'after_title'	=> '</h4>'
	));
	register_sidebars( 1, array(
		'id'		=> 'side-scroll',
		'name'		=> __( 'Scroll follow sidebar (H4 type)', 'wpthk' ),
		'description'	=> __( 'Widget to follow the scroll. The title is only h4.', 'wpthk' ),
		'before_widget'	=> '<div id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<h4 class="side-title">',
		'after_title'	=> '</h4>'
	));

	register_sidebars( 1, array(
		'id'		=> 'head-under',
		'name'		=> __( 'Under Header Widget', 'wpthk' ),
		'description'	=> __( 'Place the widget just below the header.', 'wpthk' ),
		'before_widget'	=> '<div id="%1$s" class="widget head-under %2$s">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<p class="head-under-title">',
		'after_title'	=> '</p>'
	));

	register_sidebars( 1, array(
		'id'		=> 'post-title-upper',
		'name'		=> __( 'Above Post Title Widget', 'wpthk' ),
		'description'	=> __( 'Place the widget above the post title.', 'wpthk' ),
		'before_widget'	=> '<div id="%1$s" class="widget post-title-upper %2$s">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<p class="post-title-upper-title">',
		'after_title'	=> '</p>'
	));

	register_sidebars( 1, array(
		'id'		=> 'post-title-under',
		'name'		=> __( 'Under Post Title Widget', 'wpthk' ),
		'description'	=> __( 'Place the widget under the post title.', 'wpthk' ),
		'before_widget'	=> '<div id="%1$s" class="widget post-title-under %2$s">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<p class="post-title-under-title">',
		'after_title'	=> '</p>'
	));

	register_sidebars( 1, array(
		'id'		=> 'footer-left',
		'name'		=> __( 'Footer left', 'wpthk' ) . ' (' . __( 'Title H4', 'wpthk' ) . ')',
		'description'	=> __( 'When the sidebar is offscreen, it will be not shown.', 'wpthk' ),
		'before_widget'	=> '<div id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<h4 class="footer-left-title">',
		'after_title'	=> '</h4>'
	));

	register_sidebars( 1, array(
		'id'		=> 'footer-center',
		'name'		=> __( 'Footer center', 'wpthk' ) . ' (' . __( 'Title H4', 'wpthk' ) . ')',
		'description'	=> __( 'When the sidebar is offscreen, it will be not shown.', 'wpthk' ),
		'before_widget'	=> '<div id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<h4 class="footer-center-title">',
		'after_title'	=> '</h4>'
	));

	register_sidebars( 1, array(
		'id'		=> 'footer-right',
		'name'		=> __( 'Footer right', 'wpthk' ) . ' (' . __( 'Title H4', 'wpthk' ) . ')',
		'description'	=> __( 'When the sidebar is offscreen, it will be not shown.', 'wpthk' ),
		'before_widget'	=> '<div id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<h4 class="footer-right-title">',
		'after_title'	=> '</h4>'
	));

	register_sidebars( 1, array(
		'id'		=> 'adsense-1',
		'name'		=> __( 'Single Advertisement Beneath Post', 'wpthk' ),
		'description'	=> __( 'Place single Adsense under each post.  Image, text, anything is fine.', 'wpthk' ),
		'before_widget'	=> '',
		'after_widget'	=> ''
	));
	register_sidebars( 1, array(
		'id'		=> 'adsense-2',
		'name'		=> __( 'Dual Advertisement Beneath Post (responsive ad)', 'wpthk' ),
		'description'	=> __( 'Will place 2 Adsense in the size of 336px.  For smartphones, in accordance to contents policy it will have one of the placement as &quot;display:none&quot;.  Google requires to have this responsive ad based on their policy.', 'wpthk' ),
		'before_widget'	=> '',
		'after_widget'	=> ''
	));
	register_sidebars( 1, array(
		'id'		=> 'adsense-3',
		'name'		=> __( 'Dual Advertisement Beneath Post (vertical responsive ad)', 'wpthk' ),
		'description'	=> __( 'Will vertically place 2 Adsense in the size of 336px.  For smartphones, in accordance to contents policy it will have one of the placement as &quot;display:none&quot;.  Google requires to have this responsive ad based on their policy.', 'wpthk' ),
		'before_widget'	=> '',
		'after_widget'	=> ''
	));

	register_sidebars( 1, array(
		'id'		=> 'col3-h3',
		'name'		=> __( '3 Column Left Sidebar', 'wpthk' ) . ' (' . sprintf( __( 'title %s type', 'wpthk' ), 'H3' ) . ')',
		'description'	=> sprintf( __( 'If you want the title to have %s tag.', 'wpthk' ), 'h3' ),
		'before_widget'	=> '<div id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<h3 class="side-title">',
		'after_title'	=> '</h3>'
	));
	register_sidebars( 1, array(
		'id'		=> 'col3-h4',
		'name'		=> __( '3 Column Left Sidebar', 'wpthk' ) . ' (' . sprintf( __( 'title %s type', 'wpthk' ), 'H4' ) . ')',
		'description'	=> sprintf( __( 'If you want the title to have %s tag.', 'wpthk' ), 'h4' ),
		'before_widget'	=> '<div id="%1$s" class="widget %2$s">',
		'after_widget'	=> '</div>',
		'before_title'	=> '<h4 class="side-title">',
		'after_title'	=> '</h4>'
	));

	register_widget( 'thk_qr_code' );
	register_widget( 'thk_recent_comments' );
	register_widget( 'thk_recent_posts' );
	register_widget( 'thk_follow_button' );
	register_widget( 'thk_rss_feedly' );
} );
