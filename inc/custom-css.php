<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

/*---------------------------------------------------------------------------
 * カスタマイズした内容の CSS
 *---------------------------------------------------------------------------*/
function thk_custom_css() {
	global $wpthk;

	require_once( INC . 'const.php' );
	require_once( INC . 'colors.php' );

	$conf = new defConfig();
	$colors_class = new thk_colors();

	$defaults = $conf->default_custom_variables();
	$default_cont_bg_color = $conf->over_all_default_contents_background_color();
	unset( $conf );

	$ret = '/*! wpthk customizer css */' . "\n";

	$style = array(
		'all'		=> null,
		'max_540'	=> null,
		'min_541'	=> null,
		'max_991'	=> null,
		'min_992'	=> null,
		'max_1199'	=> null,
		'min_1200'	=> null,
		'992_1199'	=> null,
		'541_991'	=> null,
	);

	/*---------------------------------------------------------------------------
	 * コンテナの最大幅
	 *---------------------------------------------------------------------------*/
	if( $wpthk['container_max_width'] !== $defaults['container_max_width'] ) {
		$mw1 = ( isset( $wpthk['container_max_width'] ) && $wpthk['container_max_width'] === 0 ) ? '1200px' :  $wpthk['container_max_width'] . 'px';
		$mw2 = ( isset( $wpthk['container_max_width'] ) && $wpthk['container_max_width'] === 0 ) ? '100%' :  $wpthk['container_max_width'] - 30 . 'px';

		$style['all'] .= '@media (min-width:' . $mw1 . '){';
		$style['all'] .= '.container{width:' . $mw2 . ';padding-left:0;padding-right:0;}';
		$style['all'] .= '.logo,#header .head-cover,#head-band-in,div[id*="head-band"] .band-menu{max-width:' . $mw2 . '!important;}';
		$style['all'] .= '#header #gnavi,#foot-in{max-width:' . $mw1 . ';}';
		if( isset( $wpthk['container_max_width'] ) && $wpthk['container_max_width'] === 0 ) {
			$style['all'] .= '.container,.logo{padding-left:15px;padding-right:15px;}';
			$style['all'] .= '#head-band-in,#foot-in{margin-left:15px;margin-right:15px;}';
			$style['all'] .= '#header #gnavi,div[id*="head-band"] .band-menu,#foot-in{max-width:100%!important;}';
		}
		$style['all'] .= '}';

		if( $wpthk['container_max_width'] < 992) {
			$style['all'] .= '@media (min-width:' . $mw1 . ') and (max-width:992px){';
			$style['all'] .= '#head-band-in,div[id*="head-band"] .band-menu{max-width:' . $mw2 . '!important;}';
			$style['all'] .= '}';
		}

	}

	/*---------------------------------------------------------------------------
	 * タイトルの配置
	 *---------------------------------------------------------------------------*/
	if( $wpthk['title_position'] === 'center' ) {
		$style['all'] .= '.info{text-align:center;}';
		$style['all'] .= '.sitename{margin:0 auto 12px auto;}';
	}
	elseif( $wpthk['title_position'] === 'right' ) {
		$style['all'] .= '.info{text-align:right;}';
		$style['all'] .= '.sitename{margin:0 0 12px auto;}';
	}

	/*---------------------------------------------------------------------------
	 * ヘッダー上の帯状メニューが常に横幅いっぱいの場合
	 *---------------------------------------------------------------------------*/
	if( isset( $wpthk['head_band_wide'] ) ) {
		$style['all'] .= '.band{width:100%;padding-left:0;padding-right:0;}';
	}

	/*---------------------------------------------------------------------------
	 * パンくずリンクの配置
	 *---------------------------------------------------------------------------*/
	if( $wpthk['breadcrumb_view'] === 'outer' ) {
		if( isset( $wpthk['logo_img'] ) && !isset( $wpthk['logo_img_up'] ) ) {
			$style['all'] .= '.logo,.logo-up{margin-bottom:0;}';	// ロゴ画像があった場合の位置調整
		}
	}
	else {
		$style['all'] .= '#breadcrumb{margin: 10px 0 30px 0;}';
		$style['all'] .= '#primary{margin-top:25px;}';
	}

	/*---------------------------------------------------------------------------
	 * グローバルナビ
	 *---------------------------------------------------------------------------*/
	/* グローバルナビ固定 ＆ 半透明化 */
	if( isset( $wpthk['global_navi_sticky'] ) && isset( $wpthk['global_navi_translucent'] ) ) {
		$style['all'] .= '#nav.is_stuck{opacity: 0.9;}';
	}

	// グローバルナビ固定 ＆ 帯メニュー固定 ＆ 検索ボックスが表示されてる時は、540px 以下で高さ +28px
	// ・・・を Javascript 側で監視するためのスタイルを挿入
	if( isset( $wpthk['global_navi_sticky'] ) && isset( $wpthk['head_band_visible'] ) && isset( $wpthk['head_band_fixed'] ) && isset( $wpthk['head_band_search'] ) ) {
		$style['max_540'] .= '#head-band{max-width: 32767px;}';
	}

	/* グローバルナビに区切り線を追加 */
	if( isset( $wpthk['global_navi_add_sep'] ) ) {
		// 自動リサイズ same の時だけ display: table なので、margin の調整の仕方が異なる
		if( $wpthk['global_navi_auto_resize'] === 'same' ) {
			$style['min_992'] .= '#gnavi div > ul, #gnavi div > ul > li{border-left:1px solid #ddd;border-right:1px solid #ddd;}';
			$style['min_992'] .= '#gnavi div > ul > li:first-child{border-left:none;}';
			$style['min_992'] .= '#gnavi div > ul > li:last-child{border-right:none;}';
			// ヘッダーに枠線がある時は、両脇の border 消す
			if( isset( $wpthk['header_border'] ) ) {
				$style['min_992'] .= '#gnavi div > ul{border-left:none;border-right:none;}';
			}
		}
		else {
			// ヘッダーに枠線がなければ、両端に border;
			if( !isset( $wpthk['header_border'] ) ) {
				$style['min_992'] .= '#gnavi div > ul{border-left:1px solid #ddd;}';
			}
			// ヘッダーに枠線があるときはナビの両端を1px内側に;
			else {
				$style['min_992'] .= '#gnavi div > ul{border-left:1px solid #ddd;border-right:1px solid #ddd;}';
			}
			$style['min_992'] .= '#gnavi ul{border-left:1px solid #ddd;border-right:1px solid #ddd;}';
			$style['min_992'] .= '#gnavi div > ul > li{margin-right:-1px;border-left:1px solid #ddd;border-right:1px solid #ddd;}';
		}
	}

	/* グローバルナビ自動リサイズ */
	if( $wpthk['global_navi_auto_resize'] === 'full' ) {
		$style['min_992'] .= <<<GNAVI_AUTO
#gnavi li {
	-moz-box-flex: 1 0 auto;
	-webkit-flex: 1 0 auto;
	-ms-flex: 1 0 auto;
	flex: 1 0 auto;
}
GNAVI_AUTO;
	}
	/* グローバルナビ全幅同じ */
	elseif( $wpthk['global_navi_auto_resize'] === 'same' ) {
		$style['min_992'] .= <<<GNAVI_AUTO
#gnavi ul {
	display: table;
	table-layout: fixed;
	width: 100%;
	border-collapse: collapse;
}
#gnavi li {
	display: table-cell;
	float: none;
	width: 100%;
}
#gnavi ul ul {
	table-layout: auto;
}
#gnavi li li {
	display: table-row;
}
GNAVI_AUTO;
	}

	/* グローバルナビを上部配置 ＆ 帯メニューを表示してる場合の位置調整 */
	if( $wpthk['global_navi_position'] !== $defaults['global_navi_position'] ) {
		if( isset( $wpthk['head_band_visible'] ) ) {
			$top = $wpthk['head_band_height'] + $wpthk['head_band_border_bottom_width'];
			$style['all'] .= '#nav{top:' . $top . 'px;}';
		}
	}

	/*---------------------------------------------------------------------------
	 * ヘッダーのトップマージン
	 *---------------------------------------------------------------------------*/
	if(
		$wpthk['head_margin_top'] !== $defaults['head_margin_top'] ||
		isset( $wpthk['header_border'] ) ||
		!isset( $wpthk['head_band_visible'] ) ||
		( isset( $wpthk['head_band_visible'] ) && $wpthk['head_band_height'] !== $defaults['head_band_height'] )
	) {
		$head_margin_top = 0;
		$head_margin_top += $wpthk['head_margin_top'];
		if( isset( $wpthk['head_band_visible'] ) ) {
			// 帯状メニューの高さ分のマージン追加
			$head_margin_top += $wpthk['head_band_height'] + $wpthk['head_band_border_bottom_width'];
		}
		if( isset( $wpthk['header_border'] ) ) {
			$head_margin_top -= 1;	// ヘッダーに枠線がある場合は -1px
		}
		$style['all'] .= '#head-in{margin-top:' . $head_margin_top . 'px;}';
	}

	// 帯メニューに検索ボックスつけてる場合の小デバイスのトップマージン調整
	if( isset( $wpthk['head_band_search'] ) ) {
		$band_height = $wpthk['head_band_height'] + $wpthk['head_band_border_bottom_width'] + 28 + 3;
		$head_margin = $wpthk['head_margin_top'] + $band_height;

		$style['max_540'] .= '#head-in {';
		$style['max_540'] .= 'margin-top:' . $head_margin . 'px;';
		$style['max_540'] .= '}';

		if( $wpthk['global_navi_position'] !== $defaults['global_navi_position'] ) {
			$style['max_540'] .= '#nav {';
			$style['max_540'] .= 'top:' . $band_height . 'px;';
			$style['max_540'] .= '}';
		}
	}

	/*---------------------------------------------------------------------------
	 * ヘッダーのパディング
	 *---------------------------------------------------------------------------*/
	if(
		$wpthk['head_padding_top']    !== $defaults['head_padding_top']    ||
		$wpthk['head_padding_bottom'] !== $defaults['head_padding_bottom'] ||
		$wpthk['head_padding_left']   !== $defaults['head_padding_left']   ||
		$wpthk['head_padding_right']  !== $defaults['head_padding_right']
	) {
		$style['all'] .= '.info{padding:';
		$style['all'] .= $wpthk['head_padding_top']    . 'px ';
		$style['all'] .= $wpthk['head_padding_right']  . 'px ';
		$style['all'] .= $wpthk['head_padding_bottom'] . 'px ';
		$style['all'] .= $wpthk['head_padding_left']   . 'px';
		$style['all'] .= ';}';
	}

	/*---------------------------------------------------------------------------
	 * カラム操作関連のスタイルを読み込む
	 *---------------------------------------------------------------------------*/
	$column_create = $wpthk['column_style'];
	if( is_customize_preview() === false && $wpthk['column_default'] === false ) {
		$column_create = '1column';
	}
	$style = thk_adjust_column_css( $style, $column_create, $defaults, $default_cont_bg_color, $colors_class );

	/*---------------------------------------------------------------------------
	 * 文字色・背景色・背景画像・枠線
	 *---------------------------------------------------------------------------*/
	/* Body */
	if( isset( $wpthk['body_color'] ) || isset( $wpthk['body_bg_color'] ) || isset( $wpthk['body_bg_img'] ) || ( isset( $wpthk['body_transparent'] ) && $wpthk['body_transparent'] !== 100 ) ) {
		$style['all'] .= 'body{';

		/* 文字色 */
		if( isset( $wpthk['body_color'] ) ) {
			$style['all'] .= 'color:' . $wpthk['body_color'] . ';';
		}

		if( isset( $wpthk['body_bg_color'] ) || isset( $wpthk['body_bg_img'] ) || ( isset( $wpthk['body_transparent'] ) && $wpthk['body_transparent'] !== 0 ) ) {
			$style['all'] .= 'background:';
			/* 背景色 */
			if( isset( $wpthk['body_bg_color'] ) ) {
				$style['all'] .= $wpthk['body_bg_color'] . ';';
			}
			/* 背景画像 */
			if( isset( $wpthk['body_bg_img'] ) ) {
				if( isset( $wpthk['body_bg_color'] ) ) {
					$style['all'] = rtrim( $style['all'], ';' ) . ' ';
				}
				$style['all'] .= 'url("' . $wpthk['body_bg_img'] . '");';
			}

			/* 背景色透過 */
			if( !isset( $wpthk['body_bg_img'] ) && isset( $wpthk['body_transparent'] ) && $wpthk['body_transparent'] !== 100 ) {
				$bg_color = isset( $wpthk['body_bg_color'] ) ? $wpthk['body_bg_color'] : $default_cont_bg_color[$wpthk['overall_image']];

				$rgb = $colors_class->colorcode_2_rgb( $bg_color );
				$transparent = $wpthk['body_transparent'] !== $defaults['body_transparent'] ? $wpthk['body_transparent'] : $defaults['body_transparent'];
				$transparent = round( $transparent / 100, 2 );

				if( isset( $wpthk['body_bg_color'] ) ) {
					$style['all'] .= 'background: rgba(' . $rgb['r'] . ',' . $rgb['g'] . ',' . $rgb['b'] . ',' . $transparent . ');';
				}
				else {
					$style['all'] .= 'rgba(' . $rgb['r'] . ',' . $rgb['g'] . ',' . $rgb['b'] . ',' . $transparent . ');';
				}
			}

			if( $wpthk['body_img_repeat'] !== 'repeat' ) {
				$style['all'] .= 'background-repeat:no-repeat;';
			}

			if( $wpthk['body_img_size'] === 'contain' ) {
				$style['all'] .= 'background-size:contain;';
			}
			elseif( $wpthk['body_img_size'] === 'cover' ) {
				$style['all'] .= 'background-size:cover;';
			}
			elseif( $wpthk['body_img_size'] === 'adjust' ) {
				$style['all'] .= 'background-size:100% auto;';
			}
			elseif( $wpthk['body_img_size'] === 'adjust2' ) {
				$style['all'] .= 'background-size:auto 100%;';
			}
			elseif( $wpthk['body_img_size'] === 'adjust3' ) {
				$style['all'] .= 'background-size:100% 100%;';
			}

			if(
				( isset( $wpthk['body_img_vertical'] ) && $wpthk['body_img_vertical'] !== $defaults['body_img_vertical'] ) ||
				( isset( $wpthk['body_img_horizontal'] ) && $wpthk['body_img_horizontal'] !== $defaults['body_img_horizontal'] )
			) {
				$body_img_vertical   = isset( $wpthk['body_img_vertical'] ) ? $wpthk['body_img_vertical'] : $defaults['body_img_vertical'];
				$body_img_vertical   = $body_img_vertical === 'middle' ? 'center' : $body_img_vertical;
				$body_img_horizontal = isset( $wpthk['body_img_horizontal'] ) ? $wpthk['body_img_horizontal'] : $defaults['body_img_horizontal'];

				$style['all'] .= 'background-position: ' . $body_img_vertical . ' ' . $body_img_horizontal . ';';
			}

			if( isset( $wpthk['body_img_fixed'] ) ) {
				$style['all'] .= 'background-attachment: fixed;';
			}

		}
		$style['all'] .= '}';

		/* 背景画像透過 */ 
		if( isset( $wpthk['body_bg_img'] ) && isset( $wpthk['body_img_transparent'] ) && $wpthk['body_img_transparent'] !== 0 ) {
			$bg_color = isset( $wpthk['body_bg_color'] ) ? $wpthk['body_bg_color'] : $default_cont_bg_color[$wpthk['overall_image']];

			$rgb = $colors_class->colorcode_2_rgb( $bg_color );
			$transparent = $wpthk['body_img_transparent'] !== $defaults['body_img_transparent'] ? $wpthk['body_img_transparent'] : $defaults['body_img_transparent'];
			$transparent = round( $transparent / 100, 2 );

			$style['all'] .= <<<BODY_BEFORE
body:before {
	content: '';
	position: fixed;
	top: 0;
	height: 100%;
	width: 100%;
	z-index: -1;
	background: rgba( {$rgb['r']}, {$rgb['g']}, {$rgb['b']}, {$transparent} );
}
BODY_BEFORE;
		}
	}

	/* Link */
	if( isset( $wpthk['body_link_color'] ) ) {
		/* リンク色 */
		if( isset( $wpthk['body_link_color'] ) ) {
			$style['all'] .= 'body a{color:' . $wpthk['body_link_color'] . ';}';
		}

	}

	if( isset( $wpthk['body_hover_color'] ) ) {
		/* リンクホバー色 */
		if( isset( $wpthk['body_hover_color'] ) ) {
			$style['all'] .= 'body a:hover{color:' . $wpthk['body_hover_color'] . ';}';
		}

	}

	/* パンくず文字色 */
	if( isset( $wpthk['breadcrumb_color'] ) ) {
		/* リンクホバー色 */
		if( isset( $wpthk['breadcrumb_color'] ) ) {
			$style['all'] .= '#breadcrumb,#breadcrumb a,breadcrumb i{color:' . $wpthk['breadcrumb_color'] . ';}';
		}

	}

	/* ヘッダー枠線 */
	if( isset( $wpthk['header_border'] ) ) {
		if( $wpthk['bootstrap_header'] === 'out' && $wpthk['container_max_width'] === 0 ) {
		}
		else {
			if( $wpthk['bootstrap_header'] === 'out' && isset( $wpthk['header_border_wide'] ) ){
				$style['all'] .= '#head-in{';
			}
			elseif( $wpthk['bootstrap_header'] === 'out' ) {
				$style['all'] .= '.head-cover{';
			}
			else {
				if( !isset( $wpthk['head_band_wide'] ) ) {
					$style['all'] .= 'div[id*="head-band"]{border:1px solid #ddd;border-top:0;}';
				}
				$style['all'] .= '#head-in{';
			}
			$style['all'] .= 'border:1px solid #ddd;';
			if( $wpthk['bootstrap_header'] === 'out' && isset( $wpthk['header_border_wide'] ) ){
				$style['all'] .= 'border-left:0;border-right:0;';
			}
			if( isset( $wpthk['global_navi_visible'] ) && $wpthk['global_navi_position'] === 'under' ) {
				$style['all'] .= 'border-bottom:0;';
			}
			$style['all'] .= '}';
		}

		if( isset( $wpthk['head_border_color'] ) ) {
			$style['all'] .= '#head-in,.head-cover,div[id*="head-band"]{border-color:' . $wpthk['head_border_color'] . ';}';
		}
		//$style['all'] .= '#nav-bottom{border-left:1px solid transparent;border-right:1px solid transparent;}';
	}

	/* フッター枠線 */
	if(
		!isset( $wpthk['footer_border_top'] )	||
		isset( $wpthk['footer_border'] )	||
		isset( $wpthk['foot_border_color'] )
	) {
		$style['all'] .= '#footer{';
		if( !isset( $wpthk['footer_border_top'] ) ) {
			$style['all'] .= 'border-top:none;';
		}
		if( isset( $wpthk['footer_border'] ) ) {
			if( $wpthk['bootstrap_footer'] === 'out' ) {
				if( !isset( $wpthk['footer_border_top'] ) ) $style['all'] .= 'border-top:1px solid #ddd;';
			}
			else {
				if( $wpthk['bootstrap_footer'] !== 'out' ) {
					$style['all'] .= 'border-left:1px solid #ddd;border-right:1px solid #ddd;';
				}
			}
		}
		if( isset( $wpthk['foot_border_color'] ) ) {
			$style['all'] .= 'border-color:' . $wpthk['foot_border_color'] . ';';
		}
		$style['all'] .= '}';
	}

	/* ヘッダー文字色と背景色 */
	if( isset( $wpthk['head_color'] ) || isset( $wpthk['head_bg_color'] ) ) {
		$style['all'] .= '#head-in{';
		/* ヘッダー文字色 */
		if( isset( $wpthk['head_color'] ) ) {
			$style['all'] .= 'color:' . $wpthk['head_color'] . ';';
		}
		/* ヘッダー背景色 */
		if( isset( $wpthk['head_bg_color'] ) ) {
			$style['all'] .= 'background:' . $wpthk['head_bg_color'] . ';';

			if( isset( $wpthk['head_transparent'] ) && $wpthk['head_transparent'] !== 100 ) {
				$rgb = $colors_class->colorcode_2_rgb( $wpthk['head_bg_color'] );
				$transparent = $wpthk['head_transparent'] !== $defaults['head_transparent'] ? $wpthk['head_transparent'] : $defaults['head_transparent'];
				$transparent = round( $transparent / 100, 2 );

				$style['all'] .= 'background: rgba(' . $rgb['r'] . ',' . $rgb['g'] . ',' . $rgb['b'] . ',' . $transparent . ');';
			}
		}
		$style['all'] .= '}';
	}

	if( isset( $wpthk['head_link_color'] ) ) {
		/* ヘッダーリンク色 */
		if( isset( $wpthk['head_link_color'] ) ) {
			$style['all'] .= '#head-in .sitename a{color:' . $wpthk['head_link_color'] . ';}';
		}

	}

	if( isset( $wpthk['head_hover_color'] ) ) {
		/* ヘッダーリンクホバー色 */
		if( isset( $wpthk['head_hover_color'] ) ) {
			$style['all'] .= '#head-in .sitename a:hover{color:' . $wpthk['head_hover_color'] . ';}';
		}

	}

	/* ヘッダー背景画像*/
	if( isset( $wpthk['head_bg_img'] ) ) {
		if( isset( $wpthk['head_img_width_max'] ) ) {
			$style['all'] .= '#head-in{background-image:';
		}
		else {
			$style['all'] .= '.head-cover{background-image:';
		}
		$style['all'] .= 'url("' . $wpthk['head_bg_img'] . '");';

		if( $wpthk['head_img_repeat'] !== 'repeat' ) {
			$style['all'] .= 'background-repeat:no-repeat;';
		}

		if( $wpthk['head_img_size'] === 'contain' ) {
			$style['all'] .= 'background-size:contain;';
		}
		elseif( $wpthk['head_img_size'] === 'cover' ) {
			$style['all'] .= 'background-size:cover;';
		}
		elseif( $wpthk['head_img_size'] === 'adjust' ) {
			$style['all'] .= 'background-size:100% auto;';
		}
		elseif( $wpthk['head_img_size'] === 'adjust2' ) {
			$style['all'] .= 'background-size:auto 100%;';
		}
		elseif( $wpthk['head_img_size'] === 'adjust3' ) {
			$style['all'] .= 'background-size:100% 100%;';
		}

		if(
			( isset( $wpthk['head_img_vertical'] ) && $wpthk['head_img_vertical'] !== $defaults['head_img_vertical'] ) ||
			( isset( $wpthk['head_img_horizontal'] ) && $wpthk['head_img_horizontal'] !== $defaults['head_img_horizontal'] )
		) {
			$head_img_vertical   = isset( $wpthk['head_img_vertical'] ) ? $wpthk['head_img_vertical'] : $defaults['head_img_vertical'];
			$head_img_vertical   = $head_img_vertical === 'middle' ? 'center' : $head_img_vertical;
			$head_img_horizontal = isset( $wpthk['head_img_horizontal'] ) ? $wpthk['head_img_horizontal'] : $defaults['head_img_horizontal'];

			$style['all'] .= 'background-position: ' . $head_img_vertical . ' ' . $head_img_horizontal . ';';
		}

		if( isset( $wpthk['head_img_fixed'] ) ) {
			$style['all'] .= 'background-attachment: fixed;';
		}

		$style['all'] .= '}';
	}

	/*---------------------------------------------------------------------------
	 * サムネイルの大きさ
	 *---------------------------------------------------------------------------*/
	if( $wpthk['thumbnail_is_size'] !== 'generate' ) {	// WordPress のデフォルトのサムネイルを使う場合
		$style['all'] .= '#list .term img{';
		if( $wpthk['thumbnail_is_size'] === 'thumbnail' ) {
			$style['all'] .= 'width:100px;height:auto;';	// PC:150px Mobile:100px
		}
		elseif( $wpthk['thumbnail_is_size'] === 'medium' ) {
			$style['all'] .= 'width:150px;height:auto;';	// PC:300px Mobile:150px
		}
		else {
			$style['all'] .= 'width:auto;height:auto;';	// 大きいサムネイル
		}
		$style['all'] .= '}';

		if( $wpthk['thumbnail_is_size'] === 'large' || $wpthk['thumbnail_is_size'] === 'full' ) {	// 大きいサムネイルの時は小デバイスで float 解除
			$style['max_540'] .= '#list .term {float:none;}';
		}

		$style['min_541'] .= '#list .term img{width:auto;height:auto;}';	// PC の時は常に auto (サムネイルかどうかの判断は WordPress側に任せる)
	}
	else {	// カスタマイズ画面で入力した値でサムネイルを作成する場合
		if(
			( $wpthk['thumbnail_width'] > 0 && $wpthk['thumbnail_width'] !== $defaults['thumbnail_width'] ) ||
			( $wpthk['thumbnail_height'] > 0 && $wpthk['thumbnail_view_height'] !== $defaults['thumbnail_view_height'] )
		) {
			$style['all'] .= '.term img{';

			if( $wpthk['thumbnail_width'] > 0 && $wpthk['thumbnail_width'] !== $defaults['thumbnail_width'] ) {
				$small_thumb_w = (int)ceil( $wpthk['thumbnail_width'] * 0.666 );
				$style['all'] .= 'width:' . $small_thumb_w . 'px;';
			}
			if( $wpthk['thumbnail_height'] > 0 && $wpthk['thumbnail_view_height'] !== 'auto' ) {
				$small_thumb_h = (int)ceil( $wpthk['thumbnail_height'] * 0.666 );
				$style['all'] .= 'height:' . $small_thumb_h . 'px;';
			}
			$style['all'] .= '}';

			$style['min_541'] .= '#list .term img{';
			if( $wpthk['thumbnail_width'] > 0 && $wpthk['thumbnail_width'] !== $defaults['thumbnail_width'] ) {
				$style['min_541'] .= 'width:' . $wpthk['thumbnail_width'] . 'px;';
			}
			if( $wpthk['thumbnail_height'] > 0 && $wpthk['thumbnail_view_height'] !== 'auto' ) {
				$style['min_541'] .= 'height:' . $wpthk['thumbnail_height'] . 'px;';
			}
			else {
				$style['min_541'] .= 'height:' . 'auto;';
			}
			$style['min_541'] .= '}';
		}
	}

	/*---------------------------------------------------------------------------
	 * サムネイル画像に対するテキスト(抜粋)の配置
	 *---------------------------------------------------------------------------*/
	if( $wpthk['thumbnail_layout'] === 'flow' ) {
		$style['min_541'] .= '#list .excerpt{overflow:visible;}';
	}
	elseif( $wpthk['thumbnail_layout'] === 'under' ) {
		$style['all'] .= '#list .term img{float:none;margin-right:0}';
	}

	/*---------------------------------------------------------------------------
	 * グローバルナビ文字色・背景色・枠線
	 *---------------------------------------------------------------------------*/
	/* グローバルナビ文字色 */
	if( isset( $wpthk['gnavi_color'] ) ) {
		$style['all'] .= '#gnavi li a,';
		$style['all'] .= '.mobile-nav {';
		$style['all'] .= 'color:' . $wpthk['gnavi_color'] . ';';
		$style['all'] .= '}';
	}

	/* グローバルナビバー背景色 */
	if( isset( $wpthk['gnavi_bar_bg_color'] ) ) {
		$style['all'] .= '#nav{';
		$style['all'] .= 'background:' . $wpthk['gnavi_bar_bg_color'] . ';';
		$style['all'] .= '}';
	}

	/* グローバルナビ背景色 */
	if( isset( $wpthk['gnavi_bg_color'] ) ) {
		$style['all'] .= '#gnavi li a, .mobile-nav {';
		$style['all'] .= 'background:' . $wpthk['gnavi_bg_color'] . ';';
		$style['all'] .= '}';

		if( isset( $wpthk['gnavi_bg_hover_color'] ) ) {
			$style['min_992'] .= '#gnavi li {';
			$style['min_992'] .= 'background:' . $wpthk['gnavi_bg_hover_color'] . ';';
			$style['min_992'] .= '}';
		}
	}

	/* グローバルナビ背景ホバー色 */
	if( isset( $wpthk['gnavi_bg_hover_color'] ) ) {
		$style['all'] .= '#gnavi li a:hover,';
		$style['all'] .= 'div.mobile-nav:hover, ul.mobile-nav li:hover{';
		$style['all'] .= 'background:' . $wpthk['gnavi_bg_hover_color'] . ';';
		$style['all'] .= '}';
	}

	/* グローバルナビ背景カレント色 */
	if( isset( $wpthk['gnavi_bg_current_color'] ) ) {
		$style['all'] .= '#gnavi .current-menu-item > a,';
		$style['all'] .= '#gnavi .current-menu-ancestor > a,';
		$style['all'] .= '#gnavi .current_page_item > a,';
		$style['all'] .= '#gnavi .current_page_ancestor > a{';
		$style['all'] .= 'background:' . $wpthk['gnavi_bg_current_color'] . ';';
		$style['all'] .= '}';
	}

	if(
		isset( $wpthk['gnavi_border_top_color'] ) || isset( $wpthk['gnavi_border_bottom_color'] ) ||
		$wpthk['gnavi_border_top_width'] !== $defaults['gnavi_border_top_width'] || $wpthk['gnavi_border_bottom_width'] !== $defaults['gnavi_border_bottom_width']
	) {
		$style['all'] .= '#nav{';

		/* グローバルナビ上の枠線色 */
		if( isset( $wpthk['gnavi_border_top_color'] ) ) {
			$style['all'] .= 'border-top-color:' . $wpthk['gnavi_border_top_color'] . ';';
		}

		/* グローバルナビ下の枠線色 */
		if( isset( $wpthk['gnavi_border_bottom_color'] ) ) {
			$style['all'] .= 'border-bottom-color:' . $wpthk['gnavi_border_bottom_color'] . ';';
		}

		/* グローバルナビ上の枠線太さ */
		if( $wpthk['gnavi_border_top_width'] !== $defaults['gnavi_border_top_width'] ) {
			$style['all'] .= 'border-top-width:' . $wpthk['gnavi_border_top_width'] . 'px;';
		}

		/* グローバルナビ下の枠線太さ */
		if( $wpthk['gnavi_border_bottom_width'] !== $defaults['gnavi_border_bottom_width'] ) {
			$style['all'] .= 'border-bottom-width:' . $wpthk['gnavi_border_bottom_width'] . 'px;';
		}

		$style['all'] .= '}';

		if( $wpthk['gnavi_border_bottom_width'] !== $defaults['gnavi_border_bottom_width'] ) {
			$style['min_992'] .= '#gnavi li ul{';
			$style['min_992'] .= 'border-top-width:' . $wpthk['gnavi_border_bottom_width'] . 'px;';
			$style['min_992'] .= '}';
		}
	}

	/* グローバルナビ区切り線の色 */
	if( isset( $wpthk['gnavi_separator_color'] ) ) {
		$style['min_992'] .= '#gnavi div > ul > li{border-color:' . $wpthk['gnavi_separator_color'] . ';}';
		$style['min_992'] .= '#gnavi li ul {border-left-color:' . $wpthk['gnavi_separator_color'] . ';border-right-color:' . $wpthk['gnavi_separator_color'] . ';}';
		$style['min_992'] .= '#gnavi div > ul > li, #gnavi li li a {border-bottom-color:' . $wpthk['gnavi_separator_color'] . ';}';
		$style['max_991'] .= '#gnavi li{border-top-color:' . $wpthk['gnavi_separator_color'] . ';}';
	}

	/* グローバルナビ上下のパディング */
	if( $wpthk['gnavi_top_buttom_padding'] !== $defaults['gnavi_top_buttom_padding'] ) {
		$style['min_992'] .= '#gnavi li a{';
		$style['min_992'] .= 'padding-top:' . $wpthk['gnavi_top_buttom_padding'] . 'px;';
		$style['min_992'] .= 'padding-bottom:' . $wpthk['gnavi_top_buttom_padding'] . 'px;';
		$style['min_992'] .= '}';
	}

	/*---------------------------------------------------------------------------
	 * 記事タイトル下メタ情報の位置調整
	 *---------------------------------------------------------------------------*/
	if(
		!isset( $wpthk['post_date_visible'] )		&&
		!isset( $wpthk['mod_date_visible'] )		&&
		!isset( $wpthk['category_meta_visible'] )	&&
		!isset( $wpthk['tag_meta_visible'] )		&&
		!isset( $wpthk['tax_meta_visible'] )
	) {
		$style['all'] .= '.post .entry-title, #front-page-title{margin-bottom:45px;}';
	}

	/*---------------------------------------------------------------------------
	 * 記事リストのタイトル下メタ情報の位置調整
	 *---------------------------------------------------------------------------*/
	if(
		!isset( $wpthk['list_post_date_visible'] )	&&
		!isset( $wpthk['list_mod_date_visible'] )	&&
		!isset( $wpthk['list_category_meta_visible'] )	&&
		!isset( $wpthk['list_tag_meta_visible'] )	&&
		!isset( $wpthk['list_tax_meta_visible'] )
	) {
		$style['all'] .= '#list .entry-title{margin-bottom:35px;}';
	}

	/*---------------------------------------------------------------------------
	 * 帯状メニュー
	 *---------------------------------------------------------------------------*/
	if( isset( $wpthk['head_band_visible'] ) ) {
		if(
			isset( $wpthk['head_band_fixed'] )		||
			isset( $wpthk['head_band_color'] )		||
			isset( $wpthk['head_band_hover_color'] )	||
			isset( $wpthk['head_band_bg_color'] )		||
			isset( $wpthk['head_band_border_bottom_color'] )||
			$wpthk['head_band_height']		!== $defaults['head_band_height'] ||
			$wpthk['head_band_border_bottom_width'] !== $defaults['head_band_border_bottom_width']
		) {
			/* 帯状メニュー固定 */
			if( isset( $wpthk['head_band_fixed'] ) ) {
				$style['all'] .= '.band{position:fixed;z-index:25;}';
			}

			$style['all'] .= 'div[id*="head-band"]{';
			/* 帯状メニューの高さ */
			if( $wpthk['head_band_height'] !== $defaults['head_band_height'] ) {
				$style['all'] .= 'height:' . $wpthk['head_band_height'] . 'px;';
				$style['all'] .= 'line-height:' . $wpthk['head_band_height'] . 'px;';

				// 検索ボックスつけてる場合の小デバイスの高さ調整
				if( isset( $wpthk['head_band_search'] ) ) {
					$height = $wpthk['head_band_height'] + 28 + 3;

					$style['max_540'] .= 'div[id*="head-band"]{';
					$style['max_540'] .= 'height:' . $height . 'px;';
					$style['max_540'] .= '}';
				}
			}

			/* 帯状メニュー背景色 */
			if( isset( $wpthk['head_band_bg_color'] ) ) {
				$style['all'] .= 'background:' . $wpthk['head_band_bg_color'] . ';';
			}
			/* 帯状メニュー下線の色 */
			if( isset( $wpthk['head_band_border_bottom_color'] ) ) {
				$style['all'] .= 'border-bottom-color:' . $wpthk['head_band_border_bottom_color'] . ';';
			}
			/* 帯状メニュー下線の太さ */
			if( $wpthk['head_band_border_bottom_width'] !== $defaults['head_band_border_bottom_width'] ) {
				$style['all'] .= 'border-bottom-width:' . $wpthk['head_band_border_bottom_width'] . 'px;';
			}
			$style['all'] .= '}';

			/* 帯状メニュー文字色 */
			if( isset( $wpthk['head_band_color'] ) ) {
				$style['all'] .= 'div[id*="head-band"] a{color:' . $wpthk['head_band_color'] . ';}';
			}

			/* 帯状メニューホバー色 */
			if( isset( $wpthk['head_band_hover_color'] ) ) {
				$style['all'] .= 'div[id*="head-band"] a:hover{color:' . $wpthk['head_band_hover_color'] . ';}';
			}
		}

		/* 帯状メニュー内の 検索ボックス */
		if( isset( $wpthk['head_band_search'] ) ) {
			// 文字色
			if( isset( $wpthk['head_search_color'] ) ) {
				$style['all'] .= '#head-search input[type="text"],';
				$style['all'] .= '#head-search button[type="submit"] {';
				$style['all'] .= 'color:' . $wpthk['head_search_color'] . ';';
				$style['all'] .= '}';

				$style['all'] .= '.head-search-field::-webkit-input-placeholder{color:' . $wpthk['head_search_color'] . ';}';
				$style['all'] .= '.head-search-field::-moz-placeholder{color:' . $wpthk['head_search_color'] . ';}';
				$style['all'] .= '.head-search-field:-moz-placeholder{color:' . $wpthk['head_search_color'] . ';}';
				$style['all'] .= '.head-search-field:-ms-input-placeholder{color:' . $wpthk['head_search_color'] . ';}';
				$style['all'] .= '.head-search-field:placeholder-shown{color:' . $wpthk['head_search_color'] . ';}';
			}

			// 背景色と透過
			if(
				isset( $wpthk['head_search_bg_color'] )	||
				$wpthk['head_search_transparent'] !== $defaults['head_search_transparent']
			) {
				$colors = array( 'r' => 200, 'g' => 200, 'b' => 200 );
				$transparent = $wpthk['head_search_transparent'] !== $defaults['head_search_transparent'] ? $wpthk['head_search_transparent'] : $defaults['head_search_transparent'];
				$transparent = round( $transparent / 100, 2 );

				if( isset( $wpthk['head_search_bg_color'] ) ) {
					$colors = $colors_class->colorcode_2_rgb( $wpthk['head_search_bg_color'] );
				}

				$style['all'] .= '#head-search form { background-color: rgba(' . $colors['r'] . ',' . $colors['g'] . ',' . $colors['b'] . ',' . $transparent . '); }';
			}
		}

		/* 帯状メニュー内の SNS フォローボタン */
		if(
			isset( $wpthk['head_band_twitter'] )	||
			isset( $wpthk['head_band_facebook'] )	||
			isset( $wpthk['head_band_hatena'] )	||
			isset( $wpthk['head_band_google'] )	||
			isset( $wpthk['head_band_youtube'] )	||
			isset( $wpthk['head_band_rss'] )	||
			isset( $wpthk['head_band_feedly'] )
		) {
			if( isset( $wpthk['head_band_follow_color'] ) ) {
				$style['all'] .= 'div[id*="head-band"] .snsf a {color:#fff;}';
				$style['all'] .= 'div[id*="head-band"] .snsf a:hover{opacity: 0.8;}';
				if( isset( $wpthk['head_band_twitter'] ) )	$style['all'] .= 'div[id*="head-band"] .twitter a{background:#55acee;}';
				if( isset( $wpthk['head_band_facebook'] ) )	$style['all'] .= 'div[id*="head-band"] .facebook a{background:#3b5998;}';
				if( isset( $wpthk['head_band_hatena'] ) )	$style['all'] .= 'div[id*="head-band"] .hatena a{background:#3c7dd1;}';
				if( isset( $wpthk['head_band_google'] ) )	$style['all'] .= 'div[id*="head-band"] .google a{background:#dd4b39;}';
				if( isset( $wpthk['head_band_youtube'] ) )	$style['all'] .= 'div[id*="head-band"] .youtube a{background:#ae3a34;}';
				if( isset( $wpthk['head_band_rss'] ) )		$style['all'] .= 'div[id*="head-band"] .rss a{background:#fe9900;}';
				if( isset( $wpthk['head_band_feedly'] ) )	$style['all'] .= 'div[id*="head-band"] .feedly a{background:#87bd33;}';
			}
		}
	}

	/*---------------------------------------------------------------------------
	 * フッター文字色・背景色
	 *---------------------------------------------------------------------------*/
	/* フッター文字色・背景色 */
	if( isset( $wpthk['foot_color'] ) || isset( $wpthk['foot_bg_color'] ) ) {
		$style['all'] .= '#footer{';
		/* フッター文字色 */
		if( isset( $wpthk['foot_color'] ) ) {
			$style['all'] .= 'color:' . $wpthk['foot_color'] . ';';
		}
		/* フッター背景色 */
		if( isset( $wpthk['foot_bg_color'] ) ) {
			$style['all'] .= 'background:' . $wpthk['foot_bg_color'] . ';';

			if( isset( $wpthk['foot_transparent'] ) &&  $wpthk['foot_transparent'] !== 100 ) {
				$rgb = $colors_class->colorcode_2_rgb( $wpthk['foot_bg_color'] );
				$transparent = $wpthk['foot_transparent'] !== $defaults['foot_transparent'] ? $wpthk['foot_transparent'] : $defaults['foot_transparent'];
				$transparent = round( $transparent / 100, 2 );

				$style['all'] .= 'background: rgba(' . $rgb['r'] . ',' . $rgb['g'] . ',' . $rgb['b'] . ',' . $transparent . ');';
			}
		}
		$style['all'] .= '}';
	}
	/* フッターリンク色 */
	if( isset( $wpthk['foot_link_color'] ) ) {
		$style['all'] .= '#footer a{color:' . $wpthk['foot_link_color'] . ';}';
	}

	/* フッターリンクホバー色 */
	if( isset( $wpthk['foot_hover_color'] ) ) {
		$style['all'] .= '#footer a:hover{color:' . $wpthk['foot_hover_color'] . ';}';

	}

	/*---------------------------------------------------------------------------
	 * アニメーション
	 *---------------------------------------------------------------------------*/

	/* イフェクトが連動するので CSS のセレクターはカンマで繋げられない？ 謎・・・ */

	// ズームイン
	$zoom = array();

	if( isset( $wpthk['anime_sitename'] ) && $wpthk['anime_sitename'] === 'zoomin' ) {
		$zoom[] = '.sitename';
	}
	if( isset( $wpthk['anime_thumbnail'] ) && $wpthk['anime_thumbnail'] === 'zoomin' ) {
		$zoom[] = '#list .term img';
	}
	if( isset( $wpthk['anime_sns_buttons'] ) && $wpthk['anime_sns_buttons'] === 'zoomin' ) {
		$zoom[] = 'div[class^=sns] ul[class^=sns] li a';
	}

	if( !empty( $zoom ) ) {
		foreach( $zoom as $val ) {
			$style['min_992'] .= <<< ZOOMIN
{$val}, {$val}:hover {
	transition: all 0.5s;
}
{$val}:hover {
	opacity: 1.0;
	transform: scale3d(1.1, 1.1, 1.0);
}
ZOOMIN;
		}
	}

	// ズームアウト
	$zoom = array();

	if( isset( $wpthk['anime_sitename'] ) && $wpthk['anime_sitename'] === 'zoomout' ) {
		$zoom[] = '.sitename';
	}
	if( isset( $wpthk['anime_thumbnail'] ) && $wpthk['anime_thumbnail'] === 'zoomout' ) {
		$zoom[] = '#list .term img';
	}
	if( isset( $wpthk['anime_sns_buttons'] ) && $wpthk['anime_sns_buttons'] === 'zoomout' ) {
		$zoom[] = 'div[class^=sns] ul[class^=sns] li a';
	}
	if( isset( $wpthk['anime_global_navi'] ) && $wpthk['anime_global_navi'] === 'zoomout' ) {
		$zoom[] = 'div[class^=sns] ul[class^=sns] li a';
	}

	if( !empty( $zoom ) ) {
		foreach( $zoom as $val ) {
			$style['min_992'] .= <<< ZOOMOUT
{$val}, {$val}:hover {
	transition: all 0.5s;
}
{$val}:hover {
	opacity: 1.0;
	transform: scale3d(0.9, 0.9, 1.0);
}
ZOOMOUT;
		}
	}

	// 上方移動
	$upward = array();

	if( isset( $wpthk['anime_global_navi'] ) && $wpthk['anime_global_navi'] === 'upward' ) {
		$upward[] = '#gnavi div > ul > li > a';
	}
	if( isset( $wpthk['anime_sns_buttons'] ) && $wpthk['anime_sns_buttons'] === 'upward' ) {
		$upward[] = 'div[class^=sns] ul[class^=sns] li a';
	}

	if( !empty( $upward ) ) {
		foreach( $upward as $val ) {
			$style['min_992'] .= <<< UPWARD
{$val}, {$val}:hover {
	transition: all 0.5s;
}
{$val}:hover{
	opacity: 1.0;
	transform: translateY(-5px);
}
UPWARD;
		}
	}

	/*---------------------------------------------------------------------------
	 * Lazy Load
	 *---------------------------------------------------------------------------*/
	if( isset( $wpthk['lazyload_enable'] ) ) {
		if( isset( $wpthk['lazyload_placeholder'] ) || isset( $wpthk['lazyload_spinner'] ) ) {
			$style['all'] .= 'img[data-lazy="1"] {background:';
			if( isset( $wpthk['lazyload_placeholder'] ) ) {
				$style['all'] .= $wpthk['lazyload_placeholder'];
			}
			if( isset( $wpthk['lazyload_spinner'] ) ) {
				$style['all'] .= ' url("' . TDEL . '/images/lazy-loading.gif") no-repeat 50% 50%';
			}
			$style['all'] .= ';}';
		}
	}

	/*---------------------------------------------------------------------------
	 * Tosrus
	 *---------------------------------------------------------------------------*/
	if( $wpthk['gallery_type'] === 'tosrus' ) {
		$style['all'] .= <<<LIGHTCASE
.post a[data-rel="tosrus"] {
	cursor: -webkit-zoom-in;
	cursor: -moz-zoom-in;
	cursor: zoom-in;
}
LIGHTCASE;
	}

	/*---------------------------------------------------------------------------
	 * Lightcase
	 *---------------------------------------------------------------------------*/
	if( $wpthk['gallery_type'] === 'lightcase' ) {
		$style['all'] .= <<<LIGHTCASE
.post a[data-rel="lightcase:myCollection"] {
	cursor: -webkit-zoom-in;
	cursor: -moz-zoom-in;
	cursor: zoom-in;
}
LIGHTCASE;
	}

	/*---------------------------------------------------------------------------
	 * Fluidbox
	 *---------------------------------------------------------------------------*/
	if( $wpthk['gallery_type'] === 'fluidbox' ) {
		$style['all'] .= <<<FLUIDBOX
.post a[data-fluidbox] {
	/*background-color: #eee;
	border: none;*/
	cursor: -webkit-zoom-in;
	cursor: -moz-zoom-in;
	cursor: zoom-in;
}
FLUIDBOX;
	}

	/* Fluidbox 固有の問題への対処 */
	if( $wpthk['gallery_type'] === 'fluidbox' ) {
		$style['all'] .= '.band, #page-top{z-index:999;}';
		$style['all'] .= '#nav{z-index:995;}';
	}

	/*---------------------------------------------------------------------------
	 * PAGE TOP ボタン
	 *---------------------------------------------------------------------------*/
	if( isset( $wpthk['page_top_color'] ) || isset( $wpthk['page_top_bg_color'] ) || !isset( $wpthk['page_top_text'] ) ) {
		$style['all'] .= '#page-top{';
		if( isset( $wpthk['page_top_color'] ) ) {
			$style['all'] .= 'color:' . $wpthk['page_top_color'] . ';';
		}
		if( isset( $wpthk['page_top_bg_color'] ) ) {
			$style['all'] .= 'background:' . $wpthk['page_top_bg_color'] . ';';
		}
		if( !isset( $wpthk['page_top_text'] ) ) {
			// アイコンだけの場合はフォントサイズを大きくする
			$style['all'] .= 'font-size:20px;font-size:2.0rem;padding:8px 14px;';
		}
		$style['all'] .= '}';
	}

	/*---------------------------------------------------------------------------
	 * 外部リンク
	 *---------------------------------------------------------------------------*/
	if( isset( $wpthk['add_external_icon'] ) ) {
		if( $wpthk['external_icon_type'] !== 'normal' || isset( $wpthk['external_icon_color'] ) ) {
			$style['all'] .= '.ext_icon:after{';
			/* 外部リンクの種類 */
			if( $wpthk['external_icon_type'] !== 'normal' ) {
				$style['all'] .= 'content:"\f14c";';
			}
			/* 外部リンクアイコンの色 */
			if( isset( $wpthk['external_icon_color'] ) ) {
				$style['all'] .= 'color:' . $wpthk['external_icon_color'] . ';';
			}
			$style['all'] .= '}';
		}
	}

	/*---------------------------------------------------------------------------
	 * SNS ボタンの位置調整
	 *---------------------------------------------------------------------------*/
	// ボタン上配置
	if( $wpthk['sns_tops_type'] === 'color' || $wpthk['sns_tops_type'] === 'white' ) {
		$color = $wpthk['sns_tops_type'] === 'color' ? '.sns-c' : '.sns-w';

		if( $wpthk['sns_tops_position'] === 'center' ) {
			$style['all'] .= '#sns-tops ' . $color . '{text-align: center;}';
		}
		if( $wpthk['sns_tops_position'] === 'right' ) {
			$style['all'] .= '#sns-tops ' . $color . '{text-align: right;}';
			$style['all'] .= '#sns-tops ' . $color . ' li{text-align: center;}';
		}
	}

	// ボタン下配置
	if( $wpthk['sns_bottoms_type'] === 'color' || $wpthk['sns_bottoms_type'] === 'white' ) {
		$color = $wpthk['sns_bottoms_type'] === 'color' ? '.sns-c' : '.sns-w';

		if( $wpthk['sns_bottoms_position'] === 'center' ) {
			$style['all'] .= '.sns-msg, #sns-bottoms ' . $color . '{text-align: center;}';
		}
		if( $wpthk['sns_bottoms_position'] === 'right' ) {
			$style['all'] .= '.sns-msg, #sns-bottoms ' . $color . '{text-align: right;}';
			$style['all'] .= '#sns-bottoms ' . $color . ' li{text-align: center;}';
		}
	}

	// カウント表示があるかないかの調整
	if( isset( $wpthk['sns_tops_enable'] ) && isset( $wpthk['sns_tops_count'] ) ) {
		if( $wpthk['sns_tops_type'] === 'color' || $wpthk['sns_tops_type'] === 'white' ) {
			$color = $wpthk['sns_tops_type'] === 'color' ? '.sns-c' : '.sns-w';
			$style['all'] .= '#sns-tops{margin-top:-30px;}';
			$style['all'] .= '#sns-tops ' . $color . ' .snsb li{margin: 35px 2px 0 0;}';
		}
		if( $wpthk['sns_tops_type'] === 'flatc' || $wpthk['sns_tops_type'] === 'flatw' ) {
			$color = $wpthk['sns_tops_type'] === 'flatc' ? '.snsf-c' : '.snsf-w';
			$style['all'] .= '#sns-tops{margin-top:-5px;}';
			$style['all'] .= '#sns-tops ' . $color . '{margin-top: 24px;}';
		}
	}
	if( isset( $wpthk['sns_bottoms_enable'] ) ) {
		if( isset( $wpthk['sns_bottoms_count'] ) ) {
			if( $wpthk['sns_bottoms_type'] === 'color' || $wpthk['sns_bottoms_type'] === 'white' ) {
				$color = $wpthk['sns_bottoms_type'] === 'color' ? '.sns-c' : '.sns-w';
				$style['all'] .= '#sns-bottoms ' . $color . ' .snsb li{margin: 35px 2px 0 0;}';
			}
			elseif( $wpthk['sns_bottoms_type'] === 'flatc' || $wpthk['sns_bottoms_type'] === 'flatw') {
				$color = $wpthk['sns_bottoms_type'] === 'flatc' ? '.snsf-c' : '.snsf-w';
				$style['all'] .= '#sns-bottoms{margin-top:35px;}';
				$style['all'] .= '#sns-bottoms ' . $color . '{margin-top: 24px;}';
			}
		}
	}
	if( !isset( $wpthk['sns_toppage_view'] ) ) {
		$style['all'] .= '.home .bottom-area #paging{margin-bottom:0;}';
	}

	$ret .= format_media_query( $style );

	return( $ret );
}

/*---------------------------------------------------------------------------
 * カラム数変更による CSS 調整
 *---------------------------------------------------------------------------*/
function thk_adjust_column_css( $style = array(), $column, $defaults, $default_cont_bg_color, $colors_class ) {
	global $wpthk;

	if( empty( $column ) ) {
		$column = isset( $wpthk['column_style'] ) ? $wpthk['column_style'] : '2column';
	}

	if( empty( $style ) ) {
		$style = array(
			'all'		=> null,
			'max_540'	=> null,
			'min_541'	=> null,
			'max_991'	=> null,
			'min_992'	=> null,
			'max_1199'	=> null,
			'min_1200'	=> null,
			'992_1199'	=> null,
			'541_991'	=> null,
		);
	}

	$side_1 = '#side';
	$side_2 = '#col3';
	$widget_1 = '#side .widget';
	$widget_2 = '#col3 .widget';
	$sidebar_1 = '.sidebar';
	$sidebar_2 = '.sidebar-2';

	$side_1_width = $wpthk['side_1_width'];
	$side_2_width = $wpthk['side_2_width'];

	if( isset( $wpthk['column3_reverse'] ) ) {
		$side_1 = '#col3';
		$side_2 = '#side';
		$sidebar_1 = '.sidebar-2';
		$sidebar_2 = '.sidebar';

		$side_1_width = $wpthk['side_2_width'];
		$side_2_width = $wpthk['side_1_width'];
	}

	// コンテンツとサイドを結合した時のベースになるパディングの値
	$indiscrete_base_padding = 22;

	// サイドバーの枠線をつける場所
	$side_point = $wpthk['content_side_discrete'] === 'indiscrete' && $wpthk['side_discrete'] === 'indiscrete' ? '#side' : 'div[id*="side-"]';

	// コンテンツとサイドバーを結合してる時は、コンテンツの枠線設定をサイドバーにも適用
	$side_border_color = isset( $wpthk['side_border_color'] ) ? $wpthk['side_border_color'] : '#ddd';
	if( $wpthk['content_side_discrete'] === 'indiscrete' && isset( $wpthk['cont_border_color'] ) ) {
		$side_border_color = isset( $wpthk['cont_border_color'] ) ? $wpthk['cont_border_color'] : '#ddd';
	}
	$cont_border_color = isset( $luxe['cont_border_color'] ) ? $luxe['cont_border_color'] : '#ddd';

	// flexbox のベンダープリフィクス
	$direction_reverse = <<<DIRECTION_REVERSE
	-webkit-box-direction: reverse;
	-moz-box-direction: reverse;
	-webkit-flex-direction: row-reverse;
	-moz-flex-direction: row-reverse;
	-ms-flex-direction: row-reverse;
	flex-direction: row-reverse;
DIRECTION_REVERSE;

	$flex_width_100 = <<<FLEX_100
	-webkit-box-flex: 0 0 100%;
	-moz-box-flex: 0 0 100%;
	-webkit-flex: 0 0 100%;
	-ms-flex: 0 0 100%;
	flex: 0 0 100%;
	width: 100%;
	max-width: 100%;
	min-width: 1px;
FLEX_100;

/* IE11 のクソみてーなバグのせいで flex-direction: column が使い物にならねーです！！！
	$direction_column = <<<DIRECTION_COLUMN
	-webkit-box-orient: vertical;
	-moz-box-orient: vertical;
	-webkit-flex-direction: column;
	-ms-flex-direction: column;
	flex-direction: column;
DIRECTION_COLUMN;

	$order_1 = <<<ORDER_1
	-webkit-box-ordinal-group: 1;
	-moz-box-ordinal-group: 1;
	-ms-flex-order: 1;
	-webkit-order: 1;
	order: 1;
ORDER_1;

	$order_2 = <<<ORDER_2
	-webkit-box-ordinal-group: 2;
	-moz-box-ordinal-group: 2;
	-ms-flex-order: 2;
	-webkit-order: 2;
	order: 2;
ORDER_2;
*/

	/*---------------------------------------------------------------------------
	 * #main と #field の max-width
	 * Flexbox で中身が飛び出てサイドバーに被らないよう幅を決めておく & 描画も速くなる
	 *---------------------------------------------------------------------------*/

	// 2カラム
	if(
		$column === '2column' &&
		(
			$wpthk['content_side_discrete'] === 'indiscrete' ||
			$wpthk['container_max_width'] !== $defaults['container_max_width'] ||
			$wpthk['side_1_width'] !== $defaults['side_1_width']
		)
	) {
		$main_width_1200 = $wpthk['container_max_width'] - $wpthk['side_1_width'] - 32; // 772
		if( $wpthk['content_side_discrete'] === 'indiscrete' ) {
			$main_width_1200 = $wpthk['container_max_width'] - $wpthk['side_1_width'] - 30; // 774 (コンテンツとサイドバー結合時は 2px 広い)
		}

		$main_width_992_1199 = 938 - $wpthk['side_1_width']; // 572
		if( $wpthk['content_side_discrete'] === 'indiscrete' ) {
			$main_width_992_1199 = 940 - $wpthk['side_1_width']; // 574 (同上)
		}

		$style['min_1200'] .= <<<FLEX
#main {
	-webkit-box-flex: 0 1 {$main_width_1200}px;
	-moz-box-flex: 0 1 {$main_width_1200}px;
	-webkit-flex: 0 1 {$main_width_1200}px;
	-ms-flex: 0 1 {$main_width_1200}px;
	flex: 0 1 {$main_width_1200}px;
	max-width: {$main_width_1200}px;
	min-width: 1px;
}
FLEX;

		$style['992_1199'] .= <<<FLEX
#main {
	-webkit-box-flex: 0 1 {$main_width_992_1199}px;
	-moz-box-flex: 0 1 {$main_width_992_1199}px;
	-webkit-flex: 0 1 {$main_width_992_1199}px;
	-ms-flex: 0 1 {$main_width_992_1199}px;
	flex: 0 1 {$main_width_992_1199}px;
	max-width: {$main_width_992_1199}px;
	min-width: 1px;
}
FLEX;
	}
	// 3カラム
	elseif( $column === '3column' ) {
		$field_width_1200 = $wpthk['container_max_width'] - $wpthk['side_1_width'] - 32; // ? ( field は常に $wpthk['side_1_width'] をマイナス)

		$main_width_1200 = $wpthk['container_max_width'] - $side_1_width - $side_2_width - 42; // ?
		if( $wpthk['content_side_discrete'] === 'indiscrete' ) {
			$main_width_1200 = $wpthk['container_max_width'] - $side_1_width - $side_2_width - 30; // ?
		}

		$main_width_992_1199 = 938 - $wpthk['side_1_width']; // ?;
		if( $wpthk['content_side_discrete'] === 'indiscrete' ) {
			$main_width_992_1199 = 940 - $side_1_width; // ?;
		}

		$style['min_1200'] .= <<<FLEX
#field {
	-webkit-box-flex: 0 1 {$field_width_1200}px;
	-moz-box-flex: 0 1 {$field_width_1200}px;
	-webkit-flex: 0 1 {$field_width_1200}px;
	-ms-flex: 0 1 {$field_width_1200}px;
	flex: 0 1 {$field_width_1200}px;
	width: {$field_width_1200}px; /* IE9 以下だと field は max-width ダメぽい */
	min-width: 1px;
}
#main {
	-webkit-box-flex: 0 1 {$main_width_1200}px;
	-moz-box-flex: 0 1 {$main_width_1200}px;
	-webkit-flex: 0 1 {$main_width_1200}px;
	-ms-flex: 0 1 {$main_width_1200}px;
	flex: 0 1 {$main_width_1200}px;
	max-width: {$main_width_1200}px;
	min-width: 1px;
}
FLEX;

		$style['992_1199'] .= <<<FLEX
#field, #main {
	-webkit-box-flex: 0 1 {$main_width_992_1199}px;
	-moz-box-flex: 0 1 {$main_width_992_1199}px;
	-webkit-flex: 0 1 {$main_width_992_1199}px;
	-ms-flex: 0 1 {$main_width_992_1199}px;
	flex: 0 1 {$main_width_992_1199}px;
	width: {$main_width_992_1199}px;
	min-width: 1px;
}
FLEX;
	}

	// コンテンツ領域が画面幅いっぱいの時は、幅を決められないので、1200px 以上の時は常に100%
	if( $wpthk['container_max_width'] === 0 ) {
		$style['min_1200'] .= <<<FLEX
#field, #main {
	-webkit-box-flex: 0 1 100%;
	-moz-box-flex: 0 1 100%;
	-webkit-flex: 0 1 100%;
	-ms-flex: 0 1 100%;
	flex: 0 1 100%;
	max-width: 100%;
	min-width: 1px;
}
FLEX;
	}

	/*---------------------------------------------------------------------------
	 * サイドバーの幅
	 *---------------------------------------------------------------------------*/
	$side_flex_basis = <<<FLEX_BASIS
-ms-flex-preferred-size: {$side_1_width}px;
-moz-flex-basis: {$side_1_width}px;
-webkit-flex-basis: {$side_1_width}px;
flex-basis: {$side_1_width}px;
FLEX_BASIS;

	$side_3_flex_basis = <<<FLEX
#col3 {
	-webkit-box-flex: 0 0 {$side_2_width}px;
	-moz-box-flex: 0 0 {$side_2_width}px;
	-webkit-flex: 0 0 {$side_2_width}px;
	-ms-flex: 0 0 {$side_2_width}px;
	flex: 0 0 {$side_2_width}px;
	width: {$side_2_width}px;
	min-width: 1px;
}
FLEX;

	// 2カラムの場合は、flex: が元の style.css に書いてあるので、flex-basis: と width: で幅変更
	if( $side_1_width !== $defaults['side_1_width'] ) $style['min_992'] .= '#side{' . $side_flex_basis . ' width:' . $side_1_width . 'px;}';

	// 3カラムの場合の 3カラム目の記述が元の style.css に無いので、flex: と width: を挿入
	if( $column === '3column') {
		$style['min_992'] .= $side_3_flex_basis;
	}

	/*---------------------------------------------------------------------------
	 * サイドバーの位置(2カラム)
	 *---------------------------------------------------------------------------*/
	if( $column === '2column' ) {

		if( $wpthk['side_position'] === 'left' ) {
			$style['min_992'] .= <<<POSITION
#primary { {$direction_reverse} }
#main { float: right; }
#core {
	margin-right: 0;
	margin-left: 10px;
}
POSITION;

		$style['max_991'] .= <<<POSITION
#primary, #main, {$side_1}{
	display: block;
	width: 100%;
	float: none;
}
#core, {$sidebar_1} {
	margin: 0 0 20px 0;
}
{$sidebar_1} {
	padding: 0;
}
POSITION;
		}
	}

	/*---------------------------------------------------------------------------
	 * サイドバーの位置(3カラム)
	 *---------------------------------------------------------------------------*/
	if( $column === '3column' ) {

		if( $wpthk['column3_position'] === 'center' ) {
			if( !isset( $wpthk['column3_reverse'] ) ) {
				$style['min_1200'] .= <<<POSITION
#field { {$direction_reverse}  float:left; }
#main { float:right; }
#core, {$sidebar_2} {
	margin-right: 10px;
}
POSITION;
			}
			else {
				$style['min_1200'] .= <<<POSITION
#field { {$direction_reverse}  float:right; }
#main { float:left; }
#core, {$sidebar_2} {
	margin-right: 10px;
}
POSITION;
			}

			if( !isset( $wpthk['column3_reverse'] ) ) {
				$style['992_1199'] .= <<<POSITION
/* #field { {\$direction_column}  float:left; } */
#field { display: block; float:left; }
#main { float: none; }
{$side_2}{ {$flex_width_100} display: block; }
#core, {$sidebar_2} {
	margin-right: 10px;
}
{$sidebar_2} {
	margin-bottom: 20px;
}
POSITION;
			}
			else {
				$style['992_1199'] .= <<<POSITION
/* #field { {\$direction_column}  float:right; } */
#field { display: block; float:right; }
#main { float: none; }
{$side_2}{ {$flex_width_100} display: block; }
#core, {$sidebar_2} {
	margin-right: 10px;
}
{$sidebar_2} {
	margin-bottom: 20px;
}
POSITION;
			}
		}

		if( $wpthk['column3_position'] === 'right' ) {
			$style['min_1200'] .= <<<POSITION
#field { float: left; }
#main { float: left; }
{$side_1}{ float: right; }
{$side_2}{ float: right; }
#core, {$sidebar_2} {
	margin-right: 10px;
}
POSITION;

			if( !isset( $wpthk['column3_reverse'] ) ) {
				$style['992_1199'] .= <<<POSITION
#field { display: block; float:left; }
#main { float: none; }
/* #field { {\$direction_column} } */
{$side_2}{ {$flex_width_100} }
#core, {$sidebar_2} {
	margin-right: 10px;
}
{$sidebar_2} {
	margin-bottom: 20px;
}
POSITION;
			}
			else {
				$style['992_1199'] .= <<<POSITION
#field { display: block; float:left; }
#main { float: none; }
{$sidebar_1} { float: right; }
/* #field { {\$direction_column} } */
{$side_2}{ {$flex_width_100} }
#core, {$sidebar_2} {
	margin-right: 10px;
}
{$sidebar_2} {
	margin-bottom: 20px;
}
POSITION;
			}
		}

		if( $wpthk['column3_position'] === 'left' ) {
			$style['min_1200'] .= <<<POSITION
#primary, #field { {$direction_reverse} }
#field, #main { float:right; }
#core {
	margin-right: 0;
	margin-left: 10px;
}
{$sidebar_2} {
	margin-left: 10px;
}
POSITION;

			if( !isset( $wpthk['column3_reverse'] ) ) {
				$style['992_1199'] .= <<<POSITION
#primary { {$direction_reverse} }
/* #field { {\$direction_column} } */
#field { display: block; float:right; }
#main { float: none; }
{$side_2}{ {$flex_width_100} }
#core, {$sidebar_2} {
	margin-left: 10px;
	margin-right: 0;
}
{$sidebar_2} {
	margin-bottom: 20px;
}
POSITION;
			}
			else {
				$style['992_1199'] .= <<<POSITION
#primary { {$direction_reverse} }
/* #field { {\$direction_column} } */
#field { display: block; float:right; }
#main { float: none; }
{$side_2}{ {$flex_width_100} }
#core, {$sidebar_2} {
	margin-left: 10px;
	margin-right: 0;
}
{$sidebar_2} {
	display: block;
	margin-bottom: 20px;
}
{$sidebar_1}, {$sidebar_2} {
	overflow: hidden;
}
POSITION;
			}
		}

		$style['max_991'] .= <<<POSITION
#primary, #field, #main, {$side_1}, {$side_2}{
	display: block;
	width: 100%;
	float: none;
}
#core, {$sidebar_1}, {$sidebar_2} {
	margin: 0 0 20px 0;
}
{$sidebar_1}, {$sidebar_2} {
	padding: 0;
}
POSITION;
	}

	/*---------------------------------------------------------------------------
	 * コンテンツ領域の結合
	 *---------------------------------------------------------------------------*/
	if( $wpthk['content_discrete'] === 'indiscrete' ) {
		$style['all'] .= <<< INDISCRETE
#list .toc, .bottom-area {
	background: none;
	border: none;
	padding-top: 0;
	padding-bottom: 0;
}
.bottom-area {
	margin-bottom: 50px;
}
INDISCRETE;
		if( $wpthk['breadcrumb_view'] === 'none' || $wpthk['breadcrumb_view'] === 'outer' ) {
			$style['all'] .= <<< INDISCRETE
#core.lcore {
	padding-top: 35px;
	border: 1px solid {$cont_border_color};
	background: {$default_cont_bg_color[$wpthk['overall_image']]};
}
INDISCRETE;
		}
		else {
			$style['all'] .= <<< INDISCRETE
#core, #core.lcore {
	padding-top: 10px;
	border: 1px solid {$cont_border_color};
	background: {$default_cont_bg_color[$wpthk['overall_image']]};
}
INDISCRETE;
		}
	}

	/*---------------------------------------------------------------------------
	 * サイドバー分離
	 *---------------------------------------------------------------------------*/
	if( $wpthk['side_discrete'] === 'discrete' ) {
		$style['all'] .= <<< DISCRETE
div[id*="side-"], #col3 {
	padding: 0;
	border: none;
	background: none;
}
#side .widget, #col3 .widget {
	margin: 0 0 15px 0;
	padding: 20px 14px;
	border: 1px solid {$side_border_color};
	background: {$default_cont_bg_color[$wpthk['overall_image']]};
}
#side-scroll {
	margin: 0;
}
DISCRETE;
	}

	/*---------------------------------------------------------------------------
	 * コンテンツ領域のパディング
	 *---------------------------------------------------------------------------*/
	if( $wpthk['content_discrete'] === 'indiscrete' && $wpthk['breadcrumb_view'] === 'inner' ) {
		$style['all'] .= '#core.lcore{padding-top:0;}';
	}

	if( $wpthk['cont_padding_top'] !== $defaults['cont_padding_top'] || $wpthk['cont_padding_bottom'] !== $defaults['cont_padding_bottom'] ) {
		if( $wpthk['content_discrete'] === 'indiscrete' ) {
			$style['all'] .= '#core.lcore, #core.pcore {';
		}
		else {
			$style['all'] .= '#list .toc, .bottom-area, #core.pcore {';
		}

		if( $wpthk['cont_padding_top'] !== $defaults['cont_padding_top'] ) {
			$style['all'] .= 'padding-top:' . $wpthk['cont_padding_top'] . 'px;';
		}
		if( $wpthk['cont_padding_bottom'] !== $defaults['cont_padding_bottom'] ) {
			$style['all'] .= 'padding-bottom:' . $wpthk['cont_padding_bottom'] . 'px;';
		}
		$style['all'] .= '}';
	}

	// パンくずリンクがコンテンツの中にある時
	if( $wpthk['breadcrumb_view'] === 'inner' ) {
		$padding_top = $wpthk['cont_padding_top'] - 25;
		if( $padding_top <= 0 ) $padding_top = 0;

		$style['all'] .= '#list .toc:first-child, #core.pcore {';
		$style['all'] .= 'padding-top:' . $padding_top . 'px;';
		$style['all'] .= '}';
	}

	if( $wpthk['cont_padding_left'] !== $defaults['cont_padding_left'] || $wpthk['cont_padding_right'] !== $defaults['cont_padding_right'] ) {
		$style['min_992'] .= '#list .toc, .bottom-area, #core.pcore {';

		if( $wpthk['cont_padding_left'] !== $defaults['cont_padding_left'] ) {
			$style['min_992'] .= 'padding-left:' . $wpthk['cont_padding_left'] . 'px;';
		}
		if( $wpthk['cont_padding_right'] !== $defaults['cont_padding_right'] ) {
			$style['min_992'] .= 'padding-right:' . $wpthk['cont_padding_right'] . 'px;';
		}

		$style['min_992'] .= '}';

		$style['541_991'] .= '#list .toc, .bottom-area, #core.pcore {';

		if( $wpthk['cont_padding_left'] !== $defaults['cont_padding_left'] ) {
			$_541_991_left = $wpthk['cont_padding_left'] - 10;
			$_541_991_left = $_541_991_left > 5 ? $_541_991_left : 5;
			$style['541_991'] .= 'padding-left:' . $_541_991_left . 'px;';
		}
		if( $wpthk['cont_padding_right'] !== $defaults['cont_padding_right'] ) {
			$_541_991_right = $wpthk['cont_padding_right'] - 10;
			$_541_991_right = $_541_991_right > 5 ? $_541_991_right : 5;
			$style['541_991'] .= 'padding-right:' . $_541_991_right . 'px;';
		}

		$style['541_991'] .= '}';
	}

	/*---------------------------------------------------------------------------
	 * コンテンツ領域のパディング変更による、その他のマージン調整
	 *---------------------------------------------------------------------------*/
	// コンテンツのパディングが変更されてる時
	if( $wpthk['cont_padding_left'] !== $defaults['cont_padding_left'] || $wpthk['cont_padding_right'] !== $defaults['cont_padding_right'] ) {
		// コンテンツとサイド結合時
		if( $wpthk['content_side_discrete'] === 'indiscrete' ) {

			/* 下ナビ、関連記事・トラックバック・992px 以上 1199px 以下ディスカッション */
			$style['992_1199'] .= '.pnavi, .related, .discussion, .tb {';
			$style['min_1200'] .= '.pnavi, .related, .tb {';

			// left
			$margin_left = 0 - $wpthk['cont_padding_left'] + $defaults['cont_padding_left'] - 25;
			$style['992_1199'] .= 'margin-left: ' . $margin_left . 'px;';

			$margin_left = 0 - $wpthk['cont_padding_left'] - $defaults['cont_padding_left'] + 25;
			$style['min_1200'] .= 'margin-left:' . $margin_left . 'px;';

			// right
			$margin_right = 0 - $wpthk['cont_padding_right'] + $defaults['cont_padding_right'] - 25;
			$style['992_1199'] .= 'margin-right: ' . $margin_right . 'px;';

			$margin_right = 0 - $wpthk['cont_padding_right'] - $defaults['cont_padding_right'] + 25;
			$style['min_1200'] .= 'margin-right:' . $margin_right . 'px;';

			$style['992_1199'] .= '}';
			$style['min_1200'] .= '}';
		}
		// コンテンツとサイド分離時
		else {
			// 992px 以上 1199px 以下 下ナビ、関連記事・ディスカッション
			$style['992_1199'] .= '.pnavi, .related, .discussion, .tb {';

			if( $wpthk['cont_padding_left'] !== $defaults['cont_padding_left'] ) {
				$margin_left = 0 - $wpthk['cont_padding_left'] - $defaults['cont_padding_left'] + 25;
				$style['992_1199'] .= 'margin-left:' . $margin_left . 'px;';
			}
			if( $wpthk['cont_padding_right'] !== $defaults['cont_padding_right'] ) {
				$margin_right = 0 - $wpthk['cont_padding_right'] - $defaults['cont_padding_right'] + 25;
				$style['992_1199'] .= 'margin-right:' . $margin_right . 'px;';
			}
			$style['992_1199'] .= '}';
		}

		// 1200px 以上 ディスカッション
		$style['min_1200'] .= '.discussion {';
		if( $wpthk['cont_padding_left'] !== $defaults['cont_padding_left'] ) {
			$margin_left = 0 - $wpthk['cont_padding_left'] - $defaults['cont_padding_left'] + 40;
			$style['min_1200'] .= 'margin-left:' . $margin_left . 'px;';
		}
		if( $wpthk['cont_padding_right'] !== $defaults['cont_padding_right'] ) {
			$margin_right = 0 - $wpthk['cont_padding_right'] - $defaults['cont_padding_right'] + 40;
			$style['min_1200'] .= 'margin-right:' . $margin_right . 'px;';
		}
		$style['min_1200'] .= '}';

		// 541px 以上 991px 以下 下ナビ、関連記事・ディスカッション
		$style['541_991'] .= '.pnavi, .related, .discussion, .tb {';
		if( $wpthk['cont_padding_left'] !== $defaults['cont_padding_left'] ) {
			$margin_left = -5;
			if( $wpthk['cont_padding_left'] >= 15 ) {
				$margin_left = 0 - $wpthk['cont_padding_left'] - $defaults['cont_padding_left'] + 35;
			}
			$style['541_991'] .= 'margin-left:' . $margin_left . 'px;';
		}

		if( $wpthk['cont_padding_right'] !== $defaults['cont_padding_right'] ) {
			$margin_right = -5;
			if( $wpthk['cont_padding_right'] >= 15 ) {
				$margin_right = 0 - $wpthk['cont_padding_right'] - $defaults['cont_padding_right'] + 35;
			}
			$style['541_991'] .= 'margin-right:' . $margin_right . 'px;';
		}
		$style['541_991'] .= '}';
	}
	// コンテンツのパディングがデフォルトで、コンテンツとサイドが結合されてる時
	elseif( $wpthk['content_side_discrete'] === 'indiscrete' ) {
		$style['992_1199'] .= '.pnavi, .related, .discussion, .tb {';
		$style['992_1199'] .= 'margin-left: -15px;';
		$style['992_1199'] .= 'margin-right: -15px;';
		$style['992_1199'] .= '}';
	}

	/*---------------------------------------------------------------------------
	 * コンテンツ領域とサイドバー結合した時
	 *---------------------------------------------------------------------------*/
	if( $wpthk['content_side_discrete'] === 'indiscrete' ) {
			$style['min_1200'] .= <<< INDISCRETE
#core, .sidebar, .sidebar-2 {
	margin-right: 0;
	margin-left: 0;
}
INDISCRETE;
			$style['max_1199'] .= <<< INDISCRETE
#core, .sidebar, .sidebar-2 {
	margin-right: 0;
	margin-left: 0;
}
INDISCRETE;

		/* 992px 以上 */
		if( $column === '2column' ) $style['min_992'] .= '#side{border:1px solid ' . $side_border_color . ';}';
		$style['min_992'] .= '#primary{overflow:hidden; border:1px solid ' . $cont_border_color . ';background:none}';
		$style['min_1200'] .= '#list .toc, #core.pcore{padding-left:' . $wpthk['cont_padding_left'] . 'px;padding-right:' . $wpthk['cont_padding_right'] . 'px;}';

		$padding_l_992_1199 = $wpthk['cont_padding_left'] < $indiscrete_base_padding ? $wpthk['cont_padding_left'] : $indiscrete_base_padding;
		$padding_r_992_1199 = $wpthk['cont_padding_right'] < $indiscrete_base_padding ? $wpthk['cont_padding_right'] : $indiscrete_base_padding;

		$style['min_992'] .= '#main{margin: 0 -1px;}';

		$side_group = $wpthk['side_discrete'] === 'indiscrete' ? $side_point : '#side .widget';
		$col3_group = $wpthk['side_discrete'] === 'indiscrete' ? '#col3' : '#col3 .widget';
		$side_widget_first = '#side .widget:first-child';
		$col3_widget_first = '#col3 .widget:first-child';

		if( $column === '3column' && isset( $wpthk['column3_reverse'] ) ) {
			$side_group = $wpthk['side_discrete'] === 'indiscrete' ? '#col3' : '#col3 .widget';
			$col3_group = $wpthk['side_discrete'] === 'indiscrete' ? $side_point : '#side .widget';
			$side_widget_first = '#col3 .widget:first-child';
			$col3_widget_first = '#side .widget:first-child';
		}

		// 3カラムのコンテンツとサイドバー上の枠線消す
		if( $column === '3column' ) {
			$style['min_992'] .= '#list .toc:first-child,#core, #core.lcore { border-top:none; }';
			if( $wpthk['side_discrete'] === 'indiscrete' ) {
				$style['min_1200'] .= $side_group . ',' . $col3_group . '{ border-top:none; }';
				$style['992_1199'] .= $side_group . '{ border-right:none; border-top:none; }';
				$style['992_1199'] .= $col3_group . '{ border-left:none; }';
			}
			else {
				$style['min_1200'] .= $side_widget_first . ',' . $col3_widget_first . '{ border-top:none; }';
				$style['992_1199'] .= $side_widget_first . '{ border-top:none; }';
				$style['992_1199'] .= $side_group . '{ border-right:none; }';
				$style['992_1199'] .= $col3_group . '{ border-left:none; }';
			}
		}

		// 2カラムのコンテンツとサイドバー上の枠線消す
		else {
			$style['min_992'] .= '#list .toc:first-child,#core, #core.lcore, ' . $side_widget_first . '{ border-top:none; }';
		}

		/* コンテンツ領域分離されてる */
		if( $wpthk['content_discrete'] === 'discrete' ) {
			$style['min_992'] .= '#core.lcore{border-left:none;border-right:none;}';
			$style['min_992'] .= '#primary,#core.lcore{border-bottom:none;}';
		}
		else {
			$style['min_992'] .= '#primary{border-bottom:none;}';
		}

		// 3カラム
		if( $column === '3column' ) {
			// サイドバー右
			if( $wpthk['column3_position'] === 'right' ) {
				$style['min_1200'] .= $side_group . '{border-right:none;}';
				$style['min_1200'] .= $col3_group . '{margin-right:-1px;}';
			}
			// サイドバー左
			elseif( $wpthk['column3_position'] === 'left' ) {
				$style['min_1200'] .= $side_group . '{border-left:none;}';
				$style['min_1200'] .= $col3_group . '{margin-left:-1px;}';
			}
			// サイドバー両脇
			else {
				$style['min_1200'] .= $side_group . '{border-right:none;}';
				$style['min_1200'] .= $col3_group . '{border-left:none;}';
			}
			// サイドバー右
			if( $wpthk['column3_position'] === 'right' ) {
				$style['992_1199'] .= $side_group . '{border-right:none;border-left:1px solid ' . $side_border_color . ';}';
				$style['992_1199'] .= '#core.lcore, #core.pcore, #list .toc, .bottom-area{border-left:none;border-right:none;}';
			}
			// サイドバー左
			elseif( $wpthk['column3_position'] === 'left' ) {
				$style['992_1199'] .= $side_group . '{border-left:none;border-right:1px solid ' . $side_border_color . ';}';
				$style['992_1199'] .= '#core.lcore, #core.pcore, #list .toc, .bottom-area{border-left:none;border-right:none;}';
			}
			// サイドバー両脇
			else {
				$style['992_1199'] .= $col3_group . '{border-right:none;}';
				$style['992_1199'] .= '#core.lcore, #core.pcore, #list .toc, .bottom-area{border-left:none;border-right:none;}';
			}
		}
		// 2カラム
		else {
			// サイドバー右
			if( $wpthk['side_position'] === 'right' ) {
				$style['min_992'] .= '#core, #core.lcore{border-left:none;}';
				$style['min_992'] .= $side_point . '{border-bottom:none;border-right:none;}';
			}
			// サイドバー左
			else {
				$style['min_992'] .= '#core, #core.lcore{border-right:none;}';
				$style['min_992'] .= $side_point . '{border-bottom:none;border-left:none;}';
			}
		}

		/* 992px 以上 1199px 未満 */

		// 3カラム
		if( $column === '3column' ) {
			$style['992_1199'] .= $side_2 . '{margin-left:1px;}';
		}

		/* 991px 以下 */

		if( $wpthk['content_discrete'] === 'discrete' ) {
			$style['max_991'] .= '#core.lcore{border:none;}';
		}
	}

	/*---------------------------------------------------------------------------
	 * 1カラム用スタイル
	 *---------------------------------------------------------------------------*/
	if(
		$wpthk['column3'] === '1column' &&
		$wpthk['column_home'] === 'default' &&
		$wpthk['column_post'] === 'default' &&
		$wpthk['column_page'] === 'default' &&
		$wpthk['column_archive'] === 'default'
	) {
		$style['all'] .= <<<COLUMN
#primary, #main {
	{$flex_width_100}
	padding: 0;
}
#main, #core {
	margin: 0;
}
COLUMN;
	}
	else {
		$tmpl_class = array(
			'.home'		=> true,
			'.single'	=> true,
			'.page'		=> true,
			'.archive'	=> true,
			'.search'	=> true,
			'.error404'	=> true
		);

		if( $wpthk['column3'] === '1column' ) {
			if( $wpthk['column_home'] !== 'default' && $wpthk['column_home'] !== '1column' ) unset( $tmpl_class['.home'] );
			if( $wpthk['column_post'] !== 'default' && $wpthk['column_post'] !== '1column' ) unset( $tmpl_class['.single'] );
			if( $wpthk['column_page'] !== 'default' && $wpthk['column_page'] !== '1column' ) {
				unset( $tmpl_class['.page'] );
				unset( $tmpl_class['.error404'] );
			}
			if( $wpthk['column_archive'] !== 'default' && $wpthk['column_archive'] !== '1column' ){
				unset( $tmpl_class['.archive'] );
				unset( $tmpl_class['.search'] );
			}
		}
		else {
			if( $wpthk['column_home'] !== '1column' ) unset( $tmpl_class['.home'] );
			if( $wpthk['column_post'] !== '1column' ) unset( $tmpl_class['.single'] );
			if( $wpthk['column_page'] !== '1column' ){
				unset( $tmpl_class['.page'] );
				unset( $tmpl_class['.error404'] );
			}
			if( $wpthk['column_archive'] !== '1column' ){
				unset( $tmpl_class['.archive'] );
				unset( $tmpl_class['.search'] );
			}
		}

		if( !empty( $tmpl_class ) ) {
			$selector_1 = '';
			$selector_2 = '';

			foreach( $tmpl_class as $key => $val ) {
				$selector_1 .= $key . ' #main,';
				$selector_2 .= $key . ' #main,' . $key . ' #core,';
			}
			$selector_1 = rtrim( $selector_1, ',' );
			$selector_2 = rtrim( $selector_2, ',' );

			$style['all'] .= <<<COLUMN
{$selector_1} {
	{$flex_width_100}
	padding: 0;
}
{$selector_2} {
	margin: 0;
}
COLUMN;
		}
	}

	/*---------------------------------------------------------------------------
	 * コンテンツ領域枠線
	 *---------------------------------------------------------------------------*/
	if( !isset( $wpthk['contents_border'] ) ) {
		$style['all'] .= '#primary, #list .toc, .bottom-area, #core, #core.lcore{border:1px solid transparent;}';
	}
	elseif( isset( $wpthk['contents_border'] ) && isset( $wpthk['cont_border_color'] ) ) {
		if( $wpthk['content_side_discrete'] === 'indiscrete' ) {
			$style['min_992'] .= '#primary{border-color:' . $wpthk['cont_border_color'] . ';}';
			$style['all'] .= '#core, #core.lcore, #list .toc, .bottom-area{border-color:' . $wpthk['cont_border_color'] . ';}';
		}
		elseif( $wpthk['content_discrete'] === 'indiscrete' ) {
			$style['all'] .= '#core, #core.lcore{border-color:' . $wpthk['cont_border_color'] . ';}';
		}
		else {
			$style['all'] .= '#list .toc, .bottom-area, #core{border-color:' . $wpthk['cont_border_color'] . ';}';
		}
	}
	if( isset( $wpthk['cont_border_radius'] ) && $wpthk['cont_border_radius'] !== $defaults['cont_border_radius'] ) {
		if( $wpthk['content_side_discrete'] === 'indiscrete' ) {
			$style['min_992'] .= '#primary{border-radius:' . $wpthk['cont_border_radius'] . 'px;}';
			$style['max_991'] .= '#core, #core.lcore, #list .toc, .bottom-area{border-radius:' . $wpthk['cont_border_radius'] . 'px;}';
		}
		elseif( $wpthk['content_discrete'] === 'indiscrete' ) {
			$style['all'] .= '#core, #core.lcore{border-radius:' . $wpthk['cont_border_radius'] . 'px;}';
		}
		else {
			$style['all'] .= '#core, #list .toc, .bottom-area{border-radius:' . $wpthk['cont_border_radius'] . 'px;}';
		}
	}

	/*---------------------------------------------------------------------------
	 * ページャー領域枠線
	 *---------------------------------------------------------------------------*/
	if( !isset( $wpthk['pagination_area_border'] ) && $wpthk['content_discrete'] === 'discrete' ) {
		$style['all'] .= '.bottom-area{border:none;background:none;}';
	}

	/*---------------------------------------------------------------------------
	 * サイドバー枠線
	 *---------------------------------------------------------------------------*/
	if( $wpthk['content_side_discrete'] === 'indiscrete' && isset( $wpthk['contents_border'] ) && isset( $wpthk['sidebar_border'] ) ) {
		if( isset( $wpthk['cont_border_radius'] ) ) {
			$wpthk['side_border_radius'] = $wpthk['cont_border_radius'];
		}
	}

	if( $wpthk['content_side_discrete'] === 'indiscrete' ) {
		if( $wpthk['side_discrete'] === 'indiscrete' ) {
			$style['all'] .= $side_point . '{border:1px solid ' . $side_border_color . ';}';
			$style['all'] .= 'div[id*="side-"]{border:none;}';
			$style['min_992'] .= $side_point . '{border-top:none;}';

			// 3カラム
			if( $column === '3column' && !isset( $wpthk['side_border_color'] ) ) {
				// サイドバー両脇 (コンテンツに枠線が無い場合など、サードバー両脇の時はコンテンツ領域と隣り合わせになるので、枠線に色が必要になる)
				if( $wpthk['column3_position'] === 'center' ) {
					$style['all'] .= $side_point . '{border-color:' . $side_border_color . ';}';
				}
			}
		}
		else {
			$style['min_992'] .= '#side{border:none;}';
		}
	}

	if( !isset( $wpthk['sidebar_border'] ) ) {
		if( $wpthk['side_discrete'] === 'discrete' ) {
			$style['all'] .= '#side .widget, #col3 .widget{border:1px solid transparent;}';
		}
		else {
			$style['all'] .= $side_point . ', #col3{border:1px solid transparent;}';
		}
	}
	if( isset( $wpthk['sidebar_border'] ) && isset( $wpthk['side_border_color'] ) ) {
		if( $column !== '3column' && $wpthk['content_side_discrete'] === 'indiscrete' && $wpthk['side_discrete'] === 'indiscrete' ) {
			if( !isset( $wpthk['contents_border'] ) || !isset( $wpthk['cont_border_color'] ) ) {
				$style['min_992'] .= '#side{border:1px solid ' . $side_border_color . ';}';
			}
			else {
				if( $wpthk['side_position'] === 'right' ) {
					$style['min_992'] .= '#side{border-left:1px solid ' . $side_border_color . ';}';
					$style['min_992'] .= '#core, #core.lcore{margin-right:0;}';
				}
				else {
					$style['min_992'] .= '#side{border-right:1px solid ' . $side_border_color . ';}';
					$style['min_992'] .= '#core, #core.lcore{margin-left:0;}';
				}
			}
			$style['min_992'] .= 'div[id*="side-"], #col3{border:transparent;}';
		}
		if( $wpthk['side_discrete'] === 'indiscrete' ) {
			if( $wpthk['content_side_discrete'] === 'indiscrete' ) {
				$style['all'] .= '#side, #col3{border-color:' . $side_border_color . ';}';
			}
			else {
				$style['all'] .= $side_point . ', #col3{border-color:' . $side_border_color . ';}';
			}
		}
		else {
			$style['all'] .= '#side .widget, #col3 .widget{border-color:' . $side_border_color . ';}';

			if( $column !== '3column' && $wpthk['content_side_discrete'] === 'indiscrete' ) {
				if( $wpthk['side_position'] === 'right' ) {
					$style['min_992'] .= '#side .widget, #col3 .widget{border-right:none;}';
				}
				else {
					$style['min_992'] .= '#side .widget, #col3 .widget{border-left:none;}';
				}
			}
		}
	}

	if( isset( $wpthk['side_border_radius'] ) && $wpthk['side_border_radius'] !== $defaults['side_border_radius'] ) {
		if( $wpthk['content_side_discrete'] === 'indiscrete' ) {
			if( $wpthk['side_discrete'] === 'discrete' ) {
				$style['max_991'] .= '#side .widget, #col3 .widget{border-radius:' . $wpthk['side_border_radius'] . 'px;}';
			}
			else {
				$style['max_991'] .= $side_point . ', #col3{border-radius:' . $wpthk['side_border_radius'] . 'px;}';
			}
		}
		else {
			if( $wpthk['side_discrete'] === 'indiscrete' ) {
				$style['all'] .= $side_point . ', #col3{border-radius:' . $wpthk['side_border_radius'] . 'px;}';
			}
			else {
				$style['all'] .= '#side .widget, #col3 .widget{border-radius:' . $wpthk['side_border_radius'] . 'px;}';
			}
		}
	}

	// Widget の変更を監視して、変更があった場合の処理
	if( function_exists('dynamic_sidebar') === true ) {
		$radius_flag = true;

		if(
			$wpthk['side_border_radius'] === 0 ||
			$wpthk['side_discrete'] === 'discrete' ||
			$wpthk['content_side_discrete'] === 'indiscrete'
		) {
				$radius_flag = false;
		}

		if( is_active_sidebar('side-scroll') === true ) {
			$style['min_992'] .= '#side-fixed{border-bottom:0;';
			if( $radius_flag === true ) {
				$style['min_992'] .= 'border-radius:' . $wpthk['side_border_radius'] . 'px ' . $wpthk['side_border_radius'] . 'px 0 0';
			}
			$style['min_992'] .= '}';
		}
		if(
			function_exists('dynamic_sidebar') === true && (
				( is_active_sidebar('side-h3') === true ) ||
				( is_active_sidebar('side-h4') === true ) ||
				( is_active_sidebar('side-top-h3') === true ) ||
				( is_active_sidebar('side-top-h4') === true ) ||
				( is_active_sidebar('side-no-top-h3') === true ) ||
				( is_active_sidebar('side-no-top-h4') === true )
			)
		) {
			$style['min_992'] .= '#side-scroll{border-top:0;}';
		}

		if(
			( is_front_page() === true && ( is_active_sidebar('side-top-h3') === true || is_active_sidebar('side-top-h4') === true ) ) ||
			( is_active_sidebar('side-no-top-h3') === true || is_active_sidebar('side-no-top-h4') === true ) ||
			( is_active_sidebar('side-h3') === true || is_active_sidebar('side-h4') === true )
		) {
			if( $radius_flag === true ) {
				$style['min_992'] .= '#side-scroll{border-top:0;';
				$style['min_992'] .= 'border-radius:0 0 ' . $wpthk['side_border_radius'] . 'px ' . $wpthk['side_border_radius'] . 'px';
				$style['min_992'] .= '}';
			}
		}
		if( $wpthk['side_discrete'] === 'discrete' ) $style['min_992'] .= '#side-scroll .widget:first-child{border-top:1px solid ' . $side_border_color . ';}';
	}

	/*---------------------------------------------------------------------------
	 * コンテンツ領域背景色
	 *---------------------------------------------------------------------------*/
	if( isset( $wpthk['cont_bg_color'] ) || ( isset( $wpthk['cont_transparent'] ) && $wpthk['cont_transparent'] !== 100 ) ) {
		$cont_bg_color = isset( $wpthk['cont_bg_color'] ) ? $wpthk['cont_bg_color'] : $default_cont_bg_color[$wpthk['overall_image']];

		if( stripos( $cont_bg_color, '#' ) !== false ) {
			$rgb = array();
			$transparent = 100;
			$trans_back = '';
			$close_992 = false;

			// コンテンツ領域背景透過の準備
			if( isset( $wpthk['cont_transparent'] ) && $wpthk['cont_transparent'] !== 100 ) {
				$rgb = $colors_class->colorcode_2_rgb( $cont_bg_color );
				$transparent = $wpthk['cont_transparent'] !== $defaults['cont_transparent'] ? $wpthk['cont_transparent'] : $defaults['cont_transparent'];
				$transparent = round( $transparent / 100, 2 );
				$trans_back = 'background: rgba(' . $rgb['r'] . ',' . $rgb['g'] . ',' . $rgb['b'] . ',' . $transparent . ');';
			}

			if( $wpthk['content_discrete'] === 'indiscrete' ) {
				if( $wpthk['content_side_discrete'] === 'discrete' ) {
					$style['all'] .= '#core.lcore, #core.pcore{background:' . $cont_bg_color . ';';
				}
				else {
					$style['min_992'] .= '#core.lcore, #core.pcore{background:none;}';
					$style['min_992'] .= '#primary{background:' . $cont_bg_color . ';';

					$style['max_991'] .= '#core.lcore, #core.pcore{background:' . $cont_bg_color . ';';
					$style['all'] .= '#core.lcore, #core.pcore{background:none;';
					$close_992 = true;
				}
			}
			else {
				if( $wpthk['content_side_discrete'] === 'discrete' ) {
					if( isset( $wpthk['pagination_area_border'] ) ) {
						$style['all'] .= '#list .toc, .bottom-area, #core.pcore{background:' . $cont_bg_color . ';';
					}
					else {
						$style['all'] .= '#list .toc, #core.pcore{background:' . $cont_bg_color . ';';
					}
				}
				else {
					$style['all'] .= '#list .toc, .bottom-area, #core.pcore{background:none;';
					$style['min_992'] .= '#primary{background:' . $cont_bg_color . ';';

					if( isset( $wpthk['pagination_area_border'] ) ) {
						$style['max_991'] .= '#list .toc, .bottom-area, #core.pcore{background:' . $cont_bg_color . ';';
					}
					else {
						$style['max_991'] .= '#list .toc, #core.pcore{background:' . $cont_bg_color . ';';
					}
					$close_992 = true;
				}
			}

			// コンテンツ領域背景透過
			if( isset( $wpthk['cont_transparent'] ) && $wpthk['cont_transparent'] !== 100 ) {
				if( $wpthk['content_side_discrete'] === 'discrete' ) $style['all'] .= $trans_back;
				if( $close_992 === true ) {
					$style['min_992'] .= $trans_back;
					$style['max_991'] .= $trans_back;
				}
			}

			if( $close_992 === true ) {
				$style['min_992'] .= '}';
				$style['max_991'] .= '}';
			}
			$style['all'] .= '}';
		}
	}

	/*---------------------------------------------------------------------------
	 * サイドバー背景色・背景画像
	 *---------------------------------------------------------------------------*/
	if(
		isset( $wpthk['side_bg_img'] )   ||
		( isset( $wpthk['side_transparent'] ) && $wpthk['side_transparent'] !== 100 ) ||
		( isset( $wpthk['cont_transparent'] ) && $wpthk['cont_transparent'] !== 100 ) ||
		( !isset( $wpthk['cont_bg_color'] ) && isset( $wpthk['side_bg_color'] ) ) ||
		( isset( $wpthk['cont_bg_color'] ) && !isset( $wpthk['side_bg_color'] ) ) ||
		( isset( $wpthk['cont_bg_color'] ) && isset( $wpthk['side_bg_color'] ) && $wpthk['cont_bg_color'] !== $wpthk['side_bg_color'] ) ||
		( $wpthk['content_side_discrete'] === 'indiscrete' && $wpthk['side_discrete'] === 'indiscrete' )
	) {
		$side_bg_color = isset( $wpthk['side_bg_color'] ) ? $wpthk['side_bg_color'] : $default_cont_bg_color[$wpthk['overall_image']];

		if( stripos( $side_bg_color, '#' ) !== false ) {
			$rgb = array();
			$transparent = 100;
			$trans_back = '';

			// サイドバー背景透過の準備
			if( isset( $wpthk['side_transparent'] ) && $wpthk['side_transparent'] !== 100 ) {
				$rgb = $colors_class->colorcode_2_rgb( $side_bg_color );
				$transparent = $wpthk['side_transparent'] !== $defaults['side_transparent'] ? $wpthk['side_transparent'] : $defaults['side_transparent'];
				$transparent = round( $transparent / 100, 2 );
				$trans_back = 'background: rgba(' . $rgb['r'] . ',' . $rgb['g'] . ',' . $rgb['b'] . ',' . $transparent . ');';
			}

			if( $wpthk['side_discrete'] === 'discrete' ) {
				$style['all'] .= '#side .widget, #col3 .widget{background:';
			}
			else {
				$style['all'] .= $side_point . ', #col3{background:';
			}

			/* サイドバー背景色 */
			if( isset( $wpthk['side_bg_color'] ) ) {
				$style['all'] .= $wpthk['side_bg_color'];
			}
			else {
				$style['all'] .= 'transparent';
			}
			/* サイドバー背景画像 */
			if( isset( $wpthk['side_bg_img'] ) ) {
				$style['all'] .= ' url(' . $wpthk['side_bg_img'] . ')';
			}

			$style['all'] .= ';';
			if( isset( $wpthk['side_transparent'] ) && $wpthk['side_transparent'] !== 100 && !isset( $wpthk['side_bg_img'] ) ) $style['all'] .= $trans_back;
			$style['all'] .= '}';

			// コンテンツ領域とサイドバーが結合してる時
			if( $wpthk['content_side_discrete'] === 'indiscrete' && $wpthk['side_discrete'] === 'indiscrete' ) {
				$style['min_992'] .= 'div[id*="side-"]{background:none;}';

				if( $column !== '3column' ) {
					$style['all'] .= 'div[id*="side-"], #side .widget{background:none;}';
					$style['all'] .= '#side{background:';
				}
				else {
					$style['all'] .= '#side{padding:0;}';
					$style['all'] .= '#side .widget, #col3 .widget{background:none;}';
					$style['all'] .= 'div[id*="side-"], #col3{background:';
				}
				/* サイドバー背景色 */
				if( isset( $wpthk['side_bg_color'] ) ) {
					$style['all'] .= $wpthk['side_bg_color'];
				}
				else {
					$style['all'] .= $default_cont_bg_color[$wpthk['overall_image']];
				}
				/* サイドバー背景画像 */
				if( isset( $wpthk['side_bg_img'] ) ) {
					$style['all'] .= ' url(' . $wpthk['side_bg_img'] . ')';
				}

				$style['all'] .= ';';
				if( isset( $wpthk['side_transparent'] ) && $wpthk['side_transparent'] !== 100 && !isset( $wpthk['side_bg_img'] ) ) $style['all'] .= $trans_back;
				$style['all'] .= '}';
			}
		}
	}

	/*---------------------------------------------------------------------------
	 * サイドバーを下まで一杯に伸ばす設定 (コンテンツとサイドバー結合時)
	 *---------------------------------------------------------------------------*/
	if( $wpthk['content_side_discrete'] === 'indiscrete' && $wpthk['side_discrete'] === 'indiscrete' ) {

		$style['min_992'] .= <<< STRETCH
#primary {
	-webkit-box-align: stretch;
	-moz-box-align: stretch;
	-ms-flex-align: stretch;
	-webkit-align-items: stretch;
	align-items: stretch;
}
STRETCH;
		$style['min_992'] .= <<< FLEXBOX
.sidebar, .sidebar-2 {
	display: -webkit-box;
	display: -webkit-flex;
	display: -ms-flexbox;
	display: -moz-box;
	display: flex;
	-webkit-box-flex: 0 0 auto;
	-moz-box-flex: 0 0 auto;
	-webkit-flex: 0 0 auto;
	-ms-flex: 0 0 auto;
	flex: 0 0 auto;
	-webkit-box-align: stretch;
	-moz-box-align: stretch;
	-ms-flex-align: stretch;
	-webkit-align-items: stretch;
	align-items: stretch;
}
#side, #col3 {
	-webkit-box-align-self: stretch;
	-moz-box-align-self: stretch;
	-ms-flex-align-self: stretch;
	-webkit-align-self: stretch;
	align-self: stretch;
}
FLEXBOX;

		$style['min_992'] .= 'div[id*="side-"]{border-bottom:0;}';
		$style['max_991'] .= '#side{padding-bottom:15px;margin-bottom:0;}';
	}

	return $style;
}

/*---------------------------------------------------------------------------
 * media query 結合
 *---------------------------------------------------------------------------*/
function format_media_query( $style ) {
	$ret = $style['all'];

	if( isset( $style['min_992'] ) ) {
		$ret .= '@media screen and (min-width: 992px) {';
		$ret .= $style['min_992'];
		$ret .= '}';
	}
	if( isset( $style['992_1199'] ) ) {
		$ret .= '@media screen and (min-width: 992px) and (max-width: 1199px) {';
		$ret .= $style['992_1199'];
		$ret .= '}';
	}

	if( isset( $style['min_1200'] ) ) {
		$ret .= '@media screen and (min-width: 1200px) {';
		$ret .= $style['min_1200'];
		$ret .= '}';
	}

	if( isset( $style['max_1199'] ) ) {
		$ret .= '@media screen and (max-width: 1199px) {';
		$ret .= $style['max_1199'];
		$ret .= '}';
	}

	if( isset( $style['541_991'] ) ) {
		$ret .= '@media screen and (min-width: 541px) and (max-width: 991px) {';
		$ret .= $style['541_991'];
		$ret .= '}';
	}

	if( isset( $style['max_991'] ) ) {
		$ret .= '@media screen and (max-width: 991px) {';
		$ret .= $style['max_991'];
		$ret .= '}';
	}

	if( isset( $style['min_541'] ) ) {
		$ret .= '@media screen and (min-width: 541px) {';
		$ret .= $style['min_541'];
		$ret .= '}';
	}

	if( isset( $style['max_540'] ) ) {
		$ret .= '@media screen and (max-width: 540px) {';
		$ret .= $style['max_540'];
		$ret .= '}';
	}

	return $ret;
}
