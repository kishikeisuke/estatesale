<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

get_header();

get_template_part( 'list' );

$bottom_area = false;
if( isset( $wpthk['pagination_visible'] ) && bootstrap_pagination( true ) === true ) {
	$bottom_area = true;
}

if( $bottom_area === true ) {
?>
<div class="bottom-area clearfix">
<?php
}

// ページネーション
if( isset( $wpthk['pagination_visible'] ) )  bootstrap_pagination();

if( $bottom_area === true ) {
?>
</div>
<?php
}
?>
</div><!--/#core-->
</main>
</div><!--/#main-->
<?php thk_call_sidebar(); ?>
</div><!--/#primary-->
<?php thk_footer(); ?>
