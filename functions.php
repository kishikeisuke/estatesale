<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

// WordPress の例の定数を使うとチェックで怒られるので再定義
define( 'TPATH', get_template_directory() );
define( 'SPATH', get_stylesheet_directory() );
define( 'DSEP' , DIRECTORY_SEPARATOR );
define( 'INC'  , TPATH . DSEP . 'inc' . DSEP );

/*---------------------------------------------------------------------------
 * WpTHK Theme only works in PHP 5.3 or later.
 * WpTHK Theme only works in WordPress 4.4 or later.
 *---------------------------------------------------------------------------*/
if(
	version_compare( PHP_VERSION, '5.3', '<' ) === true ||
	version_compare( $GLOBALS['wp_version'], '4.4-alpha', '<' ) === true
) {
	require( INC . 'back-compat.php' );
}

/*---------------------------------------------------------------------------
 * global
 *---------------------------------------------------------------------------*/
$wpthk = array();

if( is_admin() === true && current_user_can( 'edit_posts' ) === true ) {
	load_theme_textdomain( 'wpthk', TPATH . DSEP . 'languages' . DSEP . 'admin' );
}
else {
	load_theme_textdomain( 'wpthk', TPATH . DSEP . 'languages' . DSEP . 'site' );
}

require( INC . 'wpfunc.php' );
require( INC . 'widget.php' );
require( INC . 'stinger.php' );
require( INC . 'sns-cache.php' );

if( is_customize_preview() === true ) {
	require( INC . 'custom.php' );
	require( INC . 'custom-css.php' );
	require( INC . 'compress.php' );
}
elseif( is_admin() === true ) {
	if( current_user_can( 'edit_theme_options' ) === true ) {
		require( INC . 'admin-func.php' );
		$referer = wp_get_raw_referer();
		if( stripos( (string)$referer, 'wp-admin/widgets.php' ) !== false ) {
			if( stripos( $_SERVER['REQUEST_URI'], 'wp-admin/admin-ajax.php' ) !== false ) {
				thk_options_modify();
			}
		}
	}
	if( current_user_can( 'edit_posts' ) === true ) {
		require( INC . 'og-img-admin.php' );
		require( INC . 'post-update-level.php');
		add_editor_style( array( 'css/bootstrap.min.css', 'editor-style.css' ) );
	}
}

/*---------------------------------------------------------------------------
 * initialization
 *---------------------------------------------------------------------------*/
add_action( 'init', function() {
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'editor-style' );
	register_nav_menus( array('global-nav' => __( 'Header Nav (Global Nav)', 'wpthk' ) ) );
	register_nav_menus( array('head-band' => __( 'Header Band Menu', 'wpthk' ) ) );
} );

/*---------------------------------------------------------------------------
 * pre get posts
 *---------------------------------------------------------------------------*/
add_action( 'pre_get_posts', function() {
	global $wpthk;

	if( is_search() === true ) {
		get_template_part( 'inc/search-func' );
		thk_search_extend();
	}
} );

/*---------------------------------------------------------------------------
 * wp
 *---------------------------------------------------------------------------*/
add_action( 'wp', function() {
	global $wpthk;

	if( is_admin() === false ) require_once( INC . 'const.php' );
	if( is_singular() === true ) wp_enqueue_script( 'comment-reply' );

	if( isset( $wpthk['sns_count_cache_enable'] ) && is_preview() === false && is_customize_preview() === false ) {
		if(
			( is_singular() === true &&
				( isset( $wpthk['sns_count_cache_force'] ) || (
				( isset( $wpthk['sns_tops_enable'] ) && isset( $wpthk['sns_tops_count'] ) ) ||
				( isset( $wpthk['sns_bottoms_enable'] ) && isset( $wpthk['sns_bottoms_count'] ) ) ) ) ) ||
			( is_home() === true &&
				( isset( $wpthk['sns_count_cache_force'] ) || (
				( isset( $wpthk['sns_toppage_view'] ) && isset( $wpthk['sns_bottoms_count'] ) ) ) ) )
		) {
			add_filter( 'template_redirect', 'touch_sns_count_cache', 10 );
			add_filter( 'shutdown', 'set_transient_sns_count_cache', 90 );
			add_filter( 'shutdown', 'set_transient_sns_count_cache_weekly_cleanup', 95 );
			if(
				isset( $wpthk['sns_count_cache_force'] ) ||
				isset( $wpthk['feedly_share_tops_button'] ) || isset( $wpthk['feedly_share_bottoms_button'] )
			) {
				add_filter( 'template_redirect', 'touch_feedly_cache', 10 );
				add_filter( 'shutdown', 'transient_register_feedly_cache', 90 );
			}
		}
	}
} );

/*---------------------------------------------------------------------------
 * template redirect
 *---------------------------------------------------------------------------*/
add_action( 'template_redirect', function() {
	if( is_feed() === true ) return;

	if(
		is_home() === true ||
		is_singular() === true ||
		is_archive() === true ||
		is_search() === true ||
		is_404() === true
	) {
		require( INC . 'load_styles.php' );
		require( INC . 'description.php' );
		require( INC . 'load-header.php' );
	}
}, 99 );

/*---------------------------------------------------------------------------
 * 指定年からの経過年を返す：追加
 *---------------------------------------------------------------------------*/
function get_progress_date($atts) {
	extract(shortcode_atts(array(
		'year' => 0,
	), $atts));
 
	return date("Y") - $year;
}
add_shortcode('get_progress_date', 'get_progress_date');

/*---------------------------------------------------------------------------
 * 投稿ページ以外ではhentryクラスを削除：追加
 *---------------------------------------------------------------------------*/
function remove_hentry( $classes ) {
  if ( !is_single() ) {
    $classes = array_diff($classes, array('hentry'));
  }
  //これらのクラスが含まれたページではhentryを削除する
  $ng_classes = array('type-forum', 'type-topic');//ここに追加していく
  $is_include = false;
  foreach ($ng_classes as $ng_class) {
    //NGのクラス名が含まれていないか調べる
    if ( in_array($ng_class, $classes) ) {
      $is_include = true;
    }
  }
  //含まれていたらhentryを削除する
  if ($is_include) {
    $classes = array_diff($classes, array('hentry'));
  }
  return $classes;
}
add_filter('post_class', 'remove_hentry');
