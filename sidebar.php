<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

global $wpthk;

?>
<div class="sidebar">
<div id="side">
<aside<?php if( isset( $wpthk['add_role_attribute'] ) ) echo ' role="complementary"'; ?>>
<?php
if(
	function_exists('dynamic_sidebar') === true && (
		( is_active_sidebar('side-h3') === true ) ||
		( is_active_sidebar('side-h4') === true ) ||
		( is_active_sidebar('side-top-h3') === true ) ||
		( is_active_sidebar('side-top-h4') === true ) ||
		( is_active_sidebar('side-no-top-h3') === true ) ||
		( is_active_sidebar('side-no-top-h4') === true )
	)
):
?>
<div id="side-fixed">
<?php
	if( is_active_sidebar('side-h3') === true ) dynamic_sidebar( 'side-h3' );
	if( is_active_sidebar('side-h4') === true ) dynamic_sidebar( 'side-h4' );

	if( is_front_page() === true ) {
		if( is_active_sidebar('side-top-h3') === true ) dynamic_sidebar( 'side-top-h3' );
		if( is_active_sidebar('side-top-h4') === true ) dynamic_sidebar( 'side-top-h4' );
	}
	else {
		if( is_active_sidebar('side-no-top-h3') === true ) dynamic_sidebar( 'side-no-top-h3' );
		if( is_active_sidebar('side-no-top-h4') === true ) dynamic_sidebar( 'side-no-top-h4' );
	}
?>
</div>
<?php
endif;
if( function_exists('dynamic_sidebar') === true && is_active_sidebar('side-scroll') === true ):
?>
<div id="side-scroll">
<?php
	if( is_active_sidebar('side-scroll') === true ) dynamic_sidebar( 'side-scroll' );
?>
</div>
<?php
endif;
?>
</aside>
</div><!--/#side-->
</div><!--/.sidebar-->
