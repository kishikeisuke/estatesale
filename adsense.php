<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

global $wpthk;

if( isset( $wpthk['preview_adsense_visible'] ) && ( is_preview() === true || is_customize_preview() === true ) ) {}
else {

if(
	function_exists('dynamic_sidebar') === true && (
		( is_active_sidebar('adsense-1') === true ) || ( is_active_sidebar('adsense-2') === true ) || ( is_active_sidebar('adsense-3') === true )
	)
) {
?>
<div id="adsense" itemscope itemtype="https://schema.org/WPAdBlock">
<p class="ad-label" itemprop="headline name"><?php echo __('Sponsored Links', 'wpthk'); ?></p>
<?php
	switch( true ) {
		case is_active_sidebar('adsense-1'):
?>
<div id="ad-0" itemprop="about">
<?php
			dynamic_sidebar( 'adsense-1' );
?>
</div>
<?php
			break;
		case is_active_sidebar('adsense-2'):
?>
<div id="rectangle" itemprop="about">
<div class="ad-1">
<?php
			dynamic_sidebar( 'adsense-2' );
?>
</div>
<div class="ad-2">
<?php
			dynamic_sidebar( 'adsense-2' );
?>
</div>
</div>
<?php
			break;
		case is_active_sidebar('adsense-3'):
?>
<div id="rectangle" itemprop="about">
<div class="ad-1-row">
<?php
			dynamic_sidebar( 'adsense-3' );
?>
</div>
<div class="ad-2-row">
<?php
			dynamic_sidebar( 'adsense-3' );
?>
</div>
</div>
<?php
			break;
		default:
	}
?>
<div class="clearfix"></div>
</div>
<?php
}
}
