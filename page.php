<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

$isAmp = (wp_is_mobile() && isset($_GET['amp']) && $_GET['amp'] == 1) ? true : false;

get_header();

if( $wpthk['breadcrumb_view'] === 'inner' ) get_template_part( 'breadcrumb' ); ?>
<article>
<div itemprop="mainEntityOfPage" id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
<?php
if( function_exists('dynamic_sidebar') === true && is_active_sidebar('post-title-upper') === true ) {
	dynamic_sidebar( 'post-title-upper' );
}

if( have_posts() === true ) {
	while( have_posts() === true ) {
		the_post();

		if( is_front_page() === true ) { // 固定ページがフロントページだった場合は H2
?>
<h2 id="front-page-title" class="entry-title" itemprop="headline name"><?php the_title(); //タイトル ?></h2>
<div class="clearfix">
<?php
		}
		else {
?>
<h1 class="entry-title" itemprop="headline name"><?php the_title(); //タイトル ?></h1>
<div class="clearfix">
<?php
		}

		get_template_part('meta');
		if( isset( $wpthk['sns_page_view'] ) ) { // SNS 記事上
			if( isset( $wpthk['sns_tops_enable'] ) ) {
				$sns_layout = 'tops';
				get_template_part('sns');
			}
		}
		if( function_exists('dynamic_sidebar') === true && is_active_sidebar('post-title-under') === true ) {
			dynamic_sidebar( 'post-title-under' );
		}
		if ($isAmp):
			// imgの置き換え
			$protocol = empty($_SERVER["HTTPS"]) ? 'http://' : 'https://'; // http://
			$host = $_SERVER['HTTP_HOST']; // cly7796.net
			$content = apply_filters( 'the_content', get_the_content() );
			$content = str_replace( ']]>', ']]&gt;', $content );

			// img要素すべて持ってくる
    			$patternAtr = '/<img(.*?)>/i';
			preg_match_all($patternAtr, $content, $imgMatches);

			// 設定
			$targetWidth = 320;
			$patternSrc = '/src="(.*?)"/i';

			foreach ($imgMatches[1] as $value) {
				// class属性はいらないから消す
				$value2 = preg_replace('/class="(.*)"/i', '', $value);

				// imgのsrcを持ってくる
				preg_match($patternSrc, $value, $srcval);

				// 画像のサイズを取得
				list($width, $height, $type, $attr) = getimagesize($protocol.$host.$srcval[1]);

				// 高さを調整
				$targetHeight = round($targetWidth / $width * $height);

				// 置き換え後の文字列
				$append = '<amp-img src="' . $srcval[1] . '" layout="fixed" width="' . $targetWidth . '" height="' . $targetHeight. '"';

				// 変数内をエスケープ
				$srcval = preg_replace('/\//', '\\/', $srcval);
				$srcval = preg_replace('/\./', '\\.', $srcval);

				// 置き換え前の文字列
				$patternLast = '/<img src="' . $srcval[1] . '"/i';

				$content = preg_replace($patternLast, $append, $content);
			}

			// onclickを削除
			$content = preg_replace('/onclick="(.*?)"/i', '', $content);

			// styleを削除
			$content = preg_replace('/style="(.*?)"/i', '', $content);

			// img borderを削除
			preg_match('/<img(.*?)border="(.*?)"(.*)/i', $content, $borders);
			$newImg = '<img' . $borders[1] . $borders[3];
			$content = preg_replace('/<img(.*?)border="(.*?)" (.*)/i', $newImg, $content);

			// 残っているimgを変換
			$content = preg_replace('/<img/i', '<amp-img', $content);

			echo $content;
		else:
			the_content(); // 本文
		endif;
?>
</div>
<div class="meta-box">
<?php
		thk_link_pages();
		$meta_under = true;
		get_template_part('meta');
		if( isset( $wpthk['author_visible'] ) ) {
			if( $wpthk['author_page_type'] === 'auth' ) {
?>
<p class="vcard author"><i class="fa fa-pencil"></i>Posted by <span class="fn" itemprop="editor author creator copyrightHolder"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author(); ?></a></span><?php edit_post_link( __( 'Edit This', 'wpthk' ), ' (', ') ' ); ?></p>
<?php
			}
			else {
?>
<p class="vcard author"><i class="fa fa-pencil"></i>Posted by <span class="fn" itemprop="editor author creator copyrightHolder"><a href="<?php echo isset( $wpthk['thk_author_url'] ) ? $wpthk['thk_author_url'] : THK_HOME_URL; ?>"><?php the_author(); ?></a></span><?php edit_post_link( __( 'Edit This', 'wpthk' ), ' (', ') ' ); ?></p>
<?php
			}
		}
?>
</div><!--/.meta-box-->
<?php
	if(
		isset( $wpthk['sns_bottoms_enable'] ) ||
		isset( $wpthk['adsense_visible'] ) && function_exists('dynamic_sidebar') === true && (
			( is_active_sidebar('adsense-1') === true ) || ( is_active_sidebar('adsense-2') === true ) || ( is_active_sidebar('adsense-3') === true )
		)
	) {
			echo '<hr />';
	}
?>
</div><!--/.post-->
<aside>
<?php
		if( isset( $wpthk['adsense_visible'] ) && isset( $wpthk['page_adsense_visible'] ) ) { // アドセンス
			get_template_part('adsense');
		}
		if( isset( $wpthk['sns_page_view'] ) ) { // SNS 記事下
			if( isset( $wpthk['sns_bottoms_enable'] ) ) {
				if( isset( $wpthk['sns_bottoms_msg'] ) ) {
?>
<div class="sns-msg" ><h2><?php echo $wpthk['sns_bottoms_msg']; ?></h2></div>
<?php
				}
				$sns_layout = null;
				get_template_part('sns');
			}
		}
	}
}
else {
?>
<p><?php echo __( 'No posts yet', 'wpthk' ); ?></p>
<?php
}

if( isset( $wpthk['comment_page_visible'] ) ) {
	if( comments_open() === true || get_comments_number() > 0 ){
		ob_start();
		comments_template();
		$contents = ob_get_clean();
		$contents = preg_replace( '/\n+\s*</', "\n".'<', $contents );
		$contents = preg_replace( '/>\s+\n/', '>'."\n", $contents );
		$contents = preg_replace( '/>\\s+?</', '><', $contents );
		echo $contents;
	}
}

if( isset( $wpthk['trackback_page_visible'] ) && pings_open() === true ) {
?>
<div id="trackback">
<h3 class="tb"><i class="fa fa-reply-all"></i><?php echo __( 'TrackBack URL', 'wpthk' ); ?></h3>
<input type="text" name="trackback_url" size="60" value="<?php trackback_url() ?>" readonly="readonly" class="trackback-url" tabindex="0" accesskey="t" />
</div>
<?php
}
?>
</aside>
</article>
</div><!--/#core-->
</main>
</div><!--/#main-->
<?php thk_call_sidebar(); ?>
</div><!--/#primary-->
<?php thk_footer(); ?>
