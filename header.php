<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

global $wpthk;
if( !isset( $content_width ) ) $content_width = 1170;	// これ無いとチェックで怒られる

$isAmp = (wp_is_mobile() && isset($_GET['amp']) && $_GET['amp'] == 1) ? true : false;
?>
<!DOCTYPE html>
<?php if ($isAmp): ?>
<html ⚡>
<?php else: ?>
<html <?php language_attributes(); ?> itemscope itemtype="http://schema.org/WebPage">
<?php endif; ?>
<?php
if( isset( $wpthk['facebook_ogp_enable'] ) ) {
?>
<head prefix="og: http://ogp.me/ns# article: http://ogp.me/ns/article# fb: http://ogp.me/ns/fb#">
<?php
}
else {
?>
<head>
<?php
}
?>
<meta charset="<?php bloginfo('charset'); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<?php if ($isAmp): ?>
<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
<link rel="canonical" href="<?php echo get_permalink(); ?>" />
<link rel="amphtml" href="<?php echo get_permalink().'?amp=1'; ?>">
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
<style amp-custom>
<?php 
echo file_get_contents(get_stylesheet_directory_uri() . '/style.amp.css');
?>
</style>
<?php else: ?>
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=<?php echo $wpthk['user_scalable']; ?>" />
<?php endif; ?>
<meta name=”robots” content=”all”>

<?php
$next_index = false;			// <!--!nextpage--> で分割してる場合の判別
$cpage = (int)get_query_var('cpage');	// コメントをページ分割してる場合の判別

// カスタマイズ画面で設定されている <!--nextpage--> の2ページ目以降のインデクス有無によって分岐
if( is_singular() === true && isset( $wpthk['nextpage_index'] ) ) {
	if( stripos( $post->post_content, '<!--nextpage-->' ) !== 0 ) {
		$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
		if( $paged > 1 ) $next_index = true;
	}
}

if(
	( is_archive() === true && is_category() === false ) ||
	is_search() === true	||
	is_tag()    === true	||
	is_paged()  === true	||
	$next_index === true	||
	$cpage > 0		||
	is_page_template( 'pages/sitemap.php' ) === true
) {
?>
<meta name="robots" content="noindex,follow" />
<?php
}


if (!$isAmp):

thk_head();	// load header

if( is_customize_preview() === true ) {
	/* カスタマイズした Lazy Load のオプション (プレビュー用) */
	if( isset( $wpthk['lazyload_enable'] ) ) {
		require_once( INC . 'crete-javascript.php' );
		$jscript = new create_Javascript();
		echo	'<script src="', TDEL, '/js/jquery.lazyload.min.js"></script>',
			'<script>', $jscript->create_lazy_load_script(), '</script>';
	}
}

// Lazy Load の noscript のスタイル挿入
if( isset( $wpthk['lazyload_enable'] ) ) {
?>
<noscript><style>img[data-lazy="1"]{display:none;}</style></noscript>
<?php
}

endif;

get_template_part('add-header'); // ユーザーヘッダー追加用

// カスタムヘッダー
if( is_singular() === true ) {
	$addhead = get_post_meta( $post->ID, 'addhead', true );
	if( !empty( $addhead ) ) echo $addhead, "\n";
}

?>

</head>
<body <?php body_class(); ?>>

<?php if( !is_user_logged_in() ) : ?>
<?php if ($isAmp): ?>
<amp-analytics type="googleanalytics" id="analytics1">
<script type="application/json">
{
  "vars": {
    "account": "UA-88015310-1"
  },
  "triggers": {
    "trackPageview": {
      "on": "visible",
      "request": "pageview"
    }
  }
}
</script>
</amp-analytics>
<?php else: ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-88015310-1', 'auto');
  ga('send', 'pageview');

</script>
<?php endif; ?>
<?php endif; ?>

<?php
// bootstrap container Inner
if( $wpthk['bootstrap_header'] !== 'out' ) {
?>
<div class="container">
<?php
}
?>
<div id="header" itemscope itemtype="https://schema.org/WPHeader">
<header<?php if( isset( $wpthk['add_role_attribute'] ) ) echo ' role="banner"'; ?>>
<?php
// Global Navi Upper
if(
	( !isset( $wpthk['global_navi_visible'] ) && isset( $wpthk['head_band_visible'] ) ) ||
	( isset( $wpthk['global_navi_visible'] ) && $wpthk['global_navi_position'] === 'upper' )
) {
	if (!$isAmp):
		get_template_part('navigation');
	endif;
}
?>
<div id="head-in">
<div class="head-cover">
<div class="info">
<?php
if( is_front_page() === true ) {
	// フロントページは H1
?><h1 class="sitename" itemprop="name"><a href="<?php echo THK_HOME_URL; ?>"><?php
}
else {
?><p class="sitename" itemprop="name"><a href="<?php echo THK_HOME_URL; ?>"><?php
}

// Title Image
if( isset( $wpthk['title_img'] ) ) {
	echo thk_create_srcset_img_tag( $wpthk['title_img'], $alt = THK_SITENAME );
}
else {
	echo THK_SITENAME;
}
?></a><?php
if( is_front_page() === true ) {
	// フロントページは H1 (閉じタグ)
?></h1>
<?php
}
else {
?></p>
<?php
}

// Catchphrase
if( isset( $wpthk['header_catchphrase_visible'] ) ) {
?>
<p class="desc"><?php echo isset( $wpthk['header_catchphrase_change'] ) ? $wpthk['header_catchphrase_change'] : THK_DESCRIPTION; ?></p>
<?php
}
?>
</div><!--/.info-->
<?php
// Logo Image
if( isset( $wpthk['logo_img_up'] ) && isset( $wpthk['logo_img'] ) ) {
	?><div class="logo<?php if( isset( $wpthk['logo_img_up'] ) ) echo '-up'; ?>"><?php echo thk_create_srcset_img_tag( $wpthk['logo_img'] ); ?></div>
<?php
}
?>
</div><!--/.head-cover-->
<?php
// Global Navi Under
if( isset( $wpthk['global_navi_visible'] ) && $wpthk['global_navi_position'] === 'under' ) {
	if (!$isAmp):
		get_template_part('navigation');
	endif;
}
?>
</div><!--/#head-in-->
<?php
// Logo Image
if( !isset( $wpthk['logo_img_up'] ) && isset( $wpthk['logo_img'] ) ) {
	?><div class="logo<?php if( isset( $wpthk['logo_img_up'] ) ) echo '-up'; ?>"><?php echo thk_create_srcset_img_tag( $wpthk['logo_img'] ); ?></div>
<?php
}
?>
</header>
</div><!--/#header-->
<?php
// bootstrap container Outer
if( $wpthk['bootstrap_header'] === 'out' ) {
?>
<div class="container">
<?php
}

if( $wpthk['breadcrumb_view'] === 'outer' ) get_template_part( 'breadcrumb' );

if( function_exists('dynamic_sidebar') === true && is_active_sidebar('head-under') === true ) {
	dynamic_sidebar( 'head-under' );
}

if( isset( $wpthk['buffering_enable'] ) ) {
	if( ob_get_level() < 1 ) ob_start();
	if( ob_get_length() !== false ) {
	       	ob_flush();
	       	flush();
		ob_end_flush();
	}
}

?>
<div id="primary" class="clearfix">
<?php
// 3 Column
if( $wpthk['column_style'] === '3column' ) echo '<div id="field">', "\n";
?>
<div id="main">
<main<?php if( isset( $wpthk['add_role_attribute'] ) ) echo ' role="main"'; ?>>
<div id="core" class="<?php
	if( is_home() === true || is_archive() === true || is_search() === true ) {
		echo have_posts() !== true || ( is_search() === true && empty( $s ) ) ? 'pcore' : 'lcore';
	}
	else {
		echo 'pcore';
	}
?>">