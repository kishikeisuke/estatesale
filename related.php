<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

?>
<div id="related">
<?php
global $wpthk;

$categories = get_the_category($post->ID);
$category_ID = array();

foreach( $categories as $category ) {
	$category_ID[] = $category->cat_ID;
}

$args = array(
	'post__not_in' => array( $post->ID ),
	'posts_per_page'=> 5,
	'category__in' => $category_ID,
	'orderby' => 'rand',
);
$st_query = new WP_Query($args);

if( $st_query->have_posts() === true ):
	while( $st_query->have_posts() === true ):
		$st_query->the_post();
?>
<div class="toc clearfix">
<?php
		if( isset( $wpthk['thumbnail_visible'] ) ) {
?>
<div class="term"><a href="<?php the_permalink() ?>"><?php
			$post_thumbnail = has_post_thumbnail();
			$ws = 100;
			$hs = 100;

			if( !empty( $post_thumbnail ) ) {	// サムネイル
				the_post_thumbnail( 'thumb100' );
			}
			else {
?><img src="<?php echo TURI; ?>/images/no-img-100x100.png" alt="No Image" title="No Image" width="<?php echo $ws; ?>" height="<?php echo $hs; ?>" />
<?php
			}
?></a>
</div>
<?php
		}
?>
<div class="excerpt">
<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
<p><?php echo thk_excerpt_no_break( 40 ); ?></p>
<p class="read-more"><?php
		if( isset( $wpthk['read_more_text'] ) ) {
?><a href="<?php the_permalink(); ?>" class="read-more-link"><?php echo ( isset( $wpthk['read_more_short_title'] ) ) ? read_more_title_add( $wpthk['read_more_text'] ) : $wpthk['read_more_text']; // 記事を読むリンク ?></a><?php
		}
?></p>
</div>
</div>
<?php
	endwhile;
else:
?>
<p class="no-related"><?php echo __( 'No related posts yet.', 'wpthk' ); ?></p>
<?php
endif;
wp_reset_postdata();
?>
</div>
