/*! WpTHK WordPress Theme - free/libre wordpress platform
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @link http://thk.kanzae.net/
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 */

/*
------------------------------------
 記事投稿・編集画面で og:image 選択
------------------------------------ */
jQuery(document).ready(function($){
	if( typeof( _wpMediaViewsL10n ) === 'undefined' ) return;
	if( typeof( _thkOgImageViewsL10n ) === 'undefined' ) return;

	var custom_uploader;

	$('#og-img-set').click(function(e) {
		$('#og-img').addClass('image');

		e.preventDefault();
		if( custom_uploader ) {
			custom_uploader.open();
			return;
		}
		custom_uploader = wp.media({
			title: 'og:image / twitter:image ' + _wpMediaViewsL10n.select,

			library: {
				type: 'image' // 選択できるものを image だけにする
			},
			button: {
				text: _thkOgImageViewsL10n.setImage
			},
			multiple: false // true だと、複数の画像を選択可
		}); 
		custom_uploader.on('select', function() {
			var images = custom_uploader.state().get('selection');
			images.each(function(file){
				// 選択した画像をプレビューとして表示
				$('#og-img-view').html( '<img src="'+file.toJSON().url+'" />' );
				// 選択した画像の URL をカスタムフィールドの値として渡す
				$('.image').val( file.toJSON().url );
				$('#og-img').removeClass('image');
			});
		}); 
		custom_uploader.open();
	}); 

	$('#og-img-del').click(function(e) {
		$('#og-img').addClass('image');
		// 削除ボタン押されたら、プレビュー画像消す
		$('#og-img-view').html( null );
		// 削除ボタン押されたら、カスタムフィールドに null 渡す
		$('.image').val( null );
		$('#og-img').removeClass('image');
	}); 
});
