<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

global $wpthk;

get_header();

if( function_exists( 'thk_search_result' ) === false ):
function thk_search_result() {
	global $wp_query, $s; ?>
<p class="list-title"><?php echo
	sprintf( __( 'Search results of [%s]', 'wpthk' ), esc_html( $s ) ) .
	sprintf( __( ' : %s', 'wpthk' ), $wp_query->found_posts );
?></p><?php
}
endif;
?>
<input type="hidden" id="search-result" value="<?php echo esc_html( $s ); ?>" />
<?php
if( !empty( $s ) && have_posts() === true ) {
?>
<?php
	get_template_part('list');

	$bottom_area = false;
	if( isset( $wpthk['pagination_visible'] ) && bootstrap_pagination( true ) === true ) {
		$bottom_area = true;
	}

	if( $bottom_area === true ) {
?>
<div class="bottom-area clearfix">
<?php
	}

	// ページネーション
	if( isset( $wpthk['pagination_visible'] ) )  bootstrap_pagination();

	if( $bottom_area === true ) {
?>
</div>
<?php
	}
}
else {
	if( $wpthk['breadcrumb_view'] === 'inner' ) get_template_part( 'breadcrumb' );
?>
<article>
<div class="post">
<h1 class="list-title"><?php echo __( 'No search hits', 'wpthk' ); ?></h1>
<p><?php echo __('Sorry, the requested post was not found.', 'wpthk'); ?></p>
</div><!--/post-->
</article>
<?php
}
?>
</div><!--/#core-->
</main>
</div><!--/#main-->
<?php thk_call_sidebar(); ?>
</div><!--/#primary-->
<?php thk_footer(); ?>
