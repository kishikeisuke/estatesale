<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

global $wpthk, $meta_under;

$visible = array( 'P' => false, 'M' => false, 'C' => false, 'T' => false, 'X' => false );

if( is_singular() === true ) {
	if( $meta_under ) {
		if( isset( $wpthk['post_date_u_visible'] ) )		$visible['P'] = true;
		if( isset( $wpthk['mod_date_u_visible'] ) )		$visible['M'] = true;
		if( isset( $wpthk['category_meta_u_visible'] ) )	$visible['C'] = true;
		if( isset( $wpthk['tag_meta_u_visible'] ) )		$visible['T'] = true;
		if( isset( $wpthk['tax_meta_u_visible'] ) )		$visible['X'] = true;
	}
	else {
		if( isset( $wpthk['post_date_visible'] ) )		$visible['P'] = true;
		if( isset( $wpthk['mod_date_visible'] ) )		$visible['M'] = true;
		if( isset( $wpthk['category_meta_visible'] ) )		$visible['C'] = true;
		if( isset( $wpthk['tag_meta_visible'] ) )		$visible['T'] = true;
		if( isset( $wpthk['tax_meta_visible'] ) )		$visible['X'] = true;
	}
}
else {
	if( $meta_under === false ) {
		if( isset( $wpthk['list_post_date_visible'] ) )		$visible['P'] = true;
		if( isset( $wpthk['list_mod_date_visible'] ) )		$visible['M'] = true;
		if( isset( $wpthk['list_category_meta_visible'] ) )	$visible['C'] = true;
		if( isset( $wpthk['list_tag_meta_visible'] ) )		$visible['T'] = true;
		if( isset( $wpthk['list_tax_meta_visible'] ) )		$visible['X'] = true;
	}
	else {
		if( isset( $wpthk['list_post_date_u_visible'] ) )	$visible['P'] = true;
		if( isset( $wpthk['list_mod_date_u_visible'] ) )	$visible['M'] = true;
		if( isset( $wpthk['list_category_meta_u_visible'] ) )	$visible['C'] = true;
		if( isset( $wpthk['list_tag_meta_u_visible'] ) )	$visible['T'] = true;
		if( isset( $wpthk['list_tax_meta_u_visible'] ) )	$visible['X'] = true;
	}
}

if( $visible['P'] === true || $visible['M'] === true || $visible['C'] === true || $visible['T'] === true || $visible['X'] === true ) {

	$under = ( $meta_under === true ) ? ' meta-u' : '';
	$metatag = '<p class="meta' . $under . '">';
	$mdfdate  = get_the_modified_date('Ymd');
	$postdate = get_the_date('Ymd');
	$published = '';
	$meta = '';

	if( $visible['P'] === true || $visible['M'] === true ) {
		if( is_singular() === true ) {
			$meta .= '<i class="fa fa-clock-o"></i>';
			$published = ' published';
		}
		elseif( $meta_under === true ) {
			$meta .= '<i class="fa fa-repeat"></i>';
		}
		else {
			$meta .= '<i class="fa fa-calendar"></i>';
		}

		if( $postdate < $mdfdate ) {
			if( $visible['P'] === true && $visible['M'] === true ) {
				$meta .= sprintf(
					'<span class="date' . $published . '"><time class="entry-date" datetime="%1$s" itemprop="datePublished">%2$s</time></span>' .
					'<i class="fa fa-repeat"></i>' .
					'<span class="date"><time class="updated" datetime="%3$s" itemprop="dateModified">%4$s</time></span>',
					get_the_date( 'c' ), get_the_date(), get_the_modified_date( 'c' ), get_the_modified_date()
				);
			}
			elseif( $visible['P'] === true ) {
				$meta .= sprintf(
					'<span class="date' . $published . '"><time class="entry-date updated" datetime="%1$s" itemprop="datePublished">%2$s</time></span>',
					get_the_date( 'c' ), get_the_date()
				);
			}
			elseif( $visible['M'] === true ) {
				$meta .= sprintf(
					'<span class="date' . $published . '"><time class="updated" datetime="%1$s" itemprop="dateModified">%2$s</time></span>',
					get_the_modified_date( 'c' ), get_the_modified_date()
				);
			}
		}
		else {
			if( is_singular() === false && $meta_under === true ) {
				$meta = '';
			}
			else {
				if( $visible['P'] === true || $visible['M'] === true ) {
					$meta .= sprintf(
						'<span class="date' . $published . '"><time class="entry-date updated" datetime="%1$s" itemprop="datePublished">%2$s</time></span>',
						get_the_date( 'c' ), get_the_date()
					);
				}
			}
		}
	}

	if( $visible['C'] === true ) {
		$category = null;
		$cat_array = get_the_category( $wp_query->post->ID );
		if( is_array( $cat_array ) === true ) {
			foreach( $cat_array as $value) {
				$category .= ', <a href="' . get_category_link( $value->cat_ID ) . '">' . esc_html( $value->cat_name ) . '</a>';
			}
			$category = ltrim( $category, ', ' );
		}

		if( !empty( $category ) ) {
			$meta .= '<i class="fa fa-folder"></i><span class="category" itemprop="keywords">' . $category . '</span>';
		}
	}

	if( $visible['T'] === true ) {
		$tags = null;
		$tag_array = get_the_tags( $wp_query->post->ID );
		if( is_array( $tag_array ) === true ) {
			foreach( $tag_array as $value ) {
				$tags .= ', <a href="' . get_tag_link( $value->term_id ) . '">' . esc_html( $value->name ) . '</a>';
			}
			$tags = ltrim( $tags, ', ' );
		}

		if( !empty( $tags ) ) {
			$meta .= '<i class="fa fa-tags"></i><span class="tags" itemprop="keywords">' . $tags . '</span>';
		}
	}

	if( $visible['X'] === true ) {
		$taxs = null;
		$tax_names = array();
		$taxonomy_array = array();

		$taxonomies = get_taxonomies();

		foreach( $taxonomies as $taxonomy ) {
			$terms = get_the_terms( $wp_query->post->ID, $taxonomy );
			foreach ( (array)$terms as $tax ) {
				if( isset( $tax->taxonomy ) ) {
					$tax_names[] = $tax->taxonomy;
				}
			}
		}

		foreach( (array)array_unique( $tax_names ) as $value ) {
			$taxonomy_array += get_the_terms( $wp_query->post->ID, $value );
		}

		foreach( (array)$taxonomy_array as $value ) {
			$taxs .= ', <a href="' . get_term_link( $value->term_id ) . '">' . esc_html( $value->name ) . '</a>';
		}

		$taxs = ltrim( $taxs, ', ' );

		if( !empty( $taxs ) ) {
			$meta .= '<i class="fa fa-tag"></i><span class="taxs">' . $taxs . '</span>';
		}
	}

	if( !empty( $meta ) ) {
		echo $metatag, $meta, '</p>';
	}
}
