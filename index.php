<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

if( count( get_included_files() ) <= 1 ) {
	header( "HTTP/1.1 200 OK" );
	define( 'DSEP', DIRECTORY_SEPARATOR );
	require( dirname( __FILE__ ) . DSEP . 'inc' . DSEP . 'index.php' );
	exit;
}

get_header();
?>
<article>
<?php get_template_part( 'list' ); ?>
<?php // ページネーション
if( isset( $wpthk['pagination_visible'] ) )  bootstrap_pagination();
?>
</article>
</div><!--/#core-->
</main>
</div><!--/#main-->
<?php thk_call_sidebar(); ?>
</div><!--/#primary-->
<?php thk_footer(); ?>
