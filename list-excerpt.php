<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

global $wpthk, $meta_under;
?>
<figure class="term">
<?php
if( isset( $wpthk['thumbnail_visible'] ) ) {
	$post_thumbnail = has_post_thumbnail();
	if( !empty( $post_thumbnail ) ) {	// サムネイル

		$thumb = 'thumbnail';
		if( $wpthk['thumbnail_is_size'] !== 'generate' ) {
			$thumb = $wpthk['thumbnail_is_size'];
		}
		elseif( $wpthk['thumbnail_width'] > 0 && $wpthk['thumbnail_width'] !== 150 ) {
			$thumb = 'thumb' . $wpthk['thumbnail_width'] . 'x' . $wpthk['thumbnail_height'];
		}
?>
<a href="<?php the_permalink() ?>"><?php the_post_thumbnail( $thumb, array( 'itemprop' => 'image', 'class' => 'thumbnail' ) ); ?></a>
<?php
	}
	elseif( isset( $wpthk['noimage_visible'] ) ) {
?>
<a href="<?php the_permalink() ?>"><img src="<?php echo TURI; ?>/images/no-img.png" itemprop="image" class="thumbnail" alt="No Image" title="No Image" width="<?php echo $wpthk['thumbnail_width']; ?>" height="<?php echo $wpthk['thumbnail_height']; ?>" /></a>
<?php
	}
}
?>
</figure><!--/.term-->
<?php
$meta_under = true;
get_template_part('meta');
?>
<div class="excerpt" itemprop="description">
<?php
if( is_search() === true && isset( $wpthk['search_extract'] ) && $wpthk['search_extract'] === 'word' ) {
	echo thk_search_excerpt();
}
elseif( !isset( $wpthk['break_excerpt'] ) ) {
	echo thk_excerpt();
}
else {
	echo thk_excerpt_no_break( $wpthk['excerpt_length'] );
}
