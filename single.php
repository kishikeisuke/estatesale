<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

get_header();

if( $wpthk['breadcrumb_view'] === 'inner' ) get_template_part( 'breadcrumb' ); ?>
<article>
<div itemprop="mainEntityOfPage" id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>
<?php
if( function_exists('dynamic_sidebar') === true && is_active_sidebar('post-title-upper') === true ) {
	dynamic_sidebar( 'post-title-upper' );
}

if( have_posts() === true ) {
	while( have_posts() === true ) {
		the_post();
?>
<h1 class="entry-title" itemprop="headline name"><?php the_title(); //タイトル ?></h1>
<div class="clearfix">
<?php
		get_template_part('meta');
		if( isset( $wpthk['sns_tops_enable'] ) ) {
			$sns_layout = 'tops';
			get_template_part('sns'); // SNS 記事上
		}
		if( function_exists('dynamic_sidebar') === true && is_active_sidebar('post-title-under') === true ) {
			dynamic_sidebar( 'post-title-under' );
		}
		the_content(); // 本文
?>
</div>
<div class="meta-box">
<?php
		thk_link_pages();
		$meta_under = true;
		get_template_part('meta');
		if( isset( $wpthk['author_visible'] ) ) {
			if( $wpthk['author_page_type'] === 'auth' ) {
?>
<p class="vcard author"><i class="fa fa-pencil"></i>Posted by <span class="fn" itemprop="editor author creator copyrightHolder"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author(); ?></a></span><?php edit_post_link( __( 'Edit This', 'wpthk' ), ' (', ') ' ); ?></p>
<?php
			}
			else {
?>
<p class="vcard author"><i class="fa fa-pencil"></i>Posted by <span class="fn" itemprop="editor author creator copyrightHolder"><a href="<?php echo isset( $wpthk['thk_author_url'] ) ? $wpthk['thk_author_url'] : THK_HOME_URL; ?>"><?php the_author(); ?></a></span><?php edit_post_link( __( 'Edit This', 'wpthk' ), ' (', ') ' ); ?></p>
<?php
			}
		}
?>
</div><!--/.meta-box-->
<?php
	if(
		isset( $wpthk['sns_bottoms_enable'] ) ||
		isset( $wpthk['adsense_visible'] ) && function_exists('dynamic_sidebar') === true && (
			( is_active_sidebar('adsense-1') === true ) || ( is_active_sidebar('adsense-2') === true ) || ( is_active_sidebar('adsense-3') === true )
		)
	) {
			echo '<hr />';
	}
?>
</div><!--/.post-->
<aside>
<?php
		if( isset( $wpthk['adsense_visible'] ) ) get_template_part('adsense'); // アドセンス

		if( isset( $wpthk['sns_bottoms_enable'] ) ) {
			if( isset( $wpthk['sns_bottoms_msg'] ) ) {
?>
<div class="sns-msg" ><h2><?php echo $wpthk['sns_bottoms_msg']; ?></h2></div>
<?php
			}
			$sns_layout = null;
			get_template_part('sns'); // SNS 記事下
		}
	}
}
else {
?>
<p><?php echo __( 'No posts yet', 'wpthk' ); ?></p>
<?php
}
?>
<!--nav-->
<div class="pnavi">
<?php 
$next_post = get_next_post();
if( $next_post ) {
	$next_thumb = get_the_post_thumbnail($next_post->ID, 'thumb100');
	if( empty( $next_thumb ) ) $next_thumb = '<div class="no-img-next"><i class="fa fa fa-file-text"></i></div>';
?>
<div class="next"><?php next_post_link( '%link', $next_thumb . '<div class="ntitle">' . $next_post->post_title . '</div><div class="next-arrow"><i class="fa fa-arrow-right pull-right"></i>Next</div>' ); ?></div>
<?php
}
else {
?>
<div class="next"><a href="<?php echo THK_HOME_URL; ?>"><i class="fa fa-home navi-home"></i><div class="next-arrow"><i class="fa fa-arrow-right pull-right"></i>Home</div></a></div>
<?php
}

$prev_post = get_previous_post();
if( $prev_post ) {
	$prev_thumb = get_the_post_thumbnail($prev_post->ID, 'thumb100');
	if( empty( $prev_thumb ) ) $prev_thumb = '<div class="no-img-prev"><i class="fa fa fa-file-text fa-rotate-180"></i></div>';
?>
<div class="prev"><?php previous_post_link( '%link', $prev_thumb . '<div class="ptitle">' . $prev_post->post_title . '</div><div class="prev-arrow"><i class="fa fa-arrow-left pull-left"></i>Prev</div>' ); ?></div>
<?php
}
else {
?>
<div class="prev"><a href="<?php echo THK_HOME_URL; ?>"><i class="fa fa-home navi-home"></i><div class="prev-arrow"><i class="fa fa-arrow-left pull-right"></i>Home</div></a></div>
<?php
}
?>
</div><!--/pnavi-->
<!--/nav-->
<?php
if( isset( $wpthk['related_visible'] ) ) {
?>
<h2 class="related"><i class="fa fa-th-list"></i><?php echo __( 'Related Posts', 'wpthk' ); ?></h2>
<?php
	get_template_part('related');
}

if( isset( $wpthk['comment_visible'] ) ) {
	if( comments_open() === true || get_comments_number() > 0 ){
		ob_start();
		comments_template();
		$contents = ob_get_clean();
		$contents = preg_replace( '/\n+\s*</', "\n".'<', $contents );
		$contents = preg_replace( '/>\s+\n/', '>'."\n", $contents );
		$contents = preg_replace( '/>\\s+?</', '><', $contents );
		echo $contents;
	}
}

if( isset( $wpthk['trackback_visible'] ) && pings_open() === true ) {
?>
<div id="trackback">
<h3 class="tb"><i class="fa fa-reply-all"></i><?php echo __( 'TrackBack URL', 'wpthk' ); ?></h3>
<input type="text" name="trackback_url" size="60" value="<?php trackback_url() ?>" readonly="readonly" class="trackback-url" tabindex="0" accesskey="t" />
</div>
<?php
}
?>
</aside>
</article>
</div><!--/#core-->
</main>
</div><!--/#main-->
<?php thk_call_sidebar(); ?>
</div><!--/#primary-->
<?php thk_footer(); ?>
