<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

if( is_search() === true ) {
?>
<div id="list" itemscope itemtype="http://schema.org/SearchResultsPage">
<?php
}
else {
?>
<div id="list">
<?php
}

global $wpthk, $meta_under;

$b_flag = true;
$s_flag = true;

if( is_search() === true ) {
	if( $wpthk['content_discrete'] === 'discrete' ) {
		if( $b_flag === true ) {
			?><div class="toc clearfix"><?php
			if( $wpthk['breadcrumb_view'] === 'inner' ) get_template_part( 'breadcrumb' );
			thk_search_result();
			$b_flag = false;
			?></div><?php
		}
	}
}

if( have_posts() === true ) {
	while( have_posts() === true ) {
		the_post();
?>
<div class="toc clearfix">
<?php
		if( $wpthk['breadcrumb_view'] === 'inner' && $b_flag === true ) {
			get_template_part( 'breadcrumb' );
			$b_flag = false;
		}

		if( is_search() === true ) {
			if( $wpthk['content_discrete'] === 'indiscrete' && $s_flag === true ) {
				thk_search_result();
				$s_flag = false;
			}
		}
?>
<section>
<h2 class="entry-title" itemprop="headline name"><a href="<?php the_permalink(); ?>" class="entry-link" itemprop="url"><?php the_title(); ?></a></h2>
<?php
		$meta_under = false;
		get_template_part('meta');

		// ソースに無駄があるけど、速度的にこっちのが速いので if 分岐多段にした
		if( $wpthk['list_view'] === 'excerpt' ) {
			if( !isset( $wpthk['sticky_no_excerpt'] ) ) {		// sticky_post も含めて全部抜粋で表示する場合
				get_template_part('list-excerpt');	// 抜粋表示
			}
			elseif( is_sticky() === true ) {		// sticky_post の場合
				get_template_part('list-content');	// 記事全文表示
			}
			else {
				get_template_part('list-excerpt');	// 抜粋表示
			}
		}
		else {
			get_template_part('list-content');	// 記事全文表示
		}
?>
</div>
<p class="read-more"><?php
		if( isset( $wpthk['read_more_text'] ) ) {
?><a href="<?php the_permalink(); ?>" class="read-more-link" itemprop="url"><?php echo ( isset( $wpthk['read_more_short_title'] ) ) ? read_more_title_add( $wpthk['read_more_text'] ) : $wpthk['read_more_text']; // 記事を読むリンク ?></a><?php
		}
?></p>
</section>
</div><!--/.toc-->
<?php
	} // end while()
}
else {
?>
<article>
<div class="post">
<p><?php echo __('Sorry, the requested post was not found.', 'wpthk'); ?></p>
</div><!--/post-->
</article>
<?php
} // end have_posts()
?>
</div><!--/#list-->
