<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

global $wpthk;
?>
<nav itemscope itemtype="http://schema.org/SiteNavigationElement"<?php if( isset( $wpthk['add_role_attribute'] ) ) echo ' role="navigation"'; ?>>
<?php
// Global Navi Under
if( isset( $wpthk['global_navi_visible'] ) ) {
	get_template_part('navi');
}
if( isset( $wpthk['head_band_visible'] ) ) {
	get_template_part( 'head-band' );
}
?>
</nav>
