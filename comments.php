<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

?>
<div id="comments">
<h2 class="discussion"><i class="fa fa-list-alt"></i><?php echo __( 'Discussion', 'wpthk' ); ?></h2>
<h3 class="messages"><i class="fa fa-comments-o"></i><?php echo __( 'New Comments', 'wpthk' ); ?></h3>
<?php
if( have_comments() ) {
?>
<ol class="comments-list" itemscope itemtype="http://schema.org/UserComments">
<?php

/*---------------------------------------------------------------------------
 * コメントリストの表示
 *---------------------------------------------------------------------------*/

wp_list_comments( array( 'avatar_size' => 55, 'callback' =>

/*---------------------------------------------------------------------------
 * 以下 callback (コメントリスト表示部分) の中身は
 * /wp-includes/class-walker-comment.php (250行目付近) からの改変 (不要な分岐を排除して schema.org 付与)
 *---------------------------------------------------------------------------*/
function( $comment, $args, $depth ) {
?>
<li <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent', $comment ); ?> id="comment-<?php comment_ID(); ?>">
<div id="div-comment-<?php comment_ID(); ?>" class="comment-body">
<div class="comment-author vcard">
<?php
	if( 0 != $args['avatar_size'] ) {
		echo get_avatar( $comment, $args['avatar_size'] );
	}
?><cite class="fn" itemprop="creator name"><?php
	printf( '%s</cite><span class="says">', get_comment_author_link( $comment ) );
	echo __( 'says:', 'wpthk' ), '</span>';
?>
</div>
<?php
	if( '0' == $comment->comment_approved ) {
?>
<em class="comment-awaiting-moderation"><?php echo __( 'Your comment is awaiting moderation.', 'wpthk' ) ?></em>
<br />
<?php
	}
?>
<div class="comment-meta commentmetadata"><time itemprop="commentTime" datetime="<?php echo get_comment_date( 'Y-m-d', $comment ); ?>"></time>
<a href="<?php echo esc_url( get_comment_link( $comment, $args ) ); ?>"><?php
	/* translators: 1: comment date, 2: comment time */
	printf( __( '%1$s at %2$s', 'wpthk' ), get_comment_date( '', $comment ),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)', 'wpthk' ), '&nbsp;&nbsp;', '' );
?>
</div>
<div class="comment-text" itemprop="commentText">
<?php
	comment_text( $comment, array_merge( $args, array( 'add_below' => 'div-comment', 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) );

	ob_start();
		comment_reply_link( array_merge( $args, array(
		'add_below' => 'div-comment',
		'depth'     => $depth,
		'max_depth' => $args['max_depth'],
		'before'    => '<div class="reply">',
		'after'     => '</div>'
	) ) );

	$comment_reply_link = ob_get_clean();
	echo str_replace( '<a ', '<a itemprop="replyToUrl" ', $comment_reply_link );
?>
</div>
</div>
<?php
} ) );
// callback ここまで
?>
</ol>
<?php
/*---------------------------------------------------------------------------
 * コメントのページネーション
 *---------------------------------------------------------------------------*/
$cpage_count = get_comment_pages_count();
if( $cpage_count > 1 ) {
?>
<div id="c-paging"><nav><ul class="pagination">
<?php
$comments_links = paginate_comments_links( array(
	'prev_next'	=> false,
	'echo'		=> false
) );

$page_num = array();
$page_num_rev = array();
$cpage_opt = get_option('default_comments_page');

$comments_links = preg_replace( '/>(([0-9]|&hellip;)+?)<\/a>/', '>$1#explode#', $comments_links );
$comments_links = preg_replace( '/>(([0-9]|&hellip;)+?)<\/span>/', '>$1#explode#', $comments_links );
$comments_array = explode( '#explode#', $comments_links );
$comments_array = array_filter( $comments_array, 'strlen' );

for( $i = 1; $cpage_count >= $i; ++$i ) {
	$page_num[] = $i;
}

// WordPress のコメント分割で newest か oldest のどちらに設定されてるかでボタンの配置を反転させる
if( $cpage_opt === 'newest' ) {
	$comments_array = array_reverse( $comments_array, false );
	$page_num_rev = array_reverse( $page_num );	// ページ番号反転用
}

$comments_links = '';
foreach( $comments_array as $val ) {
	$num = strip_tags( $val );
	$comments_links .= stripos( $val, 'current' ) !== false ? '<li class="active">' : '<li>';
	if( $cpage_opt === 'newest' && isset( $page_num[$num - 1] ) ) {
		$comments_links .= str_replace( '>' . $page_num[$num - 1], '>' . $page_num_rev[$num - 1], $val );
	}
	else {
		$comments_links .= $val;
	}
	$comments_links .= stripos( $val, 'current' ) !== false ? '</span></li>' : '</a></li>';
}

echo( $comments_links );
?>
</ul></nav></div>
<?php
	}
}
else {
?>
<p class="no-comments"><?php echo __( 'No comments yet.  Be the first one!', 'wpthk' ); ?></p>
<?php
}
/*---------------------------------------------------------------------------
 * コメントフォームの表示
 *---------------------------------------------------------------------------*/

// aria-required があれば、required 不要なので消す( メールとコメント欄 )
$commenter = wp_get_current_commenter();
$req = get_option('require_name_email');

$fields = array(
	'author' => '<p class="comment-form-author">' . '<label for="author">' . __( 'Name', 'wpthk' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
		    '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" maxlength="245" aria-required="true" /></p>',
	'email'  => '<p class="comment-form-email"><label for="email">' . __( 'Email', 'wpthk' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
		    '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" maxlength="100" aria-describedby="email-notes" aria-required="true" /></p>',
	'url'    => '<p class="comment-form-url"><label for="url">' . __( 'Website', 'wpthk' ) . '</label> ' .
		    '<input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" maxlength="200" /></p>',
);

$args = array(
	'fields' => $fields,
	'title_reply' => '<i class="fa fa-commenting-o"></i>' . __( 'Leave a Reply', 'wpthk' ),
	'comment_field' => '<p class="comment-form-comment"><label for="comment">' . __( 'Comment', 'wpthk' ) . '</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>'
);
comment_form( $args );
?>
</div><!--/comments-->
