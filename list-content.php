<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

global $wpthk;
?>
<div class="post" itemprop="headline name">
<?php
if( is_search() === true && isset( $wpthk['search_extract'] ) && $wpthk['search_extract'] === 'word' ) {
	echo thk_search_excerpt();
}
else {
	the_content();	// 記事全文表示
}
?>
</div><!--/.post-->
<div class="more">
