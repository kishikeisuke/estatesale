<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

global $wpthk;

?>
<div class="sidebar-2">
<div id="col3">
<aside<?php if( isset( $wpthk['add_role_attribute'] ) ) echo ' role="complementary"'; ?>>
<?php
if(
	function_exists('dynamic_sidebar') === true && (
		( is_active_sidebar('col3-h3') === true ) ||
		( is_active_sidebar('col3-h4') === true )
	)
):
?>
<?php
	if( is_active_sidebar('col3-h3') === true ) dynamic_sidebar( 'col3-h3' );
	if( is_active_sidebar('col3-h4') === true ) dynamic_sidebar( 'col3-h4' );
endif;
?>
</aside>
</div><!--/#col3-->
</div><!--/.sidebar-2-->
