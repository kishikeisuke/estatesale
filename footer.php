<?php
/**
 * WpTHK WordPress Theme - free/libre wordpress platform
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * @copyright Copyright (C) 2015 Thought is free.
 * @license http://www.gnu.org/licenses/gpl-2.0.html GPL v2 or later
 * @author LunaNuko
 * @link http://thk.kanzae.net/
 * @translators rakeem( http://rakeem.jp/ )
 */

global $wpthk, $g_sns_cache_verify;
if ($isAmp):
if( $wpthk['bootstrap_footer'] !== 'in' ) {
?>
</div><!--/#container-->
<?php
}
?>
<div id="footer" itemscope itemtype="https://schema.org/WPFooter"<?php if( isset( $wpthk['add_role_attribute'] ) ) echo ' role="contentinfo"'; ?>>
<footer>
<div id="foot-in">
<?php
// Footer Widget Area
if( $wpthk['foot_widget'] !== 0 ) {
	$fwl = 'col-xs-4';
	$fwc = 'col-xs-4';
	$fwr = 'col-xs-4';
	if( $wpthk['foot_widget'] === 1 ) {
		$fwc = 'col-xs-12';
	}
	elseif( $wpthk['foot_widget'] === 2 ) {
		$fwl = 'col-xs-6';
		$fwr = 'col-xs-6';
	}

	if(
		function_exists('dynamic_sidebar') === true && (
			( is_active_sidebar('footer-left') ) === true || ( is_active_sidebar('footer-center') === true ) || ( is_active_sidebar('footer-right') === true )
		)
	) {
?>
<aside>
<?php
		if( $wpthk['foot_widget'] !== 1 ) {
?><div class="<?php echo $fwl; ?>"><?php
			if( is_active_sidebar('footer-left') === true ) dynamic_sidebar( 'footer-left' );
?></div><?php
		}
		if( $wpthk['foot_widget'] !== 2 ) {
?><div class="<?php echo $fwc; ?>"><?php
			if( is_active_sidebar('footer-center') === true ) dynamic_sidebar( 'footer-center' );
?></div><?php
		}
		if( $wpthk['foot_widget'] !== 1 ) {
?><div class="<?php echo $fwr; ?>"><?php
			if( is_active_sidebar('footer-right') === true ) dynamic_sidebar( 'footer-right' );
?></div><?php
		}
?>
</aside>
<div class="clearfix"></div>
<?php
	}
}
?>
<p class="copy">Copyright&copy;<span itemprop="copyrightHolder name"><?php bloginfo('name'); ?></span> All Rights Reserved.</p>
<p id="thk" class="copy">WordPress WpTHK Theme is provided by &quot;<a href="<?php echo THK_COPY; ?>" target="_blank">Thought is free</a>&quot;.</p>
</div><!--/#foot-in-->
</footer>
</div><!--/#footer-->
<?php
if( $wpthk['bootstrap_footer'] === 'in' ) {
?>
</div><!--/#container-->
<?php
}
?>
<div id="page-top"><i class="fa <?php echo str_replace( '_', '-', $wpthk['page_top_icon'] ); ?>"></i><?php echo isset( $wpthk['page_top_text'] ) ? '<span class="ptop"> ' . $wpthk['page_top_text'] . '</span>' : ''; ?></div>
<?php
if( $wpthk['global_navi_mobile_type'] === 'luxury' ) {
?>
<aside>
<div id="sform">
<form method="get" class="search-form" action="<?php echo THK_HOME_URL; ?>"><input type="search" class="search-field" name="s" placeholder="Search for &hellip;" /><input type="submit" class="search-submit" value="Search" /></form>
</div>
</aside>
<?php
}

if( is_customize_preview() === true ) {
	if( isset( $wpthk['jquery_load'] ) ) {
		require_once( INC . 'crete-javascript.php' );
		$jscript = new create_Javascript();
		$wpthk['awesome_load'] = 'none';

		$files = array(
			'jquery.sticky-kit.min.js',
			'jquery.smoothScroll.min.js',
			'autosize.min.js',
		);
		foreach( $files as $val ) echo '<script src="', TDEL, '/js/', $val, '"></script>';

		echo	'<script>',
			$jscript->create_wpthk_various_script( true ),
			$jscript->create_sns_count_script( true ),
			'</script>';
	}
}
// 投稿プレビュー画面で SNS のカウント数を全件 0 表示するスクリプト
if( is_preview() === true || is_customize_preview() === true ) {
	if( isset( $wpthk['jquery_load'] ) ) {
?>
<script src="<?php echo TDEL; ?>/js/preview-sns-count.js" defer></script>
<?php
	}
}

// 子 wpthkch.js もしくは wpthkch.min.js
if( !isset( $wpthk['child_script'] ) ) {
	if( $wpthk['child_js_compress'] === 'none' ) {
		if( file_exists( SPATH . DSEP . 'wpthkch.js' ) ) {
	?>
<script src="<?php echo SDEL; ?>/wpthkch.js?v=<?php echo $_SERVER['REQUEST_TIME'] ?>" defer></script>
	<?php
		}
	}
	elseif( $wpthk['child_js_compress'] === 'comp' ) {
		if( file_exists( SPATH . DSEP . 'wpthkch.min.js' ) ) {
	?>
<script src="<?php echo SDEL; ?>/wpthkch.min.js?v=<?php echo $_SERVER['REQUEST_TIME'] ?>" defer></script>
	<?php
		}
	}
}

get_template_part( 'add-footer' ); // ユーザーフッター追加用
wp_footer();

/* html5shiv.min.js & respond.min.js */
if( isset( $wpthk['html5shiv_load_type'] ) || isset( $wpthk['respondjs_load_type'] ) ) {
?>
<!--[if lt IE 9]>
<?php
	if( isset( $wpthk['html5shiv_load_type'] ) ):
?>
<script src="<?php echo TDEL, '/js' ?>/html5shiv.min.js"></script>
<?php
	endif;
	if( isset( $wpthk['respondjs_load_type'] ) ):
?>
<script src="<?php echo TDEL, '/js' ?>/respond.min.js"></script>
<?php
	endif;
?>
<![endif]-->
<?php
}

if( $wpthk['sns_tops_type'] === 'normal' || $wpthk['sns_bottoms_type'] === 'normal' ) {
	// Facebook normal button
	if( isset( $wpthk['facebook_share_tops_button'] ) || isset( $wpthk['facebook_share_bottoms_button'] )  ) {
?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php
	}
	// LinkedIn normal button
	if( isset( $wpthk['linkedin_share_tops_button'] ) || isset( $wpthk['linkedin_share_bottoms_button'] ) ) {
?>
<script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
<?php
	}
endif;
}
?>
</body>
</html>
